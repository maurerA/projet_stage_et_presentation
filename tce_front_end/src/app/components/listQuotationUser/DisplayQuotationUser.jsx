import { MDBDataTableV5 } from 'mdbreact';
import React from 'react';
import { MDBBtn } from 'mdbreact';
import { useHistory } from 'react-router-dom';
import { URL_QUOTATION_DETAIL } from './../../shared/constants/urls/urlConstants';
/**
 * 
 * @param {*} listQuotations 
 * @returns table differents quotation of one advertisement
 * @author MAURER Adrien TC-26
 */
const DisplayQuotationUser = ({ listQuotations }) => {

  const history = useHistory();

  const handleQuotationDetails = (id) => {

    history.push(URL_QUOTATION_DETAIL + id)
  }

  const data = {
    columns: [
      {
        label: 'titre de l annonce',
        field: 'title',
        width: 150
      },
      {
        label: 'status',
        field: 'statusLabel',
        width: 270
      },
      {
        label: 'prix',
        field: 'deliveryManPrice',
        width: 270
      },

      {
        label: 'détails',
        field: 'handle',
        width: 270
      }


    ],
    rows:
      listQuotations.map(quotation => {
        return (
          {
            title: quotation.title,
            statusLabel: quotation.statusLabel,
            deliveryManPrice: quotation.deliveryManPrice,
            handle: <MDBBtn color="warning" onClick={() => handleQuotationDetails(quotation.idQuotation)} rounded size="sm">détails</MDBBtn>
          }
        )
      })
  };

  return <MDBDataTableV5
    hover
    infoLabel={["lignes", "à", "sur", "éléments"]}
    searching={false}
    responsive={true}
    entriesOptions={[5, 10, 15, 20]}
    entries={5}
    pagesAmount={4}
    data={data}
    theadColor="green"
    btn={true}
    bordered={true}
    entriesLabel="nombre d'éléménts par page"

  />;

}





export default DisplayQuotationUser;