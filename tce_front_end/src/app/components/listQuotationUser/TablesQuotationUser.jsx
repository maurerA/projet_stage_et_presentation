import { MDBTable, MDBTableBody } from 'mdbreact';
import React from 'react';
import { useEffect, useState } from 'react';
import Loader from './../../shared/components/utils-components/Loader';
import { listQuotationsUser } from './../../api/backend/api.quotation';
import DisplayQuotationUser from './DisplayQuotationUser';
/**
 * attack back end to get list quotation of one advertisement
 * @param {*} advertisement id 's
 * @returns {DisplayQuotationUser}listQuotations
 * @author MAURER Adrien TC-26
 */
const TablesQuotationUser = ({ id }) => {
  const [isLoading, setIsLoading] = useState(true)
  const [listQuotations, setListQuotation] = useState({})

  useEffect(() => {
    listQuotationsUser(id)
      .then(res => {
        setListQuotation(res.data)
        setIsLoading(false)
      })
  }, [id]);

  if (isLoading) return <Loader />
  return (
    <MDBTable>
      <MDBTableBody>
        <tr>
          <td>
            <DisplayQuotationUser listQuotations={listQuotations} />
          </td>
        </tr>
      </MDBTableBody>
    </MDBTable>
  );
}

export default TablesQuotationUser;