import React, { useState, useEffect } from 'react'
import { MDBBtn } from 'mdbreact';
import { MDBModal } from 'mdbreact';
import { MDBModalHeader } from 'mdbreact';
import { MDBModalBody } from 'mdbreact';

const ModalDeleteAdvertisement = ({ id, clickDeleteAd }) => {

    useEffect(() => {
    }, [])

    const [isShowing, setIsShowing] = useState(false);

    const toggle = () => {
        setIsShowing(isShowing => !isShowing)
    }

    const cancelDelete = () => {
        setIsShowing(isShowing => !isShowing)
    }

    const cancelConfirmed = () => {
        clickDeleteAd(id)
        setIsShowing(isShowing => !isShowing)
    }

    return (
        <div>
            <MDBBtn color="warning" onClick={() => toggle()}>Supprimer l'Annonce</MDBBtn>
            <MDBModal isOpen={isShowing} toggle={() => toggle()} fullHeight position="right">
                <MDBModalHeader toggle={() => toggle()}>Confirmation De Suppression D'Annonce</MDBModalHeader>
                <MDBModalBody>
                    <h5 className="color-text orange">Êtes-vous sur de vouloir supprimer votre annonce ?</h5>
                    <MDBBtn type="button" onClick={() => cancelDelete()}>Annuler La Suppression</MDBBtn>
                    <MDBBtn type="button" onClick={() => cancelConfirmed()}>Confirmer La Suppression</MDBBtn>
                </MDBModalBody>
            </MDBModal>
        </div>
    )
}

export default ModalDeleteAdvertisement
