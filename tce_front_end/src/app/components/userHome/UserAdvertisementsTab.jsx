import React, { useEffect } from 'react'
import AdvertisementsList from './AdvertisementsList';
import HeaderTabFilterAdvertisement from './HeaderTabFilterAdvertisement';

const UserAdvertisementsTab = ({ dataNotMapped, clickCreateAdBtn, showError, handleFilter, pageCount, handlePageClick, clickDisplayDetailsAd, clickDeleteAd }) => {

    useEffect(() => {
    }, [dataNotMapped])

    return (
        <div className='container'>

            <HeaderTabFilterAdvertisement
                clickCreateAdBtn={clickCreateAdBtn}
                showError={showError}
                handleFilter={handleFilter}
            />


            <h1 className="text-center">Mes annonces</h1>
            {dataNotMapped.length > 0 ?
                <AdvertisementsList
                    dataNotMapped={dataNotMapped}
                    clickDisplayDetailsAd={clickDisplayDetailsAd}
                    pageCount={pageCount}
                    handlePageClick={handlePageClick}
                    clickDeleteAd={clickDeleteAd}
                />
                :
                <p>Vous n'avez pas d'annonces.</p>}
        </div >
    )
}

export default UserAdvertisementsTab
