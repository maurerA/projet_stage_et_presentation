import React, { useState, useEffect } from 'react'
import { MDBBtn } from 'mdbreact';
import { MDBModal } from 'mdbreact';
import { MDBModalHeader } from 'mdbreact';
import { MDBModalBody } from 'mdbreact';

const ModalDeleteAccount = ({ id, deleteAccount }) => {

    useEffect(() => {
    }, [])

    const [isShowing, setIsShowing] = useState(false);

    const toggle = () => {
        setIsShowing(isShowing => !isShowing)
    }

    const cancelDelete = () => {
        setIsShowing(isShowing => !isShowing)
    }

    const cancelConfirmed = () => {
        deleteAccount(id)
        setIsShowing(isShowing => !isShowing)
    }

    return (
        <div>
            <MDBBtn color="red" className="btn-block" onClick={() => toggle()}>Supprimer mon compte</MDBBtn>
            <MDBModal isOpen={isShowing} toggle={() => toggle()} fullHeight position="right">
                <MDBModalHeader toggle={() => toggle()}>Confirmation de suppression de compte</MDBModalHeader>
                <MDBModalBody>
                    <h5 className="color-text orange">Êtes-vous sur de vouloir supprimer votre compte ?</h5>
                    <MDBBtn type="button" onClick={() => cancelDelete()}>Annuler </MDBBtn>
                    <MDBBtn type="button" onClick={() => cancelConfirmed()}>Confirmer</MDBBtn>
                </MDBModalBody>
            </MDBModal>
        </div>
    )
}

export default ModalDeleteAccount
