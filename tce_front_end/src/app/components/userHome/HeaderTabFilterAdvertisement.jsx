import React from 'react'
import { MDBBtn, MDBFormInline, MDBInput } from 'mdbreact';

const HeaderTabFilterAdvertisement = ({ clickCreateAdBtn, showError, handleFilter }) => {
    return (
        <div styl={{ float: "right" }}>
            {/* <MDBBtn  onClick={() => clickCreateAdBtn()}>Créer une annonce</MDBBtn><br /><br /><br /> */}
            <input type="text" style={{ width: "22%" }} id="filter" placeholder="Rechercher une annonce" /><br></br>
            <MDBFormInline>
                <MDBInput label='Titre' type='checkbox' id='checkboxTitle' containerClass='mr-5' />
                <MDBInput label='Etat' type='checkbox' id='checkboxState' containerClass='mr-5' />
            </MDBFormInline>
            {showError && <p style={{ color: "red" }}>Vous devez sélectionner au moins un critère de filtre.</p>}
            <MDBBtn type="button" onClick={() =>
                handleFilter(document.getElementById("filter").value,
                    document.getElementById("checkboxTitle").checked,
                    document.getElementById("checkboxState").checked)}>Filtrer votre recherche</MDBBtn><br /><br /><br />
        </div>
    )
}

export default HeaderTabFilterAdvertisement;
