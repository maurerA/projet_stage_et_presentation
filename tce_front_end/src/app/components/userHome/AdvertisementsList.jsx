import { MDBBtn, MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import React, { useEffect } from 'react'
import ReactPaginate from 'react-paginate';
import ModalDeleteAdvertisement from './ModalDeleteAdvertisement';

const AdvertisementsList = ({ dataNotMapped, clickDisplayDetailsAd, pageCount, handlePageClick, clickDeleteAd }) => {

    useEffect(() => {
    }, [])

    return (
        <div>
            <MDBTable>
                <MDBTableHead>
                    <tr>
                        <th>Titre</th>
                        <th>Date de Creation</th>
                        <th>Etat</th>
                    </tr>
                </MDBTableHead>
                <MDBTableBody>
                    {dataNotMapped.map(ad => (
                        <tr key={ad.id}>
                            <td>{ad.title}</td>
                            <td>{ad.creationDate}</td>
                            <td>{ad.statusLabel}</td>
                            <td><MDBBtn type="button" onClick={() => clickDisplayDetailsAd(ad.id)}>Détails de l'annonce</MDBBtn></td>
                            <td><ModalDeleteAdvertisement clickDeleteAd={clickDeleteAd} id={ad.id} /></td>
                        </tr>
                    ))}

                    <tr>
                        <td>
                            <ReactPaginate
                                previousLabel={"Précédent"}
                                nextLabel={"Suivant"}
                                breakLabel={"..."}
                                breakClassName={"break-me"}
                                pageCount={pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={handlePageClick}
                                containerClassName={"pagination"}
                                subContainerClassName={"pages pagination"}
                                activeClassName={"active"} />
                        </td>
                    </tr>
                </MDBTableBody>



            </MDBTable>
        </div>
    )
}

export default AdvertisementsList;
