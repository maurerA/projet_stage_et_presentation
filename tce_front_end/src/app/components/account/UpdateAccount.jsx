import React, { useState, useEffect } from "react";
import { Form, Formik, Field } from "formik";
import ErrorMessSmall from "./../../shared/components/form-and-error-components/ErrorMessSmall";
import { MDBBtn, MDBCard, MDBCardBody, MDBCardTitle } from "mdbreact";
import {
  InsyMDBInput,
  InsyMDBSelect,
} from "../../shared/components/form-and-error-components/InputCustom";
import {
  SchemaFormUpdate,
  SchemaFormUpdateWithCompagny,
} from "../../shared/constants/formik-yup/yup/yupUser";
import {
  userAccountDisplay,
  getAllCountry,
  updateProfilAccount,
} from "../../api/backend/account";
import { useHistory } from "react-router-dom";
import { hasRole } from "../../shared/services/accountServices";
import { ROLE_USER } from "../../shared/constants/rolesConstant";
import {
  URL_HOME,
  URL_HOME_DELIVERYMAN,
  URL_HOME_USER,
} from "../../shared/constants/urls/urlConstants";
import Loader from "../../shared/components/utils-components/Loader";
import {
  toastDeleteError,
  toastDeleteSucess,
  toastUpdateSucess,
} from "./../../shared/services/toastServices";
import ModalDeleteAccount from "../userHome/ModalDeleteAccount";
import { disableAccount } from "../../api/backend/api.advertisement";
import { signOut } from "../../shared/redux-store/actions/authenticationActions";
import { useDispatch } from "react-redux";
import { removeToken } from "../../shared/services/tokenServices";

/**
 * display current user connected and update too if you want
 * @returns res
 *
 * @author MAURER adrien TC-22
 */

const FormUpdateAccount = ({ disa, setDisa }) => {
  const [errorUpdateForm, setErrorUpdateForm] = useState(false);
  const [updateProfilValues, setUpdateProfilValues] = useState({});
  const [allCountriesValues, setAllCountriesValues] = useState([]);
  const history = useHistory();
  const dispatch = useDispatch();
  const role = hasRole(ROLE_USER);
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    userAccountDisplay()
      .then((response) => {
        setUpdateProfilValues(response.data);
        getAllCountry().then((resp) => {
          setAllCountriesValues(
            resp.data.map((country) => {
              return {
                text: country.codeRef + "-" + country.label,
                value: country.id,
                checked: country.id === response.data.idCountry,
              };
            })
          );
        });
        setLoader(true);
      })
      .catch(() => setErrorUpdateForm(true));
  }, []);

  const submit = (values) => {
    updateProfilAccount(values)
      .then((res) => {
        role ? history.push(URL_HOME_USER) : history.push(URL_HOME_DELIVERYMAN);
        toastUpdateSucess();
      })
      .catch(() => setErrorUpdateForm(true));
  };

  const cancel = () => {
    role ? history.push(URL_HOME_USER) : history.push(URL_HOME_DELIVERYMAN);
  };

  const update = () => {
    setDisa(true);
  };

  const deleteAccount = () => {
    disableAccount()
      .then(() => {
        history.push(URL_HOME);
        toastDeleteSucess();
      })
      .catch(() => toastDeleteError())
      .then(() => {
        dispatch(signOut());
        removeToken();
      });
  };

  return loader ? (
    <Formik
      initialValues={updateProfilValues}
      onSubmit={submit}
      validationSchema={!role ? SchemaFormUpdateWithCompagny : SchemaFormUpdate}
    >
      <Form>
        {!role && <Company />}
        <User disa={disa} />
        <Adress allCountriesValues={allCountriesValues} disa={disa} />
        {errorUpdateForm && <ErrorMessSmall middle message="incorrect form" />}
        {disa && (
          <MDBBtn type="submit" color="warning" className="btn-block mb-2">
            Submit
          </MDBBtn>
        )}
        {!disa && (
          <MDBBtn
            color="warning"
            className="btn-block mb-2"
            onClick={() => update()}
          >
            Update
          </MDBBtn>
        )}
        <MDBBtn
          color="deep-orange"
          className="btn-block"
          onClick={() => cancel()}
        >
          Cancel
        </MDBBtn>
        <br />
        <ModalDeleteAccount deleteAccount={deleteAccount} />
      </Form>
    </Formik>
  ) : (
    <Loader />
  );
};

//composant company
const Company = () => {
  return (
    <>
      <Field
        type="text"
        name="nameCompany"
        placeholder="nameCompany"
        disabled
        component={InsyMDBInput}
        errorRight
      />
      <Field
        type="text"
        name="numberSiren"
        placeholder="numberSiren"
        disabled
        component={InsyMDBInput}
        errorRight
      />
      <Field
        type="text"
        name="numberSiretCompany"
        placeholder="numberSiretCompany"
        disabled
        component={InsyMDBInput}
        errorRight
      />
    </>
  );
};

//composant user
const User = ({ disa }) => {
  return (
    <>
      <Field
        type="text"
        name="firstName"
        placeholder="firstName"
        disabled={!disa}
        component={InsyMDBInput}
        errorRight
      />
      <Field
        type="text"
        name="lastName"
        placeholder="lastName"
        disabled={!disa}
        component={InsyMDBInput}
        errorRight
      />
      <Field
        type="text"
        name="phoneNumber"
        placeholder="phoneNumber"
        disabled
        component={InsyMDBInput}
        errorRight
      />
      <Field
        type="email"
        name="email"
        placeholder="email"
        disabled
        component={InsyMDBInput}
        errorRight
      />
    </>
  );
};

//composant Adress
const Adress = ({ allCountriesValues, disa }) => {
  return (
    <>
      <Field
        type="text"
        name="streetNumber"
        placeholder="streetNumber"
        disabled={!disa}
        component={InsyMDBInput}
        errorRight
      />
      <Field
        type="text"
        name="streetName"
        placeholder="streetName"
        disabled={!disa}
        component={InsyMDBInput}
        errorRight
      />
      <Field
        type="text"
        name="complementAddress"
        placeholder="complementAddress"
        disabled={!disa}
        component={InsyMDBInput}
        errorRight
      />
      <Field
        type="text"
        name="zipCode"
        placeholder="zipCode"
        disabled={!disa}
        component={InsyMDBInput}
        errorRight
      />
      <Field
        type="text"
        name="town"
        placeholder="town"
        disabled={!disa}
        component={InsyMDBInput}
        errorRight
      />
      {!disa ? (
        <Field
          type="text"
          name="label"
          placeholder="Country"
          disabled={!disa}
          component={InsyMDBInput}
          errorRight
        />
      ) : (
        <Field
          type="select"
          name="idCountry"
          label="select your country"
          placeholder="Country"
          options={allCountriesValues}
          component={InsyMDBSelect}
          errorRight
        />
      )}
    </>
  );
};
//form display and update
const UpdateAccount = (props) => {
  const [disa, setDisa] = useState(false);
  const Title = () => {
    return disa ? (
      <MDBCardTitle className="text-center">Update profil</MDBCardTitle>
    ) : (
      <MDBCardTitle className="text-center">Display profil</MDBCardTitle>
    );
  };

  return (
    <MDBCard style={{ width: "25rem", marging: "45rem" }}>
      <MDBCardBody>
        <Title />
        <hr />
        <FormUpdateAccount {...props} disa={disa} setDisa={setDisa} />
      </MDBCardBody>
    </MDBCard>
  );
};

export default UpdateAccount;
