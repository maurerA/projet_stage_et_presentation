import React, { Component } from 'react'
import { userAccountDisplay2 } from '../../api/backend/account';
export default class DisplayProfil extends Component {
    
      
      state = {
        user: {}
      }
      componentDidMount = () => {
        userAccountDisplay2()
        .then( response => {
            this.setState({
              user: response.data
            })
      })
    }

      render() {
        return (
          <div className="container-fluid p-5 bg-light 
          d-flex flex-column justify-content-center align-items-center">
            <p>{this.state.user.firstName} </p>
            <p>{this.state.user.lastName}</p>
            <p>{this.state.user.streetNumber}</p>  
            <p>{this.state.user.complementAddress}</p>
            <p>{this.state.user.streetName}</p>
            <p>{this.state.user.zipCode}</p>
            <p>{this.state.user.town}</p> 
            <p>{this.state.user.codeRef}</p>
            <p>{this.state.user.label}</p>
            <p>{this.state.user.nameCompany}</p>
            <p>{this.state.user.numberSiretCompany}</p>
            <p>{this.state.user.numberSiren}</p>
            
          </div>
        )
                    }
                  }
                
