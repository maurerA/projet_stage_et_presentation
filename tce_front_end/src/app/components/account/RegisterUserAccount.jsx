import React, { useEffect, useState } from "react";
import { Field, Form, Formik } from "formik";
import * as Yup from "yup";
import apiBackEnd from "../../api/backend/api.Backend";
import { useHistory } from "react-router-dom";
import { URL_BACK_REGISTER_ACCOUNT } from "../../shared/constants/urls/urlBackEnd";
import {
  toastRegisterFailed,
  toastRegisterSucess,
} from "../../shared/services/toastServices";
import { MDBBtn, MDBCard, MDBCardBody, MDBCardTitle } from "mdbreact";
import { URL_HOME_USER } from "../../shared/constants/urls/urlConstants";
import { getAllCountry } from "../../api/backend/account";
import Loader from "../../shared/components/utils-components/Loader";
import {
  InsyMDBSelect,
  InsyMDBInput,
} from "../../shared/components/form-and-error-components/InputCustom";

const RegisterUserForm = () => {
  const history = useHistory();
  const [allCountriesValues, setAllCountriesValues] = useState([]);
  const [loader, setLoader] = useState(false);

  const userSchema = Yup.object().shape({
    lastName: Yup.string().required("Champs requis"),
    firstName: Yup.string().required("Champs requis"),
    email: Yup.string()
      .email("L'email n'est pas valide")
      .required("Champs requis"),
    login: Yup.string().required("Champs requis"),
    password: Yup.string()
      .min(6, "Mot de passe pas assez sécurisé")
      .required("Champs requis"),
    streetNumber: Yup.string()
      .required()
      .matches(/^[0-9]+$/, "Nombres uniquement")
      .min(1, "Doit contenir au moins 1 chiffre")
      .max(4, "Doit contenir moins de 4 chiffres"),
    streetName: Yup.string().required("Champ requis"),
    zipCode: Yup.string()
      .required()
      .matches(/^[0-9]+$/, "Nombres uniquement")
      .min(4, "Doit contenir au moins 4 chiffres")
      .max(5, "Doit contenir moins de 5 chiffres"),
    town: Yup.string().required("Champ requis"),
    phoneNumber: Yup.string()
      .required()
      .matches(/^[0-9]+$/, "Nombres uniquement")
      .min(10, "Doit contenir au moins 10 chiffres")
      .max(16, "Doit contenir moins de 16 chiffres"),
  });

  const submit = (values) => {
    apiBackEnd
      .post(URL_BACK_REGISTER_ACCOUNT, values)
      .then((response) => {
        console.log(response);
        history.push(URL_HOME_USER);
        toastRegisterSucess();
      })
      .catch(() => {
        history.push(URL_HOME_USER);
        toastRegisterFailed();
      });
  };

  const cancel = () => {
    history.push(URL_HOME_USER);
  };

  useEffect(() => {
    getAllCountry().then((resp) => {
      setAllCountriesValues(
        resp.data.map((country) => {
          return {
            text: country.codeRef + "-" + country.label,
            value: country.id,
          };
        })
      );
    });
    setLoader(true);
  }, []);

  return loader ? (
    <Formik
      onSubmit={submit}
      initialValues={{
        lastName: "",
        firstName: "",
        email: "",
        password: "",
        login: "",
        authorities: ["ROLE_USER"],
        streetNumber: "",
        streetName: "",
        complementAddress: "",
        zipCode: "",
        town: "",
        idCountry: "",
        phoneNumber: "",
      }}
      validationSchema={userSchema}
    >
      <Form>
        <Field
          type="text"
          name="firstName"
          placeholder="firstName"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="lastName"
          placeholder="lastName"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="phoneNumber"
          placeholder="phoneNumber"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="email"
          name="email"
          placeholder="email"
          component={InsyMDBInput}
          errorLeft
        />

        <Field
          type="text"
          name="login"
          placeholder="login"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="password"
          name="password"
          placeholder="password"
          component={InsyMDBInput}
          errorLeft
        />

        <Field
          type="text"
          name="streetNumber"
          placeholder="streetNumber"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="streetName"
          placeholder="streetName"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="complementAddress"
          placeholder="complementAddress"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="zipCode"
          placeholder="zipCode"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="town"
          placeholder="town"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="select"
          name="idCountry"
          label="select your country"
          placeholder="Country"
          options={allCountriesValues}
          component={InsyMDBSelect}
          errorRight
        />

        <MDBBtn type="submit" color="warning" className="btn-block mb-2">
          Submit
        </MDBBtn>
        <MDBBtn
          color="deep-orange"
          className="btn-block"
          onClick={() => cancel()}
        >
          Cancel
        </MDBBtn>
      </Form>
    </Formik>
  ) : (
    <Loader />
  );
};

const RegisterUserAccount = () => {
  return (
    <MDBCard style={{ width: "80rem", marging: "45rem" }}>
      <MDBCardBody>
        <MDBCardTitle className="text-center">Register User</MDBCardTitle>
        <hr />
        <RegisterUserForm />
      </MDBCardBody>
    </MDBCard>
  );
};

export default RegisterUserAccount;
