import React, { useState, useEffect } from "react";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { InsyMDBInput,InsyMDBSelect } from "../../shared/components/form-and-error-components/InputCustom";
import { useHistory } from "react-router-dom";
import { MDBBtn, MDBCard, MDBCardBody, MDBCardTitle } from 'mdbreact';
import { URL_HOME_DELIVERYMAN } from "../../shared/constants/urls/urlConstants";
import {
  toastRegisterFailed,
  toastRegisterSucess,
} from "../../shared/services/toastServices";
import { getAllCountry, registerAccountUser } from "../../api/backend/account";
import Loader from "../../shared/components/utils-components/Loader";

const RegisterDeliveryForm = () => {
  const history = useHistory();
  const [allCountriesValues, setAllCountriesValues] = useState([]);
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    getAllCountry().then((resp) => {
      setAllCountriesValues(
        resp.data.map((country) => {
          return {
            text: country.codeRef + "-" + country.label,
            value: country.id,
          };
        })
      );
    });
    setLoader(true);
  }, []);

 const deliverySchema = Yup.object().shape({
    lastName: Yup.string().required("Champs requis"),
    firstName: Yup.string().required("Champs requis"),
    email: Yup.string()
      .email("L'email n'est pas valide")
      .required("Champs requis"),
    login: Yup.string().required("Champs requis"),
    password: Yup.string()
      .min(6, "Mot de passe pas assez sécurisé")
      .required("Champs requis"),
    streetNumber: Yup.string()
      .required()
      .matches(/^[0-9]+$/, "Nombres uniquement")
      .min(1, "Doit contenir au moins 1 chiffre")
      .max(4, "Doit contenir moins de 4 chiffres"),
    streetName: Yup.string().required("Champ requis"),
    zipCode: Yup.string()
      .required()
      .matches(/^[0-9]+$/, "Nombres uniquement")
      .min(4, "Doit contenir au moins 4 chiffres")
      .max(5, "Doit contenir moins de 5 chiffres"),
    town: Yup.string().required("Champ requis"),
    phoneNumber: Yup.string()
      .required()
      .matches(/^[0-9]+$/, "Nombres uniquement")
      .min(10, "Doit contenir au moins 10 chiffres")
      .max(16, "Doit contenir moins de 16 chiffres"),
    nameCompany: Yup.string().required("Champ requis"),
    numberSiretCompany: Yup.string()
      .required()
      .matches(/^[0-9]+$/, "Nombres uniquement")
      .min(9, "Doit contenir 9 chiffres")
      .max(9, "Doit contenir 9 chiffres"),
    numberSiren: Yup.string()
      .required()
      .matches(/^[0-9]+$/, "Nombres uniquement")
      .min(14, "Doit contenir 14 chiffres")
      .max(14, "Doit contenir 14 chiffres"),
  });

  const submit = (values) => {
    registerAccountUser(values)
      .then((response) => {
        console.log(response);
        history.push(URL_HOME_DELIVERYMAN);
        toastRegisterSucess();
      })
      .catch(() => {
        history.push(URL_HOME_DELIVERYMAN);
        toastRegisterFailed();
      });
  };

  const cancel = () => {
    history.push(URL_HOME_DELIVERYMAN);
  };

  return loader ? (
    <Formik
      onSubmit={submit}
      initialValues={{
        lastName: "",
        firstName: "",
        email: "",
        password: "",
        login: "",
        authorities: ["ROLE_DELIVERYMAN"],
        streetNumber: "",
        streetName: "",
        complementAddress: "",
        zipCode: "",
        town: "",
        idCountry: "",
        phoneNumber: "",
        nameCompany: "",
        numberSiretCompany: "",
        numberSiren: "",
      }}
      validationSchema={deliverySchema}
    >
      <Form>
        <Field
          type="text"
          name="firstName"
          placeholder="firstName"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="lastName"
          placeholder="lastName"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="phoneNumber"
          placeholder="phoneNumber"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="email"
          name="email"
          placeholder="email"
          component={InsyMDBInput}
          errorLeft
        />

        <Field
          type="text"
          name="login"
          placeholder="login"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="password"
          name="password"
          placeholder="password"
          component={InsyMDBInput}
          errorLeft
        />

        <Field
          type="text"
          name="streetNumber"
          placeholder="streetNumber"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="streetName"
          placeholder="streetName"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="complementAddress"
          placeholder="complementAddress"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="zipCode"
          placeholder="zipCode"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="town"
          placeholder="town"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="select"
          name="idCountry"
          label="select your country"
          placeholder="Country"
          options={allCountriesValues}
          component={InsyMDBSelect}
          errorRight
        />

        <Field
          type="text"
          name="nameCompany"
          placeholder="nameCompany"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="numberSiren"
          placeholder="numberSiren"
          component={InsyMDBInput}
          errorLeft
        />
        <Field
          type="text"
          name="numberSiretCompany"
          placeholder="numberSiret"
          component={InsyMDBInput}
          errorLeft
        />

        <MDBBtn type="submit" color="warning" className="btn-block mb-2">
          Submit
        </MDBBtn>
        <MDBBtn
          color="deep-orange"
          className="btn-block"
          onClick={() => cancel()}
        >
          Cancel
        </MDBBtn>
      </Form>
    </Formik>
  ) : (
    <Loader />
  );
};

const RegisterDeliveryAccount = () => {
  return (
    <MDBCard style={{ width: "80rem", marging: "45rem" }}>
      <MDBCardBody>
        <MDBCardTitle className="text-center">Register Delivery</MDBCardTitle>
        <hr />
        <RegisterDeliveryForm />
      </MDBCardBody>
    </MDBCard>
  );
};

export default RegisterDeliveryAccount;
