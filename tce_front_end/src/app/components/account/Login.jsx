import React from 'react';
import { Formik, Form, Field } from 'formik';
import { MDBCard, MDBCardBody, MDBCardTitle, MDBBtn } from 'mdbreact';
import { defaulValuesLogin } from './../../shared/constants/formik-yup/default-values-form/idefaultValuesUser';
import { schemaFormLogin } from './../../shared/constants/formik-yup/yup/yupUser';
import ErrorMessSmall from './../../shared/components/form-and-error-components/ErrorMessSmall';
import { InsyMDBInput } from '../../shared/components/form-and-error-components/InputCustom';


/**
 * Component Form Login
 * Use Formik to create the Form
 * 
 * @param {function} submit: submit Function
 * @param {object} initialValues: the initial values of the form
 * @param {boolean} errorLog: to display or not the message of login/mdp not valid
 * @param {object} validationSchema: validation's schema of the form
 * @author Peter Mollet
 */


const FormLogin = ({ submit, errorLog,cancel }) => (
    
    <Formik initialValues={defaulValuesLogin} onSubmit={submit} validationSchema={schemaFormLogin}>
        <Form>
            <Field type="text" name="username" placeholder="Login" component={ InsyMDBInput } errorRight/>
            <Field type='password' name='password' placeholder='Password' component={ InsyMDBInput } errorRight />
            { errorLog && <ErrorMessSmall middle message="Login/Password incorrect(s)" /> }
            <MDBBtn type='submit' color="warning" className="btn-block mb-2">Submit</MDBBtn>
            <MDBBtn color="deep-orange" className="btn-block" onClick={()=>cancel()}>Cancel</MDBBtn>
        </Form>
    </Formik>
)

/**
 * Component Login
 * 
 * will need in props:
 *  - Submit Function
 *  - initialValues
 *  - errorLog boolean
 *  - validationSchema
 * 
 * See above for information
 * 
 * @author Peter Mollet
 */
const Login = (props) => {
  
    return (
        <MDBCard style={{ width: "22rem" }}>
            <MDBCardBody>
                <MDBCardTitle className="text-center">Login</MDBCardTitle>
                <hr/>
                <FormLogin {...props} />
            </MDBCardBody>
        </MDBCard>
    );
};

export default Login;