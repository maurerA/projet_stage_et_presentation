import React, { useState, useEffect } from 'react';
import { Form, Formik, Field } from 'formik';
import { MDBBtn} from 'mdbreact';
import { InsyMDBInput} from '../../shared/components/form-and-error-components/InputCustom'
import { createQuotation, displayAdvertisement, updateQuotation } from '../../api/backend/account';
import { useHistory } from 'react-router-dom';
import { URL_HOME_DELIVERYMAN} from '../../shared/constants/urls/urlConstants';
import Loader from '../../shared/components/utils-components/Loader';
import { toastUpdateSucess } from './../../shared/services/toastServices';


//todo Mohamed prépare un onclick sur lequel il me donne l'id de l'annonce et me redirige vers ma page
const CreateQuotationForm = ({id}) => {
    const [updateAdvertisementValues, setUpdateAdvertisementValues] = useState({})
    const history = useHistory()
    const [loader, setLoader] = useState(false)
    
    
    useEffect(() => {
        displayAdvertisement(id)
            .then(response => {
                setUpdateAdvertisementValues(response.data)
                console.log ("annonce ======>",response.data) 
                setLoader(true)
            })
    },[])

    const submit = (values) => {

        updateQuotation( 
        {advertisementId:id,
        creationDateQuotation:new Date() ,
        deliveryManId:5, // une fois que enregistrer un user sera fait, je devrai récupérer l'id dans le local storage
        deliveryManPrice: values.deliveryManPrice,
        statusId:1})
            .then(res => {
                console.log(res.status)
                toastUpdateSucess()
            })
            
    }

    const cancel = () => {
        history.push(URL_HOME_DELIVERYMAN)
    }

    return (
        (loader ? (

            <Formik initialValues={updateAdvertisementValues} 
            onSubmit={submit} >
                <Form>


    

                    <Field label="Titre" name="title" disabled component={InsyMDBInput} className="mt-3" />
                    <Field label="Poids (Kg)" name="weight" disabled component={InsyMDBInput} className="mt-3" />
                    <Field label="Longueur" name="length" disabled component={InsyMDBInput} rows="2" className="mt-3" />
                    <Field label="Largeur" name="width" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Hauteur" name="height" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Numéro de rue" name="streetNumber" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Numéro de rue2" name="streetNumber2" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Nom de rue" name="streetName" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Nom de rue2" name="streetName2" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Code postal" name="zipCode" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Code postal2" name="zipCode2" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Ville" name="town" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Ville2" name="town2" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Pays" name="countryLabel" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Pays2" name="countryLabel2" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Numéro de rue" name="streetNumber" disabled component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Numéro de rue" name="streetNumber" disabled component={InsyMDBInput} className="mt-3" rows="2" />






                    <Field label="Prix" name="deliveryManPrice" component={InsyMDBInput} className="mt-3" rows="2" />


                    <MDBBtn type='submit' color="warning" className="btn-block mb-2">Submit</MDBBtn>
                    <MDBBtn color="deep-orange" className="btn-block" onClick={() => cancel()}>Cancel</MDBBtn>
                </Form>
            </Formik>) : <Loader />)
    )
}

export default CreateQuotationForm  ;
