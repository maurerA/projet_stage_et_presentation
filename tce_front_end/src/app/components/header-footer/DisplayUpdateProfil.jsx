import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { URL_UPDATE_PROFIL } from './../../shared/constants/urls/urlConstants';
/**
 * allows to display the user view of  profil connected
 * @param {*} props 
 * @returns function displayProfil() 
 * 
 * @author MAURER Adrien TC-22
 */
const DisplayUpdateProfil = (props) => {
    const isLogged = useSelector(({ authenticationReducer: { isLogged } }) => isLogged)
    const displayProfil = () => <> <NavLink to={URL_UPDATE_PROFIL} className='nav-link text-white' activeClassName='text-muted'>Mon profil</NavLink></>

    return (

        <>{isLogged && displayProfil()}</>

    )

};

export default DisplayUpdateProfil;