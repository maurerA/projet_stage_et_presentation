import React from 'react';
import { MDBNavItem, MDBDropdown, MDBDropdownToggle, MDBIcon, MDBBadge, MDBDropdownMenu, MDBDropdownItem } from 'mdbreact';
import { notificationsViewByUser } from '../../api/backend/api.notification';


const ListNotificationsUser = ({ listNotifications }) => {
    
    const handleNotifications = (id) => {
        //TODO redirection a realiser
        notificationsViewByUser(id).then(res => {
        }).catch(() => console.log("erreur de chargement de la page"))
    }
    return (

        <MDBNavItem>
            <MDBDropdown>
                <MDBDropdownToggle nav caret>
                    <MDBIcon icon="bullhorn" />
                    <MDBBadge color="danger" className="'ml-2">{listNotifications.length}</MDBBadge>
                </MDBDropdownToggle>
                <MDBDropdownMenu className="dropdown-warning">
                    {listNotifications.length === 0 ? <MDBDropdownItem>vous n'avez pas encore de notifications</MDBDropdownItem> : listNotifications.map(notification => <MDBDropdownItem  key={notification.id} onClick={() => handleNotifications(notification.id)}>{notification.message}</MDBDropdownItem>)}
                </MDBDropdownMenu>
            </MDBDropdown>
        </MDBNavItem>
    )
};

export default ListNotificationsUser;