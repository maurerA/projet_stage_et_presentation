import React, { useState, useEffect } from 'react';
import { NavLink, Link } from 'react-router-dom';
import ConnectionBtn from './ConnectionBtn';
import { URL_HOME, URL_HOME_USER, URL_USER_ADVERTISEMENT_CREATION_FORM, URL_HOME_DELIVERYMAN, URL_DELIVERYMAN_Quotations_HOME } from './../../shared/constants/urls/urlConstants';
import RegisterConsumerDeliver from './RegisterConsumerDeliver';
import LogoAnimation from './LogoAnimation';
import DisplayUpdateProfil from './DisplayUpdateProfil';
import { useSelector } from 'react-redux';
import { ROLE_USER } from '../../shared/constants/rolesConstant';
import { hasRole, isAuthenticated } from '../../shared/services/accountServices';
import { ROLE_DELIVERYMAN } from './../../shared/constants/rolesConstant';
import { getListNotificationsUser, notificationsViewByUser } from './../../api/backend/api.notification';
import { MDBNavItem, MDBDropdown, MDBDropdownToggle, MDBIcon, MDBBadge, MDBDropdownMenu, MDBDropdownItem } from 'mdbreact';


/**
 * Component deliveryHomear create with bootstrap
 * It can be change to use another one if needed
 * @author Peter Mollet
 * 
 * component RegisterConsumerDeliver &&& LogoAnimation&& notifications
 * @author MAURER adrien TC-23 et Tc-46
 */
const NavBar = () => {
    const [listNotifications, setListNotifications] = useState([]);
    const isLogged = useSelector(({ authenticationReducer: { isLogged } }) => isLogged)
    const [isLoaded, setIsLoaded] = useState(false)


    const handleNotifications = (id) => {
        //TODO redirection a realiser
        notificationsViewByUser(id).then(res => {
        }).catch(() => console.log("erreur de chargement de la page"))
    }

    useEffect(() => {
        const timer = setInterval(() => {
            notificationData()
        }, 4000);
        return () => clearInterval(timer);
    }, []);

    const notificationData = () => {
        if (isAuthenticated() && hasRole(ROLE_USER)) (
            getListNotificationsUser().then(res => {
                setListNotifications(res.data)
                setIsLoaded(true)
            }).catch(() => console.log("erreur de chargement de la page"))
        )
    };

    return (
        <header className="navbar navbar-expand-lg navbar-light bg-warning">
            <LogoAnimation />
            <Link className="navbar-brand text-white" to="/">Template Coliv</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ml-auto">
                    {isLogged && hasRole(ROLE_USER) && isLoaded &&

                        <MDBNavItem>
                            <MDBDropdown>
                                <MDBDropdownToggle nav caret>
                                    <MDBIcon icon="bullhorn" />
                                    <MDBBadge color="danger" className="'ml-2">{listNotifications.length}</MDBBadge>
                                </MDBDropdownToggle>
                                <MDBDropdownMenu className="dropdown-warning">
                                    {listNotifications.length === 0 ? <MDBDropdownItem>vous n'avez pas encore de notifications</MDBDropdownItem> : listNotifications.map(notification => <MDBDropdownItem key={notification.id} onClick={() => handleNotifications(notification.id)}>{notification.message}</MDBDropdownItem>)}
                                </MDBDropdownMenu>
                            </MDBDropdown>
                        </MDBNavItem>}
                    <li className="nav-item ml-2">
                        {!isLogged && <NavLink exact to={URL_HOME} className='nav-link text-white' activeClassName='text-muted' >Accueil</NavLink>}
                        {isLogged && hasRole(ROLE_USER) && <NavLink exact to={URL_HOME_USER} className='nav-link text-white' activeClassName='text-muted' >Accueil</NavLink>}
                        {isLogged && hasRole(ROLE_DELIVERYMAN) && <NavLink exact to={URL_HOME_DELIVERYMAN} className='nav-link text-white' activeClassName='text-muted' >Accueil</NavLink>}</li>
                    <li className="nav-item ml-2">
                        {isLogged && hasRole(ROLE_DELIVERYMAN) && <NavLink exact to={URL_DELIVERYMAN_Quotations_HOME} className='nav-link text-white' activeClassName='text-muted' >Mes Devis</NavLink>}</li>
                    <li className="nav-item ml-2">
                        {isLogged && hasRole(ROLE_DELIVERYMAN) && <NavLink exact to={URL_HOME_DELIVERYMAN} className='nav-link text-white' activeClassName='text-muted' >Mes véhicules</NavLink>}</li>
                    {isLogged && hasRole(ROLE_USER) && <li className="nav-item ml-2">
                        <NavLink exact to={URL_USER_ADVERTISEMENT_CREATION_FORM} className='nav-link text-white' activeClassName='text-muted' >Créer une annonce</NavLink>
                    </li>
                    }
                    <li className="nav-item ml-2">
                        <DisplayUpdateProfil />
                    </li>
                    <RegisterConsumerDeliver />
                    <li className="nav-item ml-2">
                        <ConnectionBtn />
                    </li>
                </ul>
            </div>
        </header>
    );
};

export default NavBar;