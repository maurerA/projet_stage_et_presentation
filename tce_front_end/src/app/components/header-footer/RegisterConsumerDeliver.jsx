import {
  MDBDropdown,
  MDBDropdownItem,
  MDBDropdownMenu,
  MDBDropdownToggle,
  MDBNavItem,
} from "mdbreact";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  URL_REGISTER_ACCOUNT_DELIVERYMAN,
  URL_REGISTER_ACCOUNT_USER,
} from "./../../shared/constants/urls/urlConstants";
/**
 * component to handle navbar selection create account
 * @param {isLogged} boolean to know if person is logged
 * @returns if nobody is logged we can to create an account else it s hidde
 *
 * @author MAURER adrien TC-2 && TC-23
 */
const RegisterConsumerDeliver = (props) => {
  const isLogged = useSelector(
    ({ authenticationReducer: { isLogged } }) => isLogged
  );

  const register = () => (
    <MDBNavItem>
      <MDBDropdown>
        <MDBDropdownToggle nav caret>
          <span className="mr-2 ">S'inscrire</span>
        </MDBDropdownToggle>
        <MDBDropdownMenu>
          <MDBDropdownItem className="bg-warning mb-2">
            <Link exact to={URL_REGISTER_ACCOUNT_USER}>
              En tant que Client
            </Link>
          </MDBDropdownItem>
          <MDBDropdownItem className="bg-warning">
            <Link exact to={URL_REGISTER_ACCOUNT_DELIVERYMAN}>
              En tant que Transporteur
            </Link>
          </MDBDropdownItem>
        </MDBDropdownMenu>
      </MDBDropdown>
    </MDBNavItem>
  );

  return <>{!isLogged && register()}</>;
};

export default RegisterConsumerDeliver;
