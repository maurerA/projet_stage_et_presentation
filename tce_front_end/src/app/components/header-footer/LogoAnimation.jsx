import React from 'react';
import imageCard from '../../assets/images/Coliv-logo-Inline.png'

function LogoAnimation(props) {
    return (
        <>
            <img className="img-fluid" alt="logo" src={imageCard} />
        </>

    );
}

export default LogoAnimation;
