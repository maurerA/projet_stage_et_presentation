import React, { Component } from 'react';
import { MDBBtn,MDBModal,MDBModalHeader,MDBModalBody} from 'mdbreact';

/** display a modal to departure adress
 * @author MAURER Adrien TC-10
 */
class ModalDepartureAddress extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal8: false,
        }
    }
    toggle = nr => () => {
        let modalNumber = 'modal' + nr
        this.setState({
            [modalNumber]: !this.state[modalNumber]
        });
    }

    render() {
        return (
            <div >
                <MDBBtn color="warning" onClick={this.toggle(8)}>Adresse de départ</MDBBtn>
                <MDBModal isOpen={this.state.modal8} toggle={this.toggle(8)} fullHeight position="right">
                    <MDBModalHeader toggle={this.toggle(8)}>Details de l'annonce</MDBModalHeader>
                    <MDBModalBody>
                        <h5 className="color-text orange">Adresse de départ: </h5>
                        <h5>numéro de rue : {this.props.detailsAdvertisement.streetNumber}</h5>
                        <h5>nom de la rue : {this.props.detailsAdvertisement.streetName}</h5>
                        <h5>complément d'adresse : {this.props.detailsAdvertisement.complementAddress}</h5>
                        <h5>code postal : {this.props.detailsAdvertisement.zipCode}</h5>
                        <h5>ville : {this.props.detailsAdvertisement.town}</h5>
                        <h5>pays : {this.props.detailsAdvertisement.countryLabel}</h5>
                    </MDBModalBody>
                </MDBModal>
            </div>
        );
    }
}


export default ModalDepartureAddress;