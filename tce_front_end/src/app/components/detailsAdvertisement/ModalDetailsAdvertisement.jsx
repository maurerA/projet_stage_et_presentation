import React from "react";
import { MDBCard, MDBBtn, MDBCardBody, MDBTypography } from "mdbreact";
import ModalDepartureAddress from "./ModalDepartureAddress";
import ModalArrivalAddress from "./ModalArrivalAddress";
import { useHistory } from "react-router-dom";
import {
  URL_HOME_DELIVERYMAN,
  URL_HOME_USER,
  URL_USER_LIST_QUOTATIONS
} from "./../../shared/constants/urls/urlConstants";
import { hasRole } from "../../shared/services/accountServices";
import { ROLE_USER } from "../../shared/constants/rolesConstant";


/**
 * display basicinformation to advertisement
 * @param {*} param0
 * @returns
 *
 * @author MAURER Adrien TC-10
 */
const ModalDetailsAdvertisement = ({
  detailsAdvertisement,
  displayCompany,
  idParam
}) => {
  const history = useHistory();
  const user = hasRole(ROLE_USER)
  
  const listQuotations = (id) =>{
    history.push(URL_USER_LIST_QUOTATIONS+`${id}`);
}
  const cancel = () => {
    hasRole(ROLE_USER)
      ? history.push(URL_HOME_USER)
      : history.push(URL_HOME_DELIVERYMAN);
  };

  return (
    <MDBCard style={{ width: "60rem", marging: "45rem" }}>
      <MDBCardBody>
        <MDBTypography blockquote bqColor="warning">
          <span className="row">
            <dt className="col-sm-3">Titre de l'annonce</dt>
            <dd className="col-sm-9">{detailsAdvertisement.title}</dd>

            <dt className="col-sm-3">Status</dt>
            <dd className="col-sm-9">{detailsAdvertisement.labelStatus}</dd>

            <dt className="col-sm-3">Poids</dt>
            <dd className="col-sm-9">{detailsAdvertisement.weight}</dd>

            <dt className="col-sm-3 ">Longueur</dt>
            <dd className="col-sm-9">{detailsAdvertisement.length}</dd>

            <dt className="col-sm-3 ">Largeur</dt>
            <dd className="col-sm-9">{detailsAdvertisement.width}</dd>

            <dt className="col-sm-3 ">Hauteur</dt>
            <dd className="col-sm-9">{detailsAdvertisement.height}</dd>

            <dt className="col-sm-3 ">Adresse de départ</dt>
            <dd className="col-sm-9">
              <ModalDepartureAddress
                detailsAdvertisement={detailsAdvertisement}
              />
            </dd>

            <dt className="col-sm-3 ">Adresse d'arrivée</dt>
            <dd className="col-sm-9">
              <ModalArrivalAddress
                detailsAdvertisement={detailsAdvertisement}
              />
            </dd>

            {displayCompany && (
              <>
                <dt className="col-sm-3 ">Société</dt>
                <dd className="col-sm-9">{detailsAdvertisement.nameCompany}</dd>

                <dt className="col-sm-3 ">Nom du livreur</dt>
                <dd className="col-sm-9">{detailsAdvertisement.lastName}</dd>
              </>
            )}
          </span>
        </MDBTypography>
     {user&& <MDBBtn 
        color="deep-orange" 
        className="btn-block  mb-2  " 
        onClick={() => listQuotations(idParam)}>voir les devis
        </MDBBtn>}
        
        <MDBBtn
          color="deep-orange"
          className="btn-block"
          onClick={() => cancel()}
        >
          Cancel
        </MDBBtn>
      </MDBCardBody>
    </MDBCard>
  );
};

export default ModalDetailsAdvertisement;
