import React, { Component } from 'react';
import { MDBBtn,MDBModalHeader,MDBModalBody,MDBModal } from 'mdbreact';

/**
 * allows to display a modal to arrival address
 * @author MAURER Adrien TC-10
 */
class ModalArrivalAddress extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal8: false,
        }
    }
    toggle =(nr) => {
        let modalNumber = 'modal' + nr
        this.setState({
            [modalNumber]: !this.state[modalNumber]
        });
    }

    render() {
        return (
            <div >
                <MDBBtn color="warning" onClick={()=>this.toggle(8)}>Adresse d'arrivée</MDBBtn>
                <MDBModal isOpen={this.state.modal8} toggle={()=>this.toggle(8)} fullHeight position="right">
                    <MDBModalHeader toggle={()=>this.toggle(8)}>Détails de l'annonce</MDBModalHeader>
                    <MDBModalBody >
                    <h5 className="color-text orange">Adresse d'arrivée : </h5>
                        <h5>numéro de rue : {this.props.detailsAdvertisement.streetNumber2}</h5>
                        <h5>nom de la rue : {this.props.detailsAdvertisement.streetName2}</h5>
                        <h5>complément d'adresse : {this.props.detailsAdvertisement.complementAddress2}</h5>
                        <h5>code postal : {this.props.detailsAdvertisement.zipCode2}</h5>
                        <h5>ville : {this.props.detailsAdvertisement.town2}</h5>
                        <h5>pays : {this.props.detailsAdvertisement.countryLabel2}</h5>
                    </MDBModalBody>
                </MDBModal>
           </div>
        );
    }
}

export default ModalArrivalAddress;