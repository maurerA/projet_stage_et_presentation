import React, { useEffect, useState } from 'react';
import { displayDetailsAdvertisement } from './../../api/backend/api.advertisement';
import Loader from './../../shared/components/utils-components/Loader';
import ErrorMessSmall from './../../shared/components/form-and-error-components/ErrorMessSmall';
import ModalDetailsAdvertisement from './ModalDetailsAdvertisement';

/**
 * back attack with axios to get an advertissement
 * @param {*} param0 
 * @returns ModalDetailsAdvertisement detailsAdvertisement={detailsAdvertisement} displayCompany={displayCompany
 * 
 * @author MAURER Adrien TC-10 
 */
const DetailsAdvertisement = ({ idParam }) => {
    const [errorDetailsAdvertisement, setErrorDetailsAdvertisement] = useState(false)
    const [detailsAdvertisement, setDetailsAdvertisement] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    const [displayCompany, setDisplayCompany] = useState(true)

    useEffect(() => {
        displayDetailsAdvertisement(idParam)
            .then(res => {
                setDetailsAdvertisement(res.data)
                if (res.data.nameCompany === null) {
                    setDisplayCompany(false)
                }
                setIsLoading(false)
            }).catch(() => setErrorDetailsAdvertisement(false))
    }, [idParam]);
 
    if (isLoading) return <Loader />

    return (
        <>
            <ModalDetailsAdvertisement detailsAdvertisement={detailsAdvertisement} displayCompany={displayCompany} idParam={idParam} />
            {errorDetailsAdvertisement && <ErrorMessSmall middle message="sorry, an error occurred while loading the page " />}
        </>
    );

};

export default DetailsAdvertisement;