import React from 'react'
import { MDBFormInline } from 'mdbreact';
import { MDBInput } from 'mdbreact';
import { MDBBtn } from 'mdbreact';

const QuotationFIleHeader = ({ showError, handleFilter }) => {
    return (
        <div>
            <div className="headerFilter">
                <input className="mr-5" type="text" style={{ width: "22%" }} id="filter" placeholder="Rechercher une annonce" /><br></br>
                <MDBFormInline>
                    <MDBInput label='Titre' type='checkbox' id='checkboxTitle' containerClass='mr-5' />
                    <MDBInput label='Etat' type='checkbox' id='checkboxState' containerClass='mr-5' />
                </MDBFormInline>
                <MDBBtn type="button" onClick={() =>
                    handleFilter(document.getElementById("filter").value,
                        document.getElementById("checkboxTitle").checked,
                        document.getElementById("checkboxState").checked)}>Filtrer votre recherche</MDBBtn>
            </div><br />
            {showError && <p style={{ color: "red" }}>Vous devez sélectionner au moins un critère de filtre.</p>}
            <br />
        </div>
    )
}

export default QuotationFIleHeader
