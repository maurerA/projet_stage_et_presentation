import React from 'react'
import { MDBBtn, MDBDataTableV5 } from 'mdbreact';
import { URL_USER_QUOTATION_CREATION_FORM } from './../../../shared/constants/urls/urlConstants';
import { useHistory } from 'react-router-dom';

const QuotationTable = ({ listOfQuotations}) => {
    

    const history = useHistory();

    const data = {
        columns: [
            {
                label: 'titre de l annonce',
                field: 'title',
                width: 150
            },
            {
                label: 'status',
                field: 'statusLabel',
                width: 270
            },
            {
                label: 'prix',
                field: 'deliveryManPrice',
                width: 270
            },

            {
                label: 'détails',
                field: 'handle',
                width: 270
            }


        ],
        rows:
            listOfQuotations.map(quotation => {
                return (
                    {
                        title: quotation.title,
                        statusLabel: quotation.statusLabel,
                        deliveryManPrice: quotation.deliveryManPrice,
                        handle: <MDBBtn color="warning" onClick={() => handleDetailsQuotationClick(quotation.idQuotation)} rounded size="sm">détails</MDBBtn>
                    }
                )
            })
    };


    const handleDetailsQuotationClick = (id) => {
            
    }


    return (
        <MDBDataTableV5
            hover
            infoLabel={["lignes", "à", "sur", "éléments"]}
            searching={false}
            responsive={true}
            entriesOptions={[5, 10, 15, 20]}
            entries={5}
            pagesAmount={4}
            data={data}
            theadColor="green"
            btn={true}
            bordered={true}
            entriesLabel="nombre d'éléménts par page"
        />
    )
}

export default QuotationTable;
