import React from "react";
import { MDBFormInline, MDBInput } from "mdbreact";

const HeaderList = ({ showError, handleFilter }) => {
  return (
    <div>
      <input type="text" id="filter" placeholder="Rechercher une annonce" />
      <br></br>
      <MDBFormInline>
        <MDBInput
          label="Titre"
          type="checkbox"
          id="checkboxTitle"
          containerClass="mr-5"
        />
        <MDBInput
          label="Etat"
          type="checkbox"
          id="checkboxState"
          containerClass="mr-5"
        />
      </MDBFormInline>
      {showError && (
        <p style={{ color: "red" }}>
          Vous devez sélectionner au moins un critère de filtre.
        </p>
      )}
      <button
        type="button"
        onClick={() =>
          handleFilter(
            document.getElementById("filter").value,
            document.getElementById("checkboxTitle").checked,
            document.getElementById("checkboxState").checked
          )
        }
      >
        Filtrer votre recherche
      </button>
      <br />
      <br />
      <br />
    </div>
  );
};

export default HeaderList;
