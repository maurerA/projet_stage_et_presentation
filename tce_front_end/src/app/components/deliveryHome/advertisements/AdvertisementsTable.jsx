import React from "react";
import ReactPaginate from "react-paginate";
import { MDBBtn, MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import { URL_USER_QUOTATION_CREATION_FORM } from './../../../shared/constants/urls/urlConstants';
import { useHistory } from 'react-router-dom';

const AdvertisementsTable = ({
  dataNotMapped,
  clickDisplayDetailsAd,
  pageCount,
  handlePageClick,
}) => {

  const history = useHistory();

  const handleCreateQuotation = (id) => {
    history.push(URL_USER_QUOTATION_CREATION_FORM + id);
  }

  return (
    <div>
      <MDBTable>
        <MDBTableHead>
          <tr>
            <th>Identifiant</th>
            <th>Titre</th>
            <th>Date de Creation</th>
          </tr>
        </MDBTableHead>
        <MDBTableBody>
          {dataNotMapped.map((ad) => (
            <tr key={ad.id}>
              <td>{ad.id}</td>
              <td>{ad.title}</td>
              <td>{ad.creationDate}</td>
              <MDBBtn type="button" onClick={() => clickDisplayDetailsAd(ad.id)}>
                Détails de l'annonce
            </MDBBtn>
              <MDBBtn type="button" onClick={() => handleCreateQuotation(ad.id)} > Créer un devis </MDBBtn>
            </tr>
          ))}

          <ReactPaginate
            previousLabel={"Précédent"}
            nextLabel={"Suivant"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={pageCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            onPageChange={handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </MDBTableBody>
      </MDBTable>
    </div>
  );
};

export default AdvertisementsTable;
