import React from "react";
import AdvertisementsTable from "./AdvertisementsTable";
import HeaderList from "./HeaderList";

const AdvertisementsPages = ({
  dataNotMapped,
  showError,
  handleFilter,
  pageCount,
  handlePageClick,
  clickDisplayDetailsAd,
}) => {
  return (
    <div className="container">
      <HeaderList showError={showError} handleFilter={handleFilter} />

      <h1 className="text-center">Annonces disponibles</h1>
      {dataNotMapped.length > 0 ? (
        <AdvertisementsTable
          dataNotMapped={dataNotMapped}
          clickDisplayDetailsAd={clickDisplayDetailsAd}
          pageCount={pageCount}
          handlePageClick={handlePageClick}
        />
      ) : (
        <p>
          Il n'y a pas d'annonces disponibles pour le moment, repassez plus tard
          !.
        </p>
      )}
    </div>
  );
};

export default AdvertisementsPages;
