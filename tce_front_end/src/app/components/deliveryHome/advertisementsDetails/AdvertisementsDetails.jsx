import React, { useEffect, useState } from "react";
import Loader from "../../../shared/components/utils-components/Loader";
import ErrorMessSmall from "../../../shared/components/form-and-error-components/ErrorMessSmall";
import ModalDetailsAdvertisement from "../../detailsAdvertisement/ModalDetailsAdvertisement";
import { displayDetailsAdvertisementForDelivery } from "../../../api/backend/api.advertisement";

/**
 * back attack with axios to get an advertissement
 * @param idParam the id of the advertisement
 * @returns ModalDetailsAdvertisement detailsAdvertisement={detailsAdvertisement} displayCompany={displayCompany
 *
 * @author Mohamed
 */
const AdvertisementsDetails = ({ idParam }) => {
  const [errorDetailsAdvertisement, setErrorDetailsAdvertisement] = useState(
    false
  );
  const [detailsAdvertisement, setDetailsAdvertisement] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [displayCompany, setDisplayCompany] = useState(true);

  useEffect(() => {
    displayDetailsAdvertisementForDelivery(idParam)
      .then((res) => {
        setDetailsAdvertisement(res.data);
        if (res.data.nameCompany === null) {
          setDisplayCompany(false);
        }
        setIsLoading(false);
      })
      .catch(() => setErrorDetailsAdvertisement(false));
  }, [idParam]);

  if (isLoading) return <Loader />;

  return (
    <>
      <ModalDetailsAdvertisement
        detailsAdvertisement={detailsAdvertisement}
        displayCompany={displayCompany}
      />
      {errorDetailsAdvertisement && (
        <ErrorMessSmall
          middle
          message="sorry, an error occurred while loading the page. Try again later. "
        />
      )}
    </>
  );
};

export default AdvertisementsDetails;
