import React from 'react';
import { MDBCard, MDBCardBody, MDBCardImage } from 'mdbreact';
import joey from '../../assets/images/joey.jpg'


const WelcomeUser = () => {

    

    return (
        <div className="d-flex justify-content-center">
            <MDBCard style={{ width: "22rem", height: "30rem" }}>
                <MDBCardImage top src={joey} overlay="white-slight" hover waves alt="MDBCard image cap" />
                <MDBCardBody className="text-center pb-5">
                    
                    <h3 className="font-weight-bold mb-3">
                        WELCOME&nbsp; 
                        <span className='blue-text'>
                    {/*}        {login}         */}
                        </span>
                        &nbsp;!
                    </h3>
                    <h4 className="font-weight-bold">HOW YA DOIN' ?</h4>
                </MDBCardBody>
            </MDBCard>
        </div>
    )
};//const login = accountLogin()

export default WelcomeUser;