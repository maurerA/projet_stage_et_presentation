import React from "react";
import { MDBCol, MDBBtn } from "mdbreact";
import { Field } from "formik";
import {
  InsyMDBInput,
  InsyMDBSelect,
} from "../../shared/components/form-and-error-components/InputCustom";

const AdvertisementFormStep3 = ({
  optionPays,
  handleNextPrevClick,
  formActivePanel1,
}) => {
  return (
    <div>
      {formActivePanel1 === 3 && (
        <MDBCol md="12">
          <h3 className="font-weight-bold pl-0 my-4">
            <strong>Adresse d'Arrivée</strong>
          </h3>
          <Field
            label="Numéro de Rue"
            name="streetNumber2"
            component={InsyMDBInput}
            className="mt-3"
          />
          <Field
            label="Complément d'Adresse"
            name="complementAddress2"
            component={InsyMDBInput}
            className="mt-3"
          />
          <Field
            label="Nom de Rue"
            name="streetName2"
            component={InsyMDBInput}
            className="mt-3"
          />
          <Field
            label="Code Postal"
            name="zipCode2"
            component={InsyMDBInput}
            className="mt-3"
          />
          <Field
            label="Ville"
            name="town2"
            component={InsyMDBInput}
            className="mt-3"
          />
          <Field
            type="select"
            name="countryId2"
            min="1"
            options={optionPays}
            component={InsyMDBSelect}
            label="Sélectionnez votre pays"
            className="mt-3"
          />
          <div className="d-flex justify-content-between">
            <MDBBtn color="indigo" rounded onClick={handleNextPrevClick(1)(2)}>
              Précédent
            </MDBBtn>
            <MDBBtn type="submit" color="indigo" rounded>
              Confirmer le formulaire
            </MDBBtn>
          </div>
        </MDBCol>
      )}
    </div>
  );
};

export default AdvertisementFormStep3;
