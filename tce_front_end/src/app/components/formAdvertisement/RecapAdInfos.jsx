import React from 'react'
import { URL_HOME_USER } from '../../shared/constants/urls/urlConstants';
import { useHistory } from 'react-router-dom';

const RecapAdInfos = ({ valuesFormAd, handleValidation }) => {


    const history = useHistory();

    const handleCancel = () => {
        history.push(URL_HOME_USER)
    }

    return (
        <div>
            <h1 name="text-center">Récapitulatif de votre annonce : </h1>

            <h2>Informations du colis : </h2><br />
            <p>Titre : {valuesFormAd.title} .</p>
            <p>Poids (Kg) : {valuesFormAd.weight} .</p>
            <p>Longueur : {valuesFormAd.length} .</p>
            <p>Largeur : {valuesFormAd.width} .</p>
            <p>Hauteur : {valuesFormAd.height} .</p><br /><br />

            <h2>Adresse de départ : </h2><br />
            <p>Numéro de Rue : {valuesFormAd.streetNumber} .</p>
            <p>Complément d'Adresse : {valuesFormAd.complementAddress} .</p>
            <p>Nom de Rue : {valuesFormAd.streetName} .</p>
            <p>Code Postal : {valuesFormAd.zipCode} .</p>
            <p>Ville : {valuesFormAd.town} .</p>
            {/* <p>Pays : {valuesFormAd.countryId} .</p> */}
            <br /><br />

            <h2>Adresse de Destination : </h2><br />
            <p>Numéro de Rue : {valuesFormAd.streetNumber2} .</p>
            <p>Complément d'Adresse : {valuesFormAd.complementAddress2} .</p>
            <p>Nom de Rue : {valuesFormAd.streetName2} .</p>
            <p>Code Postal : {valuesFormAd.zipCode2} .</p>
            <p>Ville : {valuesFormAd.town2} .</p>
            {/* <p>Pays : {valuesFormAd.countryId2} .</p> */}
            <br /><br />

            <button type="button" className="btn-block mb-2" onClick={handleCancel}>Annuler</button>
            <button type="button" className="btn-block mb-2" onClick={handleValidation}>Confirmer la création d'annonce</button>
        </div>
    )


}

export default RecapAdInfos;
