import React from 'react'
import { MDBCol, MDBBtn } from 'mdbreact';
import { Field } from 'formik';
import { InsyMDBInput } from '../../shared/components/form-and-error-components/InputCustom';
import { useHistory } from 'react-router-dom';
import { URL_HOME_USER } from '../../shared/constants/urls/urlConstants';

const AdvertisementFormStep1 = ({ handleNextPrevClick, formActivePanel1 }) => {

    const history = useHistory();

    const handleReturn = () => {
        history.push(URL_HOME_USER)
    }
    return (
        <div>
            {formActivePanel1 === 1 && (
                <MDBCol md="12">
                    <h3 className="font-weight-bold pl-0 my-4">
                        <strong>Informations sur le Colis</strong>
                    </h3>
                    <Field label="Titre" name="title" component={InsyMDBInput} className="mt-3" />
                    <Field label="Poids (Kg)" name="weight" component={InsyMDBInput} className="mt-3" />
                    <Field label="Longueur" name="length" component={InsyMDBInput} rows="2" className="mt-3" />
                    <Field label="Largeur" name="width" component={InsyMDBInput} className="mt-3" rows="2" />
                    <Field label="Hauteur" name="height" component={InsyMDBInput} className="mt-3" rows="2" />
                    <MDBBtn color="indigo" rounded className="float-left" onClick={() => handleReturn()}>
                        Retour à l'accueil
                    </MDBBtn>
                    <MDBBtn color="indigo" rounded className="float-right" onClick={handleNextPrevClick(1)(2)}>
                        Suivant
                    </MDBBtn>
                </MDBCol>
            )
            }
        </div >
    )
}

export default AdvertisementFormStep1
