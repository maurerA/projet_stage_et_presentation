import React from 'react'
import { MDBCol, MDBBtn } from 'mdbreact';
import { Field } from 'formik';
import { InsyMDBInput, InsyMDBSelect } from '../../shared/components/form-and-error-components/InputCustom';

const AdvertisementFormStep2 = ({ optionPays, handleNextPrevClick, formActivePanel1 }) => {




    return (
        <div>
            {formActivePanel1 === 2 && (
                <MDBCol md="12">
                    <h3 className="font-weight-bold pl-0 my-4">
                        <strong>Adresse de Départ</strong>
                    </h3>
                    <Field label="Numéro de Rue" name="streetNumber" component={InsyMDBInput} className="mt-3" />
                    <Field label="Complément d'Adresse" name="complementAddress" component={InsyMDBInput} className="mt-3" />
                    <Field label="Nom de Rue" name="streetName" component={InsyMDBInput} className="mt-3" />
                    <Field label="Code Postal" name="zipCode" component={InsyMDBInput} className="mt-3" />
                    <Field label="Ville" name="town" component={InsyMDBInput} className="mt-3" />
                    <Field
                        options={optionPays}
                        type="select"
                        name="countryId"
                        component={InsyMDBSelect}
                        label="Sélectionnez votre pays"
                        className="mt-3"
                    />
                    <MDBBtn color="indigo" rounded className="float-left" onClick={handleNextPrevClick(1)(1)}>
                        previous
                    </MDBBtn>
                    <MDBBtn color="indigo" rounded className="float-right" onClick={handleNextPrevClick(1)(3)}>
                        next
                    </MDBBtn>
                </MDBCol>
            )}
        </div>
    )
}

export default AdvertisementFormStep2
