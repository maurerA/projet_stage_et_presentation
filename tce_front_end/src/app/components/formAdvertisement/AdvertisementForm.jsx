import React from 'react'
import { URL_HOME_USER } from '../../shared/constants/urls/urlConstants';
import { useHistory } from 'react-router-dom';
import AdvertisementFormStep1 from './AdvertisementFormStep1';
import AdvertisementFormStep2 from './AdvertisementFormStep2';
import AdvertisementFormStep3 from './AdvertisementFormStep3';
import { useState } from 'react';
import HeaderMultiStepForm from './HeaderMultiStepForm';


const AdvertisementForm = ({ submit, optionPays }) => {

    const history = useHistory()
    const [formActivePanel1, setFormActivePanel1] = useState(1)
    const [formActivePanel1Changed, setFormActivePanel1Changed] = useState(1)
    const handleCancelAdvertisement = () => history.push(URL_HOME_USER)

    const handleNextPrevClick = (a) => (param) => (e) => {
        setFormActivePanel1(param)
        setFormActivePanel1Changed(true)
    }

    return (
        <div>
            <HeaderMultiStepForm setFormActivePanel1={setFormActivePanel1} setFormActivePanel1Changed={setFormActivePanel1Changed} formActivePanel1={formActivePanel1} formActivePanel1Changed={formActivePanel1Changed} />
            <AdvertisementFormStep1 formActivePanel1={formActivePanel1} formActivePanel1Changed={formActivePanel1Changed} handleCancelAdvertisement={handleCancelAdvertisement} handleNextPrevClick={handleNextPrevClick} />
            <AdvertisementFormStep2 optionPays={optionPays} formActivePanel1={formActivePanel1} formActivePanel1Changed={formActivePanel1Changed} handleCancelAdvertisement={handleCancelAdvertisement} handleNextPrevClick={handleNextPrevClick} />
            <AdvertisementFormStep3 optionPays={optionPays} formActivePanel1={formActivePanel1} formActivePanel1Changed={formActivePanel1Changed} handleCancelAdvertisement={handleCancelAdvertisement} handleNextPrevClick={handleNextPrevClick} />
        </div>
    )
}

export default AdvertisementForm
