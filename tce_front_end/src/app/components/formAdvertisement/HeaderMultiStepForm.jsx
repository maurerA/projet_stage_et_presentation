import React from 'react'
import { MDBStepper, MDBStep, MDBBtn } from 'mdbreact';

const HeaderMultiStepForm = ({ formActivePanel1, formActivePanel1Changed, setFormActivePanel1, setFormActivePanel1Changed }) => {

    const swapFormActive = (a) => (param) => (e) => {
        setFormActivePanel1(param)
        setFormActivePanel1Changed(true)
    }

    return (
        <div>
            <h2 className="text-center font-weight-bold pt-4 pb-5">
                <strong>Formulaire de Création d'Annonces</strong>
            </h2>
            <MDBStepper form>
                <MDBStep form>
                    <a href="#formstep1" onClick={swapFormActive(1)(1)}>
                        <MDBBtn color={formActivePanel1 === 1 ? "indigo" : "default"} circle>
                            1
                    </MDBBtn>
                    </a>
                    <p>Informations sur le Colis</p>
                </MDBStep>

                <MDBStep form>
                    <a href="#formstep2" onClick={swapFormActive(1)(2)}>
                        <MDBBtn color={formActivePanel1 === 2 ? "indigo" : "default"} circle>
                            2
                    </MDBBtn>
                    </a>
                    <p>Adresse de Départ</p>
                </MDBStep>
                <MDBStep form>
                    <a href="#formstep3" onClick={swapFormActive(1)(3)}>
                        <MDBBtn color={formActivePanel1 === 3 ? "indigo" : "default"} circle>
                            3
                    </MDBBtn>
                    </a>
                    <p>Adresse d'Arrivée</p>
                </MDBStep>
            </MDBStepper>
        </div>
    )
}

export default HeaderMultiStepForm
