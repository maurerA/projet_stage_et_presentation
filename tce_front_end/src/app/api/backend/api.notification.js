
import { URL_BACK_LIST_NOTIFICATIONS } from '../../shared/constants/urls/urlBackEnd';
import apiBackEnd from './api.Backend';
import { URL_BACK_NOTIFICATION_VIEW_BY_USER } from './../../shared/constants/urls/urlBackEnd';


export function getListNotificationsUser() {
    return apiBackEnd.get(URL_BACK_LIST_NOTIFICATIONS);
  }

export function notificationsViewByUser(id) {
    return apiBackEnd.put(URL_BACK_NOTIFICATION_VIEW_BY_USER+id);
  }
