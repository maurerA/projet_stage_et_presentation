import apiBackEnd from "./api.Backend";
import {
  URL_BACK_ALL_Country,
  URL_BACK_AUTHENTICATE,
  URL_BACK_DISPLAY_USER,
  URL_BACK_DISPLAY_COUNTRY,
  URL_BACK_CREATE_ADVERTISEMENT,
  URL_BACK_UPDATE_PROFIL,
  URL_BACK_REGISTER_ACCOUNT,
  URL_BACK_CREATE_QUOTATION,
  URL_BACK_DISPLAY_DELIVERY_AD_DETAILS,
  URL_BACK_UPDATE_QUOTATION
} from "./../../shared/constants/urls/urlBackEnd";




/**
 * 
 * @param {*} values of login and username
 * @returns  back attack with post method to check authenticate 
 * 
 * @author MAURER Adrien TC-22
 */
export function authenticate(values) {
    return apiBackEnd.post(URL_BACK_AUTHENTICATE, values)
}
/**
 * allow to catch user profil connected
 * @returns get attack with get method to get user(delivryman or customer) 
 * 
 * @author MAURER Adrien TC-22
 */
export function userAccountDisplay(){
    return apiBackEnd.get(URL_BACK_DISPLAY_USER)
}
/**
 * 
 * @returns get attack with get method to get list country
 * 
 * @author MAURER Adrien TC-22
 */
export function getAllCountry(){
    return apiBackEnd.get(URL_BACK_ALL_Country)
}

export function registerAccount(values) {
  return apiBackEnd.post(URL_BACK_REGISTER_ACCOUNT, values);
}
/**
 * allow to change user profil connected
 * @param {*} values new profil user or delivryman
 * @returns  back attack with put method
 * 
 * @author MAURER Adrien TC-22
 * 
 */
export function updateProfilAccount(values){
    return apiBackEnd.put(URL_BACK_UPDATE_PROFIL,values)
}

/**
 * @author Ibrahim El Kadiri
 * @returns back attack to get the countries list
 */
export function adressCountryDisplay(){
    return apiBackEnd.get(URL_BACK_DISPLAY_COUNTRY)
}

/**
 * @author Ibrahim El Kadiri
 * @returns the id of the advertisement of the post method to create an advertisement
 */
export function createAdvertisement(values){
    return apiBackEnd.post(URL_BACK_CREATE_ADVERTISEMENT, values)
}

/**
 * @author Fares Amal TC-17
 * @param {*}  values of quotation
 * @returns back attack with post method
 */
 export function createQuotation(values){
    return apiBackEnd.post(URL_BACK_CREATE_QUOTATION, values)
}

/**
 * @author Fares Amal TC-17
 * @param {*} id of the advertissement
 * @returns back attack with get method
 */

export function displayAdvertisement(id){
    return apiBackEnd.get(URL_BACK_DISPLAY_DELIVERY_AD_DETAILS+`${id}`)
}

/**
 * @author Fares Amal TC-20
 * @param {*} values of quotation for updating
 * @returns back attack with put method
 */
export function updateQuotation(values){
    return apiBackEnd.put(URL_BACK_UPDATE_QUOTATION, values)
}

export function registerAccountUser(values) {
  return apiBackEnd.post(URL_BACK_REGISTER_ACCOUNT, values);
}

