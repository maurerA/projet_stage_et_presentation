import apiBackEnd from "./api.Backend";
import {
  URL_BACK_CREATE_ADVERTISEMENT,
  URL_BACK_USER_ADVERTISEMENTS,
  URL_BACK_USER_ADVERTISEMENTS_DELETE,
  URL_BACK_USER_ADVERTISEMENTS_FILTERED,
  URL_BACK_USER_ADVERTISEMENTS_FILTERED_STATE,
  URL_BACK_USER_ADVERTISEMENTS_FILTERED_TITLE,
  URL_BACK_DISPLAY_DELIVERY_ADVERTISEMENTS,
  URL_BACK_DISPLAY_DETAILS_ADVERTISEMENT,
  URL_BACK_USER_QUOTATION_DETAIL,
  URL_BACK_USER_QUOTATION_ACCEPTED,
  URL_BACK_USER_QUOTATION_REFUSED,
  URL_BACK_LIST_QUOTATION_DELIVERYMAN,
  URL_BACK_DISPLAY_DETAILS_DELIVERY_ADVERTISEMENT,
  URL_BACK_DISABLE_USER,
} from "./../../shared/constants/urls/urlBackEnd";

/**
 * @author Ibrahim El Kadiri
 * @returns the id of the advertisement of the post method to create an advertisement
 */
export function createAdvertisement(values) {
  return apiBackEnd.post(URL_BACK_CREATE_ADVERTISEMENT, values);
}

/**
 * @author Ibrahim El Kadiri
 * @returns the user's advertisements of the get method
 */
export function userAdvertisements() {
  return apiBackEnd.get(URL_BACK_USER_ADVERTISEMENTS);
}

/**
 * @author Ibrahim El Kadiri
 * @returns the user's advertisements filtered with title and state of the get method
 */
export function userAdvertisementsFiltered(values) {
  return apiBackEnd.post(URL_BACK_USER_ADVERTISEMENTS_FILTERED, values);
}

/**
 * @author Ibrahim El Kadiri
 * @returns the user's advertisements filtered with title of the get method
 */
export function userAdvertisementsFilteredTitle(values) {
  return apiBackEnd.post(URL_BACK_USER_ADVERTISEMENTS_FILTERED_TITLE, values);
}

/**
 * @author Ibrahim El Kadiri
 * @returns the user's advertisements filtered with state of the get method
 */
export function userAdvertisementsFilteredState(values) {
  return apiBackEnd.post(URL_BACK_USER_ADVERTISEMENTS_FILTERED_STATE, values);
}

/**
 * @author Ibrahim El Kadiri
 * @returns the method to delete an ad with the id
 */
export function deleteAdvertisements(id) {
  return apiBackEnd.delete(URL_BACK_USER_ADVERTISEMENTS_DELETE + id);
}

/**
 * @author Mohamed
 * @returns the method to disable the user account
 */
export function disableAccount() {
  return apiBackEnd.put(URL_BACK_DISABLE_USER);
}

/**
 * @author Ibrahim El Kadiri
 * @returns the method to get the quotation details with an id
 */
export function getQuotationDetail(id) {
  return apiBackEnd.get(URL_BACK_USER_QUOTATION_DETAIL + id);
}

/**
 * @author Ibrahim El Kadiri
 * @returns the method get to accept a quotation
 */
export function acceptQuotation(id) {
  return apiBackEnd.get(URL_BACK_USER_QUOTATION_ACCEPTED + id);
}

/**
 * @author Ibrahim El Kadiri
 * @returns the method get to refuse a quotation
 */
export function refuseQuotation(id) {
  return apiBackEnd.get(URL_BACK_USER_QUOTATION_REFUSED + id);
}

export function deliveryAdvertisements() {
  return apiBackEnd.get(URL_BACK_DISPLAY_DELIVERY_ADVERTISEMENTS);
}
/**
 * allow to get advertissement details with if possible delivryMAn name and company
 * @param {*} id
 * @returns back attack with get method
 *
 * @author MAURER Adrien TC-10
 */
export function displayDetailsAdvertisement(id) {
  return apiBackEnd.get(URL_BACK_DISPLAY_DETAILS_ADVERTISEMENT + `/${id}`);
}

/**
 * allow to get advertissement details for deliverymen
 * @param {*} id
 * @returns back attack with get method
 *
 * @author Mohamed
 */
export function displayDetailsAdvertisementForDelivery(id) {
  return apiBackEnd.get(
    URL_BACK_DISPLAY_DETAILS_DELIVERY_ADVERTISEMENT + `/${id}`
  );
}

/**
 * @author Ibrahim El Kadiri
 * @returns the method get to get the list of quotations of the deliveryman connected
 */
export function displayDeliverymanQuotations() {
  return apiBackEnd.get(URL_BACK_LIST_QUOTATION_DELIVERYMAN);
}
