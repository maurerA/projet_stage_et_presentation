
import apiBackEnd from './api.Backend';
import { URL_BACK_LIST_QUOTATION_USER, URL_BACK_LIST_QUOTATION_FILTERED_DELIVERYMAN } from './../../shared/constants/urls/urlBackEnd';
/**
 * 
 * @param {*} id 
 * @returns back attack to get the quotation list of advertisement
 * @author MAURER Adrien TC-26
 */
export function listQuotationsUser(id){
    return apiBackEnd.get(URL_BACK_LIST_QUOTATION_USER+`/${id}`)
}

export function listQuotationsDeliveryManFiltered(values){
    return apiBackEnd.post(URL_BACK_LIST_QUOTATION_FILTERED_DELIVERYMAN, values)
}

