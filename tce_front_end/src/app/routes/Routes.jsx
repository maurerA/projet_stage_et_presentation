import React from "react";
import { Switch, Route } from "react-router-dom";
import {
  URL_HOME,
  URL_LOGIN,
  URL_ADMIN_HOME,
  URL_USER_DETAILS_ADVERTISEMENT,
  URL_DELIVERYMAN_DETAILS_ADVERTISEMENT,
  URL_HOME_DELIVERYMAN,
  URL_HOME_USER,

  URL_USER_ADVERTISEMENT_CREATION_FORM,
  URL_USER_ADVERTISEMENT_CREATION_FORM_RECAP,
  URL_UPDATE_PROFIL,
  URL_QUOTATION_DETAIL,
  URL_USER_LIST_QUOTATIONS,
  URL_DELIVERYMAN_Quotations_HOME,
  URL_USER_QUOTATION_CREATION_FORM,
  URL_REGISTER_ACCOUNT_USER,
  URL_REGISTER_ACCOUNT_DELIVERYMAN,
} from "./../shared/constants/urls/urlConstants";
import HomeView from "../views/HomeView";
import LoginView from "../views/LoginView";
import { customHistory } from "./../shared/services/historyServices";
import AdminHomeView from "./../views/AdminHomeView";
import {
  ROLE_ADMIN,
  ROLE_DELIVERYMAN,
  ROLE_USER,
} from "./../shared/constants/rolesConstant";
import { PrivateRoute } from "../shared/components/utils-components/PrivateRoute";
import DeliveryHomeView from "../views/DeliveryHomeView";
import UserHome from "./../views/UserHome";
import CreateAdvertisementForm from "./../views/CreateAdvertisementForm";
import RegisterUserAccount from "./../components/account/RegisterUserAccount";
import UpdateAccountView from "./../views/UpdateAccountView";
import RecapAdInfos from "../components/formAdvertisement/RecapAdInfos";
import QuotationDetails from "./../views/QuotationDetails";
import DetailsAdvertisementView from "./../views/DetailsAdvertisementView";
import ListQuotationsUserView from "./../views/ListQuotationsUserView";
import QuotationListDeliveryman from "./../views/QuotationListDeliveryman";
import CreateQuotationView from "./../views/CreateQuotationView";
import RegisterDeliveryAccount from "../components/account/RegisterDeliveryAccount";

/**
 * Routes of the application
 * with public and private route
 *
 * @author Peter Mollet
 *
 * change private en public route for HomeView
 * create Route for compoenents : DeliveryHomeView, UserHome, RegisterAccount,listAccount,DetailsAdvertisementView
 * @author MAURER Adrien TC-2,C-23,TC-10,Tc-26
 */
const Routes = () => {
  return (
    <Switch history={customHistory}>
      {/**public Routes */}
      <Route exact path={URL_HOME} component={HomeView} />
      <Route path={URL_LOGIN} component={LoginView} />
      <Route path={URL_REGISTER_ACCOUNT_USER} component={RegisterUserAccount} />
      <Route
        path={URL_REGISTER_ACCOUNT_DELIVERYMAN}
        component={RegisterDeliveryAccount}
      />
      {/**private Routes */}
      <PrivateRoute
        path={URL_ADMIN_HOME}
        component={AdminHomeView}
        roles={[ROLE_ADMIN]}
      />

      <PrivateRoute
        path={URL_HOME_DELIVERYMAN}
        component={DeliveryHomeView}
        roles={[ROLE_DELIVERYMAN]}
      />

      <PrivateRoute
        path={URL_HOME_USER}
        component={UserHome}
        roles={[ROLE_USER]}
      />

      <PrivateRoute
        path={URL_UPDATE_PROFIL}
        component={UpdateAccountView}
        roles={[ROLE_USER, ROLE_DELIVERYMAN]}
      />

      <PrivateRoute
        path={URL_HOME_USER}
        component={UserHome}
        roles={[ROLE_USER]}
      />

      <PrivateRoute
        exact
        path={URL_USER_ADVERTISEMENT_CREATION_FORM}
        component={CreateAdvertisementForm}
        roles={[ROLE_USER]}
      />

      <PrivateRoute
        exact
        path={URL_USER_ADVERTISEMENT_CREATION_FORM_RECAP}
        component={RecapAdInfos}
        roles={[ROLE_USER]}
      />

      <PrivateRoute
        exact
        path={URL_QUOTATION_DETAIL + ":id"}
        component={QuotationDetails}
        roles={[ROLE_USER]}
      />

      <PrivateRoute
        exact
        path={URL_USER_DETAILS_ADVERTISEMENT + ":id"}
        component={DetailsAdvertisementView}
        roles={[ROLE_USER]}
      />
      <PrivateRoute
        exact
        path={URL_DELIVERYMAN_DETAILS_ADVERTISEMENT + ":id"}
        component={DetailsAdvertisementView}
        roles={[ROLE_DELIVERYMAN]}
      />
      <PrivateRoute
        exact
        path={URL_USER_LIST_QUOTATIONS + ":id"}
        component={ListQuotationsUserView}
        roles={[ROLE_USER]}
      />

      <PrivateRoute
        exact
        path={URL_DELIVERYMAN_Quotations_HOME}
        component={QuotationListDeliveryman}
        roles={[ROLE_DELIVERYMAN]}
      />

      <PrivateRoute
        exact
        path={URL_USER_QUOTATION_CREATION_FORM + ":id"}
        component={CreateQuotationView}
        roles={[ROLE_DELIVERYMAN]}
      />
    </Switch>
  );
};

export default Routes;
