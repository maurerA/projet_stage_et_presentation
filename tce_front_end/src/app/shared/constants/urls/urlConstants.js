/**
 * TC-2
 * 2 types of possible url road constants:
 * - 1) public roads accessible to all,
 * - 2 ) private roads with user road, deliver road, admin road and specific road.
 *
 * @author MAURER adrien
 */
//-------public road :-------//
//HOME
export const URL_HOME = "/";

//lOGIN
export const URL_LOGIN = "/login";
//register consumer && deliver warning consumer and deliver are same component but differents input
export const URL_REGISTER_ACCOUNT_USER = "/registerAccount/user";
export const URL_REGISTER_ACCOUNT_DELIVERYMAN = "/registerAccount/deliveryman";

//------private road : ------//

//user road :

export const URL_HOME_USER = "/user";

export const URL_USER_ADVERTISEMENT_CREATION_FORM = "/advertisement/create";

export const URL_USER_ADVERTISEMENT_CREATION_FORM_RECAP =
  "/advertisement/recap";

export const URL_USER_DETAILS_ADVERTISEMENT = "/details_advertisement/";

export const URL_USER_LIST_QUOTATIONS = "/list_quotations/";

//deliver road :
export const URL_HOME_DELIVERYMAN = "/deliver";

export const URL_DELIVERYMAN_DETAILS_ADVERTISEMENT =
  "/details_delivery_advertisement/";

export const URL_DELIVERYMAN_Quotations_HOME = "/deliveryman/quotations";

export const URL_USER_QUOTATION_CREATION_FORM = '/quotation/create/'

export const URL_USER_QUOTATION_UPDATE_FORM = '/quotation/update'


//admin road :
export const URL_ADMIN_HOME = "/admin";

//for consumer and deliver

export const URL_UPDATE_PROFIL = "/update_Profil";

export const URL_QUOTATION_DETAIL = "/quotation_detail/";

