export const URL_BACK_AUTHENTICATE = "/authenticate";

export const URL_BACK_DISPLAY_USER = "/profile-user-account";

export const URL_BACK_DISABLE_USER = "/disable";

export const URL_BACK_REGISTER_ACCOUNT = "/register";

export const URL_BACK_UPDATE_PROFIL = "/profile-user-account";

export const URL_BACK_ALL_Country = "/countries";

export const URL_BACK_DISPLAY_COUNTRY = "/allCountries";

export const URL_BACK_CREATE_ADVERTISEMENT = "/advertisements/create";

export const URL_BACK_USER_ADVERTISEMENTS = "/advertisements/myAdvertisements";

export const URL_BACK_USER_ADVERTISEMENTS_DELETE = "/advertisements/";

export const URL_BACK_USER_ADVERTISEMENTS_FILTERED =
  "/advertisements/myAdvertisementsfiltered";

export const URL_BACK_USER_QUOTATION_DETAIL = "/quotation/";

export const URL_BACK_USER_QUOTATION_ACCEPTED =
  "/advertisement/quotationAccepted/";

export const URL_BACK_USER_QUOTATION_REFUSED =
  "/advertisement/quotationRefused/";

export const URL_BACK_USER_ADVERTISEMENTS_FILTERED_TITLE =
  "/advertisements/myAdvertisements/filtered/byTitle";

export const URL_BACK_USER_ADVERTISEMENTS_FILTERED_STATE =
  "/advertisements/myAdvertisements/filtered/byState";

export const URL_BACK_DISPLAY_DETAILS_ADVERTISEMENT = "/details-advertisements";

export const URL_BACK_DISPLAY_DETAILS_DELIVERY_ADVERTISEMENT =
  "/details-advertisements-for-deliverymen";

export const URL_BACK_DISPLAY_DELIVERY_ADVERTISEMENTS =
  "/advertisements/display-availables";

export const URL_BACK_LIST_QUOTATION_USER = "/advertisement/quotations";

export const URL_BACK_LIST_QUOTATION_DELIVERYMAN = "/myquotations";

export const URL_BACK_LIST_QUOTATION_FILTERED_DELIVERYMAN = '/myquotationsfiltered';

export const URL_BACK_CREATE_QUOTATION = '/quotation/create'

export const URL_BACK_UPDATE_QUOTATION = '/quotation/update'


export const URL_BACK_DISPLAY_DELIVERY_AD_DETAILS = "/details-advertisements-for-deliverymen/";

export const URL_BACK_LIST_NOTIFICATIONS = '/user-notifications';

export const URL_BACK_NOTIFICATION_VIEW_BY_USER = '/notifications/';


