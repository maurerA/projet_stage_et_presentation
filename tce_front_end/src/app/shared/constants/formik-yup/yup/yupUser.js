import * as Yup from 'yup'

export const schemaFormLogin = Yup.object().shape({
    username: Yup.string().required("Required input"),
    password: Yup.string().required("Required input")
})

export const SchemaFormUpdate = Yup.object().shape({
    
       
    firstName : Yup.string().required("Champs requis"),
    lastName : Yup.string().required("Champs requis"),
    streetName :  Yup.string().required("Champs requis"),
    streetNumber : Yup.string().required("Champs requis"),
    town : Yup.string().required("Champs requis"),
    zipCode : Yup.string().required("Champs requis"),
    label : Yup.string().required("Champs requis"),
    codeRef : Yup.string().required("Champs requis")
  
})

export const SchemaFormUpdateWithCompagny = Yup.object().shape({
    
    nameCompany : Yup.string().required("Champs requis"),
    firstName : Yup.string().required("Champs requis"),
    lastName : Yup.string().required("Champs requis"),
    streetName :  Yup.string().required("Champs requis"),
    streetNumber : Yup.string().required("Champs requis"),
    town : Yup.string().required("Champs requis"),
    zipCode : Yup.string().required("Champs requis"),
    label : Yup.string().required("Champs requis"),
    codeRef : Yup.string().required("Champs requis")
  
})


