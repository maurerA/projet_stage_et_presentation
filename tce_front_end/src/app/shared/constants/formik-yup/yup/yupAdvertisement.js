import * as Yup from 'yup'

export const schemaFormAdvertisement = Yup.object().shape({
    title: Yup.string().required("Champ requis"),
    length: Yup.number().required("Champ requis").min(1).typeError("Ce champ n'accepte que les nombres"),
    width: Yup.number().required("Champ requis").min(1).typeError("Ce champ n'accepte que les nombres"),
    height: Yup.number().required("Champ requis").min(1).typeError("Ce champ n'accepte que les nombres"),
    weight: Yup.number().required("Champ requis").min(0.1).typeError("Ce champ n'accepte que les nombres"),
    streetNumber: Yup.string().required("Champ requis"),
    streetName: Yup.string().required("Champ requis"),
    town: Yup.string().required("Champ requis"),
    zipCode: Yup.string().required("Champ requis"),
    countryId: Yup.number().required("Champ requis").typeError("Merci de sélectionner un pays."),
    streetNumber2: Yup.string().required("Champ requis"),
    streetName2: Yup.string().required("Champ requis"),
    town2: Yup.string().required("Champ requis"),
    zipCode2: Yup.string().required("Champ requis"),
    countryId2: Yup.number().required("Champ requis").typeError("Merci de sélectionner un pays."),
})