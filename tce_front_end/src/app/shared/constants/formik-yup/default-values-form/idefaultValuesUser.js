export const defaulValuesLogin = { 
    username:'', 
    password:'' 
}

export const defaultProfilValues = {
	
        activated: true,
        addressId: 0,
        authorities: [
          ""
        ],
        codeRef: "",
        companyId: 0,
        complementAddress: "",
        createdBy: "",
        createdDate: "",
        email: "",
        firstName: "",
        id: 0,
        idCountry: 0,
        imageUrl: "",
        label: "",
        langKey: "",
        lastModifiedBy: "",
        lastModifiedDate: "",
        lastName: "",
        login: "",
        nameCompany: "",
        numberSiren: "",
        numberSiretCompany: "",
        password: "",
        phoneNumber: "",
        streetName: "",
        streetNumber: "",
        town: "",
        userAccountId: 0,
        zipCode: ""
      
}

export const defaultAllCountryValues = [
    {
      codeRef: "",
      id: 0,
      label: ""
    }
]
