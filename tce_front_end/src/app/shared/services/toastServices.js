import { toast } from "react-toastify";
import { getPayloadToken } from "./tokenServices";
/**
 * This service allows the
 * customer to be notified
 * about the status of the actions
 * taken
 * TC-2
 * @author MAURER Adrien
 */
export const toastLoginSucess = () => {
  const login = getPayloadToken();
  toast.success("Bienvenue sur votre compte," + login.sub + " !", {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
};
export const toastLoginUnsuccess = () => {
  toast.error("Vous n'êtes pas autorisé à vous connecter", {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
};

export const toastUpdateSucess = () => {
  const login = getPayloadToken();
  toast.success(
    login.sub +
      " ,your changes have been taken into account. An email has been sent to you.",
    {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }
  );
};

/*
@author Mohamed
set up toast for displaying messages
*/
export const toastRegisterSucess = () => {
  toast.success("Félicitations, votre compte a bien été crée !", {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
};

export const toastRegisterFailed = () => {
  toast.error("Un problème est survenu lors de la création de votre compte !", {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
};

export const toastDeleteSucess = () => {
  toast.success("Votre compte a bien été supprimé !", {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
};

export const toastDeleteError = () => {
  toast.error(
    "Un problème est survenu lors de la suppression de votre compte !",
    {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }
  );
};
export const toastQuotationSucess = () => {
  const login = getPayloadToken();
  toast.success(
    login.sub +
      " ,votre devis a bien été enregistré, une notification a été envoyée à l'auteur de cette annonce.",
    {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }
  );
};
