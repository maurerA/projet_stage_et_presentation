import React from "react";
import {  MDBRow, MDBCol } from "mdbreact";



/**
 *  Put the contenant in a flex box
 * 
 * @example  <FlexIt col="3" mb="4">
 *               <MyComponent />
 *               <MyComponent />
 *               <MyComponent />
 *           </FlexIt>
 * 
 * @param {string} col: the column size (see bootstrap)
 * @param {string} mb: th margin bootstrap (sse bootstrap)
 * @author Mohamed Nechab, Peter Mollet
 */
const FlexIt = ({ children, col, mb }) => {
    return(
        <div>
            <MDBRow className="m-0">
                {children.map((child, index) => (
                    <MDBCol size={col} className={`p-0 mb-${mb ? mb : '5'}`} key={index}>
                        {child}
                    </MDBCol>
                ))}
            </MDBRow>
        </div>
    )
}
export default FlexIt;