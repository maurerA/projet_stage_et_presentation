import React, { useState, useEffect } from 'react'
import { displayDeliverymanQuotations } from '../api/backend/api.advertisement'
import { MDBTable, MDBTableBody } from 'mdbreact';
import Loader from './../shared/components/utils-components/Loader';
import QuotationTable from './../components/deliveryHome/quotations/QuotationTable';
import QuotationFIleHeader from './../components/deliveryHome/quotations/QuotationFIleHeader';
import { listQuotationsDeliveryManFiltered } from '../api/backend/api.quotation';

const QuotationListDeliveryman = () => {

    const [listOfQuotations, setListOfQuotations] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    const [showError, setShowError] = useState(false);

    useEffect(() => {

        displayDeliverymanQuotations().then(res => {
            setListOfQuotations(res.data)
            setIsLoading(false)
        })
    }, [])

    const getQuotationFiltered = (filtre, filter) => {
        listQuotationsDeliveryManFiltered({ contentFilter: filtre, filterType: filter }).then(res => {
            setListOfQuotations(res.data)

        })
    }


    const handleFilter = (title, checkboxTitle, checkboxState) => {
        setShowError(false);
        (checkboxState === false) && (checkboxTitle === false) && setShowError(true)
        if (!showError) {
            (checkboxTitle === true) && (checkboxState === true) && getQuotationFiltered(title, "ALL");
            (checkboxTitle === true) && (checkboxState === false) && getQuotationFiltered(title, "TITLE");
            (checkboxState === true) && (checkboxTitle === false) && getQuotationFiltered(title, "STATE");
        }
    };

    if (isLoading) return <Loader />
    return (
        <div>
            <MDBTable>
                <MDBTableBody>
                    <tr>
                        <td>
                            <QuotationFIleHeader handleFilter={handleFilter} showError={showError} />
                            <QuotationTable listOfQuotations={listOfQuotations} />
                        </td>
                    </tr>
                </MDBTableBody>
            </MDBTable>
        </div>
    )
}

export default QuotationListDeliveryman
