import React, { useState, useEffect } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody } from "mdbreact";
import { adressCountryDisplay, createAdvertisement } from '../api/backend/account'
import { defaultValuesAddress } from '../shared/constants/formik-yup/default-values-form/defaultValuesAddress'
import { Formik, Form } from 'formik';
import Loader from "../shared/components/utils-components/Loader";
import { URL_HOME_USER } from "../shared/constants/urls/urlConstants";
import { useHistory } from 'react-router-dom';
import RecapAdInfos from '../components/formAdvertisement/RecapAdInfos';
import AdvertisementForm from "../components/formAdvertisement/AdvertisementForm";
import { schemaFormAdvertisement } from "../shared/constants/formik-yup/yup/yupAdvertisement";
import { toast } from "react-toastify";
import ErrorMessSmall from "../shared/components/form-and-error-components/ErrorMessSmall";

const CreateAdvertisementForm = () => {

	const [isLoading, setLoading] = useState(true);
	const history = useHistory();
	const [optionPays, setOptionPays] = useState([{}])
	const [valuesFormAd, setValuesFormAd] = useState({})
	const [verifDisplay, setVerifDisplay] = useState(false)


	useEffect(() => {
		setValuesFormAd({})
		setVerifDisplay(false)
		adressCountryDisplay().then(res => {
			setOptionPays(res.data.map((country) => {
				return {
					text: country.codeRef + ' - ' + country.label,
					value: country.id,
					checked: false
				}
			}))
		})
		setLoading(false)
	}, [])

	const handleValidation = () => {
		createAdvertisement(valuesFormAd)
			.then(res => history.push(URL_HOME_USER))
			.catch((err, response) => {
				toast.error("Votre annonce n'a pas été enregistrée. Merci de réessayer ultérieurement ou contacter un administrateur.")
			})
	}


	const submit = values => {
		setVerifDisplay(true)
		setValuesFormAd(values)
	}

	if (isLoading) return <Loader />
	return (
		<MDBContainer className="createAdvertisement mt-3">
			<MDBRow>
				<MDBCol xl="6" lg="7" md="10">
					<MDBCard>
						<MDBCardBody>
							<Formik
								initialValues={defaultValuesAddress}
								validationSchema={schemaFormAdvertisement}
								onSubmit={submit}
							>
								{({ errors, touched }) => (
									<Form>
										{verifDisplay ?
											<RecapAdInfos valuesFormAd={valuesFormAd} handleValidation={handleValidation} />
											:
											<AdvertisementForm submit={submit} optionPays={optionPays} />}
										<div className="text-center">
											{/* {Object.entries(errors).length !== 0 && Object.entries(touched).length !== 0 &&
												<ErrorMessSmall message="Votre formulaire n'est pas intégralement rempli"></ErrorMessSmall>} */}
										</div>
									</Form>
								)}
							</Formik>
						</MDBCardBody>
					</MDBCard>
				</MDBCol>
			</MDBRow>
		</MDBContainer>
	);
};


export default CreateAdvertisementForm;