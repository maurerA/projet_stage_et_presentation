import React, { useState, useEffect } from 'react'
import Loader from './../shared/components/utils-components/Loader';
import { acceptQuotation, getQuotationDetail, refuseQuotation } from './../api/backend/api.advertisement';
import { MDBBtn } from 'mdbreact';
import { URL_USER_LIST_QUOTATIONS } from '../shared/constants/urls/urlConstants';
import { useHistory } from 'react-router-dom';

const QuotationDetails = (props) => {
    const history = useHistory();
    const [isLoading, setLoading] = useState(true);
    const [quotation, setQuotation] = useState({});
    const [quotationId, setQuotationId] = useState(0);

    useEffect(() => {
        getDetailQuotation()
        setQuotationId(quotation.statusId);
        console.log("QUOTATION ID USE EFFECT" + quotationId);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getDetailQuotation = () => {
        getQuotationDetail(props.match.params.id).then(res => {
            console.log("RES" + JSON.stringify(res.data));
            console.log("RES" + res.data.statusId);
            setQuotationId(res.data.statusId);
            console.log("QUOTATION ID THEN" + quotationId);
            const data = res.data;
            setQuotation(data);
            setLoading(false);
        }).then()
    }

    const acceptedQuotation = () => {
        acceptQuotation(props.match.params.id).then(res => {
            setQuotationId(3)
        })
    }

    const refusedQuotation = () => {
        refuseQuotation(props.match.params.id).then(res => {
            setQuotationId(4)
        })
    }

    const handleReturn = () => {
        history.push(URL_USER_LIST_QUOTATIONS + quotation.advertisementId)
    }


    if (isLoading) return <Loader />
    return (
        <div>
            <p>Détail Devis Reçu</p>
            <p>Nom Du Transporteur : {quotation.deliveryManName}</p>
            <p>Prix Du Transport : {quotation.deliveryManPrice}</p>
            <p>Nom De La Compagnie : {quotation.nameCompany}</p>
            <p>Siren De La Compagnie : {quotation.numberSiren}</p>
            <p>Numéro Siret De La Compagnie : {quotation.numberSiretCompany}</p>
            <p>Date De Création Du Devis : {quotation.creationDateQuotation}</p>
            <p>Etat du Devis : {quotation.statusLabel}</p>
            {quotationId === 1
                && <MDBBtn type="button" onClick={refusedQuotation}>Refuser Le Devis</MDBBtn>}
            {quotationId === 1
                && <MDBBtn type="button" onClick={acceptedQuotation}>Accepter Le Devis</MDBBtn>}
            {quotationId === 3
                && <p style={{ color: "green" }}>Vous avez accepté ce devis.</p>}
            {quotationId === 4
                && <p style={{ color: "red" }}>Vous avez refusé ce devis.</p>}

            <br /><br />
            <MDBBtn type="button" onClick={() => handleReturn()}>Retour à la Liste De Devis</MDBBtn>
        </div>
    )
}

export default QuotationDetails
