import React, { useState } from "react";
import AdvertisementsDetails from "../components/deliveryHome/advertisementsDetails/AdvertisementsDetails";
import DetailsAdvertisement from "../components/detailsAdvertisement/DetailsAdvertisement";
import { ROLE_USER } from "../shared/constants/rolesConstant";
import { hasRole } from "../shared/services/accountServices";

/**
 * allows to display details advertisement
 * @param {*} props
 * @returns DetailsAdvertisement with idParam
 *
 * @author MAURER Adrien TC-10
 */
const DetailsAdvertisementView = (props) => {
  const [idParam] = useState(props.match.params.id);

  if (hasRole(ROLE_USER)) {
    return (
      <div className="container d-flex justify-content-center mt-5 ">
        <DetailsAdvertisement idParam={idParam} />
      </div>
    );
  }
  return (
    <div className="container d-flex justify-content-center mt-5 ">
      <AdvertisementsDetails idParam={idParam} />
    </div>
  );
};

export default DetailsAdvertisementView;
