import React, { useEffect, useState } from 'react';
import { userAdvertisements, userAdvertisementsFiltered, userAdvertisementsFilteredTitle, deleteAdvertisements, userAdvertisementsFilteredState } from '../api/backend/api.advertisement';
import UserAdvertisementsTab from '../components/userHome/UserAdvertisementsTab';
import { URL_USER_ADVERTISEMENT_CREATION_FORM, URL_USER_DETAILS_ADVERTISEMENT } from './../shared/constants/urls/urlConstants';
import Loader from './../shared/components/utils-components/Loader';

const UserHome = ({ history }) => {

    const [isLoading, setLoading] = useState(true);
    const [showError, setShowError] = useState(false);
    const [offset, setOffset] = useState(0);
    const [dataNotMapped, setDataNotMapped] = useState([]);
    const [perPage] = useState(5);
    const [pageCount, setPageCount] = useState(0)

    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {
        userAdvertisements().then(res => {
            const data = res.data;
            setDataNotMapped(data.slice(offset, offset + perPage));
            setPageCount(Math.ceil(data.length / perPage));
            setLoading(false);
        })
    }

    const clickCreateAdBtn = () => {
        history.push(URL_USER_ADVERTISEMENT_CREATION_FORM)
    }

    const clickDisplayDetailsAd = (id) => {
        history.push(URL_USER_DETAILS_ADVERTISEMENT + `${id}`)

    }

    const clickDeleteAd = (id) => {
        setLoading(true)
        deleteAdvertisements(id).then(
            setDataNotMapped(dataNotMapped.filter(advertisement => advertisement.id !== id)),
            setLoading(false))
    }

    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        (e.selected === 0) ?
            setOffset(0)
            :
            setOffset(selectedPage * perPage)


    };

    const getAdFilterWithTitleAndState = (titre) => {
        userAdvertisementsFiltered({ title: titre }).then(res => {
            const data = res.data;
            const slice = data.slice(offset, offset + perPage)
            setDataNotMapped(slice)
            setPageCount(Math.ceil(data.length / perPage))
        })
    }

    const getAdFilterWithTitle = (titre) => {
        userAdvertisementsFilteredTitle({ title: titre }).then(res => {
            const data = res.data;
            const slice = data.slice(offset, offset + perPage)
            setDataNotMapped(slice)
            setPageCount(Math.ceil(data.length / perPage))
        })
    }

    const getAdFilterWithState = (titre) => {
        userAdvertisementsFilteredState({ title: titre }).then(res => {
            const data = res.data;
            const slice = data.slice(offset, offset + perPage)
            setDataNotMapped(slice)
            setPageCount(Math.ceil(data.length / perPage))
        })
    }


    const handleFilter = (title, checkboxTitle, checkboxState) => {
        setShowError(false);
        (checkboxState === false) && (checkboxTitle === false) && setShowError(true) && setDataNotMapped([])
        if (!showError) {
            (checkboxTitle === true) && (checkboxState === true) && getAdFilterWithTitleAndState(title);
            (checkboxTitle === true) && (checkboxState === false) && getAdFilterWithTitle(title);
            (checkboxState === true) && (checkboxTitle === false) && getAdFilterWithState(title);
        }
    };

    if (isLoading) return <Loader />
    return (
        <UserAdvertisementsTab
            dataNotMapped={dataNotMapped}
            clickCreateAdBtn={clickCreateAdBtn}
            showError={showError}
            handleFilter={handleFilter}
            pageCount={pageCount}
            handlePageClick={handlePageClick}
            clickDisplayDetailsAd={clickDisplayDetailsAd}
            clickDeleteAd={clickDeleteAd}
        />


    );
}

export default UserHome;