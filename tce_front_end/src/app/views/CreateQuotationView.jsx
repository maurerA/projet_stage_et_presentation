import React,{useState} from 'react'
import CreateQuotationForm from '../components/formQuotation/CreateQuotationForm'

const CreateQuotationView = (props) => {
    const [id] = useState(props.match.params.id)

    return(
        <div >

        <CreateQuotationForm id={id}/>

        </div>
    

    )
}

    


export default CreateQuotationView ;