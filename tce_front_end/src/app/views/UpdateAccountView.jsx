import { MDBBox } from 'mdbreact';
import UpdateAccount from './../components/account/UpdateAccount';

/**
 * 
 * @returns 
 * 
 * @author MAURER Adrien TC-22
 */
function UpdateAccountView() {
    
    return (
      
        <MDBBox className="ml-2 mt-2 hapyDelivryMan " >
            <UpdateAccount/>
        </MDBBox>
      
       
    );
}

export default UpdateAccountView;