import { MDBBox, MDBIcon } from 'mdbreact';
import React from 'react'
import { MDBRow } from 'mdbreact';
import { MDBContainer } from 'mdbreact';
import { MDBCol } from 'mdbreact';
import { MDBBtn } from 'mdbreact';
import colis from '../assets/images/paydDelivery.png'
// import colis1 from ''
// import colis2 from ''


const HomeView = () => {

    const toBeUser = () => {

    }

    const toBeDeliveryMan = () => {

    }

    return (
        <div className="homePAge mt-5">
            <MDBContainer >
                <MDBRow >
                    <MDBCol></MDBCol>
                    <MDBCol></MDBCol>
                    <MDBCol >
                        <h2 className="white-text">Trouver un transporteur à proximité et au meilleur prix</h2>
                        <h6 className="white-text ">Coliv' est une application innvovante vous permettant d'envoyer vos biens où
                        que vous soyez et où vous le souhaitez ainsi que de suivre l'avancée de
                         l'expédition en temps réel</h6>
                        <br />
                        <MDBBtn color="white" className="warning-text" onClick={toBeUser()}>Expedier des colis  <MDBIcon far size="1x" className="amber-text pr-3" icon="arrow-alt-circle-right" /></MDBBtn>
                        <br />
                        <br />
                        <h6 className="white-text ">Coliv' est une application innvovante vous permettant d'envoyervos biens où
                        que vous soyez et où vous le souhaitez ainsi que de suivre l'avancée de
                         l'expédition en temps réel</h6>
                        <br />
                         <MDBBtn color="white" className="warning-text mb-2" onClick={toBeDeliveryMan()}>Livrer des colis <MDBIcon far size="1x" className="amber-text pr-3" icon="arrow-alt-circle-right" /></MDBBtn>

                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        </div>

    );
};

export default HomeView;