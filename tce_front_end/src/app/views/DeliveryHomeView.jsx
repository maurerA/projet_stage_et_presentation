import React, { useEffect, useState } from "react";
import { useHistory } from 'react-router';
import {
  deliveryAdvertisements,
  userAdvertisementsFiltered,
  userAdvertisementsFilteredTitle,
  userAdvertisementsFilteredState,
} from "../api/backend/api.advertisement";
import { URL_DELIVERYMAN_DETAILS_ADVERTISEMENT, URL_DELIVERYMAN_Quotations_HOME, URL_USER_QUOTATION_CREATION_FORM } from "./../shared/constants/urls/urlConstants";
import Loader from "./../shared/components/utils-components/Loader";
import AdvertisementsPages from "../components/deliveryHome/advertisements/AdvertisementsPages";
import { MDBBtn } from "mdbreact";

const DeliveryHomeView = ({ history }) => {
  const [isLoading, setLoading] = useState(true);
  const [showError, setShowError] = useState(false);
  const [offset, setOffset] = useState(0);
  const [dataNotMapped, setDataNotMapped] = useState([]);
  const [perPage] = useState(5);
  const [reload] = useState(false);
  const [pageCount, setPageCount] = useState(0);

  useEffect(() => {
    getDeliveryAdvs();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [offset, reload]);


  const redirectForCreateQuotation = (id) =>{
    history.push(URL_USER_QUOTATION_CREATION_FORM+`${id}`)
  }

  const redirectForUpdateQuotation = (id) =>{
    history.push(URL_USER_QUOTATION_CREATION_FORM+`${id}`)}
  

  const getDeliveryAdvs = async () => {
    deliveryAdvertisements().then((res) => {
      const data = res.data;
      setDataNotMapped(data.slice(offset, offset + perPage));
      setPageCount(Math.ceil(data.length / perPage));
      setLoading(false);
    });
  };

  const clickDisplayDetailsAd = (id) => {
    history.push(URL_DELIVERYMAN_DETAILS_ADVERTISEMENT + `${id}`);
  };

  const handlePageClick = (e) => {
    const selectedPage = e.selected;
    e.selected === 0 ? setOffset(0) : setOffset(selectedPage * perPage);
  };

  const getAdFilterWithTitleAndState = (titre) => {
    userAdvertisementsFiltered({ title: titre }).then((res) => {
      const data = res.data;
      const slice = data.slice(offset, offset + perPage);
      setDataNotMapped(slice);
      setPageCount(Math.ceil(data.length / perPage));
    });
  };

  const getAdFilterWithTitle = (titre) => {
    userAdvertisementsFilteredTitle({ title: titre }).then((res) => {
      const data = res.data;
      const slice = data.slice(offset, offset + perPage);
      setDataNotMapped(slice);
      setPageCount(Math.ceil(data.length / perPage));
    });
  };

  const getAdFilterWithState = (titre) => {
    userAdvertisementsFilteredState({ title: titre }).then((res) => {
      const data = res.data;
      const slice = data.slice(offset, offset + perPage);
      setDataNotMapped(slice);
      setPageCount(Math.ceil(data.length / perPage));
    });
  };

  const handleFilter = (title, checkboxTitle, checkboxState) => {
    setShowError(false);
    checkboxState === false && checkboxTitle === false && setShowError(true);
    if (!showError) {
      checkboxTitle === true &&
        checkboxState === true &&
        getAdFilterWithTitleAndState(title);
      checkboxTitle === true &&
        checkboxState === false &&
        getAdFilterWithTitle(title);
      checkboxState === true &&
        checkboxTitle === false &&
        getAdFilterWithState(title);
    }
  };

  const handleDisplayQuotations = () => {
    history.push(URL_DELIVERYMAN_Quotations_HOME);
  }

  if (isLoading) return <Loader />;
  return (
    <div>
      
      <MDBBtn type="button" onClick = {() => redirectForCreateQuotation(1)}> creer un devis </MDBBtn> 
      <MDBBtn type="button" onClick = {() => redirectForUpdateQuotation(2)}> modifier un devis </MDBBtn> 
      <MDBBtn type="button" onClick={() => handleDisplayQuotations()}>Lister mes devis</MDBBtn><br /><br /><br />

      {/* <MDBBtn type="button" onClick = {() => redirect(1)}> creer un devis </MDBBtn> 
      <MDBBtn type="button" onClick={() => handleDisplayQuotations()}>Lister mes devis</MDBBtn><br /><br /><br /> */}
      <AdvertisementsPages
        dataNotMapped={dataNotMapped}
        showError={showError}
        handleFilter={handleFilter}
        pageCount={pageCount}
        handlePageClick={handlePageClick}
        clickDisplayDetailsAd={clickDisplayDetailsAd}
      />
    </div>
  );
};

export default DeliveryHomeView;
