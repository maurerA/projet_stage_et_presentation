import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { authenticate } from './../api/backend/account'
import Login from '../components/account/Login'
import { isAuthenticated } from '../shared/services/accountServices';
import { URL_HOME, URL_HOME_DELIVERYMAN, URL_HOME_USER } from './../shared/constants/urls/urlConstants';
import { signIn, signOut } from '../shared/redux-store/actions/authenticationActions';
import { accountRoles } from './../shared/services/accountServices';
import { ROLE_USER, ROLE_DELIVERYMAN } from '../shared/constants/rolesConstant';
import { toastLoginSucess, toastLoginUnsuccess } from './../shared/services/toastServices';
import { MDBBox } from 'mdbreact';

/**
 * View/Page Login
 * 
 * @param {object} history 
 * @author Peter Mollet
 */
const LoginView = ({ history }) => {
    const [errorLog, setErrorLog] = useState(false)
    const dispatch = useDispatch()

    /** 
     *  TC-2
     *  role management and road redirection
     * @param {roles}  values 
     * 
     * @author MAURER adrien 
     */
    const handleLogin = (values) => {

        authenticate(values).then(res => {
            if (res.status === 200 && res.data.id_token) {
                dispatch(signIn(res.data.id_token))
                //catch role in the claims
                const roles = accountRoles();
                //checking the size of the list
                const length = roles.length
                //differents conditions

                //if user is authenticated with the role user
                //isAuthenticated && roles.toString().includes(ROLE_USER)
                if (length === 1 && isAuthenticated && roles.toString() === ROLE_USER) {
                    toastLoginSucess()
                    history.push(URL_HOME_USER)
                }
                //if user is authenticated with the role deliver
                else if (length === 1 && isAuthenticated && roles.toString() === ROLE_DELIVERYMAN) {
                    toastLoginSucess()
                    history.push(URL_HOME_DELIVERYMAN)
                }
                else {
                    dispatch(signOut())
                    toastLoginUnsuccess()
                    history.push(URL_HOME)
                }
                

            }
        }).catch(() => setErrorLog(true))
    }
    /**
     * button allows return at home
     * 
     * @author MAURER Adrien TC-2
     */
    const cancel = () => {
        history.push(URL_HOME)
    }

//container d-flex justify-content-center mt-5

    return (
   
        <MDBBox className="container d-flex justify-content-left mt-5 bg" >
            <Login submit={handleLogin} errorLog={errorLog} cancel={cancel} />
        </MDBBox>
       
     
    );
};

export default LoginView

