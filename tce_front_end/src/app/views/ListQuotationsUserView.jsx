import React from 'react';
import TablesQuotationUser from './../components/listQuotationUser/TablesQuotationUser';
import { MDBContainer } from 'mdbreact';
/**
 * prepare a table information
 * @param {*} props 
 * @returns id contain in param
 * @author MAURER Adrien TC-26
 */
const ListQuotationsUserView = (props) => {
            return (
                <MDBContainer>
                     <TablesQuotationUser id={props.match.params.id}/>
                </MDBContainer>
              
            )
            };

export default ListQuotationsUserView;