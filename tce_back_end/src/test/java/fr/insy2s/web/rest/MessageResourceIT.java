package fr.insy2s.web.rest;

import static fr.insy2s.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.Advertisement;
import fr.insy2s.domain.Message;
import fr.insy2s.domain.Photo;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.MessageRepository;
import fr.insy2s.service.MessageQueryService;
import fr.insy2s.service.MessageService;
import fr.insy2s.service.dto.MessageCriteria;
import fr.insy2s.service.dto.MessageDTO;
import fr.insy2s.service.mapper.MessageMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MessageResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class MessageResourceIT {
    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_MESSAGE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_MESSAGE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_MESSAGE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private MessageService messageService;

    @Autowired
    private MessageQueryService messageQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMessageMockMvc;

    private Message message;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Message createEntity(EntityManager em) {
        Message message = new Message().message(DEFAULT_MESSAGE).dateMessage(DEFAULT_DATE_MESSAGE);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        message.setReceiver(userAccount);
        // Add required entity
        message.setSender(userAccount);
        return message;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Message createUpdatedEntity(EntityManager em) {
        Message message = new Message().message(UPDATED_MESSAGE).dateMessage(UPDATED_DATE_MESSAGE);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createUpdatedEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        message.setReceiver(userAccount);
        // Add required entity
        message.setSender(userAccount);
        return message;
    }

    @BeforeEach
    public void initTest() {
        message = createEntity(em);
    }

    @Test
    @Transactional
    public void createMessage() throws Exception {
        int databaseSizeBeforeCreate = messageRepository.findAll().size();
        // Create the Message
        MessageDTO messageDTO = messageMapper.toDto(message);
        restMessageMockMvc
            .perform(post("/api/messages").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isCreated());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeCreate + 1);
        Message testMessage = messageList.get(messageList.size() - 1);
        assertThat(testMessage.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testMessage.getDateMessage()).isEqualTo(DEFAULT_DATE_MESSAGE);
    }

    @Test
    @Transactional
    public void createMessageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = messageRepository.findAll().size();

        // Create the Message with an existing ID
        message.setId(1L);
        MessageDTO messageDTO = messageMapper.toDto(message);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMessageMockMvc
            .perform(post("/api/messages").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkMessageIsRequired() throws Exception {
        int databaseSizeBeforeTest = messageRepository.findAll().size();
        // set the field null
        message.setMessage(null);

        // Create the Message, which fails.
        MessageDTO messageDTO = messageMapper.toDto(message);

        restMessageMockMvc
            .perform(post("/api/messages").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isBadRequest());

        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateMessageIsRequired() throws Exception {
        int databaseSizeBeforeTest = messageRepository.findAll().size();
        // set the field null
        message.setDateMessage(null);

        // Create the Message, which fails.
        MessageDTO messageDTO = messageMapper.toDto(message);

        restMessageMockMvc
            .perform(post("/api/messages").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isBadRequest());

        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMessages() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList
        restMessageMockMvc
            .perform(get("/api/messages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(message.getId().intValue())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].dateMessage").value(hasItem(sameInstant(DEFAULT_DATE_MESSAGE))));
    }

    @Test
    @Transactional
    public void getMessage() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get the message
        restMessageMockMvc
            .perform(get("/api/messages/{id}", message.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(message.getId().intValue()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE))
            .andExpect(jsonPath("$.dateMessage").value(sameInstant(DEFAULT_DATE_MESSAGE)));
    }

    @Test
    @Transactional
    public void getMessagesByIdFiltering() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        Long id = message.getId();

        defaultMessageShouldBeFound("id.equals=" + id);
        defaultMessageShouldNotBeFound("id.notEquals=" + id);

        defaultMessageShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMessageShouldNotBeFound("id.greaterThan=" + id);

        defaultMessageShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMessageShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllMessagesByMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message equals to DEFAULT_MESSAGE
        defaultMessageShouldBeFound("message.equals=" + DEFAULT_MESSAGE);

        // Get all the messageList where message equals to UPDATED_MESSAGE
        defaultMessageShouldNotBeFound("message.equals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByMessageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message not equals to DEFAULT_MESSAGE
        defaultMessageShouldNotBeFound("message.notEquals=" + DEFAULT_MESSAGE);

        // Get all the messageList where message not equals to UPDATED_MESSAGE
        defaultMessageShouldBeFound("message.notEquals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByMessageIsInShouldWork() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message in DEFAULT_MESSAGE or UPDATED_MESSAGE
        defaultMessageShouldBeFound("message.in=" + DEFAULT_MESSAGE + "," + UPDATED_MESSAGE);

        // Get all the messageList where message equals to UPDATED_MESSAGE
        defaultMessageShouldNotBeFound("message.in=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message is not null
        defaultMessageShouldBeFound("message.specified=true");

        // Get all the messageList where message is null
        defaultMessageShouldNotBeFound("message.specified=false");
    }

    @Test
    @Transactional
    public void getAllMessagesByMessageContainsSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message contains DEFAULT_MESSAGE
        defaultMessageShouldBeFound("message.contains=" + DEFAULT_MESSAGE);

        // Get all the messageList where message contains UPDATED_MESSAGE
        defaultMessageShouldNotBeFound("message.contains=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByMessageNotContainsSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message does not contain DEFAULT_MESSAGE
        defaultMessageShouldNotBeFound("message.doesNotContain=" + DEFAULT_MESSAGE);

        // Get all the messageList where message does not contain UPDATED_MESSAGE
        defaultMessageShouldBeFound("message.doesNotContain=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByDateMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage equals to DEFAULT_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.equals=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage equals to UPDATED_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.equals=" + UPDATED_DATE_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByDateMessageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage not equals to DEFAULT_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.notEquals=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage not equals to UPDATED_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.notEquals=" + UPDATED_DATE_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByDateMessageIsInShouldWork() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage in DEFAULT_DATE_MESSAGE or UPDATED_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.in=" + DEFAULT_DATE_MESSAGE + "," + UPDATED_DATE_MESSAGE);

        // Get all the messageList where dateMessage equals to UPDATED_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.in=" + UPDATED_DATE_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByDateMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage is not null
        defaultMessageShouldBeFound("dateMessage.specified=true");

        // Get all the messageList where dateMessage is null
        defaultMessageShouldNotBeFound("dateMessage.specified=false");
    }

    @Test
    @Transactional
    public void getAllMessagesByDateMessageIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage is greater than or equal to DEFAULT_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.greaterThanOrEqual=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage is greater than or equal to UPDATED_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.greaterThanOrEqual=" + UPDATED_DATE_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByDateMessageIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage is less than or equal to DEFAULT_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.lessThanOrEqual=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage is less than or equal to SMALLER_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.lessThanOrEqual=" + SMALLER_DATE_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByDateMessageIsLessThanSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage is less than DEFAULT_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.lessThan=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage is less than UPDATED_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.lessThan=" + UPDATED_DATE_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByDateMessageIsGreaterThanSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage is greater than DEFAULT_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.greaterThan=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage is greater than SMALLER_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.greaterThan=" + SMALLER_DATE_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllMessagesByPhotoIsEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);
        Photo photo = PhotoResourceIT.createEntity(em);
        em.persist(photo);
        em.flush();
        message.setPhoto(photo);
        messageRepository.saveAndFlush(message);
        Long photoId = photo.getId();

        // Get all the messageList where photo equals to photoId
        defaultMessageShouldBeFound("photoId.equals=" + photoId);

        // Get all the messageList where photo equals to photoId + 1
        defaultMessageShouldNotBeFound("photoId.equals=" + (photoId + 1));
    }

    @Test
    @Transactional
    public void getAllMessagesByReceiverIsEqualToSomething() throws Exception {
        // Get already existing entity
        UserAccount receiver = message.getReceiver();
        messageRepository.saveAndFlush(message);
        Long receiverId = receiver.getId();

        // Get all the messageList where receiver equals to receiverId
        defaultMessageShouldBeFound("receiverId.equals=" + receiverId);

        // Get all the messageList where receiver equals to receiverId + 1
        defaultMessageShouldNotBeFound("receiverId.equals=" + (receiverId + 1));
    }

    @Test
    @Transactional
    public void getAllMessagesBySenderIsEqualToSomething() throws Exception {
        // Get already existing entity
        UserAccount sender = message.getSender();
        messageRepository.saveAndFlush(message);
        Long senderId = sender.getId();

        // Get all the messageList where sender equals to senderId
        defaultMessageShouldBeFound("senderId.equals=" + senderId);

        // Get all the messageList where sender equals to senderId + 1
        defaultMessageShouldNotBeFound("senderId.equals=" + (senderId + 1));
    }

    @Test
    @Transactional
    public void getAllMessagesByAdvertisementIsEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);
        Advertisement advertisement = AdvertisementResourceIT.createEntity(em);
        em.persist(advertisement);
        em.flush();
        message.setAdvertisement(advertisement);
        messageRepository.saveAndFlush(message);
        Long advertisementId = advertisement.getId();

        // Get all the messageList where advertisement equals to advertisementId
        defaultMessageShouldBeFound("advertisementId.equals=" + advertisementId);

        // Get all the messageList where advertisement equals to advertisementId + 1
        defaultMessageShouldNotBeFound("advertisementId.equals=" + (advertisementId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMessageShouldBeFound(String filter) throws Exception {
        restMessageMockMvc
            .perform(get("/api/messages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(message.getId().intValue())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].dateMessage").value(hasItem(sameInstant(DEFAULT_DATE_MESSAGE))));

        // Check, that the count call also returns 1
        restMessageMockMvc
            .perform(get("/api/messages/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMessageShouldNotBeFound(String filter) throws Exception {
        restMessageMockMvc
            .perform(get("/api/messages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMessageMockMvc
            .perform(get("/api/messages/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingMessage() throws Exception {
        // Get the message
        restMessageMockMvc.perform(get("/api/messages/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMessage() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        int databaseSizeBeforeUpdate = messageRepository.findAll().size();

        // Update the message
        Message updatedMessage = messageRepository.findById(message.getId()).get();
        // Disconnect from session so that the updates on updatedMessage are not directly saved in db
        em.detach(updatedMessage);
        updatedMessage.message(UPDATED_MESSAGE).dateMessage(UPDATED_DATE_MESSAGE);
        MessageDTO messageDTO = messageMapper.toDto(updatedMessage);

        restMessageMockMvc
            .perform(put("/api/messages").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isOk());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
        Message testMessage = messageList.get(messageList.size() - 1);
        assertThat(testMessage.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testMessage.getDateMessage()).isEqualTo(UPDATED_DATE_MESSAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingMessage() throws Exception {
        int databaseSizeBeforeUpdate = messageRepository.findAll().size();

        // Create the Message
        MessageDTO messageDTO = messageMapper.toDto(message);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMessageMockMvc
            .perform(put("/api/messages").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMessage() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        int databaseSizeBeforeDelete = messageRepository.findAll().size();

        // Delete the message
        restMessageMockMvc
            .perform(delete("/api/messages/{id}", message.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
