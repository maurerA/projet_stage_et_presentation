package fr.insy2s.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.Advertisement;
import fr.insy2s.domain.Photo;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.PhotoRepository;
import fr.insy2s.service.PhotoQueryService;
import fr.insy2s.service.PhotoService;
import fr.insy2s.service.dto.PhotoCriteria;
import fr.insy2s.service.dto.PhotoDTO;
import fr.insy2s.service.mapper.PhotoMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link PhotoResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PhotoResourceIT {
    private static final byte[] DEFAULT_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PHOTO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PHOTO_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_PHOTO_PATH = "AAAAAAAAAA";
    private static final String UPDATED_PHOTO_PATH = "BBBBBBBBBB";

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private PhotoMapper photoMapper;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private PhotoQueryService photoQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhotoMockMvc;

    private Photo photo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Photo createEntity(EntityManager em) {
        Photo photo = new Photo().photo(DEFAULT_PHOTO).photoContentType(DEFAULT_PHOTO_CONTENT_TYPE).photoPath(DEFAULT_PHOTO_PATH);
        return photo;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Photo createUpdatedEntity(EntityManager em) {
        Photo photo = new Photo().photo(UPDATED_PHOTO).photoContentType(UPDATED_PHOTO_CONTENT_TYPE).photoPath(UPDATED_PHOTO_PATH);
        return photo;
    }

    @BeforeEach
    public void initTest() {
        photo = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhoto() throws Exception {
        int databaseSizeBeforeCreate = photoRepository.findAll().size();
        // Create the Photo
        PhotoDTO photoDTO = photoMapper.toDto(photo);
        restPhotoMockMvc
            .perform(post("/api/photos").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(photoDTO)))
            .andExpect(status().isCreated());

        // Validate the Photo in the database
        List<Photo> photoList = photoRepository.findAll();
        assertThat(photoList).hasSize(databaseSizeBeforeCreate + 1);
        Photo testPhoto = photoList.get(photoList.size() - 1);
        assertThat(testPhoto.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testPhoto.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
        assertThat(testPhoto.getPhotoPath()).isEqualTo(DEFAULT_PHOTO_PATH);
    }

    @Test
    @Transactional
    public void createPhotoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = photoRepository.findAll().size();

        // Create the Photo with an existing ID
        photo.setId(1L);
        PhotoDTO photoDTO = photoMapper.toDto(photo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhotoMockMvc
            .perform(post("/api/photos").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(photoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Photo in the database
        List<Photo> photoList = photoRepository.findAll();
        assertThat(photoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPhotos() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList
        restPhotoMockMvc
            .perform(get("/api/photos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(photo.getId().intValue())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].photoPath").value(hasItem(DEFAULT_PHOTO_PATH)));
    }

    @Test
    @Transactional
    public void getPhoto() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get the photo
        restPhotoMockMvc
            .perform(get("/api/photos/{id}", photo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(photo.getId().intValue()))
            .andExpect(jsonPath("$.photoContentType").value(DEFAULT_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.photo").value(Base64Utils.encodeToString(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.photoPath").value(DEFAULT_PHOTO_PATH));
    }

    @Test
    @Transactional
    public void getPhotosByIdFiltering() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        Long id = photo.getId();

        defaultPhotoShouldBeFound("id.equals=" + id);
        defaultPhotoShouldNotBeFound("id.notEquals=" + id);

        defaultPhotoShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhotoShouldNotBeFound("id.greaterThan=" + id);

        defaultPhotoShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhotoShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllPhotosByPhotoPathIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where photoPath equals to DEFAULT_PHOTO_PATH
        defaultPhotoShouldBeFound("photoPath.equals=" + DEFAULT_PHOTO_PATH);

        // Get all the photoList where photoPath equals to UPDATED_PHOTO_PATH
        defaultPhotoShouldNotBeFound("photoPath.equals=" + UPDATED_PHOTO_PATH);
    }

    @Test
    @Transactional
    public void getAllPhotosByPhotoPathIsNotEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where photoPath not equals to DEFAULT_PHOTO_PATH
        defaultPhotoShouldNotBeFound("photoPath.notEquals=" + DEFAULT_PHOTO_PATH);

        // Get all the photoList where photoPath not equals to UPDATED_PHOTO_PATH
        defaultPhotoShouldBeFound("photoPath.notEquals=" + UPDATED_PHOTO_PATH);
    }

    @Test
    @Transactional
    public void getAllPhotosByPhotoPathIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where photoPath in DEFAULT_PHOTO_PATH or UPDATED_PHOTO_PATH
        defaultPhotoShouldBeFound("photoPath.in=" + DEFAULT_PHOTO_PATH + "," + UPDATED_PHOTO_PATH);

        // Get all the photoList where photoPath equals to UPDATED_PHOTO_PATH
        defaultPhotoShouldNotBeFound("photoPath.in=" + UPDATED_PHOTO_PATH);
    }

    @Test
    @Transactional
    public void getAllPhotosByPhotoPathIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where photoPath is not null
        defaultPhotoShouldBeFound("photoPath.specified=true");

        // Get all the photoList where photoPath is null
        defaultPhotoShouldNotBeFound("photoPath.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByPhotoPathContainsSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where photoPath contains DEFAULT_PHOTO_PATH
        defaultPhotoShouldBeFound("photoPath.contains=" + DEFAULT_PHOTO_PATH);

        // Get all the photoList where photoPath contains UPDATED_PHOTO_PATH
        defaultPhotoShouldNotBeFound("photoPath.contains=" + UPDATED_PHOTO_PATH);
    }

    @Test
    @Transactional
    public void getAllPhotosByPhotoPathNotContainsSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where photoPath does not contain DEFAULT_PHOTO_PATH
        defaultPhotoShouldNotBeFound("photoPath.doesNotContain=" + DEFAULT_PHOTO_PATH);

        // Get all the photoList where photoPath does not contain UPDATED_PHOTO_PATH
        defaultPhotoShouldBeFound("photoPath.doesNotContain=" + UPDATED_PHOTO_PATH);
    }

    @Test
    @Transactional
    public void getAllPhotosByDeliveryManIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);
        UserAccount deliveryMan = UserAccountResourceIT.createEntity(em);
        em.persist(deliveryMan);
        em.flush();
        photo.setDeliveryMan(deliveryMan);
        photoRepository.saveAndFlush(photo);
        Long deliveryManId = deliveryMan.getId();

        // Get all the photoList where deliveryMan equals to deliveryManId
        defaultPhotoShouldBeFound("deliveryManId.equals=" + deliveryManId);

        // Get all the photoList where deliveryMan equals to deliveryManId + 1
        defaultPhotoShouldNotBeFound("deliveryManId.equals=" + (deliveryManId + 1));
    }

    @Test
    @Transactional
    public void getAllPhotosByAdvertisementIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);
        Advertisement advertisement = AdvertisementResourceIT.createEntity(em);
        em.persist(advertisement);
        em.flush();
        photo.setAdvertisement(advertisement);
        photoRepository.saveAndFlush(photo);
        Long advertisementId = advertisement.getId();

        // Get all the photoList where advertisement equals to advertisementId
        defaultPhotoShouldBeFound("advertisementId.equals=" + advertisementId);

        // Get all the photoList where advertisement equals to advertisementId + 1
        defaultPhotoShouldNotBeFound("advertisementId.equals=" + (advertisementId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhotoShouldBeFound(String filter) throws Exception {
        restPhotoMockMvc
            .perform(get("/api/photos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(photo.getId().intValue())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].photoPath").value(hasItem(DEFAULT_PHOTO_PATH)));

        // Check, that the count call also returns 1
        restPhotoMockMvc
            .perform(get("/api/photos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhotoShouldNotBeFound(String filter) throws Exception {
        restPhotoMockMvc
            .perform(get("/api/photos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhotoMockMvc
            .perform(get("/api/photos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingPhoto() throws Exception {
        // Get the photo
        restPhotoMockMvc.perform(get("/api/photos/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhoto() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        int databaseSizeBeforeUpdate = photoRepository.findAll().size();

        // Update the photo
        Photo updatedPhoto = photoRepository.findById(photo.getId()).get();
        // Disconnect from session so that the updates on updatedPhoto are not directly saved in db
        em.detach(updatedPhoto);
        updatedPhoto.photo(UPDATED_PHOTO).photoContentType(UPDATED_PHOTO_CONTENT_TYPE).photoPath(UPDATED_PHOTO_PATH);
        PhotoDTO photoDTO = photoMapper.toDto(updatedPhoto);

        restPhotoMockMvc
            .perform(put("/api/photos").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(photoDTO)))
            .andExpect(status().isOk());

        // Validate the Photo in the database
        List<Photo> photoList = photoRepository.findAll();
        assertThat(photoList).hasSize(databaseSizeBeforeUpdate);
        Photo testPhoto = photoList.get(photoList.size() - 1);
        assertThat(testPhoto.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testPhoto.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testPhoto.getPhotoPath()).isEqualTo(UPDATED_PHOTO_PATH);
    }

    @Test
    @Transactional
    public void updateNonExistingPhoto() throws Exception {
        int databaseSizeBeforeUpdate = photoRepository.findAll().size();

        // Create the Photo
        PhotoDTO photoDTO = photoMapper.toDto(photo);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhotoMockMvc
            .perform(put("/api/photos").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(photoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Photo in the database
        List<Photo> photoList = photoRepository.findAll();
        assertThat(photoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhoto() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        int databaseSizeBeforeDelete = photoRepository.findAll().size();

        // Delete the photo
        restPhotoMockMvc
            .perform(delete("/api/photos/{id}", photo.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Photo> photoList = photoRepository.findAll();
        assertThat(photoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
