package fr.insy2s.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.AdvertisementStatus;
import fr.insy2s.repository.AdvertisementStatusRepository;
import fr.insy2s.service.AdvertisementStatusQueryService;
import fr.insy2s.service.AdvertisementStatusService;
import fr.insy2s.service.dto.AdvertisementStatusCriteria;
import fr.insy2s.service.dto.AdvertisementStatusDTO;
import fr.insy2s.service.mapper.AdvertisementStatusMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AdvertisementStatusResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AdvertisementStatusResourceIT {
    private static final String DEFAULT_CODE_REF = "AAAAAAAAAA";
    private static final String UPDATED_CODE_REF = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    @Autowired
    private AdvertisementStatusRepository advertisementStatusRepository;

    @Autowired
    private AdvertisementStatusMapper advertisementStatusMapper;

    @Autowired
    private AdvertisementStatusService advertisementStatusService;

    @Autowired
    private AdvertisementStatusQueryService advertisementStatusQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAdvertisementStatusMockMvc;

    private AdvertisementStatus advertisementStatus;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdvertisementStatus createEntity(EntityManager em) {
        AdvertisementStatus advertisementStatus = new AdvertisementStatus().codeRef(DEFAULT_CODE_REF).label(DEFAULT_LABEL);
        return advertisementStatus;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdvertisementStatus createUpdatedEntity(EntityManager em) {
        AdvertisementStatus advertisementStatus = new AdvertisementStatus().codeRef(UPDATED_CODE_REF).label(UPDATED_LABEL);
        return advertisementStatus;
    }

    @BeforeEach
    public void initTest() {
        advertisementStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdvertisementStatus() throws Exception {
        int databaseSizeBeforeCreate = advertisementStatusRepository.findAll().size();
        // Create the AdvertisementStatus
        AdvertisementStatusDTO advertisementStatusDTO = advertisementStatusMapper.toDto(advertisementStatus);
        restAdvertisementStatusMockMvc
            .perform(
                post("/api/advertisement-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementStatusDTO))
            )
            .andExpect(status().isCreated());

        // Validate the AdvertisementStatus in the database
        List<AdvertisementStatus> advertisementStatusList = advertisementStatusRepository.findAll();
        assertThat(advertisementStatusList).hasSize(databaseSizeBeforeCreate + 1);
        AdvertisementStatus testAdvertisementStatus = advertisementStatusList.get(advertisementStatusList.size() - 1);
        assertThat(testAdvertisementStatus.getCodeRef()).isEqualTo(DEFAULT_CODE_REF);
        assertThat(testAdvertisementStatus.getLabel()).isEqualTo(DEFAULT_LABEL);
    }

    @Test
    @Transactional
    public void createAdvertisementStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = advertisementStatusRepository.findAll().size();

        // Create the AdvertisementStatus with an existing ID
        advertisementStatus.setId(1L);
        AdvertisementStatusDTO advertisementStatusDTO = advertisementStatusMapper.toDto(advertisementStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdvertisementStatusMockMvc
            .perform(
                post("/api/advertisement-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementStatusDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AdvertisementStatus in the database
        List<AdvertisementStatus> advertisementStatusList = advertisementStatusRepository.findAll();
        assertThat(advertisementStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeRefIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementStatusRepository.findAll().size();
        // set the field null
        advertisementStatus.setCodeRef(null);

        // Create the AdvertisementStatus, which fails.
        AdvertisementStatusDTO advertisementStatusDTO = advertisementStatusMapper.toDto(advertisementStatus);

        restAdvertisementStatusMockMvc
            .perform(
                post("/api/advertisement-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementStatusDTO))
            )
            .andExpect(status().isBadRequest());

        List<AdvertisementStatus> advertisementStatusList = advertisementStatusRepository.findAll();
        assertThat(advertisementStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementStatusRepository.findAll().size();
        // set the field null
        advertisementStatus.setLabel(null);

        // Create the AdvertisementStatus, which fails.
        AdvertisementStatusDTO advertisementStatusDTO = advertisementStatusMapper.toDto(advertisementStatus);

        restAdvertisementStatusMockMvc
            .perform(
                post("/api/advertisement-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementStatusDTO))
            )
            .andExpect(status().isBadRequest());

        List<AdvertisementStatus> advertisementStatusList = advertisementStatusRepository.findAll();
        assertThat(advertisementStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatuses() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList
        restAdvertisementStatusMockMvc
            .perform(get("/api/advertisement-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(advertisementStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeRef").value(hasItem(DEFAULT_CODE_REF)))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)));
    }

    @Test
    @Transactional
    public void getAdvertisementStatus() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get the advertisementStatus
        restAdvertisementStatusMockMvc
            .perform(get("/api/advertisement-statuses/{id}", advertisementStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(advertisementStatus.getId().intValue()))
            .andExpect(jsonPath("$.codeRef").value(DEFAULT_CODE_REF))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL));
    }

    @Test
    @Transactional
    public void getAdvertisementStatusesByIdFiltering() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        Long id = advertisementStatus.getId();

        defaultAdvertisementStatusShouldBeFound("id.equals=" + id);
        defaultAdvertisementStatusShouldNotBeFound("id.notEquals=" + id);

        defaultAdvertisementStatusShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAdvertisementStatusShouldNotBeFound("id.greaterThan=" + id);

        defaultAdvertisementStatusShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAdvertisementStatusShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByCodeRefIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where codeRef equals to DEFAULT_CODE_REF
        defaultAdvertisementStatusShouldBeFound("codeRef.equals=" + DEFAULT_CODE_REF);

        // Get all the advertisementStatusList where codeRef equals to UPDATED_CODE_REF
        defaultAdvertisementStatusShouldNotBeFound("codeRef.equals=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByCodeRefIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where codeRef not equals to DEFAULT_CODE_REF
        defaultAdvertisementStatusShouldNotBeFound("codeRef.notEquals=" + DEFAULT_CODE_REF);

        // Get all the advertisementStatusList where codeRef not equals to UPDATED_CODE_REF
        defaultAdvertisementStatusShouldBeFound("codeRef.notEquals=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByCodeRefIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where codeRef in DEFAULT_CODE_REF or UPDATED_CODE_REF
        defaultAdvertisementStatusShouldBeFound("codeRef.in=" + DEFAULT_CODE_REF + "," + UPDATED_CODE_REF);

        // Get all the advertisementStatusList where codeRef equals to UPDATED_CODE_REF
        defaultAdvertisementStatusShouldNotBeFound("codeRef.in=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByCodeRefIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where codeRef is not null
        defaultAdvertisementStatusShouldBeFound("codeRef.specified=true");

        // Get all the advertisementStatusList where codeRef is null
        defaultAdvertisementStatusShouldNotBeFound("codeRef.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByCodeRefContainsSomething() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where codeRef contains DEFAULT_CODE_REF
        defaultAdvertisementStatusShouldBeFound("codeRef.contains=" + DEFAULT_CODE_REF);

        // Get all the advertisementStatusList where codeRef contains UPDATED_CODE_REF
        defaultAdvertisementStatusShouldNotBeFound("codeRef.contains=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByCodeRefNotContainsSomething() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where codeRef does not contain DEFAULT_CODE_REF
        defaultAdvertisementStatusShouldNotBeFound("codeRef.doesNotContain=" + DEFAULT_CODE_REF);

        // Get all the advertisementStatusList where codeRef does not contain UPDATED_CODE_REF
        defaultAdvertisementStatusShouldBeFound("codeRef.doesNotContain=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where label equals to DEFAULT_LABEL
        defaultAdvertisementStatusShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the advertisementStatusList where label equals to UPDATED_LABEL
        defaultAdvertisementStatusShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where label not equals to DEFAULT_LABEL
        defaultAdvertisementStatusShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the advertisementStatusList where label not equals to UPDATED_LABEL
        defaultAdvertisementStatusShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultAdvertisementStatusShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the advertisementStatusList where label equals to UPDATED_LABEL
        defaultAdvertisementStatusShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where label is not null
        defaultAdvertisementStatusShouldBeFound("label.specified=true");

        // Get all the advertisementStatusList where label is null
        defaultAdvertisementStatusShouldNotBeFound("label.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByLabelContainsSomething() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where label contains DEFAULT_LABEL
        defaultAdvertisementStatusShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the advertisementStatusList where label contains UPDATED_LABEL
        defaultAdvertisementStatusShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllAdvertisementStatusesByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        // Get all the advertisementStatusList where label does not contain DEFAULT_LABEL
        defaultAdvertisementStatusShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the advertisementStatusList where label does not contain UPDATED_LABEL
        defaultAdvertisementStatusShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAdvertisementStatusShouldBeFound(String filter) throws Exception {
        restAdvertisementStatusMockMvc
            .perform(get("/api/advertisement-statuses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(advertisementStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeRef").value(hasItem(DEFAULT_CODE_REF)))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)));

        // Check, that the count call also returns 1
        restAdvertisementStatusMockMvc
            .perform(get("/api/advertisement-statuses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAdvertisementStatusShouldNotBeFound(String filter) throws Exception {
        restAdvertisementStatusMockMvc
            .perform(get("/api/advertisement-statuses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAdvertisementStatusMockMvc
            .perform(get("/api/advertisement-statuses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingAdvertisementStatus() throws Exception {
        // Get the advertisementStatus
        restAdvertisementStatusMockMvc.perform(get("/api/advertisement-statuses/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdvertisementStatus() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        int databaseSizeBeforeUpdate = advertisementStatusRepository.findAll().size();

        // Update the advertisementStatus
        AdvertisementStatus updatedAdvertisementStatus = advertisementStatusRepository.findById(advertisementStatus.getId()).get();
        // Disconnect from session so that the updates on updatedAdvertisementStatus are not directly saved in db
        em.detach(updatedAdvertisementStatus);
        updatedAdvertisementStatus.codeRef(UPDATED_CODE_REF).label(UPDATED_LABEL);
        AdvertisementStatusDTO advertisementStatusDTO = advertisementStatusMapper.toDto(updatedAdvertisementStatus);

        restAdvertisementStatusMockMvc
            .perform(
                put("/api/advertisement-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementStatusDTO))
            )
            .andExpect(status().isOk());

        // Validate the AdvertisementStatus in the database
        List<AdvertisementStatus> advertisementStatusList = advertisementStatusRepository.findAll();
        assertThat(advertisementStatusList).hasSize(databaseSizeBeforeUpdate);
        AdvertisementStatus testAdvertisementStatus = advertisementStatusList.get(advertisementStatusList.size() - 1);
        assertThat(testAdvertisementStatus.getCodeRef()).isEqualTo(UPDATED_CODE_REF);
        assertThat(testAdvertisementStatus.getLabel()).isEqualTo(UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void updateNonExistingAdvertisementStatus() throws Exception {
        int databaseSizeBeforeUpdate = advertisementStatusRepository.findAll().size();

        // Create the AdvertisementStatus
        AdvertisementStatusDTO advertisementStatusDTO = advertisementStatusMapper.toDto(advertisementStatus);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdvertisementStatusMockMvc
            .perform(
                put("/api/advertisement-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementStatusDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AdvertisementStatus in the database
        List<AdvertisementStatus> advertisementStatusList = advertisementStatusRepository.findAll();
        assertThat(advertisementStatusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAdvertisementStatus() throws Exception {
        // Initialize the database
        advertisementStatusRepository.saveAndFlush(advertisementStatus);

        int databaseSizeBeforeDelete = advertisementStatusRepository.findAll().size();

        // Delete the advertisementStatus
        restAdvertisementStatusMockMvc
            .perform(delete("/api/advertisement-statuses/{id}", advertisementStatus.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AdvertisementStatus> advertisementStatusList = advertisementStatusRepository.findAll();
        assertThat(advertisementStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
