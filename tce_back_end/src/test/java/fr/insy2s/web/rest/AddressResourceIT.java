package fr.insy2s.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.Address;
import fr.insy2s.domain.Country;
import fr.insy2s.repository.AddressRepository;
import fr.insy2s.service.AddressQueryService;
import fr.insy2s.service.AddressService;
import fr.insy2s.service.dto.AddressCriteria;
import fr.insy2s.service.dto.AddressDTO;
import fr.insy2s.service.mapper.AddressMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AddressResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AddressResourceIT {
    private static final String DEFAULT_STREET_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_STREET_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_COMPLEMENT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_COMPLEMENT_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_STREET_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STREET_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ZIP_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ZIP_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TOWN = "AAAAAAAAAA";
    private static final String UPDATED_TOWN = "BBBBBBBBBB";

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private AddressService addressService;

    @Autowired
    private AddressQueryService addressQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAddressMockMvc;

    private Address address;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Address createEntity(EntityManager em) {
        Address address = new Address()
            .streetNumber(DEFAULT_STREET_NUMBER)
            .complementAddress(DEFAULT_COMPLEMENT_ADDRESS)
            .streetName(DEFAULT_STREET_NAME)
            .zipCode(DEFAULT_ZIP_CODE)
            .town(DEFAULT_TOWN);
        // Add required entity
        Country country;
        if (TestUtil.findAll(em, Country.class).isEmpty()) {
            country = CountryResourceIT.createEntity(em);
            em.persist(country);
            em.flush();
        } else {
            country = TestUtil.findAll(em, Country.class).get(0);
        }
        address.setCountry(country);
        return address;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Address createUpdatedEntity(EntityManager em) {
        Address address = new Address()
            .streetNumber(UPDATED_STREET_NUMBER)
            .complementAddress(UPDATED_COMPLEMENT_ADDRESS)
            .streetName(UPDATED_STREET_NAME)
            .zipCode(UPDATED_ZIP_CODE)
            .town(UPDATED_TOWN);
        // Add required entity
        Country country;
        if (TestUtil.findAll(em, Country.class).isEmpty()) {
            country = CountryResourceIT.createUpdatedEntity(em);
            em.persist(country);
            em.flush();
        } else {
            country = TestUtil.findAll(em, Country.class).get(0);
        }
        address.setCountry(country);
        return address;
    }

    @BeforeEach
    public void initTest() {
        address = createEntity(em);
    }

    @Test
    @Transactional
    public void createAddress() throws Exception {
        int databaseSizeBeforeCreate = addressRepository.findAll().size();
        // Create the Address
        AddressDTO addressDTO = addressMapper.toDto(address);
        restAddressMockMvc
            .perform(post("/api/addresses").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isCreated());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeCreate + 1);
        Address testAddress = addressList.get(addressList.size() - 1);
        assertThat(testAddress.getStreetNumber()).isEqualTo(DEFAULT_STREET_NUMBER);
        assertThat(testAddress.getComplementAddress()).isEqualTo(DEFAULT_COMPLEMENT_ADDRESS);
        assertThat(testAddress.getStreetName()).isEqualTo(DEFAULT_STREET_NAME);
        assertThat(testAddress.getZipCode()).isEqualTo(DEFAULT_ZIP_CODE);
        assertThat(testAddress.getTown()).isEqualTo(DEFAULT_TOWN);
    }

    @Test
    @Transactional
    public void createAddressWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = addressRepository.findAll().size();

        // Create the Address with an existing ID
        address.setId(1L);
        AddressDTO addressDTO = addressMapper.toDto(address);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAddressMockMvc
            .perform(post("/api/addresses").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStreetNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressRepository.findAll().size();
        // set the field null
        address.setStreetNumber(null);

        // Create the Address, which fails.
        AddressDTO addressDTO = addressMapper.toDto(address);

        restAddressMockMvc
            .perform(post("/api/addresses").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStreetNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressRepository.findAll().size();
        // set the field null
        address.setStreetName(null);

        // Create the Address, which fails.
        AddressDTO addressDTO = addressMapper.toDto(address);

        restAddressMockMvc
            .perform(post("/api/addresses").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkZipCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressRepository.findAll().size();
        // set the field null
        address.setZipCode(null);

        // Create the Address, which fails.
        AddressDTO addressDTO = addressMapper.toDto(address);

        restAddressMockMvc
            .perform(post("/api/addresses").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTownIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressRepository.findAll().size();
        // set the field null
        address.setTown(null);

        // Create the Address, which fails.
        AddressDTO addressDTO = addressMapper.toDto(address);

        restAddressMockMvc
            .perform(post("/api/addresses").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAddresses() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList
        restAddressMockMvc
            .perform(get("/api/addresses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(address.getId().intValue())))
            .andExpect(jsonPath("$.[*].streetNumber").value(hasItem(DEFAULT_STREET_NUMBER)))
            .andExpect(jsonPath("$.[*].complementAddress").value(hasItem(DEFAULT_COMPLEMENT_ADDRESS)))
            .andExpect(jsonPath("$.[*].streetName").value(hasItem(DEFAULT_STREET_NAME)))
            .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE)))
            .andExpect(jsonPath("$.[*].town").value(hasItem(DEFAULT_TOWN)));
    }

    @Test
    @Transactional
    public void getAddress() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get the address
        restAddressMockMvc
            .perform(get("/api/addresses/{id}", address.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(address.getId().intValue()))
            .andExpect(jsonPath("$.streetNumber").value(DEFAULT_STREET_NUMBER))
            .andExpect(jsonPath("$.complementAddress").value(DEFAULT_COMPLEMENT_ADDRESS))
            .andExpect(jsonPath("$.streetName").value(DEFAULT_STREET_NAME))
            .andExpect(jsonPath("$.zipCode").value(DEFAULT_ZIP_CODE))
            .andExpect(jsonPath("$.town").value(DEFAULT_TOWN));
    }

    @Test
    @Transactional
    public void getAddressesByIdFiltering() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        Long id = address.getId();

        defaultAddressShouldBeFound("id.equals=" + id);
        defaultAddressShouldNotBeFound("id.notEquals=" + id);

        defaultAddressShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAddressShouldNotBeFound("id.greaterThan=" + id);

        defaultAddressShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAddressShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetNumber equals to DEFAULT_STREET_NUMBER
        defaultAddressShouldBeFound("streetNumber.equals=" + DEFAULT_STREET_NUMBER);

        // Get all the addressList where streetNumber equals to UPDATED_STREET_NUMBER
        defaultAddressShouldNotBeFound("streetNumber.equals=" + UPDATED_STREET_NUMBER);
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetNumber not equals to DEFAULT_STREET_NUMBER
        defaultAddressShouldNotBeFound("streetNumber.notEquals=" + DEFAULT_STREET_NUMBER);

        // Get all the addressList where streetNumber not equals to UPDATED_STREET_NUMBER
        defaultAddressShouldBeFound("streetNumber.notEquals=" + UPDATED_STREET_NUMBER);
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNumberIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetNumber in DEFAULT_STREET_NUMBER or UPDATED_STREET_NUMBER
        defaultAddressShouldBeFound("streetNumber.in=" + DEFAULT_STREET_NUMBER + "," + UPDATED_STREET_NUMBER);

        // Get all the addressList where streetNumber equals to UPDATED_STREET_NUMBER
        defaultAddressShouldNotBeFound("streetNumber.in=" + UPDATED_STREET_NUMBER);
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetNumber is not null
        defaultAddressShouldBeFound("streetNumber.specified=true");

        // Get all the addressList where streetNumber is null
        defaultAddressShouldNotBeFound("streetNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNumberContainsSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetNumber contains DEFAULT_STREET_NUMBER
        defaultAddressShouldBeFound("streetNumber.contains=" + DEFAULT_STREET_NUMBER);

        // Get all the addressList where streetNumber contains UPDATED_STREET_NUMBER
        defaultAddressShouldNotBeFound("streetNumber.contains=" + UPDATED_STREET_NUMBER);
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNumberNotContainsSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetNumber does not contain DEFAULT_STREET_NUMBER
        defaultAddressShouldNotBeFound("streetNumber.doesNotContain=" + DEFAULT_STREET_NUMBER);

        // Get all the addressList where streetNumber does not contain UPDATED_STREET_NUMBER
        defaultAddressShouldBeFound("streetNumber.doesNotContain=" + UPDATED_STREET_NUMBER);
    }

    @Test
    @Transactional
    public void getAllAddressesByComplementAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where complementAddress equals to DEFAULT_COMPLEMENT_ADDRESS
        defaultAddressShouldBeFound("complementAddress.equals=" + DEFAULT_COMPLEMENT_ADDRESS);

        // Get all the addressList where complementAddress equals to UPDATED_COMPLEMENT_ADDRESS
        defaultAddressShouldNotBeFound("complementAddress.equals=" + UPDATED_COMPLEMENT_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllAddressesByComplementAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where complementAddress not equals to DEFAULT_COMPLEMENT_ADDRESS
        defaultAddressShouldNotBeFound("complementAddress.notEquals=" + DEFAULT_COMPLEMENT_ADDRESS);

        // Get all the addressList where complementAddress not equals to UPDATED_COMPLEMENT_ADDRESS
        defaultAddressShouldBeFound("complementAddress.notEquals=" + UPDATED_COMPLEMENT_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllAddressesByComplementAddressIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where complementAddress in DEFAULT_COMPLEMENT_ADDRESS or UPDATED_COMPLEMENT_ADDRESS
        defaultAddressShouldBeFound("complementAddress.in=" + DEFAULT_COMPLEMENT_ADDRESS + "," + UPDATED_COMPLEMENT_ADDRESS);

        // Get all the addressList where complementAddress equals to UPDATED_COMPLEMENT_ADDRESS
        defaultAddressShouldNotBeFound("complementAddress.in=" + UPDATED_COMPLEMENT_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllAddressesByComplementAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where complementAddress is not null
        defaultAddressShouldBeFound("complementAddress.specified=true");

        // Get all the addressList where complementAddress is null
        defaultAddressShouldNotBeFound("complementAddress.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByComplementAddressContainsSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where complementAddress contains DEFAULT_COMPLEMENT_ADDRESS
        defaultAddressShouldBeFound("complementAddress.contains=" + DEFAULT_COMPLEMENT_ADDRESS);

        // Get all the addressList where complementAddress contains UPDATED_COMPLEMENT_ADDRESS
        defaultAddressShouldNotBeFound("complementAddress.contains=" + UPDATED_COMPLEMENT_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllAddressesByComplementAddressNotContainsSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where complementAddress does not contain DEFAULT_COMPLEMENT_ADDRESS
        defaultAddressShouldNotBeFound("complementAddress.doesNotContain=" + DEFAULT_COMPLEMENT_ADDRESS);

        // Get all the addressList where complementAddress does not contain UPDATED_COMPLEMENT_ADDRESS
        defaultAddressShouldBeFound("complementAddress.doesNotContain=" + UPDATED_COMPLEMENT_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNameIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetName equals to DEFAULT_STREET_NAME
        defaultAddressShouldBeFound("streetName.equals=" + DEFAULT_STREET_NAME);

        // Get all the addressList where streetName equals to UPDATED_STREET_NAME
        defaultAddressShouldNotBeFound("streetName.equals=" + UPDATED_STREET_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetName not equals to DEFAULT_STREET_NAME
        defaultAddressShouldNotBeFound("streetName.notEquals=" + DEFAULT_STREET_NAME);

        // Get all the addressList where streetName not equals to UPDATED_STREET_NAME
        defaultAddressShouldBeFound("streetName.notEquals=" + UPDATED_STREET_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNameIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetName in DEFAULT_STREET_NAME or UPDATED_STREET_NAME
        defaultAddressShouldBeFound("streetName.in=" + DEFAULT_STREET_NAME + "," + UPDATED_STREET_NAME);

        // Get all the addressList where streetName equals to UPDATED_STREET_NAME
        defaultAddressShouldNotBeFound("streetName.in=" + UPDATED_STREET_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetName is not null
        defaultAddressShouldBeFound("streetName.specified=true");

        // Get all the addressList where streetName is null
        defaultAddressShouldNotBeFound("streetName.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNameContainsSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetName contains DEFAULT_STREET_NAME
        defaultAddressShouldBeFound("streetName.contains=" + DEFAULT_STREET_NAME);

        // Get all the addressList where streetName contains UPDATED_STREET_NAME
        defaultAddressShouldNotBeFound("streetName.contains=" + UPDATED_STREET_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressesByStreetNameNotContainsSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where streetName does not contain DEFAULT_STREET_NAME
        defaultAddressShouldNotBeFound("streetName.doesNotContain=" + DEFAULT_STREET_NAME);

        // Get all the addressList where streetName does not contain UPDATED_STREET_NAME
        defaultAddressShouldBeFound("streetName.doesNotContain=" + UPDATED_STREET_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressesByZipCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where zipCode equals to DEFAULT_ZIP_CODE
        defaultAddressShouldBeFound("zipCode.equals=" + DEFAULT_ZIP_CODE);

        // Get all the addressList where zipCode equals to UPDATED_ZIP_CODE
        defaultAddressShouldNotBeFound("zipCode.equals=" + UPDATED_ZIP_CODE);
    }

    @Test
    @Transactional
    public void getAllAddressesByZipCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where zipCode not equals to DEFAULT_ZIP_CODE
        defaultAddressShouldNotBeFound("zipCode.notEquals=" + DEFAULT_ZIP_CODE);

        // Get all the addressList where zipCode not equals to UPDATED_ZIP_CODE
        defaultAddressShouldBeFound("zipCode.notEquals=" + UPDATED_ZIP_CODE);
    }

    @Test
    @Transactional
    public void getAllAddressesByZipCodeIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where zipCode in DEFAULT_ZIP_CODE or UPDATED_ZIP_CODE
        defaultAddressShouldBeFound("zipCode.in=" + DEFAULT_ZIP_CODE + "," + UPDATED_ZIP_CODE);

        // Get all the addressList where zipCode equals to UPDATED_ZIP_CODE
        defaultAddressShouldNotBeFound("zipCode.in=" + UPDATED_ZIP_CODE);
    }

    @Test
    @Transactional
    public void getAllAddressesByZipCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where zipCode is not null
        defaultAddressShouldBeFound("zipCode.specified=true");

        // Get all the addressList where zipCode is null
        defaultAddressShouldNotBeFound("zipCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByZipCodeContainsSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where zipCode contains DEFAULT_ZIP_CODE
        defaultAddressShouldBeFound("zipCode.contains=" + DEFAULT_ZIP_CODE);

        // Get all the addressList where zipCode contains UPDATED_ZIP_CODE
        defaultAddressShouldNotBeFound("zipCode.contains=" + UPDATED_ZIP_CODE);
    }

    @Test
    @Transactional
    public void getAllAddressesByZipCodeNotContainsSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where zipCode does not contain DEFAULT_ZIP_CODE
        defaultAddressShouldNotBeFound("zipCode.doesNotContain=" + DEFAULT_ZIP_CODE);

        // Get all the addressList where zipCode does not contain UPDATED_ZIP_CODE
        defaultAddressShouldBeFound("zipCode.doesNotContain=" + UPDATED_ZIP_CODE);
    }

    @Test
    @Transactional
    public void getAllAddressesByTownIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where town equals to DEFAULT_TOWN
        defaultAddressShouldBeFound("town.equals=" + DEFAULT_TOWN);

        // Get all the addressList where town equals to UPDATED_TOWN
        defaultAddressShouldNotBeFound("town.equals=" + UPDATED_TOWN);
    }

    @Test
    @Transactional
    public void getAllAddressesByTownIsNotEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where town not equals to DEFAULT_TOWN
        defaultAddressShouldNotBeFound("town.notEquals=" + DEFAULT_TOWN);

        // Get all the addressList where town not equals to UPDATED_TOWN
        defaultAddressShouldBeFound("town.notEquals=" + UPDATED_TOWN);
    }

    @Test
    @Transactional
    public void getAllAddressesByTownIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where town in DEFAULT_TOWN or UPDATED_TOWN
        defaultAddressShouldBeFound("town.in=" + DEFAULT_TOWN + "," + UPDATED_TOWN);

        // Get all the addressList where town equals to UPDATED_TOWN
        defaultAddressShouldNotBeFound("town.in=" + UPDATED_TOWN);
    }

    @Test
    @Transactional
    public void getAllAddressesByTownIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where town is not null
        defaultAddressShouldBeFound("town.specified=true");

        // Get all the addressList where town is null
        defaultAddressShouldNotBeFound("town.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByTownContainsSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where town contains DEFAULT_TOWN
        defaultAddressShouldBeFound("town.contains=" + DEFAULT_TOWN);

        // Get all the addressList where town contains UPDATED_TOWN
        defaultAddressShouldNotBeFound("town.contains=" + UPDATED_TOWN);
    }

    @Test
    @Transactional
    public void getAllAddressesByTownNotContainsSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where town does not contain DEFAULT_TOWN
        defaultAddressShouldNotBeFound("town.doesNotContain=" + DEFAULT_TOWN);

        // Get all the addressList where town does not contain UPDATED_TOWN
        defaultAddressShouldBeFound("town.doesNotContain=" + UPDATED_TOWN);
    }

    @Test
    @Transactional
    public void getAllAddressesByCountryIsEqualToSomething() throws Exception {
        // Get already existing entity
        Country country = address.getCountry();
        addressRepository.saveAndFlush(address);
        Long countryId = country.getId();

        // Get all the addressList where country equals to countryId
        defaultAddressShouldBeFound("countryId.equals=" + countryId);

        // Get all the addressList where country equals to countryId + 1
        defaultAddressShouldNotBeFound("countryId.equals=" + (countryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAddressShouldBeFound(String filter) throws Exception {
        restAddressMockMvc
            .perform(get("/api/addresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(address.getId().intValue())))
            .andExpect(jsonPath("$.[*].streetNumber").value(hasItem(DEFAULT_STREET_NUMBER)))
            .andExpect(jsonPath("$.[*].complementAddress").value(hasItem(DEFAULT_COMPLEMENT_ADDRESS)))
            .andExpect(jsonPath("$.[*].streetName").value(hasItem(DEFAULT_STREET_NAME)))
            .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE)))
            .andExpect(jsonPath("$.[*].town").value(hasItem(DEFAULT_TOWN)));

        // Check, that the count call also returns 1
        restAddressMockMvc
            .perform(get("/api/addresses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAddressShouldNotBeFound(String filter) throws Exception {
        restAddressMockMvc
            .perform(get("/api/addresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAddressMockMvc
            .perform(get("/api/addresses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingAddress() throws Exception {
        // Get the address
        restAddressMockMvc.perform(get("/api/addresses/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAddress() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        int databaseSizeBeforeUpdate = addressRepository.findAll().size();

        // Update the address
        Address updatedAddress = addressRepository.findById(address.getId()).get();
        // Disconnect from session so that the updates on updatedAddress are not directly saved in db
        em.detach(updatedAddress);
        updatedAddress
            .streetNumber(UPDATED_STREET_NUMBER)
            .complementAddress(UPDATED_COMPLEMENT_ADDRESS)
            .streetName(UPDATED_STREET_NAME)
            .zipCode(UPDATED_ZIP_CODE)
            .town(UPDATED_TOWN);
        AddressDTO addressDTO = addressMapper.toDto(updatedAddress);

        restAddressMockMvc
            .perform(put("/api/addresses").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isOk());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeUpdate);
        Address testAddress = addressList.get(addressList.size() - 1);
        assertThat(testAddress.getStreetNumber()).isEqualTo(UPDATED_STREET_NUMBER);
        assertThat(testAddress.getComplementAddress()).isEqualTo(UPDATED_COMPLEMENT_ADDRESS);
        assertThat(testAddress.getStreetName()).isEqualTo(UPDATED_STREET_NAME);
        assertThat(testAddress.getZipCode()).isEqualTo(UPDATED_ZIP_CODE);
        assertThat(testAddress.getTown()).isEqualTo(UPDATED_TOWN);
    }

    @Test
    @Transactional
    public void updateNonExistingAddress() throws Exception {
        int databaseSizeBeforeUpdate = addressRepository.findAll().size();

        // Create the Address
        AddressDTO addressDTO = addressMapper.toDto(address);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAddressMockMvc
            .perform(put("/api/addresses").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAddress() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        int databaseSizeBeforeDelete = addressRepository.findAll().size();

        // Delete the address
        restAddressMockMvc
            .perform(delete("/api/addresses/{id}", address.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
