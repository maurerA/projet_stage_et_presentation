package fr.insy2s.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.Company;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.CompanyRepository;
import fr.insy2s.service.CompanyQueryService;
import fr.insy2s.service.CompanyService;
import fr.insy2s.service.dto.CompanyCriteria;
import fr.insy2s.service.dto.CompanyDTO;
import fr.insy2s.service.mapper.CompanyMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CompanyResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompanyResourceIT {
    private static final String DEFAULT_NAME_COMPANY = "AAAAAAAAAA";
    private static final String UPDATED_NAME_COMPANY = "BBBBBBBBBB";

    private static final String DEFAULT_NUMBER_SIRET_COMPANY = "AAAAAAAAAA";
    private static final String UPDATED_NUMBER_SIRET_COMPANY = "BBBBBBBBBB";

    private static final String DEFAULT_NUMBER_SIREN = "AAAAAAAAAA";
    private static final String UPDATED_NUMBER_SIREN = "BBBBBBBBBB";

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyMapper companyMapper;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyQueryService companyQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanyMockMvc;

    private Company company;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Company createEntity(EntityManager em) {
        Company company = new Company()
            .nameCompany(DEFAULT_NAME_COMPANY)
            .numberSiretCompany(DEFAULT_NUMBER_SIRET_COMPANY)
            .numberSiren(DEFAULT_NUMBER_SIREN);
        return company;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Company createUpdatedEntity(EntityManager em) {
        Company company = new Company()
            .nameCompany(UPDATED_NAME_COMPANY)
            .numberSiretCompany(UPDATED_NUMBER_SIRET_COMPANY)
            .numberSiren(UPDATED_NUMBER_SIREN);
        return company;
    }

    @BeforeEach
    public void initTest() {
        company = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompany() throws Exception {
        int databaseSizeBeforeCreate = companyRepository.findAll().size();
        // Create the Company
        CompanyDTO companyDTO = companyMapper.toDto(company);
        restCompanyMockMvc
            .perform(post("/api/companies").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isCreated());

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeCreate + 1);
        Company testCompany = companyList.get(companyList.size() - 1);
        assertThat(testCompany.getNameCompany()).isEqualTo(DEFAULT_NAME_COMPANY);
        assertThat(testCompany.getNumberSiretCompany()).isEqualTo(DEFAULT_NUMBER_SIRET_COMPANY);
        assertThat(testCompany.getNumberSiren()).isEqualTo(DEFAULT_NUMBER_SIREN);
    }

    @Test
    @Transactional
    public void createCompanyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companyRepository.findAll().size();

        // Create the Company with an existing ID
        company.setId(1L);
        CompanyDTO companyDTO = companyMapper.toDto(company);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyMockMvc
            .perform(post("/api/companies").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameCompanyIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().size();
        // set the field null
        company.setNameCompany(null);

        // Create the Company, which fails.
        CompanyDTO companyDTO = companyMapper.toDto(company);

        restCompanyMockMvc
            .perform(post("/api/companies").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumberSiretCompanyIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().size();
        // set the field null
        company.setNumberSiretCompany(null);

        // Create the Company, which fails.
        CompanyDTO companyDTO = companyMapper.toDto(company);

        restCompanyMockMvc
            .perform(post("/api/companies").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumberSirenIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().size();
        // set the field null
        company.setNumberSiren(null);

        // Create the Company, which fails.
        CompanyDTO companyDTO = companyMapper.toDto(company);

        restCompanyMockMvc
            .perform(post("/api/companies").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanies() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList
        restCompanyMockMvc
            .perform(get("/api/companies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(company.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameCompany").value(hasItem(DEFAULT_NAME_COMPANY)))
            .andExpect(jsonPath("$.[*].numberSiretCompany").value(hasItem(DEFAULT_NUMBER_SIRET_COMPANY)))
            .andExpect(jsonPath("$.[*].numberSiren").value(hasItem(DEFAULT_NUMBER_SIREN)));
    }

    @Test
    @Transactional
    public void getCompany() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get the company
        restCompanyMockMvc
            .perform(get("/api/companies/{id}", company.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(company.getId().intValue()))
            .andExpect(jsonPath("$.nameCompany").value(DEFAULT_NAME_COMPANY))
            .andExpect(jsonPath("$.numberSiretCompany").value(DEFAULT_NUMBER_SIRET_COMPANY))
            .andExpect(jsonPath("$.numberSiren").value(DEFAULT_NUMBER_SIREN));
    }

    @Test
    @Transactional
    public void getCompaniesByIdFiltering() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        Long id = company.getId();

        defaultCompanyShouldBeFound("id.equals=" + id);
        defaultCompanyShouldNotBeFound("id.notEquals=" + id);

        defaultCompanyShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanyShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanyShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanyShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameCompanyIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where nameCompany equals to DEFAULT_NAME_COMPANY
        defaultCompanyShouldBeFound("nameCompany.equals=" + DEFAULT_NAME_COMPANY);

        // Get all the companyList where nameCompany equals to UPDATED_NAME_COMPANY
        defaultCompanyShouldNotBeFound("nameCompany.equals=" + UPDATED_NAME_COMPANY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameCompanyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where nameCompany not equals to DEFAULT_NAME_COMPANY
        defaultCompanyShouldNotBeFound("nameCompany.notEquals=" + DEFAULT_NAME_COMPANY);

        // Get all the companyList where nameCompany not equals to UPDATED_NAME_COMPANY
        defaultCompanyShouldBeFound("nameCompany.notEquals=" + UPDATED_NAME_COMPANY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameCompanyIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where nameCompany in DEFAULT_NAME_COMPANY or UPDATED_NAME_COMPANY
        defaultCompanyShouldBeFound("nameCompany.in=" + DEFAULT_NAME_COMPANY + "," + UPDATED_NAME_COMPANY);

        // Get all the companyList where nameCompany equals to UPDATED_NAME_COMPANY
        defaultCompanyShouldNotBeFound("nameCompany.in=" + UPDATED_NAME_COMPANY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameCompanyIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where nameCompany is not null
        defaultCompanyShouldBeFound("nameCompany.specified=true");

        // Get all the companyList where nameCompany is null
        defaultCompanyShouldNotBeFound("nameCompany.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameCompanyContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where nameCompany contains DEFAULT_NAME_COMPANY
        defaultCompanyShouldBeFound("nameCompany.contains=" + DEFAULT_NAME_COMPANY);

        // Get all the companyList where nameCompany contains UPDATED_NAME_COMPANY
        defaultCompanyShouldNotBeFound("nameCompany.contains=" + UPDATED_NAME_COMPANY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameCompanyNotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where nameCompany does not contain DEFAULT_NAME_COMPANY
        defaultCompanyShouldNotBeFound("nameCompany.doesNotContain=" + DEFAULT_NAME_COMPANY);

        // Get all the companyList where nameCompany does not contain UPDATED_NAME_COMPANY
        defaultCompanyShouldBeFound("nameCompany.doesNotContain=" + UPDATED_NAME_COMPANY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSiretCompanyIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiretCompany equals to DEFAULT_NUMBER_SIRET_COMPANY
        defaultCompanyShouldBeFound("numberSiretCompany.equals=" + DEFAULT_NUMBER_SIRET_COMPANY);

        // Get all the companyList where numberSiretCompany equals to UPDATED_NUMBER_SIRET_COMPANY
        defaultCompanyShouldNotBeFound("numberSiretCompany.equals=" + UPDATED_NUMBER_SIRET_COMPANY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSiretCompanyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiretCompany not equals to DEFAULT_NUMBER_SIRET_COMPANY
        defaultCompanyShouldNotBeFound("numberSiretCompany.notEquals=" + DEFAULT_NUMBER_SIRET_COMPANY);

        // Get all the companyList where numberSiretCompany not equals to UPDATED_NUMBER_SIRET_COMPANY
        defaultCompanyShouldBeFound("numberSiretCompany.notEquals=" + UPDATED_NUMBER_SIRET_COMPANY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSiretCompanyIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiretCompany in DEFAULT_NUMBER_SIRET_COMPANY or UPDATED_NUMBER_SIRET_COMPANY
        defaultCompanyShouldBeFound("numberSiretCompany.in=" + DEFAULT_NUMBER_SIRET_COMPANY + "," + UPDATED_NUMBER_SIRET_COMPANY);

        // Get all the companyList where numberSiretCompany equals to UPDATED_NUMBER_SIRET_COMPANY
        defaultCompanyShouldNotBeFound("numberSiretCompany.in=" + UPDATED_NUMBER_SIRET_COMPANY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSiretCompanyIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiretCompany is not null
        defaultCompanyShouldBeFound("numberSiretCompany.specified=true");

        // Get all the companyList where numberSiretCompany is null
        defaultCompanyShouldNotBeFound("numberSiretCompany.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSiretCompanyContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiretCompany contains DEFAULT_NUMBER_SIRET_COMPANY
        defaultCompanyShouldBeFound("numberSiretCompany.contains=" + DEFAULT_NUMBER_SIRET_COMPANY);

        // Get all the companyList where numberSiretCompany contains UPDATED_NUMBER_SIRET_COMPANY
        defaultCompanyShouldNotBeFound("numberSiretCompany.contains=" + UPDATED_NUMBER_SIRET_COMPANY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSiretCompanyNotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiretCompany does not contain DEFAULT_NUMBER_SIRET_COMPANY
        defaultCompanyShouldNotBeFound("numberSiretCompany.doesNotContain=" + DEFAULT_NUMBER_SIRET_COMPANY);

        // Get all the companyList where numberSiretCompany does not contain UPDATED_NUMBER_SIRET_COMPANY
        defaultCompanyShouldBeFound("numberSiretCompany.doesNotContain=" + UPDATED_NUMBER_SIRET_COMPANY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSirenIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiren equals to DEFAULT_NUMBER_SIREN
        defaultCompanyShouldBeFound("numberSiren.equals=" + DEFAULT_NUMBER_SIREN);

        // Get all the companyList where numberSiren equals to UPDATED_NUMBER_SIREN
        defaultCompanyShouldNotBeFound("numberSiren.equals=" + UPDATED_NUMBER_SIREN);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSirenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiren not equals to DEFAULT_NUMBER_SIREN
        defaultCompanyShouldNotBeFound("numberSiren.notEquals=" + DEFAULT_NUMBER_SIREN);

        // Get all the companyList where numberSiren not equals to UPDATED_NUMBER_SIREN
        defaultCompanyShouldBeFound("numberSiren.notEquals=" + UPDATED_NUMBER_SIREN);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSirenIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiren in DEFAULT_NUMBER_SIREN or UPDATED_NUMBER_SIREN
        defaultCompanyShouldBeFound("numberSiren.in=" + DEFAULT_NUMBER_SIREN + "," + UPDATED_NUMBER_SIREN);

        // Get all the companyList where numberSiren equals to UPDATED_NUMBER_SIREN
        defaultCompanyShouldNotBeFound("numberSiren.in=" + UPDATED_NUMBER_SIREN);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSirenIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiren is not null
        defaultCompanyShouldBeFound("numberSiren.specified=true");

        // Get all the companyList where numberSiren is null
        defaultCompanyShouldNotBeFound("numberSiren.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSirenContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiren contains DEFAULT_NUMBER_SIREN
        defaultCompanyShouldBeFound("numberSiren.contains=" + DEFAULT_NUMBER_SIREN);

        // Get all the companyList where numberSiren contains UPDATED_NUMBER_SIREN
        defaultCompanyShouldNotBeFound("numberSiren.contains=" + UPDATED_NUMBER_SIREN);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNumberSirenNotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where numberSiren does not contain DEFAULT_NUMBER_SIREN
        defaultCompanyShouldNotBeFound("numberSiren.doesNotContain=" + DEFAULT_NUMBER_SIREN);

        // Get all the companyList where numberSiren does not contain UPDATED_NUMBER_SIREN
        defaultCompanyShouldBeFound("numberSiren.doesNotContain=" + UPDATED_NUMBER_SIREN);
    }

    @Test
    @Transactional
    public void getAllCompaniesByListDeliveryManIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);
        UserAccount listDeliveryMan = UserAccountResourceIT.createEntity(em);
        em.persist(listDeliveryMan);
        em.flush();
        company.addListDeliveryMan(listDeliveryMan);
        companyRepository.saveAndFlush(company);
        Long listDeliveryManId = listDeliveryMan.getId();

        // Get all the companyList where listDeliveryMan equals to listDeliveryManId
        defaultCompanyShouldBeFound("listDeliveryManId.equals=" + listDeliveryManId);

        // Get all the companyList where listDeliveryMan equals to listDeliveryManId + 1
        defaultCompanyShouldNotBeFound("listDeliveryManId.equals=" + (listDeliveryManId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanyShouldBeFound(String filter) throws Exception {
        restCompanyMockMvc
            .perform(get("/api/companies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(company.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameCompany").value(hasItem(DEFAULT_NAME_COMPANY)))
            .andExpect(jsonPath("$.[*].numberSiretCompany").value(hasItem(DEFAULT_NUMBER_SIRET_COMPANY)))
            .andExpect(jsonPath("$.[*].numberSiren").value(hasItem(DEFAULT_NUMBER_SIREN)));

        // Check, that the count call also returns 1
        restCompanyMockMvc
            .perform(get("/api/companies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanyShouldNotBeFound(String filter) throws Exception {
        restCompanyMockMvc
            .perform(get("/api/companies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanyMockMvc
            .perform(get("/api/companies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompany() throws Exception {
        // Get the company
        restCompanyMockMvc.perform(get("/api/companies/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompany() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        int databaseSizeBeforeUpdate = companyRepository.findAll().size();

        // Update the company
        Company updatedCompany = companyRepository.findById(company.getId()).get();
        // Disconnect from session so that the updates on updatedCompany are not directly saved in db
        em.detach(updatedCompany);
        updatedCompany.nameCompany(UPDATED_NAME_COMPANY).numberSiretCompany(UPDATED_NUMBER_SIRET_COMPANY).numberSiren(UPDATED_NUMBER_SIREN);
        CompanyDTO companyDTO = companyMapper.toDto(updatedCompany);

        restCompanyMockMvc
            .perform(put("/api/companies").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isOk());

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);
        Company testCompany = companyList.get(companyList.size() - 1);
        assertThat(testCompany.getNameCompany()).isEqualTo(UPDATED_NAME_COMPANY);
        assertThat(testCompany.getNumberSiretCompany()).isEqualTo(UPDATED_NUMBER_SIRET_COMPANY);
        assertThat(testCompany.getNumberSiren()).isEqualTo(UPDATED_NUMBER_SIREN);
    }

    @Test
    @Transactional
    public void updateNonExistingCompany() throws Exception {
        int databaseSizeBeforeUpdate = companyRepository.findAll().size();

        // Create the Company
        CompanyDTO companyDTO = companyMapper.toDto(company);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyMockMvc
            .perform(put("/api/companies").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompany() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        int databaseSizeBeforeDelete = companyRepository.findAll().size();

        // Delete the company
        restCompanyMockMvc
            .perform(delete("/api/companies/{id}", company.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
