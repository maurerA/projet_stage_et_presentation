package fr.insy2s.web.rest;

import static fr.insy2s.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.Notation;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.NotationRepository;
import fr.insy2s.service.NotationQueryService;
import fr.insy2s.service.NotationService;
import fr.insy2s.service.dto.NotationCriteria;
import fr.insy2s.service.dto.NotationDTO;
import fr.insy2s.service.mapper.NotationMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link NotationResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class NotationResourceIT {
    private static final Integer DEFAULT_NUMBER_STAR = 1;
    private static final Integer UPDATED_NUMBER_STAR = 2;
    private static final Integer SMALLER_NUMBER_STAR = 1 - 1;

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_NOTE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_NOTE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_NOTE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private NotationRepository notationRepository;

    @Autowired
    private NotationMapper notationMapper;

    @Autowired
    private NotationService notationService;

    @Autowired
    private NotationQueryService notationQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNotationMockMvc;

    private Notation notation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notation createEntity(EntityManager em) {
        Notation notation = new Notation().numberStar(DEFAULT_NUMBER_STAR).message(DEFAULT_MESSAGE).dateNote(DEFAULT_DATE_NOTE);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        notation.setReceiver(userAccount);
        // Add required entity
        notation.setSender(userAccount);
        return notation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notation createUpdatedEntity(EntityManager em) {
        Notation notation = new Notation().numberStar(UPDATED_NUMBER_STAR).message(UPDATED_MESSAGE).dateNote(UPDATED_DATE_NOTE);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createUpdatedEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        notation.setReceiver(userAccount);
        // Add required entity
        notation.setSender(userAccount);
        return notation;
    }

    @BeforeEach
    public void initTest() {
        notation = createEntity(em);
    }

    @Test
    @Transactional
    public void createNotation() throws Exception {
        int databaseSizeBeforeCreate = notationRepository.findAll().size();
        // Create the Notation
        NotationDTO notationDTO = notationMapper.toDto(notation);
        restNotationMockMvc
            .perform(post("/api/notations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notationDTO)))
            .andExpect(status().isCreated());

        // Validate the Notation in the database
        List<Notation> notationList = notationRepository.findAll();
        assertThat(notationList).hasSize(databaseSizeBeforeCreate + 1);
        Notation testNotation = notationList.get(notationList.size() - 1);
        assertThat(testNotation.getNumberStar()).isEqualTo(DEFAULT_NUMBER_STAR);
        assertThat(testNotation.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testNotation.getDateNote()).isEqualTo(DEFAULT_DATE_NOTE);
    }

    @Test
    @Transactional
    public void createNotationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = notationRepository.findAll().size();

        // Create the Notation with an existing ID
        notation.setId(1L);
        NotationDTO notationDTO = notationMapper.toDto(notation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotationMockMvc
            .perform(post("/api/notations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Notation in the database
        List<Notation> notationList = notationRepository.findAll();
        assertThat(notationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNumberStarIsRequired() throws Exception {
        int databaseSizeBeforeTest = notationRepository.findAll().size();
        // set the field null
        notation.setNumberStar(null);

        // Create the Notation, which fails.
        NotationDTO notationDTO = notationMapper.toDto(notation);

        restNotationMockMvc
            .perform(post("/api/notations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notationDTO)))
            .andExpect(status().isBadRequest());

        List<Notation> notationList = notationRepository.findAll();
        assertThat(notationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateNoteIsRequired() throws Exception {
        int databaseSizeBeforeTest = notationRepository.findAll().size();
        // set the field null
        notation.setDateNote(null);

        // Create the Notation, which fails.
        NotationDTO notationDTO = notationMapper.toDto(notation);

        restNotationMockMvc
            .perform(post("/api/notations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notationDTO)))
            .andExpect(status().isBadRequest());

        List<Notation> notationList = notationRepository.findAll();
        assertThat(notationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNotations() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList
        restNotationMockMvc
            .perform(get("/api/notations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notation.getId().intValue())))
            .andExpect(jsonPath("$.[*].numberStar").value(hasItem(DEFAULT_NUMBER_STAR)))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].dateNote").value(hasItem(sameInstant(DEFAULT_DATE_NOTE))));
    }

    @Test
    @Transactional
    public void getNotation() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get the notation
        restNotationMockMvc
            .perform(get("/api/notations/{id}", notation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(notation.getId().intValue()))
            .andExpect(jsonPath("$.numberStar").value(DEFAULT_NUMBER_STAR))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE))
            .andExpect(jsonPath("$.dateNote").value(sameInstant(DEFAULT_DATE_NOTE)));
    }

    @Test
    @Transactional
    public void getNotationsByIdFiltering() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        Long id = notation.getId();

        defaultNotationShouldBeFound("id.equals=" + id);
        defaultNotationShouldNotBeFound("id.notEquals=" + id);

        defaultNotationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNotationShouldNotBeFound("id.greaterThan=" + id);

        defaultNotationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNotationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllNotationsByNumberStarIsEqualToSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where numberStar equals to DEFAULT_NUMBER_STAR
        defaultNotationShouldBeFound("numberStar.equals=" + DEFAULT_NUMBER_STAR);

        // Get all the notationList where numberStar equals to UPDATED_NUMBER_STAR
        defaultNotationShouldNotBeFound("numberStar.equals=" + UPDATED_NUMBER_STAR);
    }

    @Test
    @Transactional
    public void getAllNotationsByNumberStarIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where numberStar not equals to DEFAULT_NUMBER_STAR
        defaultNotationShouldNotBeFound("numberStar.notEquals=" + DEFAULT_NUMBER_STAR);

        // Get all the notationList where numberStar not equals to UPDATED_NUMBER_STAR
        defaultNotationShouldBeFound("numberStar.notEquals=" + UPDATED_NUMBER_STAR);
    }

    @Test
    @Transactional
    public void getAllNotationsByNumberStarIsInShouldWork() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where numberStar in DEFAULT_NUMBER_STAR or UPDATED_NUMBER_STAR
        defaultNotationShouldBeFound("numberStar.in=" + DEFAULT_NUMBER_STAR + "," + UPDATED_NUMBER_STAR);

        // Get all the notationList where numberStar equals to UPDATED_NUMBER_STAR
        defaultNotationShouldNotBeFound("numberStar.in=" + UPDATED_NUMBER_STAR);
    }

    @Test
    @Transactional
    public void getAllNotationsByNumberStarIsNullOrNotNull() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where numberStar is not null
        defaultNotationShouldBeFound("numberStar.specified=true");

        // Get all the notationList where numberStar is null
        defaultNotationShouldNotBeFound("numberStar.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotationsByNumberStarIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where numberStar is greater than or equal to DEFAULT_NUMBER_STAR
        defaultNotationShouldBeFound("numberStar.greaterThanOrEqual=" + DEFAULT_NUMBER_STAR);

        // Get all the notationList where numberStar is greater than or equal to UPDATED_NUMBER_STAR
        defaultNotationShouldNotBeFound("numberStar.greaterThanOrEqual=" + UPDATED_NUMBER_STAR);
    }

    @Test
    @Transactional
    public void getAllNotationsByNumberStarIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where numberStar is less than or equal to DEFAULT_NUMBER_STAR
        defaultNotationShouldBeFound("numberStar.lessThanOrEqual=" + DEFAULT_NUMBER_STAR);

        // Get all the notationList where numberStar is less than or equal to SMALLER_NUMBER_STAR
        defaultNotationShouldNotBeFound("numberStar.lessThanOrEqual=" + SMALLER_NUMBER_STAR);
    }

    @Test
    @Transactional
    public void getAllNotationsByNumberStarIsLessThanSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where numberStar is less than DEFAULT_NUMBER_STAR
        defaultNotationShouldNotBeFound("numberStar.lessThan=" + DEFAULT_NUMBER_STAR);

        // Get all the notationList where numberStar is less than UPDATED_NUMBER_STAR
        defaultNotationShouldBeFound("numberStar.lessThan=" + UPDATED_NUMBER_STAR);
    }

    @Test
    @Transactional
    public void getAllNotationsByNumberStarIsGreaterThanSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where numberStar is greater than DEFAULT_NUMBER_STAR
        defaultNotationShouldNotBeFound("numberStar.greaterThan=" + DEFAULT_NUMBER_STAR);

        // Get all the notationList where numberStar is greater than SMALLER_NUMBER_STAR
        defaultNotationShouldBeFound("numberStar.greaterThan=" + SMALLER_NUMBER_STAR);
    }

    @Test
    @Transactional
    public void getAllNotationsByMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where message equals to DEFAULT_MESSAGE
        defaultNotationShouldBeFound("message.equals=" + DEFAULT_MESSAGE);

        // Get all the notationList where message equals to UPDATED_MESSAGE
        defaultNotationShouldNotBeFound("message.equals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotationsByMessageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where message not equals to DEFAULT_MESSAGE
        defaultNotationShouldNotBeFound("message.notEquals=" + DEFAULT_MESSAGE);

        // Get all the notationList where message not equals to UPDATED_MESSAGE
        defaultNotationShouldBeFound("message.notEquals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotationsByMessageIsInShouldWork() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where message in DEFAULT_MESSAGE or UPDATED_MESSAGE
        defaultNotationShouldBeFound("message.in=" + DEFAULT_MESSAGE + "," + UPDATED_MESSAGE);

        // Get all the notationList where message equals to UPDATED_MESSAGE
        defaultNotationShouldNotBeFound("message.in=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotationsByMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where message is not null
        defaultNotationShouldBeFound("message.specified=true");

        // Get all the notationList where message is null
        defaultNotationShouldNotBeFound("message.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotationsByMessageContainsSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where message contains DEFAULT_MESSAGE
        defaultNotationShouldBeFound("message.contains=" + DEFAULT_MESSAGE);

        // Get all the notationList where message contains UPDATED_MESSAGE
        defaultNotationShouldNotBeFound("message.contains=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotationsByMessageNotContainsSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where message does not contain DEFAULT_MESSAGE
        defaultNotationShouldNotBeFound("message.doesNotContain=" + DEFAULT_MESSAGE);

        // Get all the notationList where message does not contain UPDATED_MESSAGE
        defaultNotationShouldBeFound("message.doesNotContain=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotationsByDateNoteIsEqualToSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where dateNote equals to DEFAULT_DATE_NOTE
        defaultNotationShouldBeFound("dateNote.equals=" + DEFAULT_DATE_NOTE);

        // Get all the notationList where dateNote equals to UPDATED_DATE_NOTE
        defaultNotationShouldNotBeFound("dateNote.equals=" + UPDATED_DATE_NOTE);
    }

    @Test
    @Transactional
    public void getAllNotationsByDateNoteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where dateNote not equals to DEFAULT_DATE_NOTE
        defaultNotationShouldNotBeFound("dateNote.notEquals=" + DEFAULT_DATE_NOTE);

        // Get all the notationList where dateNote not equals to UPDATED_DATE_NOTE
        defaultNotationShouldBeFound("dateNote.notEquals=" + UPDATED_DATE_NOTE);
    }

    @Test
    @Transactional
    public void getAllNotationsByDateNoteIsInShouldWork() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where dateNote in DEFAULT_DATE_NOTE or UPDATED_DATE_NOTE
        defaultNotationShouldBeFound("dateNote.in=" + DEFAULT_DATE_NOTE + "," + UPDATED_DATE_NOTE);

        // Get all the notationList where dateNote equals to UPDATED_DATE_NOTE
        defaultNotationShouldNotBeFound("dateNote.in=" + UPDATED_DATE_NOTE);
    }

    @Test
    @Transactional
    public void getAllNotationsByDateNoteIsNullOrNotNull() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where dateNote is not null
        defaultNotationShouldBeFound("dateNote.specified=true");

        // Get all the notationList where dateNote is null
        defaultNotationShouldNotBeFound("dateNote.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotationsByDateNoteIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where dateNote is greater than or equal to DEFAULT_DATE_NOTE
        defaultNotationShouldBeFound("dateNote.greaterThanOrEqual=" + DEFAULT_DATE_NOTE);

        // Get all the notationList where dateNote is greater than or equal to UPDATED_DATE_NOTE
        defaultNotationShouldNotBeFound("dateNote.greaterThanOrEqual=" + UPDATED_DATE_NOTE);
    }

    @Test
    @Transactional
    public void getAllNotationsByDateNoteIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where dateNote is less than or equal to DEFAULT_DATE_NOTE
        defaultNotationShouldBeFound("dateNote.lessThanOrEqual=" + DEFAULT_DATE_NOTE);

        // Get all the notationList where dateNote is less than or equal to SMALLER_DATE_NOTE
        defaultNotationShouldNotBeFound("dateNote.lessThanOrEqual=" + SMALLER_DATE_NOTE);
    }

    @Test
    @Transactional
    public void getAllNotationsByDateNoteIsLessThanSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where dateNote is less than DEFAULT_DATE_NOTE
        defaultNotationShouldNotBeFound("dateNote.lessThan=" + DEFAULT_DATE_NOTE);

        // Get all the notationList where dateNote is less than UPDATED_DATE_NOTE
        defaultNotationShouldBeFound("dateNote.lessThan=" + UPDATED_DATE_NOTE);
    }

    @Test
    @Transactional
    public void getAllNotationsByDateNoteIsGreaterThanSomething() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        // Get all the notationList where dateNote is greater than DEFAULT_DATE_NOTE
        defaultNotationShouldNotBeFound("dateNote.greaterThan=" + DEFAULT_DATE_NOTE);

        // Get all the notationList where dateNote is greater than SMALLER_DATE_NOTE
        defaultNotationShouldBeFound("dateNote.greaterThan=" + SMALLER_DATE_NOTE);
    }

    @Test
    @Transactional
    public void getAllNotationsByReceiverIsEqualToSomething() throws Exception {
        // Get already existing entity
        UserAccount receiver = notation.getReceiver();
        notationRepository.saveAndFlush(notation);
        Long receiverId = receiver.getId();

        // Get all the notationList where receiver equals to receiverId
        defaultNotationShouldBeFound("receiverId.equals=" + receiverId);

        // Get all the notationList where receiver equals to receiverId + 1
        defaultNotationShouldNotBeFound("receiverId.equals=" + (receiverId + 1));
    }

    @Test
    @Transactional
    public void getAllNotationsBySenderIsEqualToSomething() throws Exception {
        // Get already existing entity
        UserAccount sender = notation.getSender();
        notationRepository.saveAndFlush(notation);
        Long senderId = sender.getId();

        // Get all the notationList where sender equals to senderId
        defaultNotationShouldBeFound("senderId.equals=" + senderId);

        // Get all the notationList where sender equals to senderId + 1
        defaultNotationShouldNotBeFound("senderId.equals=" + (senderId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNotationShouldBeFound(String filter) throws Exception {
        restNotationMockMvc
            .perform(get("/api/notations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notation.getId().intValue())))
            .andExpect(jsonPath("$.[*].numberStar").value(hasItem(DEFAULT_NUMBER_STAR)))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].dateNote").value(hasItem(sameInstant(DEFAULT_DATE_NOTE))));

        // Check, that the count call also returns 1
        restNotationMockMvc
            .perform(get("/api/notations/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNotationShouldNotBeFound(String filter) throws Exception {
        restNotationMockMvc
            .perform(get("/api/notations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNotationMockMvc
            .perform(get("/api/notations/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingNotation() throws Exception {
        // Get the notation
        restNotationMockMvc.perform(get("/api/notations/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNotation() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        int databaseSizeBeforeUpdate = notationRepository.findAll().size();

        // Update the notation
        Notation updatedNotation = notationRepository.findById(notation.getId()).get();
        // Disconnect from session so that the updates on updatedNotation are not directly saved in db
        em.detach(updatedNotation);
        updatedNotation.numberStar(UPDATED_NUMBER_STAR).message(UPDATED_MESSAGE).dateNote(UPDATED_DATE_NOTE);
        NotationDTO notationDTO = notationMapper.toDto(updatedNotation);

        restNotationMockMvc
            .perform(put("/api/notations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notationDTO)))
            .andExpect(status().isOk());

        // Validate the Notation in the database
        List<Notation> notationList = notationRepository.findAll();
        assertThat(notationList).hasSize(databaseSizeBeforeUpdate);
        Notation testNotation = notationList.get(notationList.size() - 1);
        assertThat(testNotation.getNumberStar()).isEqualTo(UPDATED_NUMBER_STAR);
        assertThat(testNotation.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testNotation.getDateNote()).isEqualTo(UPDATED_DATE_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingNotation() throws Exception {
        int databaseSizeBeforeUpdate = notationRepository.findAll().size();

        // Create the Notation
        NotationDTO notationDTO = notationMapper.toDto(notation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotationMockMvc
            .perform(put("/api/notations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Notation in the database
        List<Notation> notationList = notationRepository.findAll();
        assertThat(notationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNotation() throws Exception {
        // Initialize the database
        notationRepository.saveAndFlush(notation);

        int databaseSizeBeforeDelete = notationRepository.findAll().size();

        // Delete the notation
        restNotationMockMvc
            .perform(delete("/api/notations/{id}", notation.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Notation> notationList = notationRepository.findAll();
        assertThat(notationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
