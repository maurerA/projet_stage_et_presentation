package fr.insy2s.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.VehicleType;
import fr.insy2s.repository.VehicleTypeRepository;
import fr.insy2s.service.VehicleTypeQueryService;
import fr.insy2s.service.VehicleTypeService;
import fr.insy2s.service.dto.VehicleTypeCriteria;
import fr.insy2s.service.dto.VehicleTypeDTO;
import fr.insy2s.service.mapper.VehicleTypeMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link VehicleTypeResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class VehicleTypeResourceIT {
    private static final String DEFAULT_CODE_REF = "AAAAAAAAAA";
    private static final String UPDATED_CODE_REF = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    @Autowired
    private VehicleTypeRepository vehicleTypeRepository;

    @Autowired
    private VehicleTypeMapper vehicleTypeMapper;

    @Autowired
    private VehicleTypeService vehicleTypeService;

    @Autowired
    private VehicleTypeQueryService vehicleTypeQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVehicleTypeMockMvc;

    private VehicleType vehicleType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VehicleType createEntity(EntityManager em) {
        VehicleType vehicleType = new VehicleType().codeRef(DEFAULT_CODE_REF).label(DEFAULT_LABEL);
        return vehicleType;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VehicleType createUpdatedEntity(EntityManager em) {
        VehicleType vehicleType = new VehicleType().codeRef(UPDATED_CODE_REF).label(UPDATED_LABEL);
        return vehicleType;
    }

    @BeforeEach
    public void initTest() {
        vehicleType = createEntity(em);
    }

    @Test
    @Transactional
    public void createVehicleType() throws Exception {
        int databaseSizeBeforeCreate = vehicleTypeRepository.findAll().size();
        // Create the VehicleType
        VehicleTypeDTO vehicleTypeDTO = vehicleTypeMapper.toDto(vehicleType);
        restVehicleTypeMockMvc
            .perform(
                post("/api/vehicle-types")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vehicleTypeDTO))
            )
            .andExpect(status().isCreated());

        // Validate the VehicleType in the database
        List<VehicleType> vehicleTypeList = vehicleTypeRepository.findAll();
        assertThat(vehicleTypeList).hasSize(databaseSizeBeforeCreate + 1);
        VehicleType testVehicleType = vehicleTypeList.get(vehicleTypeList.size() - 1);
        assertThat(testVehicleType.getCodeRef()).isEqualTo(DEFAULT_CODE_REF);
        assertThat(testVehicleType.getLabel()).isEqualTo(DEFAULT_LABEL);
    }

    @Test
    @Transactional
    public void createVehicleTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vehicleTypeRepository.findAll().size();

        // Create the VehicleType with an existing ID
        vehicleType.setId(1L);
        VehicleTypeDTO vehicleTypeDTO = vehicleTypeMapper.toDto(vehicleType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVehicleTypeMockMvc
            .perform(
                post("/api/vehicle-types")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vehicleTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VehicleType in the database
        List<VehicleType> vehicleTypeList = vehicleTypeRepository.findAll();
        assertThat(vehicleTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeRefIsRequired() throws Exception {
        int databaseSizeBeforeTest = vehicleTypeRepository.findAll().size();
        // set the field null
        vehicleType.setCodeRef(null);

        // Create the VehicleType, which fails.
        VehicleTypeDTO vehicleTypeDTO = vehicleTypeMapper.toDto(vehicleType);

        restVehicleTypeMockMvc
            .perform(
                post("/api/vehicle-types")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vehicleTypeDTO))
            )
            .andExpect(status().isBadRequest());

        List<VehicleType> vehicleTypeList = vehicleTypeRepository.findAll();
        assertThat(vehicleTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = vehicleTypeRepository.findAll().size();
        // set the field null
        vehicleType.setLabel(null);

        // Create the VehicleType, which fails.
        VehicleTypeDTO vehicleTypeDTO = vehicleTypeMapper.toDto(vehicleType);

        restVehicleTypeMockMvc
            .perform(
                post("/api/vehicle-types")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vehicleTypeDTO))
            )
            .andExpect(status().isBadRequest());

        List<VehicleType> vehicleTypeList = vehicleTypeRepository.findAll();
        assertThat(vehicleTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVehicleTypes() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList
        restVehicleTypeMockMvc
            .perform(get("/api/vehicle-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicleType.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeRef").value(hasItem(DEFAULT_CODE_REF)))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)));
    }

    @Test
    @Transactional
    public void getVehicleType() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get the vehicleType
        restVehicleTypeMockMvc
            .perform(get("/api/vehicle-types/{id}", vehicleType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(vehicleType.getId().intValue()))
            .andExpect(jsonPath("$.codeRef").value(DEFAULT_CODE_REF))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL));
    }

    @Test
    @Transactional
    public void getVehicleTypesByIdFiltering() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        Long id = vehicleType.getId();

        defaultVehicleTypeShouldBeFound("id.equals=" + id);
        defaultVehicleTypeShouldNotBeFound("id.notEquals=" + id);

        defaultVehicleTypeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultVehicleTypeShouldNotBeFound("id.greaterThan=" + id);

        defaultVehicleTypeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultVehicleTypeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByCodeRefIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where codeRef equals to DEFAULT_CODE_REF
        defaultVehicleTypeShouldBeFound("codeRef.equals=" + DEFAULT_CODE_REF);

        // Get all the vehicleTypeList where codeRef equals to UPDATED_CODE_REF
        defaultVehicleTypeShouldNotBeFound("codeRef.equals=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByCodeRefIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where codeRef not equals to DEFAULT_CODE_REF
        defaultVehicleTypeShouldNotBeFound("codeRef.notEquals=" + DEFAULT_CODE_REF);

        // Get all the vehicleTypeList where codeRef not equals to UPDATED_CODE_REF
        defaultVehicleTypeShouldBeFound("codeRef.notEquals=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByCodeRefIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where codeRef in DEFAULT_CODE_REF or UPDATED_CODE_REF
        defaultVehicleTypeShouldBeFound("codeRef.in=" + DEFAULT_CODE_REF + "," + UPDATED_CODE_REF);

        // Get all the vehicleTypeList where codeRef equals to UPDATED_CODE_REF
        defaultVehicleTypeShouldNotBeFound("codeRef.in=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByCodeRefIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where codeRef is not null
        defaultVehicleTypeShouldBeFound("codeRef.specified=true");

        // Get all the vehicleTypeList where codeRef is null
        defaultVehicleTypeShouldNotBeFound("codeRef.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByCodeRefContainsSomething() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where codeRef contains DEFAULT_CODE_REF
        defaultVehicleTypeShouldBeFound("codeRef.contains=" + DEFAULT_CODE_REF);

        // Get all the vehicleTypeList where codeRef contains UPDATED_CODE_REF
        defaultVehicleTypeShouldNotBeFound("codeRef.contains=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByCodeRefNotContainsSomething() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where codeRef does not contain DEFAULT_CODE_REF
        defaultVehicleTypeShouldNotBeFound("codeRef.doesNotContain=" + DEFAULT_CODE_REF);

        // Get all the vehicleTypeList where codeRef does not contain UPDATED_CODE_REF
        defaultVehicleTypeShouldBeFound("codeRef.doesNotContain=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where label equals to DEFAULT_LABEL
        defaultVehicleTypeShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the vehicleTypeList where label equals to UPDATED_LABEL
        defaultVehicleTypeShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where label not equals to DEFAULT_LABEL
        defaultVehicleTypeShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the vehicleTypeList where label not equals to UPDATED_LABEL
        defaultVehicleTypeShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultVehicleTypeShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the vehicleTypeList where label equals to UPDATED_LABEL
        defaultVehicleTypeShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where label is not null
        defaultVehicleTypeShouldBeFound("label.specified=true");

        // Get all the vehicleTypeList where label is null
        defaultVehicleTypeShouldNotBeFound("label.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByLabelContainsSomething() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where label contains DEFAULT_LABEL
        defaultVehicleTypeShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the vehicleTypeList where label contains UPDATED_LABEL
        defaultVehicleTypeShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllVehicleTypesByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        // Get all the vehicleTypeList where label does not contain DEFAULT_LABEL
        defaultVehicleTypeShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the vehicleTypeList where label does not contain UPDATED_LABEL
        defaultVehicleTypeShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultVehicleTypeShouldBeFound(String filter) throws Exception {
        restVehicleTypeMockMvc
            .perform(get("/api/vehicle-types?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicleType.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeRef").value(hasItem(DEFAULT_CODE_REF)))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)));

        // Check, that the count call also returns 1
        restVehicleTypeMockMvc
            .perform(get("/api/vehicle-types/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultVehicleTypeShouldNotBeFound(String filter) throws Exception {
        restVehicleTypeMockMvc
            .perform(get("/api/vehicle-types?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restVehicleTypeMockMvc
            .perform(get("/api/vehicle-types/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingVehicleType() throws Exception {
        // Get the vehicleType
        restVehicleTypeMockMvc.perform(get("/api/vehicle-types/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVehicleType() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        int databaseSizeBeforeUpdate = vehicleTypeRepository.findAll().size();

        // Update the vehicleType
        VehicleType updatedVehicleType = vehicleTypeRepository.findById(vehicleType.getId()).get();
        // Disconnect from session so that the updates on updatedVehicleType are not directly saved in db
        em.detach(updatedVehicleType);
        updatedVehicleType.codeRef(UPDATED_CODE_REF).label(UPDATED_LABEL);
        VehicleTypeDTO vehicleTypeDTO = vehicleTypeMapper.toDto(updatedVehicleType);

        restVehicleTypeMockMvc
            .perform(
                put("/api/vehicle-types").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleTypeDTO))
            )
            .andExpect(status().isOk());

        // Validate the VehicleType in the database
        List<VehicleType> vehicleTypeList = vehicleTypeRepository.findAll();
        assertThat(vehicleTypeList).hasSize(databaseSizeBeforeUpdate);
        VehicleType testVehicleType = vehicleTypeList.get(vehicleTypeList.size() - 1);
        assertThat(testVehicleType.getCodeRef()).isEqualTo(UPDATED_CODE_REF);
        assertThat(testVehicleType.getLabel()).isEqualTo(UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void updateNonExistingVehicleType() throws Exception {
        int databaseSizeBeforeUpdate = vehicleTypeRepository.findAll().size();

        // Create the VehicleType
        VehicleTypeDTO vehicleTypeDTO = vehicleTypeMapper.toDto(vehicleType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVehicleTypeMockMvc
            .perform(
                put("/api/vehicle-types").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VehicleType in the database
        List<VehicleType> vehicleTypeList = vehicleTypeRepository.findAll();
        assertThat(vehicleTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVehicleType() throws Exception {
        // Initialize the database
        vehicleTypeRepository.saveAndFlush(vehicleType);

        int databaseSizeBeforeDelete = vehicleTypeRepository.findAll().size();

        // Delete the vehicleType
        restVehicleTypeMockMvc
            .perform(delete("/api/vehicle-types/{id}", vehicleType.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VehicleType> vehicleTypeList = vehicleTypeRepository.findAll();
        assertThat(vehicleTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
