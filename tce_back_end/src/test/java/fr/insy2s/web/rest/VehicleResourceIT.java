package fr.insy2s.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.domain.Vehicle;
import fr.insy2s.domain.VehicleType;
import fr.insy2s.repository.VehicleRepository;
import fr.insy2s.service.VehicleQueryService;
import fr.insy2s.service.VehicleService;
import fr.insy2s.service.dto.VehicleCriteria;
import fr.insy2s.service.dto.VehicleDTO;
import fr.insy2s.service.mapper.VehicleMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link VehicleResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class VehicleResourceIT {
    private static final Float DEFAULT_MAX_CAPACITY_LENGTH = 0F;
    private static final Float UPDATED_MAX_CAPACITY_LENGTH = 1F;
    private static final Float SMALLER_MAX_CAPACITY_LENGTH = 0F - 1F;

    private static final Float DEFAULT_MAX_CAPACITY_WIDTH = 0F;
    private static final Float UPDATED_MAX_CAPACITY_WIDTH = 1F;
    private static final Float SMALLER_MAX_CAPACITY_WIDTH = 0F - 1F;

    private static final Float DEFAULT_MAX_CAPACITY_HEIGHT = 0F;
    private static final Float UPDATED_MAX_CAPACITY_HEIGHT = 1F;
    private static final Float SMALLER_MAX_CAPACITY_HEIGHT = 0F - 1F;

    private static final Float DEFAULT_MAX_CAPACITY_WEIGHT = 0F;
    private static final Float UPDATED_MAX_CAPACITY_WEIGHT = 1F;
    private static final Float SMALLER_MAX_CAPACITY_WEIGHT = 0F - 1F;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private VehicleMapper vehicleMapper;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private VehicleQueryService vehicleQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVehicleMockMvc;

    private Vehicle vehicle;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vehicle createEntity(EntityManager em) {
        Vehicle vehicle = new Vehicle()
            .maxCapacityLength(DEFAULT_MAX_CAPACITY_LENGTH)
            .maxCapacityWidth(DEFAULT_MAX_CAPACITY_WIDTH)
            .maxCapacityHeight(DEFAULT_MAX_CAPACITY_HEIGHT)
            .maxCapacityWeight(DEFAULT_MAX_CAPACITY_WEIGHT)
            .active(DEFAULT_ACTIVE);
        // Add required entity
        VehicleType vehicleType;
        if (TestUtil.findAll(em, VehicleType.class).isEmpty()) {
            vehicleType = VehicleTypeResourceIT.createEntity(em);
            em.persist(vehicleType);
            em.flush();
        } else {
            vehicleType = TestUtil.findAll(em, VehicleType.class).get(0);
        }
        vehicle.setType(vehicleType);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        vehicle.setDeliveryMan(userAccount);
        return vehicle;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vehicle createUpdatedEntity(EntityManager em) {
        Vehicle vehicle = new Vehicle()
            .maxCapacityLength(UPDATED_MAX_CAPACITY_LENGTH)
            .maxCapacityWidth(UPDATED_MAX_CAPACITY_WIDTH)
            .maxCapacityHeight(UPDATED_MAX_CAPACITY_HEIGHT)
            .maxCapacityWeight(UPDATED_MAX_CAPACITY_WEIGHT)
            .active(UPDATED_ACTIVE);
        // Add required entity
        VehicleType vehicleType;
        if (TestUtil.findAll(em, VehicleType.class).isEmpty()) {
            vehicleType = VehicleTypeResourceIT.createUpdatedEntity(em);
            em.persist(vehicleType);
            em.flush();
        } else {
            vehicleType = TestUtil.findAll(em, VehicleType.class).get(0);
        }
        vehicle.setType(vehicleType);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createUpdatedEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        vehicle.setDeliveryMan(userAccount);
        return vehicle;
    }

    @BeforeEach
    public void initTest() {
        vehicle = createEntity(em);
    }

    @Test
    @Transactional
    public void createVehicle() throws Exception {
        int databaseSizeBeforeCreate = vehicleRepository.findAll().size();
        // Create the Vehicle
        VehicleDTO vehicleDTO = vehicleMapper.toDto(vehicle);
        restVehicleMockMvc
            .perform(post("/api/vehicles").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleDTO)))
            .andExpect(status().isCreated());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeCreate + 1);
        Vehicle testVehicle = vehicleList.get(vehicleList.size() - 1);
        assertThat(testVehicle.getMaxCapacityLength()).isEqualTo(DEFAULT_MAX_CAPACITY_LENGTH);
        assertThat(testVehicle.getMaxCapacityWidth()).isEqualTo(DEFAULT_MAX_CAPACITY_WIDTH);
        assertThat(testVehicle.getMaxCapacityHeight()).isEqualTo(DEFAULT_MAX_CAPACITY_HEIGHT);
        assertThat(testVehicle.getMaxCapacityWeight()).isEqualTo(DEFAULT_MAX_CAPACITY_WEIGHT);
        assertThat(testVehicle.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createVehicleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vehicleRepository.findAll().size();

        // Create the Vehicle with an existing ID
        vehicle.setId(1L);
        VehicleDTO vehicleDTO = vehicleMapper.toDto(vehicle);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVehicleMockMvc
            .perform(post("/api/vehicles").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkMaxCapacityLengthIsRequired() throws Exception {
        int databaseSizeBeforeTest = vehicleRepository.findAll().size();
        // set the field null
        vehicle.setMaxCapacityLength(null);

        // Create the Vehicle, which fails.
        VehicleDTO vehicleDTO = vehicleMapper.toDto(vehicle);

        restVehicleMockMvc
            .perform(post("/api/vehicles").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleDTO)))
            .andExpect(status().isBadRequest());

        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxCapacityWidthIsRequired() throws Exception {
        int databaseSizeBeforeTest = vehicleRepository.findAll().size();
        // set the field null
        vehicle.setMaxCapacityWidth(null);

        // Create the Vehicle, which fails.
        VehicleDTO vehicleDTO = vehicleMapper.toDto(vehicle);

        restVehicleMockMvc
            .perform(post("/api/vehicles").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleDTO)))
            .andExpect(status().isBadRequest());

        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxCapacityHeightIsRequired() throws Exception {
        int databaseSizeBeforeTest = vehicleRepository.findAll().size();
        // set the field null
        vehicle.setMaxCapacityHeight(null);

        // Create the Vehicle, which fails.
        VehicleDTO vehicleDTO = vehicleMapper.toDto(vehicle);

        restVehicleMockMvc
            .perform(post("/api/vehicles").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleDTO)))
            .andExpect(status().isBadRequest());

        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxCapacityWeightIsRequired() throws Exception {
        int databaseSizeBeforeTest = vehicleRepository.findAll().size();
        // set the field null
        vehicle.setMaxCapacityWeight(null);

        // Create the Vehicle, which fails.
        VehicleDTO vehicleDTO = vehicleMapper.toDto(vehicle);

        restVehicleMockMvc
            .perform(post("/api/vehicles").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleDTO)))
            .andExpect(status().isBadRequest());

        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = vehicleRepository.findAll().size();
        // set the field null
        vehicle.setActive(null);

        // Create the Vehicle, which fails.
        VehicleDTO vehicleDTO = vehicleMapper.toDto(vehicle);

        restVehicleMockMvc
            .perform(post("/api/vehicles").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleDTO)))
            .andExpect(status().isBadRequest());

        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVehicles() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList
        restVehicleMockMvc
            .perform(get("/api/vehicles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicle.getId().intValue())))
            .andExpect(jsonPath("$.[*].maxCapacityLength").value(hasItem(DEFAULT_MAX_CAPACITY_LENGTH.doubleValue())))
            .andExpect(jsonPath("$.[*].maxCapacityWidth").value(hasItem(DEFAULT_MAX_CAPACITY_WIDTH.doubleValue())))
            .andExpect(jsonPath("$.[*].maxCapacityHeight").value(hasItem(DEFAULT_MAX_CAPACITY_HEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].maxCapacityWeight").value(hasItem(DEFAULT_MAX_CAPACITY_WEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void getVehicle() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get the vehicle
        restVehicleMockMvc
            .perform(get("/api/vehicles/{id}", vehicle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(vehicle.getId().intValue()))
            .andExpect(jsonPath("$.maxCapacityLength").value(DEFAULT_MAX_CAPACITY_LENGTH.doubleValue()))
            .andExpect(jsonPath("$.maxCapacityWidth").value(DEFAULT_MAX_CAPACITY_WIDTH.doubleValue()))
            .andExpect(jsonPath("$.maxCapacityHeight").value(DEFAULT_MAX_CAPACITY_HEIGHT.doubleValue()))
            .andExpect(jsonPath("$.maxCapacityWeight").value(DEFAULT_MAX_CAPACITY_WEIGHT.doubleValue()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getVehiclesByIdFiltering() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        Long id = vehicle.getId();

        defaultVehicleShouldBeFound("id.equals=" + id);
        defaultVehicleShouldNotBeFound("id.notEquals=" + id);

        defaultVehicleShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultVehicleShouldNotBeFound("id.greaterThan=" + id);

        defaultVehicleShouldBeFound("id.lessThanOrEqual=" + id);
        defaultVehicleShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityLengthIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityLength equals to DEFAULT_MAX_CAPACITY_LENGTH
        defaultVehicleShouldBeFound("maxCapacityLength.equals=" + DEFAULT_MAX_CAPACITY_LENGTH);

        // Get all the vehicleList where maxCapacityLength equals to UPDATED_MAX_CAPACITY_LENGTH
        defaultVehicleShouldNotBeFound("maxCapacityLength.equals=" + UPDATED_MAX_CAPACITY_LENGTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityLengthIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityLength not equals to DEFAULT_MAX_CAPACITY_LENGTH
        defaultVehicleShouldNotBeFound("maxCapacityLength.notEquals=" + DEFAULT_MAX_CAPACITY_LENGTH);

        // Get all the vehicleList where maxCapacityLength not equals to UPDATED_MAX_CAPACITY_LENGTH
        defaultVehicleShouldBeFound("maxCapacityLength.notEquals=" + UPDATED_MAX_CAPACITY_LENGTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityLengthIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityLength in DEFAULT_MAX_CAPACITY_LENGTH or UPDATED_MAX_CAPACITY_LENGTH
        defaultVehicleShouldBeFound("maxCapacityLength.in=" + DEFAULT_MAX_CAPACITY_LENGTH + "," + UPDATED_MAX_CAPACITY_LENGTH);

        // Get all the vehicleList where maxCapacityLength equals to UPDATED_MAX_CAPACITY_LENGTH
        defaultVehicleShouldNotBeFound("maxCapacityLength.in=" + UPDATED_MAX_CAPACITY_LENGTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityLengthIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityLength is not null
        defaultVehicleShouldBeFound("maxCapacityLength.specified=true");

        // Get all the vehicleList where maxCapacityLength is null
        defaultVehicleShouldNotBeFound("maxCapacityLength.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityLengthIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityLength is greater than or equal to DEFAULT_MAX_CAPACITY_LENGTH
        defaultVehicleShouldBeFound("maxCapacityLength.greaterThanOrEqual=" + DEFAULT_MAX_CAPACITY_LENGTH);

        // Get all the vehicleList where maxCapacityLength is greater than or equal to UPDATED_MAX_CAPACITY_LENGTH
        defaultVehicleShouldNotBeFound("maxCapacityLength.greaterThanOrEqual=" + UPDATED_MAX_CAPACITY_LENGTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityLengthIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityLength is less than or equal to DEFAULT_MAX_CAPACITY_LENGTH
        defaultVehicleShouldBeFound("maxCapacityLength.lessThanOrEqual=" + DEFAULT_MAX_CAPACITY_LENGTH);

        // Get all the vehicleList where maxCapacityLength is less than or equal to SMALLER_MAX_CAPACITY_LENGTH
        defaultVehicleShouldNotBeFound("maxCapacityLength.lessThanOrEqual=" + SMALLER_MAX_CAPACITY_LENGTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityLengthIsLessThanSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityLength is less than DEFAULT_MAX_CAPACITY_LENGTH
        defaultVehicleShouldNotBeFound("maxCapacityLength.lessThan=" + DEFAULT_MAX_CAPACITY_LENGTH);

        // Get all the vehicleList where maxCapacityLength is less than UPDATED_MAX_CAPACITY_LENGTH
        defaultVehicleShouldBeFound("maxCapacityLength.lessThan=" + UPDATED_MAX_CAPACITY_LENGTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityLengthIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityLength is greater than DEFAULT_MAX_CAPACITY_LENGTH
        defaultVehicleShouldNotBeFound("maxCapacityLength.greaterThan=" + DEFAULT_MAX_CAPACITY_LENGTH);

        // Get all the vehicleList where maxCapacityLength is greater than SMALLER_MAX_CAPACITY_LENGTH
        defaultVehicleShouldBeFound("maxCapacityLength.greaterThan=" + SMALLER_MAX_CAPACITY_LENGTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWidthIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWidth equals to DEFAULT_MAX_CAPACITY_WIDTH
        defaultVehicleShouldBeFound("maxCapacityWidth.equals=" + DEFAULT_MAX_CAPACITY_WIDTH);

        // Get all the vehicleList where maxCapacityWidth equals to UPDATED_MAX_CAPACITY_WIDTH
        defaultVehicleShouldNotBeFound("maxCapacityWidth.equals=" + UPDATED_MAX_CAPACITY_WIDTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWidthIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWidth not equals to DEFAULT_MAX_CAPACITY_WIDTH
        defaultVehicleShouldNotBeFound("maxCapacityWidth.notEquals=" + DEFAULT_MAX_CAPACITY_WIDTH);

        // Get all the vehicleList where maxCapacityWidth not equals to UPDATED_MAX_CAPACITY_WIDTH
        defaultVehicleShouldBeFound("maxCapacityWidth.notEquals=" + UPDATED_MAX_CAPACITY_WIDTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWidthIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWidth in DEFAULT_MAX_CAPACITY_WIDTH or UPDATED_MAX_CAPACITY_WIDTH
        defaultVehicleShouldBeFound("maxCapacityWidth.in=" + DEFAULT_MAX_CAPACITY_WIDTH + "," + UPDATED_MAX_CAPACITY_WIDTH);

        // Get all the vehicleList where maxCapacityWidth equals to UPDATED_MAX_CAPACITY_WIDTH
        defaultVehicleShouldNotBeFound("maxCapacityWidth.in=" + UPDATED_MAX_CAPACITY_WIDTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWidthIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWidth is not null
        defaultVehicleShouldBeFound("maxCapacityWidth.specified=true");

        // Get all the vehicleList where maxCapacityWidth is null
        defaultVehicleShouldNotBeFound("maxCapacityWidth.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWidthIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWidth is greater than or equal to DEFAULT_MAX_CAPACITY_WIDTH
        defaultVehicleShouldBeFound("maxCapacityWidth.greaterThanOrEqual=" + DEFAULT_MAX_CAPACITY_WIDTH);

        // Get all the vehicleList where maxCapacityWidth is greater than or equal to UPDATED_MAX_CAPACITY_WIDTH
        defaultVehicleShouldNotBeFound("maxCapacityWidth.greaterThanOrEqual=" + UPDATED_MAX_CAPACITY_WIDTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWidthIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWidth is less than or equal to DEFAULT_MAX_CAPACITY_WIDTH
        defaultVehicleShouldBeFound("maxCapacityWidth.lessThanOrEqual=" + DEFAULT_MAX_CAPACITY_WIDTH);

        // Get all the vehicleList where maxCapacityWidth is less than or equal to SMALLER_MAX_CAPACITY_WIDTH
        defaultVehicleShouldNotBeFound("maxCapacityWidth.lessThanOrEqual=" + SMALLER_MAX_CAPACITY_WIDTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWidthIsLessThanSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWidth is less than DEFAULT_MAX_CAPACITY_WIDTH
        defaultVehicleShouldNotBeFound("maxCapacityWidth.lessThan=" + DEFAULT_MAX_CAPACITY_WIDTH);

        // Get all the vehicleList where maxCapacityWidth is less than UPDATED_MAX_CAPACITY_WIDTH
        defaultVehicleShouldBeFound("maxCapacityWidth.lessThan=" + UPDATED_MAX_CAPACITY_WIDTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWidthIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWidth is greater than DEFAULT_MAX_CAPACITY_WIDTH
        defaultVehicleShouldNotBeFound("maxCapacityWidth.greaterThan=" + DEFAULT_MAX_CAPACITY_WIDTH);

        // Get all the vehicleList where maxCapacityWidth is greater than SMALLER_MAX_CAPACITY_WIDTH
        defaultVehicleShouldBeFound("maxCapacityWidth.greaterThan=" + SMALLER_MAX_CAPACITY_WIDTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityHeightIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityHeight equals to DEFAULT_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldBeFound("maxCapacityHeight.equals=" + DEFAULT_MAX_CAPACITY_HEIGHT);

        // Get all the vehicleList where maxCapacityHeight equals to UPDATED_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityHeight.equals=" + UPDATED_MAX_CAPACITY_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityHeightIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityHeight not equals to DEFAULT_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityHeight.notEquals=" + DEFAULT_MAX_CAPACITY_HEIGHT);

        // Get all the vehicleList where maxCapacityHeight not equals to UPDATED_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldBeFound("maxCapacityHeight.notEquals=" + UPDATED_MAX_CAPACITY_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityHeightIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityHeight in DEFAULT_MAX_CAPACITY_HEIGHT or UPDATED_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldBeFound("maxCapacityHeight.in=" + DEFAULT_MAX_CAPACITY_HEIGHT + "," + UPDATED_MAX_CAPACITY_HEIGHT);

        // Get all the vehicleList where maxCapacityHeight equals to UPDATED_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityHeight.in=" + UPDATED_MAX_CAPACITY_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityHeightIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityHeight is not null
        defaultVehicleShouldBeFound("maxCapacityHeight.specified=true");

        // Get all the vehicleList where maxCapacityHeight is null
        defaultVehicleShouldNotBeFound("maxCapacityHeight.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityHeightIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityHeight is greater than or equal to DEFAULT_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldBeFound("maxCapacityHeight.greaterThanOrEqual=" + DEFAULT_MAX_CAPACITY_HEIGHT);

        // Get all the vehicleList where maxCapacityHeight is greater than or equal to UPDATED_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityHeight.greaterThanOrEqual=" + UPDATED_MAX_CAPACITY_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityHeightIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityHeight is less than or equal to DEFAULT_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldBeFound("maxCapacityHeight.lessThanOrEqual=" + DEFAULT_MAX_CAPACITY_HEIGHT);

        // Get all the vehicleList where maxCapacityHeight is less than or equal to SMALLER_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityHeight.lessThanOrEqual=" + SMALLER_MAX_CAPACITY_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityHeightIsLessThanSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityHeight is less than DEFAULT_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityHeight.lessThan=" + DEFAULT_MAX_CAPACITY_HEIGHT);

        // Get all the vehicleList where maxCapacityHeight is less than UPDATED_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldBeFound("maxCapacityHeight.lessThan=" + UPDATED_MAX_CAPACITY_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityHeightIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityHeight is greater than DEFAULT_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityHeight.greaterThan=" + DEFAULT_MAX_CAPACITY_HEIGHT);

        // Get all the vehicleList where maxCapacityHeight is greater than SMALLER_MAX_CAPACITY_HEIGHT
        defaultVehicleShouldBeFound("maxCapacityHeight.greaterThan=" + SMALLER_MAX_CAPACITY_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWeightIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWeight equals to DEFAULT_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldBeFound("maxCapacityWeight.equals=" + DEFAULT_MAX_CAPACITY_WEIGHT);

        // Get all the vehicleList where maxCapacityWeight equals to UPDATED_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityWeight.equals=" + UPDATED_MAX_CAPACITY_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWeightIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWeight not equals to DEFAULT_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityWeight.notEquals=" + DEFAULT_MAX_CAPACITY_WEIGHT);

        // Get all the vehicleList where maxCapacityWeight not equals to UPDATED_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldBeFound("maxCapacityWeight.notEquals=" + UPDATED_MAX_CAPACITY_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWeightIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWeight in DEFAULT_MAX_CAPACITY_WEIGHT or UPDATED_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldBeFound("maxCapacityWeight.in=" + DEFAULT_MAX_CAPACITY_WEIGHT + "," + UPDATED_MAX_CAPACITY_WEIGHT);

        // Get all the vehicleList where maxCapacityWeight equals to UPDATED_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityWeight.in=" + UPDATED_MAX_CAPACITY_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWeightIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWeight is not null
        defaultVehicleShouldBeFound("maxCapacityWeight.specified=true");

        // Get all the vehicleList where maxCapacityWeight is null
        defaultVehicleShouldNotBeFound("maxCapacityWeight.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWeightIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWeight is greater than or equal to DEFAULT_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldBeFound("maxCapacityWeight.greaterThanOrEqual=" + DEFAULT_MAX_CAPACITY_WEIGHT);

        // Get all the vehicleList where maxCapacityWeight is greater than or equal to UPDATED_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityWeight.greaterThanOrEqual=" + UPDATED_MAX_CAPACITY_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWeightIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWeight is less than or equal to DEFAULT_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldBeFound("maxCapacityWeight.lessThanOrEqual=" + DEFAULT_MAX_CAPACITY_WEIGHT);

        // Get all the vehicleList where maxCapacityWeight is less than or equal to SMALLER_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityWeight.lessThanOrEqual=" + SMALLER_MAX_CAPACITY_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWeightIsLessThanSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWeight is less than DEFAULT_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityWeight.lessThan=" + DEFAULT_MAX_CAPACITY_WEIGHT);

        // Get all the vehicleList where maxCapacityWeight is less than UPDATED_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldBeFound("maxCapacityWeight.lessThan=" + UPDATED_MAX_CAPACITY_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByMaxCapacityWeightIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where maxCapacityWeight is greater than DEFAULT_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldNotBeFound("maxCapacityWeight.greaterThan=" + DEFAULT_MAX_CAPACITY_WEIGHT);

        // Get all the vehicleList where maxCapacityWeight is greater than SMALLER_MAX_CAPACITY_WEIGHT
        defaultVehicleShouldBeFound("maxCapacityWeight.greaterThan=" + SMALLER_MAX_CAPACITY_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByActiveIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where active equals to DEFAULT_ACTIVE
        defaultVehicleShouldBeFound("active.equals=" + DEFAULT_ACTIVE);

        // Get all the vehicleList where active equals to UPDATED_ACTIVE
        defaultVehicleShouldNotBeFound("active.equals=" + UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllVehiclesByActiveIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where active not equals to DEFAULT_ACTIVE
        defaultVehicleShouldNotBeFound("active.notEquals=" + DEFAULT_ACTIVE);

        // Get all the vehicleList where active not equals to UPDATED_ACTIVE
        defaultVehicleShouldBeFound("active.notEquals=" + UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllVehiclesByActiveIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where active in DEFAULT_ACTIVE or UPDATED_ACTIVE
        defaultVehicleShouldBeFound("active.in=" + DEFAULT_ACTIVE + "," + UPDATED_ACTIVE);

        // Get all the vehicleList where active equals to UPDATED_ACTIVE
        defaultVehicleShouldNotBeFound("active.in=" + UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllVehiclesByActiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where active is not null
        defaultVehicleShouldBeFound("active.specified=true");

        // Get all the vehicleList where active is null
        defaultVehicleShouldNotBeFound("active.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByTypeIsEqualToSomething() throws Exception {
        // Get already existing entity
        VehicleType type = vehicle.getType();
        vehicleRepository.saveAndFlush(vehicle);
        Long typeId = type.getId();

        // Get all the vehicleList where type equals to typeId
        defaultVehicleShouldBeFound("typeId.equals=" + typeId);

        // Get all the vehicleList where type equals to typeId + 1
        defaultVehicleShouldNotBeFound("typeId.equals=" + (typeId + 1));
    }

    @Test
    @Transactional
    public void getAllVehiclesByDeliveryManIsEqualToSomething() throws Exception {
        // Get already existing entity
        UserAccount deliveryMan = vehicle.getDeliveryMan();
        vehicleRepository.saveAndFlush(vehicle);
        Long deliveryManId = deliveryMan.getId();

        // Get all the vehicleList where deliveryMan equals to deliveryManId
        defaultVehicleShouldBeFound("deliveryManId.equals=" + deliveryManId);

        // Get all the vehicleList where deliveryMan equals to deliveryManId + 1
        defaultVehicleShouldNotBeFound("deliveryManId.equals=" + (deliveryManId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultVehicleShouldBeFound(String filter) throws Exception {
        restVehicleMockMvc
            .perform(get("/api/vehicles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicle.getId().intValue())))
            .andExpect(jsonPath("$.[*].maxCapacityLength").value(hasItem(DEFAULT_MAX_CAPACITY_LENGTH.doubleValue())))
            .andExpect(jsonPath("$.[*].maxCapacityWidth").value(hasItem(DEFAULT_MAX_CAPACITY_WIDTH.doubleValue())))
            .andExpect(jsonPath("$.[*].maxCapacityHeight").value(hasItem(DEFAULT_MAX_CAPACITY_HEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].maxCapacityWeight").value(hasItem(DEFAULT_MAX_CAPACITY_WEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));

        // Check, that the count call also returns 1
        restVehicleMockMvc
            .perform(get("/api/vehicles/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultVehicleShouldNotBeFound(String filter) throws Exception {
        restVehicleMockMvc
            .perform(get("/api/vehicles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restVehicleMockMvc
            .perform(get("/api/vehicles/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingVehicle() throws Exception {
        // Get the vehicle
        restVehicleMockMvc.perform(get("/api/vehicles/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVehicle() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        int databaseSizeBeforeUpdate = vehicleRepository.findAll().size();

        // Update the vehicle
        Vehicle updatedVehicle = vehicleRepository.findById(vehicle.getId()).get();
        // Disconnect from session so that the updates on updatedVehicle are not directly saved in db
        em.detach(updatedVehicle);
        updatedVehicle
            .maxCapacityLength(UPDATED_MAX_CAPACITY_LENGTH)
            .maxCapacityWidth(UPDATED_MAX_CAPACITY_WIDTH)
            .maxCapacityHeight(UPDATED_MAX_CAPACITY_HEIGHT)
            .maxCapacityWeight(UPDATED_MAX_CAPACITY_WEIGHT)
            .active(UPDATED_ACTIVE);
        VehicleDTO vehicleDTO = vehicleMapper.toDto(updatedVehicle);

        restVehicleMockMvc
            .perform(put("/api/vehicles").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleDTO)))
            .andExpect(status().isOk());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeUpdate);
        Vehicle testVehicle = vehicleList.get(vehicleList.size() - 1);
        assertThat(testVehicle.getMaxCapacityLength()).isEqualTo(UPDATED_MAX_CAPACITY_LENGTH);
        assertThat(testVehicle.getMaxCapacityWidth()).isEqualTo(UPDATED_MAX_CAPACITY_WIDTH);
        assertThat(testVehicle.getMaxCapacityHeight()).isEqualTo(UPDATED_MAX_CAPACITY_HEIGHT);
        assertThat(testVehicle.getMaxCapacityWeight()).isEqualTo(UPDATED_MAX_CAPACITY_WEIGHT);
        assertThat(testVehicle.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingVehicle() throws Exception {
        int databaseSizeBeforeUpdate = vehicleRepository.findAll().size();

        // Create the Vehicle
        VehicleDTO vehicleDTO = vehicleMapper.toDto(vehicle);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVehicleMockMvc
            .perform(put("/api/vehicles").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vehicleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVehicle() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        int databaseSizeBeforeDelete = vehicleRepository.findAll().size();

        // Delete the vehicle
        restVehicleMockMvc
            .perform(delete("/api/vehicles/{id}", vehicle.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
