package fr.insy2s.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.Address;
import fr.insy2s.domain.Company;
import fr.insy2s.domain.User;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.UserAccountRepository;
import fr.insy2s.service.UserAccountQueryService;
import fr.insy2s.service.UserAccountService;
import fr.insy2s.service.dto.UserAccountCriteria;
import fr.insy2s.service.dto.UserAccountDTO;
import fr.insy2s.service.mapper.UserAccountMapper;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link UserAccountResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class UserAccountResourceIT {
    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final Boolean DEFAULT_AVAILABLE = false;
    private static final Boolean UPDATED_AVAILABLE = true;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Mock
    private UserAccountRepository userAccountRepositoryMock;

    @Autowired
    private UserAccountMapper userAccountMapper;

    @Mock
    private UserAccountService userAccountServiceMock;

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private UserAccountQueryService userAccountQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserAccountMockMvc;

    private UserAccount userAccount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserAccount createEntity(EntityManager em) {
        UserAccount userAccount = new UserAccount().phoneNumber(DEFAULT_PHONE_NUMBER).available(DEFAULT_AVAILABLE);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        userAccount.setUser(user);
        // Add required entity
        Address address;
        if (TestUtil.findAll(em, Address.class).isEmpty()) {
            address = AddressResourceIT.createEntity(em);
            em.persist(address);
            em.flush();
        } else {
            address = TestUtil.findAll(em, Address.class).get(0);
        }
        userAccount.setAdresse(address);
        return userAccount;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserAccount createUpdatedEntity(EntityManager em) {
        UserAccount userAccount = new UserAccount().phoneNumber(UPDATED_PHONE_NUMBER).available(UPDATED_AVAILABLE);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        userAccount.setUser(user);
        // Add required entity
        Address address;
        if (TestUtil.findAll(em, Address.class).isEmpty()) {
            address = AddressResourceIT.createUpdatedEntity(em);
            em.persist(address);
            em.flush();
        } else {
            address = TestUtil.findAll(em, Address.class).get(0);
        }
        userAccount.setAdresse(address);
        return userAccount;
    }

    @BeforeEach
    public void initTest() {
        userAccount = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserAccount() throws Exception {
        int databaseSizeBeforeCreate = userAccountRepository.findAll().size();
        // Create the UserAccount
        UserAccountDTO userAccountDTO = userAccountMapper.toDto(userAccount);
        restUserAccountMockMvc
            .perform(
                post("/api/user-accounts")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userAccountDTO))
            )
            .andExpect(status().isCreated());

        // Validate the UserAccount in the database
        List<UserAccount> userAccountList = userAccountRepository.findAll();
        assertThat(userAccountList).hasSize(databaseSizeBeforeCreate + 1);
        UserAccount testUserAccount = userAccountList.get(userAccountList.size() - 1);
        assertThat(testUserAccount.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testUserAccount.isAvailable()).isEqualTo(DEFAULT_AVAILABLE);
    }

    @Test
    @Transactional
    public void createUserAccountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userAccountRepository.findAll().size();

        // Create the UserAccount with an existing ID
        userAccount.setId(1L);
        UserAccountDTO userAccountDTO = userAccountMapper.toDto(userAccount);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserAccountMockMvc
            .perform(
                post("/api/user-accounts")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserAccount in the database
        List<UserAccount> userAccountList = userAccountRepository.findAll();
        assertThat(userAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPhoneNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = userAccountRepository.findAll().size();
        // set the field null
        userAccount.setPhoneNumber(null);

        // Create the UserAccount, which fails.
        UserAccountDTO userAccountDTO = userAccountMapper.toDto(userAccount);

        restUserAccountMockMvc
            .perform(
                post("/api/user-accounts")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userAccountDTO))
            )
            .andExpect(status().isBadRequest());

        List<UserAccount> userAccountList = userAccountRepository.findAll();
        assertThat(userAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAvailableIsRequired() throws Exception {
        int databaseSizeBeforeTest = userAccountRepository.findAll().size();
        // set the field null
        userAccount.setAvailable(null);

        // Create the UserAccount, which fails.
        UserAccountDTO userAccountDTO = userAccountMapper.toDto(userAccount);

        restUserAccountMockMvc
            .perform(
                post("/api/user-accounts")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userAccountDTO))
            )
            .andExpect(status().isBadRequest());

        List<UserAccount> userAccountList = userAccountRepository.findAll();
        assertThat(userAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserAccounts() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList
        restUserAccountMockMvc
            .perform(get("/api/user-accounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].available").value(hasItem(DEFAULT_AVAILABLE.booleanValue())));
    }

    @SuppressWarnings({ "unchecked" })
    public void getAllUserAccountsWithEagerRelationshipsIsEnabled() throws Exception {
        when(userAccountServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restUserAccountMockMvc.perform(get("/api/user-accounts?eagerload=true")).andExpect(status().isOk());

        verify(userAccountServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    public void getAllUserAccountsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(userAccountServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restUserAccountMockMvc.perform(get("/api/user-accounts?eagerload=true")).andExpect(status().isOk());

        verify(userAccountServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getUserAccount() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get the userAccount
        restUserAccountMockMvc
            .perform(get("/api/user-accounts/{id}", userAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userAccount.getId().intValue()))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.available").value(DEFAULT_AVAILABLE.booleanValue()));
    }

    @Test
    @Transactional
    public void getUserAccountsByIdFiltering() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        Long id = userAccount.getId();

        defaultUserAccountShouldBeFound("id.equals=" + id);
        defaultUserAccountShouldNotBeFound("id.notEquals=" + id);

        defaultUserAccountShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUserAccountShouldNotBeFound("id.greaterThan=" + id);

        defaultUserAccountShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUserAccountShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllUserAccountsByPhoneNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList where phoneNumber equals to DEFAULT_PHONE_NUMBER
        defaultUserAccountShouldBeFound("phoneNumber.equals=" + DEFAULT_PHONE_NUMBER);

        // Get all the userAccountList where phoneNumber equals to UPDATED_PHONE_NUMBER
        defaultUserAccountShouldNotBeFound("phoneNumber.equals=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllUserAccountsByPhoneNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList where phoneNumber not equals to DEFAULT_PHONE_NUMBER
        defaultUserAccountShouldNotBeFound("phoneNumber.notEquals=" + DEFAULT_PHONE_NUMBER);

        // Get all the userAccountList where phoneNumber not equals to UPDATED_PHONE_NUMBER
        defaultUserAccountShouldBeFound("phoneNumber.notEquals=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllUserAccountsByPhoneNumberIsInShouldWork() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList where phoneNumber in DEFAULT_PHONE_NUMBER or UPDATED_PHONE_NUMBER
        defaultUserAccountShouldBeFound("phoneNumber.in=" + DEFAULT_PHONE_NUMBER + "," + UPDATED_PHONE_NUMBER);

        // Get all the userAccountList where phoneNumber equals to UPDATED_PHONE_NUMBER
        defaultUserAccountShouldNotBeFound("phoneNumber.in=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllUserAccountsByPhoneNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList where phoneNumber is not null
        defaultUserAccountShouldBeFound("phoneNumber.specified=true");

        // Get all the userAccountList where phoneNumber is null
        defaultUserAccountShouldNotBeFound("phoneNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserAccountsByPhoneNumberContainsSomething() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList where phoneNumber contains DEFAULT_PHONE_NUMBER
        defaultUserAccountShouldBeFound("phoneNumber.contains=" + DEFAULT_PHONE_NUMBER);

        // Get all the userAccountList where phoneNumber contains UPDATED_PHONE_NUMBER
        defaultUserAccountShouldNotBeFound("phoneNumber.contains=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllUserAccountsByPhoneNumberNotContainsSomething() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList where phoneNumber does not contain DEFAULT_PHONE_NUMBER
        defaultUserAccountShouldNotBeFound("phoneNumber.doesNotContain=" + DEFAULT_PHONE_NUMBER);

        // Get all the userAccountList where phoneNumber does not contain UPDATED_PHONE_NUMBER
        defaultUserAccountShouldBeFound("phoneNumber.doesNotContain=" + UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllUserAccountsByAvailableIsEqualToSomething() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList where available equals to DEFAULT_AVAILABLE
        defaultUserAccountShouldBeFound("available.equals=" + DEFAULT_AVAILABLE);

        // Get all the userAccountList where available equals to UPDATED_AVAILABLE
        defaultUserAccountShouldNotBeFound("available.equals=" + UPDATED_AVAILABLE);
    }

    @Test
    @Transactional
    public void getAllUserAccountsByAvailableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList where available not equals to DEFAULT_AVAILABLE
        defaultUserAccountShouldNotBeFound("available.notEquals=" + DEFAULT_AVAILABLE);

        // Get all the userAccountList where available not equals to UPDATED_AVAILABLE
        defaultUserAccountShouldBeFound("available.notEquals=" + UPDATED_AVAILABLE);
    }

    @Test
    @Transactional
    public void getAllUserAccountsByAvailableIsInShouldWork() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList where available in DEFAULT_AVAILABLE or UPDATED_AVAILABLE
        defaultUserAccountShouldBeFound("available.in=" + DEFAULT_AVAILABLE + "," + UPDATED_AVAILABLE);

        // Get all the userAccountList where available equals to UPDATED_AVAILABLE
        defaultUserAccountShouldNotBeFound("available.in=" + UPDATED_AVAILABLE);
    }

    @Test
    @Transactional
    public void getAllUserAccountsByAvailableIsNullOrNotNull() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        // Get all the userAccountList where available is not null
        defaultUserAccountShouldBeFound("available.specified=true");

        // Get all the userAccountList where available is null
        defaultUserAccountShouldNotBeFound("available.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserAccountsByUserIsEqualToSomething() throws Exception {
        // Get already existing entity
        User user = userAccount.getUser();
        userAccountRepository.saveAndFlush(userAccount);
        Long userId = user.getId();

        // Get all the userAccountList where user equals to userId
        defaultUserAccountShouldBeFound("userId.equals=" + userId);

        // Get all the userAccountList where user equals to userId + 1
        defaultUserAccountShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    @Test
    @Transactional
    public void getAllUserAccountsByAdresseIsEqualToSomething() throws Exception {
        // Get already existing entity
        Address adresse = userAccount.getAdresse();
        userAccountRepository.saveAndFlush(userAccount);
        Long adresseId = adresse.getId();

        // Get all the userAccountList where adresse equals to adresseId
        defaultUserAccountShouldBeFound("adresseId.equals=" + adresseId);

        // Get all the userAccountList where adresse equals to adresseId + 1
        defaultUserAccountShouldNotBeFound("adresseId.equals=" + (adresseId + 1));
    }

    @Test
    @Transactional
    public void getAllUserAccountsByListBlockedIsEqualToSomething() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);
        UserAccount listBlocked = UserAccountResourceIT.createEntity(em);
        em.persist(listBlocked);
        em.flush();
        userAccount.addListBlocked(listBlocked);
        userAccountRepository.saveAndFlush(userAccount);
        Long listBlockedId = listBlocked.getId();

        // Get all the userAccountList where listBlocked equals to listBlockedId
        defaultUserAccountShouldBeFound("listBlockedId.equals=" + listBlockedId);

        // Get all the userAccountList where listBlocked equals to listBlockedId + 1
        defaultUserAccountShouldNotBeFound("listBlockedId.equals=" + (listBlockedId + 1));
    }

    @Test
    @Transactional
    public void getAllUserAccountsByCompanyIsEqualToSomething() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);
        Company company = CompanyResourceIT.createEntity(em);
        em.persist(company);
        em.flush();
        userAccount.setCompany(company);
        userAccountRepository.saveAndFlush(userAccount);
        Long companyId = company.getId();

        // Get all the userAccountList where company equals to companyId
        defaultUserAccountShouldBeFound("companyId.equals=" + companyId);

        // Get all the userAccountList where company equals to companyId + 1
        defaultUserAccountShouldNotBeFound("companyId.equals=" + (companyId + 1));
    }

    @Test
    @Transactional
    public void getAllUserAccountsByListBlockedByIsEqualToSomething() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);
        UserAccount listBlockedBy = UserAccountResourceIT.createEntity(em);
        em.persist(listBlockedBy);
        em.flush();
        userAccount.addListBlockedBy(listBlockedBy);
        userAccountRepository.saveAndFlush(userAccount);
        Long listBlockedById = listBlockedBy.getId();

        // Get all the userAccountList where listBlockedBy equals to listBlockedById
        defaultUserAccountShouldBeFound("listBlockedById.equals=" + listBlockedById);

        // Get all the userAccountList where listBlockedBy equals to listBlockedById + 1
        defaultUserAccountShouldNotBeFound("listBlockedById.equals=" + (listBlockedById + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserAccountShouldBeFound(String filter) throws Exception {
        restUserAccountMockMvc
            .perform(get("/api/user-accounts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].available").value(hasItem(DEFAULT_AVAILABLE.booleanValue())));

        // Check, that the count call also returns 1
        restUserAccountMockMvc
            .perform(get("/api/user-accounts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserAccountShouldNotBeFound(String filter) throws Exception {
        restUserAccountMockMvc
            .perform(get("/api/user-accounts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserAccountMockMvc
            .perform(get("/api/user-accounts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingUserAccount() throws Exception {
        // Get the userAccount
        restUserAccountMockMvc.perform(get("/api/user-accounts/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserAccount() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        int databaseSizeBeforeUpdate = userAccountRepository.findAll().size();

        // Update the userAccount
        UserAccount updatedUserAccount = userAccountRepository.findById(userAccount.getId()).get();
        // Disconnect from session so that the updates on updatedUserAccount are not directly saved in db
        em.detach(updatedUserAccount);
        updatedUserAccount.phoneNumber(UPDATED_PHONE_NUMBER).available(UPDATED_AVAILABLE);
        UserAccountDTO userAccountDTO = userAccountMapper.toDto(updatedUserAccount);

        restUserAccountMockMvc
            .perform(
                put("/api/user-accounts").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userAccountDTO))
            )
            .andExpect(status().isOk());

        // Validate the UserAccount in the database
        List<UserAccount> userAccountList = userAccountRepository.findAll();
        assertThat(userAccountList).hasSize(databaseSizeBeforeUpdate);
        UserAccount testUserAccount = userAccountList.get(userAccountList.size() - 1);
        assertThat(testUserAccount.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testUserAccount.isAvailable()).isEqualTo(UPDATED_AVAILABLE);
    }

    @Test
    @Transactional
    public void updateNonExistingUserAccount() throws Exception {
        int databaseSizeBeforeUpdate = userAccountRepository.findAll().size();

        // Create the UserAccount
        UserAccountDTO userAccountDTO = userAccountMapper.toDto(userAccount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserAccountMockMvc
            .perform(
                put("/api/user-accounts").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserAccount in the database
        List<UserAccount> userAccountList = userAccountRepository.findAll();
        assertThat(userAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserAccount() throws Exception {
        // Initialize the database
        userAccountRepository.saveAndFlush(userAccount);

        int databaseSizeBeforeDelete = userAccountRepository.findAll().size();

        // Delete the userAccount
        restUserAccountMockMvc
            .perform(delete("/api/user-accounts/{id}", userAccount.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserAccount> userAccountList = userAccountRepository.findAll();
        assertThat(userAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
