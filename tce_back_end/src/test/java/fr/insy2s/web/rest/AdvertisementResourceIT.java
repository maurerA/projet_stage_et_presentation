package fr.insy2s.web.rest;

import static fr.insy2s.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.Address;
import fr.insy2s.domain.Advertisement;
import fr.insy2s.domain.AdvertisementStatus;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.AdvertisementRepository;
import fr.insy2s.service.AdvertisementQueryService;
import fr.insy2s.service.AdvertisementService;
import fr.insy2s.service.dto.AdvertisementCriteria;
import fr.insy2s.service.dto.AdvertisementDTO;
import fr.insy2s.service.mapper.AdvertisementMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AdvertisementResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AdvertisementResourceIT {
    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final Float DEFAULT_WEIGHT = 1F;
    private static final Float UPDATED_WEIGHT = 2F;
    private static final Float SMALLER_WEIGHT = 1F - 1F;

    private static final Float DEFAULT_LENGTH = 1F;
    private static final Float UPDATED_LENGTH = 2F;
    private static final Float SMALLER_LENGTH = 1F - 1F;

    private static final Float DEFAULT_WIDTH = 1F;
    private static final Float UPDATED_WIDTH = 2F;
    private static final Float SMALLER_WIDTH = 1F - 1F;

    private static final Float DEFAULT_HEIGHT = 1F;
    private static final Float UPDATED_HEIGHT = 2F;
    private static final Float SMALLER_HEIGHT = 1F - 1F;

    private static final Float DEFAULT_DISTANCE_KM = 1F;
    private static final Float UPDATED_DISTANCE_KM = 2F;
    private static final Float SMALLER_DISTANCE_KM = 1F - 1F;

    private static final Boolean DEFAULT_DELIVERED = false;
    private static final Boolean UPDATED_DELIVERED = true;

    private static final ZonedDateTime DEFAULT_CREATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_CREATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_WISH_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_WISH_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_WISH_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_EFFECTIVE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_EFFECTIVE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_EFFECTIVE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private AdvertisementRepository advertisementRepository;

    @Autowired
    private AdvertisementMapper advertisementMapper;

    @Autowired
    private AdvertisementService advertisementService;

    @Autowired
    private AdvertisementQueryService advertisementQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAdvertisementMockMvc;

    private Advertisement advertisement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Advertisement createEntity(EntityManager em) {
        Advertisement advertisement = new Advertisement()
            .title(DEFAULT_TITLE)
            .weight(DEFAULT_WEIGHT)
            .length(DEFAULT_LENGTH)
            .width(DEFAULT_WIDTH)
            .height(DEFAULT_HEIGHT)
            .distanceKm(DEFAULT_DISTANCE_KM)
            .delivered(DEFAULT_DELIVERED)
            .creationDate(DEFAULT_CREATION_DATE)
            .wishDate(DEFAULT_WISH_DATE)
            .effectiveDate(DEFAULT_EFFECTIVE_DATE);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        advertisement.setCreatedBy(userAccount);
        // Add required entity
        Address address;
        if (TestUtil.findAll(em, Address.class).isEmpty()) {
            address = AddressResourceIT.createEntity(em);
            em.persist(address);
            em.flush();
        } else {
            address = TestUtil.findAll(em, Address.class).get(0);
        }
        advertisement.setStartAddress(address);
        // Add required entity
        advertisement.setArrivalAddress(address);
        return advertisement;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Advertisement createUpdatedEntity(EntityManager em) {
        Advertisement advertisement = new Advertisement()
            .title(UPDATED_TITLE)
            .weight(UPDATED_WEIGHT)
            .length(UPDATED_LENGTH)
            .width(UPDATED_WIDTH)
            .height(UPDATED_HEIGHT)
            .distanceKm(UPDATED_DISTANCE_KM)
            .delivered(UPDATED_DELIVERED)
            .creationDate(UPDATED_CREATION_DATE)
            .wishDate(UPDATED_WISH_DATE)
            .effectiveDate(UPDATED_EFFECTIVE_DATE);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createUpdatedEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        advertisement.setCreatedBy(userAccount);
        // Add required entity
        Address address;
        if (TestUtil.findAll(em, Address.class).isEmpty()) {
            address = AddressResourceIT.createUpdatedEntity(em);
            em.persist(address);
            em.flush();
        } else {
            address = TestUtil.findAll(em, Address.class).get(0);
        }
        advertisement.setStartAddress(address);
        // Add required entity
        advertisement.setArrivalAddress(address);
        return advertisement;
    }

    @BeforeEach
    public void initTest() {
        advertisement = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdvertisement() throws Exception {
        int databaseSizeBeforeCreate = advertisementRepository.findAll().size();
        // Create the Advertisement
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);
        restAdvertisementMockMvc
            .perform(
                post("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Advertisement in the database
        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeCreate + 1);
        Advertisement testAdvertisement = advertisementList.get(advertisementList.size() - 1);
        assertThat(testAdvertisement.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testAdvertisement.getWeight()).isEqualTo(DEFAULT_WEIGHT);
        assertThat(testAdvertisement.getLength()).isEqualTo(DEFAULT_LENGTH);
        assertThat(testAdvertisement.getWidth()).isEqualTo(DEFAULT_WIDTH);
        assertThat(testAdvertisement.getHeight()).isEqualTo(DEFAULT_HEIGHT);
        assertThat(testAdvertisement.getDistanceKm()).isEqualTo(DEFAULT_DISTANCE_KM);
        assertThat(testAdvertisement.isDelivered()).isEqualTo(DEFAULT_DELIVERED);
        assertThat(testAdvertisement.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testAdvertisement.getWishDate()).isEqualTo(DEFAULT_WISH_DATE);
        assertThat(testAdvertisement.getEffectiveDate()).isEqualTo(DEFAULT_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void createAdvertisementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = advertisementRepository.findAll().size();

        // Create the Advertisement with an existing ID
        advertisement.setId(1L);
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdvertisementMockMvc
            .perform(
                post("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Advertisement in the database
        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementRepository.findAll().size();
        // set the field null
        advertisement.setTitle(null);

        // Create the Advertisement, which fails.
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        restAdvertisementMockMvc
            .perform(
                post("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isBadRequest());

        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkWeightIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementRepository.findAll().size();
        // set the field null
        advertisement.setWeight(null);

        // Create the Advertisement, which fails.
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        restAdvertisementMockMvc
            .perform(
                post("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isBadRequest());

        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLengthIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementRepository.findAll().size();
        // set the field null
        advertisement.setLength(null);

        // Create the Advertisement, which fails.
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        restAdvertisementMockMvc
            .perform(
                post("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isBadRequest());

        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkWidthIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementRepository.findAll().size();
        // set the field null
        advertisement.setWidth(null);

        // Create the Advertisement, which fails.
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        restAdvertisementMockMvc
            .perform(
                post("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isBadRequest());

        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHeightIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementRepository.findAll().size();
        // set the field null
        advertisement.setHeight(null);

        // Create the Advertisement, which fails.
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        restAdvertisementMockMvc
            .perform(
                post("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isBadRequest());

        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDistanceKmIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementRepository.findAll().size();
        // set the field null
        advertisement.setDistanceKm(null);

        // Create the Advertisement, which fails.
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        restAdvertisementMockMvc
            .perform(
                post("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isBadRequest());

        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDeliveredIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementRepository.findAll().size();
        // set the field null
        advertisement.setDelivered(null);

        // Create the Advertisement, which fails.
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        restAdvertisementMockMvc
            .perform(
                post("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isBadRequest());

        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementRepository.findAll().size();
        // set the field null
        advertisement.setCreationDate(null);

        // Create the Advertisement, which fails.
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        restAdvertisementMockMvc
            .perform(
                post("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isBadRequest());

        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAdvertisements() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList
        restAdvertisementMockMvc
            .perform(get("/api/advertisements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(advertisement.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].length").value(hasItem(DEFAULT_LENGTH.doubleValue())))
            .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH.doubleValue())))
            .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].distanceKm").value(hasItem(DEFAULT_DISTANCE_KM.doubleValue())))
            .andExpect(jsonPath("$.[*].delivered").value(hasItem(DEFAULT_DELIVERED.booleanValue())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(sameInstant(DEFAULT_CREATION_DATE))))
            .andExpect(jsonPath("$.[*].wishDate").value(hasItem(sameInstant(DEFAULT_WISH_DATE))))
            .andExpect(jsonPath("$.[*].effectiveDate").value(hasItem(sameInstant(DEFAULT_EFFECTIVE_DATE))));
    }

    @Test
    @Transactional
    public void getAdvertisement() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get the advertisement
        restAdvertisementMockMvc
            .perform(get("/api/advertisements/{id}", advertisement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(advertisement.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.weight").value(DEFAULT_WEIGHT.doubleValue()))
            .andExpect(jsonPath("$.length").value(DEFAULT_LENGTH.doubleValue()))
            .andExpect(jsonPath("$.width").value(DEFAULT_WIDTH.doubleValue()))
            .andExpect(jsonPath("$.height").value(DEFAULT_HEIGHT.doubleValue()))
            .andExpect(jsonPath("$.distanceKm").value(DEFAULT_DISTANCE_KM.doubleValue()))
            .andExpect(jsonPath("$.delivered").value(DEFAULT_DELIVERED.booleanValue()))
            .andExpect(jsonPath("$.creationDate").value(sameInstant(DEFAULT_CREATION_DATE)))
            .andExpect(jsonPath("$.wishDate").value(sameInstant(DEFAULT_WISH_DATE)))
            .andExpect(jsonPath("$.effectiveDate").value(sameInstant(DEFAULT_EFFECTIVE_DATE)));
    }

    @Test
    @Transactional
    public void getAdvertisementsByIdFiltering() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        Long id = advertisement.getId();

        defaultAdvertisementShouldBeFound("id.equals=" + id);
        defaultAdvertisementShouldNotBeFound("id.notEquals=" + id);

        defaultAdvertisementShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAdvertisementShouldNotBeFound("id.greaterThan=" + id);

        defaultAdvertisementShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAdvertisementShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where title equals to DEFAULT_TITLE
        defaultAdvertisementShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the advertisementList where title equals to UPDATED_TITLE
        defaultAdvertisementShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByTitleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where title not equals to DEFAULT_TITLE
        defaultAdvertisementShouldNotBeFound("title.notEquals=" + DEFAULT_TITLE);

        // Get all the advertisementList where title not equals to UPDATED_TITLE
        defaultAdvertisementShouldBeFound("title.notEquals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultAdvertisementShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the advertisementList where title equals to UPDATED_TITLE
        defaultAdvertisementShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where title is not null
        defaultAdvertisementShouldBeFound("title.specified=true");

        // Get all the advertisementList where title is null
        defaultAdvertisementShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByTitleContainsSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where title contains DEFAULT_TITLE
        defaultAdvertisementShouldBeFound("title.contains=" + DEFAULT_TITLE);

        // Get all the advertisementList where title contains UPDATED_TITLE
        defaultAdvertisementShouldNotBeFound("title.contains=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByTitleNotContainsSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where title does not contain DEFAULT_TITLE
        defaultAdvertisementShouldNotBeFound("title.doesNotContain=" + DEFAULT_TITLE);

        // Get all the advertisementList where title does not contain UPDATED_TITLE
        defaultAdvertisementShouldBeFound("title.doesNotContain=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWeightIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where weight equals to DEFAULT_WEIGHT
        defaultAdvertisementShouldBeFound("weight.equals=" + DEFAULT_WEIGHT);

        // Get all the advertisementList where weight equals to UPDATED_WEIGHT
        defaultAdvertisementShouldNotBeFound("weight.equals=" + UPDATED_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWeightIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where weight not equals to DEFAULT_WEIGHT
        defaultAdvertisementShouldNotBeFound("weight.notEquals=" + DEFAULT_WEIGHT);

        // Get all the advertisementList where weight not equals to UPDATED_WEIGHT
        defaultAdvertisementShouldBeFound("weight.notEquals=" + UPDATED_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWeightIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where weight in DEFAULT_WEIGHT or UPDATED_WEIGHT
        defaultAdvertisementShouldBeFound("weight.in=" + DEFAULT_WEIGHT + "," + UPDATED_WEIGHT);

        // Get all the advertisementList where weight equals to UPDATED_WEIGHT
        defaultAdvertisementShouldNotBeFound("weight.in=" + UPDATED_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWeightIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where weight is not null
        defaultAdvertisementShouldBeFound("weight.specified=true");

        // Get all the advertisementList where weight is null
        defaultAdvertisementShouldNotBeFound("weight.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWeightIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where weight is greater than or equal to DEFAULT_WEIGHT
        defaultAdvertisementShouldBeFound("weight.greaterThanOrEqual=" + DEFAULT_WEIGHT);

        // Get all the advertisementList where weight is greater than or equal to UPDATED_WEIGHT
        defaultAdvertisementShouldNotBeFound("weight.greaterThanOrEqual=" + UPDATED_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWeightIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where weight is less than or equal to DEFAULT_WEIGHT
        defaultAdvertisementShouldBeFound("weight.lessThanOrEqual=" + DEFAULT_WEIGHT);

        // Get all the advertisementList where weight is less than or equal to SMALLER_WEIGHT
        defaultAdvertisementShouldNotBeFound("weight.lessThanOrEqual=" + SMALLER_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWeightIsLessThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where weight is less than DEFAULT_WEIGHT
        defaultAdvertisementShouldNotBeFound("weight.lessThan=" + DEFAULT_WEIGHT);

        // Get all the advertisementList where weight is less than UPDATED_WEIGHT
        defaultAdvertisementShouldBeFound("weight.lessThan=" + UPDATED_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWeightIsGreaterThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where weight is greater than DEFAULT_WEIGHT
        defaultAdvertisementShouldNotBeFound("weight.greaterThan=" + DEFAULT_WEIGHT);

        // Get all the advertisementList where weight is greater than SMALLER_WEIGHT
        defaultAdvertisementShouldBeFound("weight.greaterThan=" + SMALLER_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByLengthIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where length equals to DEFAULT_LENGTH
        defaultAdvertisementShouldBeFound("length.equals=" + DEFAULT_LENGTH);

        // Get all the advertisementList where length equals to UPDATED_LENGTH
        defaultAdvertisementShouldNotBeFound("length.equals=" + UPDATED_LENGTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByLengthIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where length not equals to DEFAULT_LENGTH
        defaultAdvertisementShouldNotBeFound("length.notEquals=" + DEFAULT_LENGTH);

        // Get all the advertisementList where length not equals to UPDATED_LENGTH
        defaultAdvertisementShouldBeFound("length.notEquals=" + UPDATED_LENGTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByLengthIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where length in DEFAULT_LENGTH or UPDATED_LENGTH
        defaultAdvertisementShouldBeFound("length.in=" + DEFAULT_LENGTH + "," + UPDATED_LENGTH);

        // Get all the advertisementList where length equals to UPDATED_LENGTH
        defaultAdvertisementShouldNotBeFound("length.in=" + UPDATED_LENGTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByLengthIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where length is not null
        defaultAdvertisementShouldBeFound("length.specified=true");

        // Get all the advertisementList where length is null
        defaultAdvertisementShouldNotBeFound("length.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByLengthIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where length is greater than or equal to DEFAULT_LENGTH
        defaultAdvertisementShouldBeFound("length.greaterThanOrEqual=" + DEFAULT_LENGTH);

        // Get all the advertisementList where length is greater than or equal to UPDATED_LENGTH
        defaultAdvertisementShouldNotBeFound("length.greaterThanOrEqual=" + UPDATED_LENGTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByLengthIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where length is less than or equal to DEFAULT_LENGTH
        defaultAdvertisementShouldBeFound("length.lessThanOrEqual=" + DEFAULT_LENGTH);

        // Get all the advertisementList where length is less than or equal to SMALLER_LENGTH
        defaultAdvertisementShouldNotBeFound("length.lessThanOrEqual=" + SMALLER_LENGTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByLengthIsLessThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where length is less than DEFAULT_LENGTH
        defaultAdvertisementShouldNotBeFound("length.lessThan=" + DEFAULT_LENGTH);

        // Get all the advertisementList where length is less than UPDATED_LENGTH
        defaultAdvertisementShouldBeFound("length.lessThan=" + UPDATED_LENGTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByLengthIsGreaterThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where length is greater than DEFAULT_LENGTH
        defaultAdvertisementShouldNotBeFound("length.greaterThan=" + DEFAULT_LENGTH);

        // Get all the advertisementList where length is greater than SMALLER_LENGTH
        defaultAdvertisementShouldBeFound("length.greaterThan=" + SMALLER_LENGTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWidthIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where width equals to DEFAULT_WIDTH
        defaultAdvertisementShouldBeFound("width.equals=" + DEFAULT_WIDTH);

        // Get all the advertisementList where width equals to UPDATED_WIDTH
        defaultAdvertisementShouldNotBeFound("width.equals=" + UPDATED_WIDTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWidthIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where width not equals to DEFAULT_WIDTH
        defaultAdvertisementShouldNotBeFound("width.notEquals=" + DEFAULT_WIDTH);

        // Get all the advertisementList where width not equals to UPDATED_WIDTH
        defaultAdvertisementShouldBeFound("width.notEquals=" + UPDATED_WIDTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWidthIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where width in DEFAULT_WIDTH or UPDATED_WIDTH
        defaultAdvertisementShouldBeFound("width.in=" + DEFAULT_WIDTH + "," + UPDATED_WIDTH);

        // Get all the advertisementList where width equals to UPDATED_WIDTH
        defaultAdvertisementShouldNotBeFound("width.in=" + UPDATED_WIDTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWidthIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where width is not null
        defaultAdvertisementShouldBeFound("width.specified=true");

        // Get all the advertisementList where width is null
        defaultAdvertisementShouldNotBeFound("width.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWidthIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where width is greater than or equal to DEFAULT_WIDTH
        defaultAdvertisementShouldBeFound("width.greaterThanOrEqual=" + DEFAULT_WIDTH);

        // Get all the advertisementList where width is greater than or equal to UPDATED_WIDTH
        defaultAdvertisementShouldNotBeFound("width.greaterThanOrEqual=" + UPDATED_WIDTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWidthIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where width is less than or equal to DEFAULT_WIDTH
        defaultAdvertisementShouldBeFound("width.lessThanOrEqual=" + DEFAULT_WIDTH);

        // Get all the advertisementList where width is less than or equal to SMALLER_WIDTH
        defaultAdvertisementShouldNotBeFound("width.lessThanOrEqual=" + SMALLER_WIDTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWidthIsLessThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where width is less than DEFAULT_WIDTH
        defaultAdvertisementShouldNotBeFound("width.lessThan=" + DEFAULT_WIDTH);

        // Get all the advertisementList where width is less than UPDATED_WIDTH
        defaultAdvertisementShouldBeFound("width.lessThan=" + UPDATED_WIDTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWidthIsGreaterThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where width is greater than DEFAULT_WIDTH
        defaultAdvertisementShouldNotBeFound("width.greaterThan=" + DEFAULT_WIDTH);

        // Get all the advertisementList where width is greater than SMALLER_WIDTH
        defaultAdvertisementShouldBeFound("width.greaterThan=" + SMALLER_WIDTH);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByHeightIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where height equals to DEFAULT_HEIGHT
        defaultAdvertisementShouldBeFound("height.equals=" + DEFAULT_HEIGHT);

        // Get all the advertisementList where height equals to UPDATED_HEIGHT
        defaultAdvertisementShouldNotBeFound("height.equals=" + UPDATED_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByHeightIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where height not equals to DEFAULT_HEIGHT
        defaultAdvertisementShouldNotBeFound("height.notEquals=" + DEFAULT_HEIGHT);

        // Get all the advertisementList where height not equals to UPDATED_HEIGHT
        defaultAdvertisementShouldBeFound("height.notEquals=" + UPDATED_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByHeightIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where height in DEFAULT_HEIGHT or UPDATED_HEIGHT
        defaultAdvertisementShouldBeFound("height.in=" + DEFAULT_HEIGHT + "," + UPDATED_HEIGHT);

        // Get all the advertisementList where height equals to UPDATED_HEIGHT
        defaultAdvertisementShouldNotBeFound("height.in=" + UPDATED_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByHeightIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where height is not null
        defaultAdvertisementShouldBeFound("height.specified=true");

        // Get all the advertisementList where height is null
        defaultAdvertisementShouldNotBeFound("height.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByHeightIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where height is greater than or equal to DEFAULT_HEIGHT
        defaultAdvertisementShouldBeFound("height.greaterThanOrEqual=" + DEFAULT_HEIGHT);

        // Get all the advertisementList where height is greater than or equal to UPDATED_HEIGHT
        defaultAdvertisementShouldNotBeFound("height.greaterThanOrEqual=" + UPDATED_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByHeightIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where height is less than or equal to DEFAULT_HEIGHT
        defaultAdvertisementShouldBeFound("height.lessThanOrEqual=" + DEFAULT_HEIGHT);

        // Get all the advertisementList where height is less than or equal to SMALLER_HEIGHT
        defaultAdvertisementShouldNotBeFound("height.lessThanOrEqual=" + SMALLER_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByHeightIsLessThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where height is less than DEFAULT_HEIGHT
        defaultAdvertisementShouldNotBeFound("height.lessThan=" + DEFAULT_HEIGHT);

        // Get all the advertisementList where height is less than UPDATED_HEIGHT
        defaultAdvertisementShouldBeFound("height.lessThan=" + UPDATED_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByHeightIsGreaterThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where height is greater than DEFAULT_HEIGHT
        defaultAdvertisementShouldNotBeFound("height.greaterThan=" + DEFAULT_HEIGHT);

        // Get all the advertisementList where height is greater than SMALLER_HEIGHT
        defaultAdvertisementShouldBeFound("height.greaterThan=" + SMALLER_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDistanceKmIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where distanceKm equals to DEFAULT_DISTANCE_KM
        defaultAdvertisementShouldBeFound("distanceKm.equals=" + DEFAULT_DISTANCE_KM);

        // Get all the advertisementList where distanceKm equals to UPDATED_DISTANCE_KM
        defaultAdvertisementShouldNotBeFound("distanceKm.equals=" + UPDATED_DISTANCE_KM);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDistanceKmIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where distanceKm not equals to DEFAULT_DISTANCE_KM
        defaultAdvertisementShouldNotBeFound("distanceKm.notEquals=" + DEFAULT_DISTANCE_KM);

        // Get all the advertisementList where distanceKm not equals to UPDATED_DISTANCE_KM
        defaultAdvertisementShouldBeFound("distanceKm.notEquals=" + UPDATED_DISTANCE_KM);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDistanceKmIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where distanceKm in DEFAULT_DISTANCE_KM or UPDATED_DISTANCE_KM
        defaultAdvertisementShouldBeFound("distanceKm.in=" + DEFAULT_DISTANCE_KM + "," + UPDATED_DISTANCE_KM);

        // Get all the advertisementList where distanceKm equals to UPDATED_DISTANCE_KM
        defaultAdvertisementShouldNotBeFound("distanceKm.in=" + UPDATED_DISTANCE_KM);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDistanceKmIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where distanceKm is not null
        defaultAdvertisementShouldBeFound("distanceKm.specified=true");

        // Get all the advertisementList where distanceKm is null
        defaultAdvertisementShouldNotBeFound("distanceKm.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDistanceKmIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where distanceKm is greater than or equal to DEFAULT_DISTANCE_KM
        defaultAdvertisementShouldBeFound("distanceKm.greaterThanOrEqual=" + DEFAULT_DISTANCE_KM);

        // Get all the advertisementList where distanceKm is greater than or equal to UPDATED_DISTANCE_KM
        defaultAdvertisementShouldNotBeFound("distanceKm.greaterThanOrEqual=" + UPDATED_DISTANCE_KM);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDistanceKmIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where distanceKm is less than or equal to DEFAULT_DISTANCE_KM
        defaultAdvertisementShouldBeFound("distanceKm.lessThanOrEqual=" + DEFAULT_DISTANCE_KM);

        // Get all the advertisementList where distanceKm is less than or equal to SMALLER_DISTANCE_KM
        defaultAdvertisementShouldNotBeFound("distanceKm.lessThanOrEqual=" + SMALLER_DISTANCE_KM);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDistanceKmIsLessThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where distanceKm is less than DEFAULT_DISTANCE_KM
        defaultAdvertisementShouldNotBeFound("distanceKm.lessThan=" + DEFAULT_DISTANCE_KM);

        // Get all the advertisementList where distanceKm is less than UPDATED_DISTANCE_KM
        defaultAdvertisementShouldBeFound("distanceKm.lessThan=" + UPDATED_DISTANCE_KM);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDistanceKmIsGreaterThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where distanceKm is greater than DEFAULT_DISTANCE_KM
        defaultAdvertisementShouldNotBeFound("distanceKm.greaterThan=" + DEFAULT_DISTANCE_KM);

        // Get all the advertisementList where distanceKm is greater than SMALLER_DISTANCE_KM
        defaultAdvertisementShouldBeFound("distanceKm.greaterThan=" + SMALLER_DISTANCE_KM);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDeliveredIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where delivered equals to DEFAULT_DELIVERED
        defaultAdvertisementShouldBeFound("delivered.equals=" + DEFAULT_DELIVERED);

        // Get all the advertisementList where delivered equals to UPDATED_DELIVERED
        defaultAdvertisementShouldNotBeFound("delivered.equals=" + UPDATED_DELIVERED);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDeliveredIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where delivered not equals to DEFAULT_DELIVERED
        defaultAdvertisementShouldNotBeFound("delivered.notEquals=" + DEFAULT_DELIVERED);

        // Get all the advertisementList where delivered not equals to UPDATED_DELIVERED
        defaultAdvertisementShouldBeFound("delivered.notEquals=" + UPDATED_DELIVERED);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDeliveredIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where delivered in DEFAULT_DELIVERED or UPDATED_DELIVERED
        defaultAdvertisementShouldBeFound("delivered.in=" + DEFAULT_DELIVERED + "," + UPDATED_DELIVERED);

        // Get all the advertisementList where delivered equals to UPDATED_DELIVERED
        defaultAdvertisementShouldNotBeFound("delivered.in=" + UPDATED_DELIVERED);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByDeliveredIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where delivered is not null
        defaultAdvertisementShouldBeFound("delivered.specified=true");

        // Get all the advertisementList where delivered is null
        defaultAdvertisementShouldNotBeFound("delivered.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByCreationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where creationDate equals to DEFAULT_CREATION_DATE
        defaultAdvertisementShouldBeFound("creationDate.equals=" + DEFAULT_CREATION_DATE);

        // Get all the advertisementList where creationDate equals to UPDATED_CREATION_DATE
        defaultAdvertisementShouldNotBeFound("creationDate.equals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByCreationDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where creationDate not equals to DEFAULT_CREATION_DATE
        defaultAdvertisementShouldNotBeFound("creationDate.notEquals=" + DEFAULT_CREATION_DATE);

        // Get all the advertisementList where creationDate not equals to UPDATED_CREATION_DATE
        defaultAdvertisementShouldBeFound("creationDate.notEquals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByCreationDateIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where creationDate in DEFAULT_CREATION_DATE or UPDATED_CREATION_DATE
        defaultAdvertisementShouldBeFound("creationDate.in=" + DEFAULT_CREATION_DATE + "," + UPDATED_CREATION_DATE);

        // Get all the advertisementList where creationDate equals to UPDATED_CREATION_DATE
        defaultAdvertisementShouldNotBeFound("creationDate.in=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByCreationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where creationDate is not null
        defaultAdvertisementShouldBeFound("creationDate.specified=true");

        // Get all the advertisementList where creationDate is null
        defaultAdvertisementShouldNotBeFound("creationDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByCreationDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where creationDate is greater than or equal to DEFAULT_CREATION_DATE
        defaultAdvertisementShouldBeFound("creationDate.greaterThanOrEqual=" + DEFAULT_CREATION_DATE);

        // Get all the advertisementList where creationDate is greater than or equal to UPDATED_CREATION_DATE
        defaultAdvertisementShouldNotBeFound("creationDate.greaterThanOrEqual=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByCreationDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where creationDate is less than or equal to DEFAULT_CREATION_DATE
        defaultAdvertisementShouldBeFound("creationDate.lessThanOrEqual=" + DEFAULT_CREATION_DATE);

        // Get all the advertisementList where creationDate is less than or equal to SMALLER_CREATION_DATE
        defaultAdvertisementShouldNotBeFound("creationDate.lessThanOrEqual=" + SMALLER_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByCreationDateIsLessThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where creationDate is less than DEFAULT_CREATION_DATE
        defaultAdvertisementShouldNotBeFound("creationDate.lessThan=" + DEFAULT_CREATION_DATE);

        // Get all the advertisementList where creationDate is less than UPDATED_CREATION_DATE
        defaultAdvertisementShouldBeFound("creationDate.lessThan=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByCreationDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where creationDate is greater than DEFAULT_CREATION_DATE
        defaultAdvertisementShouldNotBeFound("creationDate.greaterThan=" + DEFAULT_CREATION_DATE);

        // Get all the advertisementList where creationDate is greater than SMALLER_CREATION_DATE
        defaultAdvertisementShouldBeFound("creationDate.greaterThan=" + SMALLER_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWishDateIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where wishDate equals to DEFAULT_WISH_DATE
        defaultAdvertisementShouldBeFound("wishDate.equals=" + DEFAULT_WISH_DATE);

        // Get all the advertisementList where wishDate equals to UPDATED_WISH_DATE
        defaultAdvertisementShouldNotBeFound("wishDate.equals=" + UPDATED_WISH_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWishDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where wishDate not equals to DEFAULT_WISH_DATE
        defaultAdvertisementShouldNotBeFound("wishDate.notEquals=" + DEFAULT_WISH_DATE);

        // Get all the advertisementList where wishDate not equals to UPDATED_WISH_DATE
        defaultAdvertisementShouldBeFound("wishDate.notEquals=" + UPDATED_WISH_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWishDateIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where wishDate in DEFAULT_WISH_DATE or UPDATED_WISH_DATE
        defaultAdvertisementShouldBeFound("wishDate.in=" + DEFAULT_WISH_DATE + "," + UPDATED_WISH_DATE);

        // Get all the advertisementList where wishDate equals to UPDATED_WISH_DATE
        defaultAdvertisementShouldNotBeFound("wishDate.in=" + UPDATED_WISH_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWishDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where wishDate is not null
        defaultAdvertisementShouldBeFound("wishDate.specified=true");

        // Get all the advertisementList where wishDate is null
        defaultAdvertisementShouldNotBeFound("wishDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWishDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where wishDate is greater than or equal to DEFAULT_WISH_DATE
        defaultAdvertisementShouldBeFound("wishDate.greaterThanOrEqual=" + DEFAULT_WISH_DATE);

        // Get all the advertisementList where wishDate is greater than or equal to UPDATED_WISH_DATE
        defaultAdvertisementShouldNotBeFound("wishDate.greaterThanOrEqual=" + UPDATED_WISH_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWishDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where wishDate is less than or equal to DEFAULT_WISH_DATE
        defaultAdvertisementShouldBeFound("wishDate.lessThanOrEqual=" + DEFAULT_WISH_DATE);

        // Get all the advertisementList where wishDate is less than or equal to SMALLER_WISH_DATE
        defaultAdvertisementShouldNotBeFound("wishDate.lessThanOrEqual=" + SMALLER_WISH_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWishDateIsLessThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where wishDate is less than DEFAULT_WISH_DATE
        defaultAdvertisementShouldNotBeFound("wishDate.lessThan=" + DEFAULT_WISH_DATE);

        // Get all the advertisementList where wishDate is less than UPDATED_WISH_DATE
        defaultAdvertisementShouldBeFound("wishDate.lessThan=" + UPDATED_WISH_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByWishDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where wishDate is greater than DEFAULT_WISH_DATE
        defaultAdvertisementShouldNotBeFound("wishDate.greaterThan=" + DEFAULT_WISH_DATE);

        // Get all the advertisementList where wishDate is greater than SMALLER_WISH_DATE
        defaultAdvertisementShouldBeFound("wishDate.greaterThan=" + SMALLER_WISH_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByEffectiveDateIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where effectiveDate equals to DEFAULT_EFFECTIVE_DATE
        defaultAdvertisementShouldBeFound("effectiveDate.equals=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the advertisementList where effectiveDate equals to UPDATED_EFFECTIVE_DATE
        defaultAdvertisementShouldNotBeFound("effectiveDate.equals=" + UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByEffectiveDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where effectiveDate not equals to DEFAULT_EFFECTIVE_DATE
        defaultAdvertisementShouldNotBeFound("effectiveDate.notEquals=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the advertisementList where effectiveDate not equals to UPDATED_EFFECTIVE_DATE
        defaultAdvertisementShouldBeFound("effectiveDate.notEquals=" + UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByEffectiveDateIsInShouldWork() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where effectiveDate in DEFAULT_EFFECTIVE_DATE or UPDATED_EFFECTIVE_DATE
        defaultAdvertisementShouldBeFound("effectiveDate.in=" + DEFAULT_EFFECTIVE_DATE + "," + UPDATED_EFFECTIVE_DATE);

        // Get all the advertisementList where effectiveDate equals to UPDATED_EFFECTIVE_DATE
        defaultAdvertisementShouldNotBeFound("effectiveDate.in=" + UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByEffectiveDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where effectiveDate is not null
        defaultAdvertisementShouldBeFound("effectiveDate.specified=true");

        // Get all the advertisementList where effectiveDate is null
        defaultAdvertisementShouldNotBeFound("effectiveDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByEffectiveDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where effectiveDate is greater than or equal to DEFAULT_EFFECTIVE_DATE
        defaultAdvertisementShouldBeFound("effectiveDate.greaterThanOrEqual=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the advertisementList where effectiveDate is greater than or equal to UPDATED_EFFECTIVE_DATE
        defaultAdvertisementShouldNotBeFound("effectiveDate.greaterThanOrEqual=" + UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByEffectiveDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where effectiveDate is less than or equal to DEFAULT_EFFECTIVE_DATE
        defaultAdvertisementShouldBeFound("effectiveDate.lessThanOrEqual=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the advertisementList where effectiveDate is less than or equal to SMALLER_EFFECTIVE_DATE
        defaultAdvertisementShouldNotBeFound("effectiveDate.lessThanOrEqual=" + SMALLER_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByEffectiveDateIsLessThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where effectiveDate is less than DEFAULT_EFFECTIVE_DATE
        defaultAdvertisementShouldNotBeFound("effectiveDate.lessThan=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the advertisementList where effectiveDate is less than UPDATED_EFFECTIVE_DATE
        defaultAdvertisementShouldBeFound("effectiveDate.lessThan=" + UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByEffectiveDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList where effectiveDate is greater than DEFAULT_EFFECTIVE_DATE
        defaultAdvertisementShouldNotBeFound("effectiveDate.greaterThan=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the advertisementList where effectiveDate is greater than SMALLER_EFFECTIVE_DATE
        defaultAdvertisementShouldBeFound("effectiveDate.greaterThan=" + SMALLER_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByCreatedByIsEqualToSomething() throws Exception {
        // Get already existing entity
        UserAccount createdBy = advertisement.getCreatedBy();
        advertisementRepository.saveAndFlush(advertisement);
        Long createdById = createdBy.getId();

        // Get all the advertisementList where createdBy equals to createdById
        defaultAdvertisementShouldBeFound("createdById.equals=" + createdById);

        // Get all the advertisementList where createdBy equals to createdById + 1
        defaultAdvertisementShouldNotBeFound("createdById.equals=" + (createdById + 1));
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);
        AdvertisementStatus status = AdvertisementStatusResourceIT.createEntity(em);
        em.persist(status);
        em.flush();
        advertisement.setStatus(status);
        advertisementRepository.saveAndFlush(advertisement);
        Long statusId = status.getId();

        // Get all the advertisementList where status equals to statusId
        defaultAdvertisementShouldBeFound("statusId.equals=" + statusId);

        // Get all the advertisementList where status equals to statusId + 1
        defaultAdvertisementShouldNotBeFound("statusId.equals=" + (statusId + 1));
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByStartAddressIsEqualToSomething() throws Exception {
        // Get already existing entity
        Address startAddress = advertisement.getStartAddress();
        advertisementRepository.saveAndFlush(advertisement);
        Long startAddressId = startAddress.getId();

        // Get all the advertisementList where startAddress equals to startAddressId
        defaultAdvertisementShouldBeFound("startAddressId.equals=" + startAddressId);

        // Get all the advertisementList where startAddress equals to startAddressId + 1
        defaultAdvertisementShouldNotBeFound("startAddressId.equals=" + (startAddressId + 1));
    }

    @Test
    @Transactional
    public void getAllAdvertisementsByArrivalAddressIsEqualToSomething() throws Exception {
        // Get already existing entity
        Address arrivalAddress = advertisement.getArrivalAddress();
        advertisementRepository.saveAndFlush(advertisement);
        Long arrivalAddressId = arrivalAddress.getId();

        // Get all the advertisementList where arrivalAddress equals to arrivalAddressId
        defaultAdvertisementShouldBeFound("arrivalAddressId.equals=" + arrivalAddressId);

        // Get all the advertisementList where arrivalAddress equals to arrivalAddressId + 1
        defaultAdvertisementShouldNotBeFound("arrivalAddressId.equals=" + (arrivalAddressId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAdvertisementShouldBeFound(String filter) throws Exception {
        restAdvertisementMockMvc
            .perform(get("/api/advertisements?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(advertisement.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].length").value(hasItem(DEFAULT_LENGTH.doubleValue())))
            .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH.doubleValue())))
            .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].distanceKm").value(hasItem(DEFAULT_DISTANCE_KM.doubleValue())))
            .andExpect(jsonPath("$.[*].delivered").value(hasItem(DEFAULT_DELIVERED.booleanValue())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(sameInstant(DEFAULT_CREATION_DATE))))
            .andExpect(jsonPath("$.[*].wishDate").value(hasItem(sameInstant(DEFAULT_WISH_DATE))))
            .andExpect(jsonPath("$.[*].effectiveDate").value(hasItem(sameInstant(DEFAULT_EFFECTIVE_DATE))));

        // Check, that the count call also returns 1
        restAdvertisementMockMvc
            .perform(get("/api/advertisements/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAdvertisementShouldNotBeFound(String filter) throws Exception {
        restAdvertisementMockMvc
            .perform(get("/api/advertisements?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAdvertisementMockMvc
            .perform(get("/api/advertisements/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingAdvertisement() throws Exception {
        // Get the advertisement
        restAdvertisementMockMvc.perform(get("/api/advertisements/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdvertisement() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        int databaseSizeBeforeUpdate = advertisementRepository.findAll().size();

        // Update the advertisement
        Advertisement updatedAdvertisement = advertisementRepository.findById(advertisement.getId()).get();
        // Disconnect from session so that the updates on updatedAdvertisement are not directly saved in db
        em.detach(updatedAdvertisement);
        updatedAdvertisement
            .title(UPDATED_TITLE)
            .weight(UPDATED_WEIGHT)
            .length(UPDATED_LENGTH)
            .width(UPDATED_WIDTH)
            .height(UPDATED_HEIGHT)
            .distanceKm(UPDATED_DISTANCE_KM)
            .delivered(UPDATED_DELIVERED)
            .creationDate(UPDATED_CREATION_DATE)
            .wishDate(UPDATED_WISH_DATE)
            .effectiveDate(UPDATED_EFFECTIVE_DATE);
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(updatedAdvertisement);

        restAdvertisementMockMvc
            .perform(
                put("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isOk());

        // Validate the Advertisement in the database
        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeUpdate);
        Advertisement testAdvertisement = advertisementList.get(advertisementList.size() - 1);
        assertThat(testAdvertisement.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testAdvertisement.getWeight()).isEqualTo(UPDATED_WEIGHT);
        assertThat(testAdvertisement.getLength()).isEqualTo(UPDATED_LENGTH);
        assertThat(testAdvertisement.getWidth()).isEqualTo(UPDATED_WIDTH);
        assertThat(testAdvertisement.getHeight()).isEqualTo(UPDATED_HEIGHT);
        assertThat(testAdvertisement.getDistanceKm()).isEqualTo(UPDATED_DISTANCE_KM);
        assertThat(testAdvertisement.isDelivered()).isEqualTo(UPDATED_DELIVERED);
        assertThat(testAdvertisement.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testAdvertisement.getWishDate()).isEqualTo(UPDATED_WISH_DATE);
        assertThat(testAdvertisement.getEffectiveDate()).isEqualTo(UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingAdvertisement() throws Exception {
        int databaseSizeBeforeUpdate = advertisementRepository.findAll().size();

        // Create the Advertisement
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdvertisementMockMvc
            .perform(
                put("/api/advertisements")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advertisementDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Advertisement in the database
        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAdvertisement() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        int databaseSizeBeforeDelete = advertisementRepository.findAll().size();

        // Delete the advertisement
        restAdvertisementMockMvc
            .perform(delete("/api/advertisements/{id}", advertisement.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
