package fr.insy2s.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.QuotationStatus;
import fr.insy2s.repository.QuotationStatusRepository;
import fr.insy2s.service.QuotationStatusQueryService;
import fr.insy2s.service.QuotationStatusService;
import fr.insy2s.service.dto.QuotationStatusCriteria;
import fr.insy2s.service.dto.QuotationStatusDTO;
import fr.insy2s.service.mapper.QuotationStatusMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link QuotationStatusResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class QuotationStatusResourceIT {
    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_REF = "AAAAAAAAAA";
    private static final String UPDATED_CODE_REF = "BBBBBBBBBB";

    @Autowired
    private QuotationStatusRepository quotationStatusRepository;

    @Autowired
    private QuotationStatusMapper quotationStatusMapper;

    @Autowired
    private QuotationStatusService quotationStatusService;

    @Autowired
    private QuotationStatusQueryService quotationStatusQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restQuotationStatusMockMvc;

    private QuotationStatus quotationStatus;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QuotationStatus createEntity(EntityManager em) {
        QuotationStatus quotationStatus = new QuotationStatus().label(DEFAULT_LABEL).codeRef(DEFAULT_CODE_REF);
        return quotationStatus;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QuotationStatus createUpdatedEntity(EntityManager em) {
        QuotationStatus quotationStatus = new QuotationStatus().label(UPDATED_LABEL).codeRef(UPDATED_CODE_REF);
        return quotationStatus;
    }

    @BeforeEach
    public void initTest() {
        quotationStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createQuotationStatus() throws Exception {
        int databaseSizeBeforeCreate = quotationStatusRepository.findAll().size();
        // Create the QuotationStatus
        QuotationStatusDTO quotationStatusDTO = quotationStatusMapper.toDto(quotationStatus);
        restQuotationStatusMockMvc
            .perform(
                post("/api/quotation-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quotationStatusDTO))
            )
            .andExpect(status().isCreated());

        // Validate the QuotationStatus in the database
        List<QuotationStatus> quotationStatusList = quotationStatusRepository.findAll();
        assertThat(quotationStatusList).hasSize(databaseSizeBeforeCreate + 1);
        QuotationStatus testQuotationStatus = quotationStatusList.get(quotationStatusList.size() - 1);
        assertThat(testQuotationStatus.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testQuotationStatus.getCodeRef()).isEqualTo(DEFAULT_CODE_REF);
    }

    @Test
    @Transactional
    public void createQuotationStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = quotationStatusRepository.findAll().size();

        // Create the QuotationStatus with an existing ID
        quotationStatus.setId(1L);
        QuotationStatusDTO quotationStatusDTO = quotationStatusMapper.toDto(quotationStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuotationStatusMockMvc
            .perform(
                post("/api/quotation-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quotationStatusDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuotationStatus in the database
        List<QuotationStatus> quotationStatusList = quotationStatusRepository.findAll();
        assertThat(quotationStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = quotationStatusRepository.findAll().size();
        // set the field null
        quotationStatus.setLabel(null);

        // Create the QuotationStatus, which fails.
        QuotationStatusDTO quotationStatusDTO = quotationStatusMapper.toDto(quotationStatus);

        restQuotationStatusMockMvc
            .perform(
                post("/api/quotation-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quotationStatusDTO))
            )
            .andExpect(status().isBadRequest());

        List<QuotationStatus> quotationStatusList = quotationStatusRepository.findAll();
        assertThat(quotationStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeRefIsRequired() throws Exception {
        int databaseSizeBeforeTest = quotationStatusRepository.findAll().size();
        // set the field null
        quotationStatus.setCodeRef(null);

        // Create the QuotationStatus, which fails.
        QuotationStatusDTO quotationStatusDTO = quotationStatusMapper.toDto(quotationStatus);

        restQuotationStatusMockMvc
            .perform(
                post("/api/quotation-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quotationStatusDTO))
            )
            .andExpect(status().isBadRequest());

        List<QuotationStatus> quotationStatusList = quotationStatusRepository.findAll();
        assertThat(quotationStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllQuotationStatuses() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList
        restQuotationStatusMockMvc
            .perform(get("/api/quotation-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quotationStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].codeRef").value(hasItem(DEFAULT_CODE_REF)));
    }

    @Test
    @Transactional
    public void getQuotationStatus() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get the quotationStatus
        restQuotationStatusMockMvc
            .perform(get("/api/quotation-statuses/{id}", quotationStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(quotationStatus.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.codeRef").value(DEFAULT_CODE_REF));
    }

    @Test
    @Transactional
    public void getQuotationStatusesByIdFiltering() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        Long id = quotationStatus.getId();

        defaultQuotationStatusShouldBeFound("id.equals=" + id);
        defaultQuotationStatusShouldNotBeFound("id.notEquals=" + id);

        defaultQuotationStatusShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultQuotationStatusShouldNotBeFound("id.greaterThan=" + id);

        defaultQuotationStatusShouldBeFound("id.lessThanOrEqual=" + id);
        defaultQuotationStatusShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where label equals to DEFAULT_LABEL
        defaultQuotationStatusShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the quotationStatusList where label equals to UPDATED_LABEL
        defaultQuotationStatusShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where label not equals to DEFAULT_LABEL
        defaultQuotationStatusShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the quotationStatusList where label not equals to UPDATED_LABEL
        defaultQuotationStatusShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultQuotationStatusShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the quotationStatusList where label equals to UPDATED_LABEL
        defaultQuotationStatusShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where label is not null
        defaultQuotationStatusShouldBeFound("label.specified=true");

        // Get all the quotationStatusList where label is null
        defaultQuotationStatusShouldNotBeFound("label.specified=false");
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByLabelContainsSomething() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where label contains DEFAULT_LABEL
        defaultQuotationStatusShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the quotationStatusList where label contains UPDATED_LABEL
        defaultQuotationStatusShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where label does not contain DEFAULT_LABEL
        defaultQuotationStatusShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the quotationStatusList where label does not contain UPDATED_LABEL
        defaultQuotationStatusShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByCodeRefIsEqualToSomething() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where codeRef equals to DEFAULT_CODE_REF
        defaultQuotationStatusShouldBeFound("codeRef.equals=" + DEFAULT_CODE_REF);

        // Get all the quotationStatusList where codeRef equals to UPDATED_CODE_REF
        defaultQuotationStatusShouldNotBeFound("codeRef.equals=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByCodeRefIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where codeRef not equals to DEFAULT_CODE_REF
        defaultQuotationStatusShouldNotBeFound("codeRef.notEquals=" + DEFAULT_CODE_REF);

        // Get all the quotationStatusList where codeRef not equals to UPDATED_CODE_REF
        defaultQuotationStatusShouldBeFound("codeRef.notEquals=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByCodeRefIsInShouldWork() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where codeRef in DEFAULT_CODE_REF or UPDATED_CODE_REF
        defaultQuotationStatusShouldBeFound("codeRef.in=" + DEFAULT_CODE_REF + "," + UPDATED_CODE_REF);

        // Get all the quotationStatusList where codeRef equals to UPDATED_CODE_REF
        defaultQuotationStatusShouldNotBeFound("codeRef.in=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByCodeRefIsNullOrNotNull() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where codeRef is not null
        defaultQuotationStatusShouldBeFound("codeRef.specified=true");

        // Get all the quotationStatusList where codeRef is null
        defaultQuotationStatusShouldNotBeFound("codeRef.specified=false");
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByCodeRefContainsSomething() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where codeRef contains DEFAULT_CODE_REF
        defaultQuotationStatusShouldBeFound("codeRef.contains=" + DEFAULT_CODE_REF);

        // Get all the quotationStatusList where codeRef contains UPDATED_CODE_REF
        defaultQuotationStatusShouldNotBeFound("codeRef.contains=" + UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void getAllQuotationStatusesByCodeRefNotContainsSomething() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        // Get all the quotationStatusList where codeRef does not contain DEFAULT_CODE_REF
        defaultQuotationStatusShouldNotBeFound("codeRef.doesNotContain=" + DEFAULT_CODE_REF);

        // Get all the quotationStatusList where codeRef does not contain UPDATED_CODE_REF
        defaultQuotationStatusShouldBeFound("codeRef.doesNotContain=" + UPDATED_CODE_REF);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultQuotationStatusShouldBeFound(String filter) throws Exception {
        restQuotationStatusMockMvc
            .perform(get("/api/quotation-statuses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quotationStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].codeRef").value(hasItem(DEFAULT_CODE_REF)));

        // Check, that the count call also returns 1
        restQuotationStatusMockMvc
            .perform(get("/api/quotation-statuses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultQuotationStatusShouldNotBeFound(String filter) throws Exception {
        restQuotationStatusMockMvc
            .perform(get("/api/quotation-statuses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restQuotationStatusMockMvc
            .perform(get("/api/quotation-statuses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingQuotationStatus() throws Exception {
        // Get the quotationStatus
        restQuotationStatusMockMvc.perform(get("/api/quotation-statuses/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQuotationStatus() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        int databaseSizeBeforeUpdate = quotationStatusRepository.findAll().size();

        // Update the quotationStatus
        QuotationStatus updatedQuotationStatus = quotationStatusRepository.findById(quotationStatus.getId()).get();
        // Disconnect from session so that the updates on updatedQuotationStatus are not directly saved in db
        em.detach(updatedQuotationStatus);
        updatedQuotationStatus.label(UPDATED_LABEL).codeRef(UPDATED_CODE_REF);
        QuotationStatusDTO quotationStatusDTO = quotationStatusMapper.toDto(updatedQuotationStatus);

        restQuotationStatusMockMvc
            .perform(
                put("/api/quotation-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quotationStatusDTO))
            )
            .andExpect(status().isOk());

        // Validate the QuotationStatus in the database
        List<QuotationStatus> quotationStatusList = quotationStatusRepository.findAll();
        assertThat(quotationStatusList).hasSize(databaseSizeBeforeUpdate);
        QuotationStatus testQuotationStatus = quotationStatusList.get(quotationStatusList.size() - 1);
        assertThat(testQuotationStatus.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testQuotationStatus.getCodeRef()).isEqualTo(UPDATED_CODE_REF);
    }

    @Test
    @Transactional
    public void updateNonExistingQuotationStatus() throws Exception {
        int databaseSizeBeforeUpdate = quotationStatusRepository.findAll().size();

        // Create the QuotationStatus
        QuotationStatusDTO quotationStatusDTO = quotationStatusMapper.toDto(quotationStatus);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuotationStatusMockMvc
            .perform(
                put("/api/quotation-statuses")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quotationStatusDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuotationStatus in the database
        List<QuotationStatus> quotationStatusList = quotationStatusRepository.findAll();
        assertThat(quotationStatusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteQuotationStatus() throws Exception {
        // Initialize the database
        quotationStatusRepository.saveAndFlush(quotationStatus);

        int databaseSizeBeforeDelete = quotationStatusRepository.findAll().size();

        // Delete the quotationStatus
        restQuotationStatusMockMvc
            .perform(delete("/api/quotation-statuses/{id}", quotationStatus.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<QuotationStatus> quotationStatusList = quotationStatusRepository.findAll();
        assertThat(quotationStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
