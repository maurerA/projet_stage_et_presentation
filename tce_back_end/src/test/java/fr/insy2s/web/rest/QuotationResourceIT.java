package fr.insy2s.web.rest;

import static fr.insy2s.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.Advertisement;
import fr.insy2s.domain.Quotation;
import fr.insy2s.domain.QuotationStatus;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.QuotationRepository;
import fr.insy2s.service.QuotationQueryService;
import fr.insy2s.service.QuotationService;
import fr.insy2s.service.dto.QuotationCriteria;
import fr.insy2s.service.dto.QuotationDTO;
import fr.insy2s.service.mapper.QuotationMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link QuotationResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class QuotationResourceIT {
    private static final Float DEFAULT_DELIVERY_MAN_PRICE = 1F;
    private static final Float UPDATED_DELIVERY_MAN_PRICE = 2F;
    private static final Float SMALLER_DELIVERY_MAN_PRICE = 1F - 1F;

    private static final ZonedDateTime DEFAULT_CREATION_DATE_QUOTATION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATION_DATE_QUOTATION = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_CREATION_DATE_QUOTATION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private QuotationRepository quotationRepository;

    @Autowired
    private QuotationMapper quotationMapper;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private QuotationQueryService quotationQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restQuotationMockMvc;

    private Quotation quotation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Quotation createEntity(EntityManager em) {
        Quotation quotation = new Quotation()
            .deliveryManPrice(DEFAULT_DELIVERY_MAN_PRICE)
            .creationDateQuotation(DEFAULT_CREATION_DATE_QUOTATION);
        // Add required entity
        QuotationStatus quotationStatus;
        if (TestUtil.findAll(em, QuotationStatus.class).isEmpty()) {
            quotationStatus = QuotationStatusResourceIT.createEntity(em);
            em.persist(quotationStatus);
            em.flush();
        } else {
            quotationStatus = TestUtil.findAll(em, QuotationStatus.class).get(0);
        }
        quotation.setStatus(quotationStatus);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        quotation.setDeliveryMan(userAccount);
        // Add required entity
        Advertisement advertisement;
        if (TestUtil.findAll(em, Advertisement.class).isEmpty()) {
            advertisement = AdvertisementResourceIT.createEntity(em);
            em.persist(advertisement);
            em.flush();
        } else {
            advertisement = TestUtil.findAll(em, Advertisement.class).get(0);
        }
        quotation.setAdvertisement(advertisement);
        return quotation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Quotation createUpdatedEntity(EntityManager em) {
        Quotation quotation = new Quotation()
            .deliveryManPrice(UPDATED_DELIVERY_MAN_PRICE)
            .creationDateQuotation(UPDATED_CREATION_DATE_QUOTATION);
        // Add required entity
        QuotationStatus quotationStatus;
        if (TestUtil.findAll(em, QuotationStatus.class).isEmpty()) {
            quotationStatus = QuotationStatusResourceIT.createUpdatedEntity(em);
            em.persist(quotationStatus);
            em.flush();
        } else {
            quotationStatus = TestUtil.findAll(em, QuotationStatus.class).get(0);
        }
        quotation.setStatus(quotationStatus);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createUpdatedEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        quotation.setDeliveryMan(userAccount);
        // Add required entity
        Advertisement advertisement;
        if (TestUtil.findAll(em, Advertisement.class).isEmpty()) {
            advertisement = AdvertisementResourceIT.createUpdatedEntity(em);
            em.persist(advertisement);
            em.flush();
        } else {
            advertisement = TestUtil.findAll(em, Advertisement.class).get(0);
        }
        quotation.setAdvertisement(advertisement);
        return quotation;
    }

    @BeforeEach
    public void initTest() {
        quotation = createEntity(em);
    }

    @Test
    @Transactional
    public void createQuotation() throws Exception {
        int databaseSizeBeforeCreate = quotationRepository.findAll().size();
        // Create the Quotation
        QuotationDTO quotationDTO = quotationMapper.toDto(quotation);
        restQuotationMockMvc
            .perform(
                post("/api/quotations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quotationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Quotation in the database
        List<Quotation> quotationList = quotationRepository.findAll();
        assertThat(quotationList).hasSize(databaseSizeBeforeCreate + 1);
        Quotation testQuotation = quotationList.get(quotationList.size() - 1);
        assertThat(testQuotation.getDeliveryManPrice()).isEqualTo(DEFAULT_DELIVERY_MAN_PRICE);
        assertThat(testQuotation.getCreationDateQuotation()).isEqualTo(DEFAULT_CREATION_DATE_QUOTATION);
    }

    @Test
    @Transactional
    public void createQuotationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = quotationRepository.findAll().size();

        // Create the Quotation with an existing ID
        quotation.setId(1L);
        QuotationDTO quotationDTO = quotationMapper.toDto(quotation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuotationMockMvc
            .perform(
                post("/api/quotations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quotationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Quotation in the database
        List<Quotation> quotationList = quotationRepository.findAll();
        assertThat(quotationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDeliveryManPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = quotationRepository.findAll().size();
        // set the field null
        quotation.setDeliveryManPrice(null);

        // Create the Quotation, which fails.
        QuotationDTO quotationDTO = quotationMapper.toDto(quotation);

        restQuotationMockMvc
            .perform(
                post("/api/quotations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quotationDTO))
            )
            .andExpect(status().isBadRequest());

        List<Quotation> quotationList = quotationRepository.findAll();
        assertThat(quotationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreationDateQuotationIsRequired() throws Exception {
        int databaseSizeBeforeTest = quotationRepository.findAll().size();
        // set the field null
        quotation.setCreationDateQuotation(null);

        // Create the Quotation, which fails.
        QuotationDTO quotationDTO = quotationMapper.toDto(quotation);

        restQuotationMockMvc
            .perform(
                post("/api/quotations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quotationDTO))
            )
            .andExpect(status().isBadRequest());

        List<Quotation> quotationList = quotationRepository.findAll();
        assertThat(quotationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllQuotations() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList
        restQuotationMockMvc
            .perform(get("/api/quotations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quotation.getId().intValue())))
            .andExpect(jsonPath("$.[*].deliveryManPrice").value(hasItem(DEFAULT_DELIVERY_MAN_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].creationDateQuotation").value(hasItem(sameInstant(DEFAULT_CREATION_DATE_QUOTATION))));
    }

    @Test
    @Transactional
    public void getQuotation() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get the quotation
        restQuotationMockMvc
            .perform(get("/api/quotations/{id}", quotation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(quotation.getId().intValue()))
            .andExpect(jsonPath("$.deliveryManPrice").value(DEFAULT_DELIVERY_MAN_PRICE.doubleValue()))
            .andExpect(jsonPath("$.creationDateQuotation").value(sameInstant(DEFAULT_CREATION_DATE_QUOTATION)));
    }

    @Test
    @Transactional
    public void getQuotationsByIdFiltering() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        Long id = quotation.getId();

        defaultQuotationShouldBeFound("id.equals=" + id);
        defaultQuotationShouldNotBeFound("id.notEquals=" + id);

        defaultQuotationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultQuotationShouldNotBeFound("id.greaterThan=" + id);

        defaultQuotationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultQuotationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllQuotationsByDeliveryManPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where deliveryManPrice equals to DEFAULT_DELIVERY_MAN_PRICE
        defaultQuotationShouldBeFound("deliveryManPrice.equals=" + DEFAULT_DELIVERY_MAN_PRICE);

        // Get all the quotationList where deliveryManPrice equals to UPDATED_DELIVERY_MAN_PRICE
        defaultQuotationShouldNotBeFound("deliveryManPrice.equals=" + UPDATED_DELIVERY_MAN_PRICE);
    }

    @Test
    @Transactional
    public void getAllQuotationsByDeliveryManPriceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where deliveryManPrice not equals to DEFAULT_DELIVERY_MAN_PRICE
        defaultQuotationShouldNotBeFound("deliveryManPrice.notEquals=" + DEFAULT_DELIVERY_MAN_PRICE);

        // Get all the quotationList where deliveryManPrice not equals to UPDATED_DELIVERY_MAN_PRICE
        defaultQuotationShouldBeFound("deliveryManPrice.notEquals=" + UPDATED_DELIVERY_MAN_PRICE);
    }

    @Test
    @Transactional
    public void getAllQuotationsByDeliveryManPriceIsInShouldWork() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where deliveryManPrice in DEFAULT_DELIVERY_MAN_PRICE or UPDATED_DELIVERY_MAN_PRICE
        defaultQuotationShouldBeFound("deliveryManPrice.in=" + DEFAULT_DELIVERY_MAN_PRICE + "," + UPDATED_DELIVERY_MAN_PRICE);

        // Get all the quotationList where deliveryManPrice equals to UPDATED_DELIVERY_MAN_PRICE
        defaultQuotationShouldNotBeFound("deliveryManPrice.in=" + UPDATED_DELIVERY_MAN_PRICE);
    }

    @Test
    @Transactional
    public void getAllQuotationsByDeliveryManPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where deliveryManPrice is not null
        defaultQuotationShouldBeFound("deliveryManPrice.specified=true");

        // Get all the quotationList where deliveryManPrice is null
        defaultQuotationShouldNotBeFound("deliveryManPrice.specified=false");
    }

    @Test
    @Transactional
    public void getAllQuotationsByDeliveryManPriceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where deliveryManPrice is greater than or equal to DEFAULT_DELIVERY_MAN_PRICE
        defaultQuotationShouldBeFound("deliveryManPrice.greaterThanOrEqual=" + DEFAULT_DELIVERY_MAN_PRICE);

        // Get all the quotationList where deliveryManPrice is greater than or equal to UPDATED_DELIVERY_MAN_PRICE
        defaultQuotationShouldNotBeFound("deliveryManPrice.greaterThanOrEqual=" + UPDATED_DELIVERY_MAN_PRICE);
    }

    @Test
    @Transactional
    public void getAllQuotationsByDeliveryManPriceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where deliveryManPrice is less than or equal to DEFAULT_DELIVERY_MAN_PRICE
        defaultQuotationShouldBeFound("deliveryManPrice.lessThanOrEqual=" + DEFAULT_DELIVERY_MAN_PRICE);

        // Get all the quotationList where deliveryManPrice is less than or equal to SMALLER_DELIVERY_MAN_PRICE
        defaultQuotationShouldNotBeFound("deliveryManPrice.lessThanOrEqual=" + SMALLER_DELIVERY_MAN_PRICE);
    }

    @Test
    @Transactional
    public void getAllQuotationsByDeliveryManPriceIsLessThanSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where deliveryManPrice is less than DEFAULT_DELIVERY_MAN_PRICE
        defaultQuotationShouldNotBeFound("deliveryManPrice.lessThan=" + DEFAULT_DELIVERY_MAN_PRICE);

        // Get all the quotationList where deliveryManPrice is less than UPDATED_DELIVERY_MAN_PRICE
        defaultQuotationShouldBeFound("deliveryManPrice.lessThan=" + UPDATED_DELIVERY_MAN_PRICE);
    }

    @Test
    @Transactional
    public void getAllQuotationsByDeliveryManPriceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where deliveryManPrice is greater than DEFAULT_DELIVERY_MAN_PRICE
        defaultQuotationShouldNotBeFound("deliveryManPrice.greaterThan=" + DEFAULT_DELIVERY_MAN_PRICE);

        // Get all the quotationList where deliveryManPrice is greater than SMALLER_DELIVERY_MAN_PRICE
        defaultQuotationShouldBeFound("deliveryManPrice.greaterThan=" + SMALLER_DELIVERY_MAN_PRICE);
    }

    @Test
    @Transactional
    public void getAllQuotationsByCreationDateQuotationIsEqualToSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where creationDateQuotation equals to DEFAULT_CREATION_DATE_QUOTATION
        defaultQuotationShouldBeFound("creationDateQuotation.equals=" + DEFAULT_CREATION_DATE_QUOTATION);

        // Get all the quotationList where creationDateQuotation equals to UPDATED_CREATION_DATE_QUOTATION
        defaultQuotationShouldNotBeFound("creationDateQuotation.equals=" + UPDATED_CREATION_DATE_QUOTATION);
    }

    @Test
    @Transactional
    public void getAllQuotationsByCreationDateQuotationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where creationDateQuotation not equals to DEFAULT_CREATION_DATE_QUOTATION
        defaultQuotationShouldNotBeFound("creationDateQuotation.notEquals=" + DEFAULT_CREATION_DATE_QUOTATION);

        // Get all the quotationList where creationDateQuotation not equals to UPDATED_CREATION_DATE_QUOTATION
        defaultQuotationShouldBeFound("creationDateQuotation.notEquals=" + UPDATED_CREATION_DATE_QUOTATION);
    }

    @Test
    @Transactional
    public void getAllQuotationsByCreationDateQuotationIsInShouldWork() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where creationDateQuotation in DEFAULT_CREATION_DATE_QUOTATION or UPDATED_CREATION_DATE_QUOTATION
        defaultQuotationShouldBeFound(
            "creationDateQuotation.in=" + DEFAULT_CREATION_DATE_QUOTATION + "," + UPDATED_CREATION_DATE_QUOTATION
        );

        // Get all the quotationList where creationDateQuotation equals to UPDATED_CREATION_DATE_QUOTATION
        defaultQuotationShouldNotBeFound("creationDateQuotation.in=" + UPDATED_CREATION_DATE_QUOTATION);
    }

    @Test
    @Transactional
    public void getAllQuotationsByCreationDateQuotationIsNullOrNotNull() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where creationDateQuotation is not null
        defaultQuotationShouldBeFound("creationDateQuotation.specified=true");

        // Get all the quotationList where creationDateQuotation is null
        defaultQuotationShouldNotBeFound("creationDateQuotation.specified=false");
    }

    @Test
    @Transactional
    public void getAllQuotationsByCreationDateQuotationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where creationDateQuotation is greater than or equal to DEFAULT_CREATION_DATE_QUOTATION
        defaultQuotationShouldBeFound("creationDateQuotation.greaterThanOrEqual=" + DEFAULT_CREATION_DATE_QUOTATION);

        // Get all the quotationList where creationDateQuotation is greater than or equal to UPDATED_CREATION_DATE_QUOTATION
        defaultQuotationShouldNotBeFound("creationDateQuotation.greaterThanOrEqual=" + UPDATED_CREATION_DATE_QUOTATION);
    }

    @Test
    @Transactional
    public void getAllQuotationsByCreationDateQuotationIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where creationDateQuotation is less than or equal to DEFAULT_CREATION_DATE_QUOTATION
        defaultQuotationShouldBeFound("creationDateQuotation.lessThanOrEqual=" + DEFAULT_CREATION_DATE_QUOTATION);

        // Get all the quotationList where creationDateQuotation is less than or equal to SMALLER_CREATION_DATE_QUOTATION
        defaultQuotationShouldNotBeFound("creationDateQuotation.lessThanOrEqual=" + SMALLER_CREATION_DATE_QUOTATION);
    }

    @Test
    @Transactional
    public void getAllQuotationsByCreationDateQuotationIsLessThanSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where creationDateQuotation is less than DEFAULT_CREATION_DATE_QUOTATION
        defaultQuotationShouldNotBeFound("creationDateQuotation.lessThan=" + DEFAULT_CREATION_DATE_QUOTATION);

        // Get all the quotationList where creationDateQuotation is less than UPDATED_CREATION_DATE_QUOTATION
        defaultQuotationShouldBeFound("creationDateQuotation.lessThan=" + UPDATED_CREATION_DATE_QUOTATION);
    }

    @Test
    @Transactional
    public void getAllQuotationsByCreationDateQuotationIsGreaterThanSomething() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        // Get all the quotationList where creationDateQuotation is greater than DEFAULT_CREATION_DATE_QUOTATION
        defaultQuotationShouldNotBeFound("creationDateQuotation.greaterThan=" + DEFAULT_CREATION_DATE_QUOTATION);

        // Get all the quotationList where creationDateQuotation is greater than SMALLER_CREATION_DATE_QUOTATION
        defaultQuotationShouldBeFound("creationDateQuotation.greaterThan=" + SMALLER_CREATION_DATE_QUOTATION);
    }

    @Test
    @Transactional
    public void getAllQuotationsByStatusIsEqualToSomething() throws Exception {
        // Get already existing entity
        QuotationStatus status = quotation.getStatus();
        quotationRepository.saveAndFlush(quotation);
        Long statusId = status.getId();

        // Get all the quotationList where status equals to statusId
        defaultQuotationShouldBeFound("statusId.equals=" + statusId);

        // Get all the quotationList where status equals to statusId + 1
        defaultQuotationShouldNotBeFound("statusId.equals=" + (statusId + 1));
    }

    @Test
    @Transactional
    public void getAllQuotationsByDeliveryManIsEqualToSomething() throws Exception {
        // Get already existing entity
        UserAccount deliveryMan = quotation.getDeliveryMan();
        quotationRepository.saveAndFlush(quotation);
        Long deliveryManId = deliveryMan.getId();

        // Get all the quotationList where deliveryMan equals to deliveryManId
        defaultQuotationShouldBeFound("deliveryManId.equals=" + deliveryManId);

        // Get all the quotationList where deliveryMan equals to deliveryManId + 1
        defaultQuotationShouldNotBeFound("deliveryManId.equals=" + (deliveryManId + 1));
    }

    @Test
    @Transactional
    public void getAllQuotationsByAdvertisementIsEqualToSomething() throws Exception {
        // Get already existing entity
        Advertisement advertisement = quotation.getAdvertisement();
        quotationRepository.saveAndFlush(quotation);
        Long advertisementId = advertisement.getId();

        // Get all the quotationList where advertisement equals to advertisementId
        defaultQuotationShouldBeFound("advertisementId.equals=" + advertisementId);

        // Get all the quotationList where advertisement equals to advertisementId + 1
        defaultQuotationShouldNotBeFound("advertisementId.equals=" + (advertisementId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultQuotationShouldBeFound(String filter) throws Exception {
        restQuotationMockMvc
            .perform(get("/api/quotations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quotation.getId().intValue())))
            .andExpect(jsonPath("$.[*].deliveryManPrice").value(hasItem(DEFAULT_DELIVERY_MAN_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].creationDateQuotation").value(hasItem(sameInstant(DEFAULT_CREATION_DATE_QUOTATION))));

        // Check, that the count call also returns 1
        restQuotationMockMvc
            .perform(get("/api/quotations/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultQuotationShouldNotBeFound(String filter) throws Exception {
        restQuotationMockMvc
            .perform(get("/api/quotations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restQuotationMockMvc
            .perform(get("/api/quotations/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingQuotation() throws Exception {
        // Get the quotation
        restQuotationMockMvc.perform(get("/api/quotations/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQuotation() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        int databaseSizeBeforeUpdate = quotationRepository.findAll().size();

        // Update the quotation
        Quotation updatedQuotation = quotationRepository.findById(quotation.getId()).get();
        // Disconnect from session so that the updates on updatedQuotation are not directly saved in db
        em.detach(updatedQuotation);
        updatedQuotation.deliveryManPrice(UPDATED_DELIVERY_MAN_PRICE).creationDateQuotation(UPDATED_CREATION_DATE_QUOTATION);
        QuotationDTO quotationDTO = quotationMapper.toDto(updatedQuotation);

        restQuotationMockMvc
            .perform(
                put("/api/quotations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quotationDTO))
            )
            .andExpect(status().isOk());

        // Validate the Quotation in the database
        List<Quotation> quotationList = quotationRepository.findAll();
        assertThat(quotationList).hasSize(databaseSizeBeforeUpdate);
        Quotation testQuotation = quotationList.get(quotationList.size() - 1);
        assertThat(testQuotation.getDeliveryManPrice()).isEqualTo(UPDATED_DELIVERY_MAN_PRICE);
        assertThat(testQuotation.getCreationDateQuotation()).isEqualTo(UPDATED_CREATION_DATE_QUOTATION);
    }

    @Test
    @Transactional
    public void updateNonExistingQuotation() throws Exception {
        int databaseSizeBeforeUpdate = quotationRepository.findAll().size();

        // Create the Quotation
        QuotationDTO quotationDTO = quotationMapper.toDto(quotation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuotationMockMvc
            .perform(
                put("/api/quotations").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quotationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Quotation in the database
        List<Quotation> quotationList = quotationRepository.findAll();
        assertThat(quotationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteQuotation() throws Exception {
        // Initialize the database
        quotationRepository.saveAndFlush(quotation);

        int databaseSizeBeforeDelete = quotationRepository.findAll().size();

        // Delete the quotation
        restQuotationMockMvc
            .perform(delete("/api/quotations/{id}", quotation.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Quotation> quotationList = quotationRepository.findAll();
        assertThat(quotationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
