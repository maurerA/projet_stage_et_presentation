package fr.insy2s.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.insy2s.TcebackendApp;
import fr.insy2s.domain.Notification;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.NotificationRepository;
import fr.insy2s.service.NotificationQueryService;
import fr.insy2s.service.NotificationService;
import fr.insy2s.service.dto.NotificationCriteria;
import fr.insy2s.service.dto.NotificationDTO;
import fr.insy2s.service.mapper.NotificationMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link NotificationResource} REST controller.
 */
@SpringBootTest(classes = TcebackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class NotificationResourceIT {
    private static final String DEFAULT_ENTITY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ENTITY_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_ENTITY_ID = 1L;
    private static final Long UPDATED_ENTITY_ID = 2L;
    private static final Long SMALLER_ENTITY_ID = 1L - 1L;

    private static final Boolean DEFAULT_VIEWED = false;
    private static final Boolean UPDATED_VIEWED = true;

    private static final LocalDate DEFAULT_NOTIFICATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NOTIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NOTIFICATION_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private NotificationMapper notificationMapper;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private NotificationQueryService notificationQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNotificationMockMvc;

    private Notification notification;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notification createEntity(EntityManager em) {
        Notification notification = new Notification()
            .entityName(DEFAULT_ENTITY_NAME)
            .entityId(DEFAULT_ENTITY_ID)
            .viewed(DEFAULT_VIEWED)
            .notificationDate(DEFAULT_NOTIFICATION_DATE)
            .message(DEFAULT_MESSAGE);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        notification.setUserAccount(userAccount);
        return notification;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notification createUpdatedEntity(EntityManager em) {
        Notification notification = new Notification()
            .entityName(UPDATED_ENTITY_NAME)
            .entityId(UPDATED_ENTITY_ID)
            .viewed(UPDATED_VIEWED)
            .notificationDate(UPDATED_NOTIFICATION_DATE)
            .message(UPDATED_MESSAGE);
        // Add required entity
        UserAccount userAccount;
        if (TestUtil.findAll(em, UserAccount.class).isEmpty()) {
            userAccount = UserAccountResourceIT.createUpdatedEntity(em);
            em.persist(userAccount);
            em.flush();
        } else {
            userAccount = TestUtil.findAll(em, UserAccount.class).get(0);
        }
        notification.setUserAccount(userAccount);
        return notification;
    }

    @BeforeEach
    public void initTest() {
        notification = createEntity(em);
    }

    @Test
    @Transactional
    public void createNotification() throws Exception {
        int databaseSizeBeforeCreate = notificationRepository.findAll().size();
        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);
        restNotificationMockMvc
            .perform(
                post("/api/notifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeCreate + 1);
        Notification testNotification = notificationList.get(notificationList.size() - 1);
        assertThat(testNotification.getEntityName()).isEqualTo(DEFAULT_ENTITY_NAME);
        assertThat(testNotification.getEntityId()).isEqualTo(DEFAULT_ENTITY_ID);
        assertThat(testNotification.isViewed()).isEqualTo(DEFAULT_VIEWED);
        assertThat(testNotification.getNotificationDate()).isEqualTo(DEFAULT_NOTIFICATION_DATE);
        assertThat(testNotification.getMessage()).isEqualTo(DEFAULT_MESSAGE);
    }

    @Test
    @Transactional
    public void createNotificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = notificationRepository.findAll().size();

        // Create the Notification with an existing ID
        notification.setId(1L);
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotificationMockMvc
            .perform(
                post("/api/notifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkViewedIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationRepository.findAll().size();
        // set the field null
        notification.setViewed(null);

        // Create the Notification, which fails.
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        restNotificationMockMvc
            .perform(
                post("/api/notifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isBadRequest());

        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNotificationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationRepository.findAll().size();
        // set the field null
        notification.setNotificationDate(null);

        // Create the Notification, which fails.
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        restNotificationMockMvc
            .perform(
                post("/api/notifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isBadRequest());

        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNotifications() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList
        restNotificationMockMvc
            .perform(get("/api/notifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notification.getId().intValue())))
            .andExpect(jsonPath("$.[*].entityName").value(hasItem(DEFAULT_ENTITY_NAME)))
            .andExpect(jsonPath("$.[*].entityId").value(hasItem(DEFAULT_ENTITY_ID.intValue())))
            .andExpect(jsonPath("$.[*].viewed").value(hasItem(DEFAULT_VIEWED.booleanValue())))
            .andExpect(jsonPath("$.[*].notificationDate").value(hasItem(DEFAULT_NOTIFICATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)));
    }

    @Test
    @Transactional
    public void getNotification() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get the notification
        restNotificationMockMvc
            .perform(get("/api/notifications/{id}", notification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(notification.getId().intValue()))
            .andExpect(jsonPath("$.entityName").value(DEFAULT_ENTITY_NAME))
            .andExpect(jsonPath("$.entityId").value(DEFAULT_ENTITY_ID.intValue()))
            .andExpect(jsonPath("$.viewed").value(DEFAULT_VIEWED.booleanValue()))
            .andExpect(jsonPath("$.notificationDate").value(DEFAULT_NOTIFICATION_DATE.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE));
    }

    @Test
    @Transactional
    public void getNotificationsByIdFiltering() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        Long id = notification.getId();

        defaultNotificationShouldBeFound("id.equals=" + id);
        defaultNotificationShouldNotBeFound("id.notEquals=" + id);

        defaultNotificationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNotificationShouldNotBeFound("id.greaterThan=" + id);

        defaultNotificationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNotificationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityNameIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityName equals to DEFAULT_ENTITY_NAME
        defaultNotificationShouldBeFound("entityName.equals=" + DEFAULT_ENTITY_NAME);

        // Get all the notificationList where entityName equals to UPDATED_ENTITY_NAME
        defaultNotificationShouldNotBeFound("entityName.equals=" + UPDATED_ENTITY_NAME);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityName not equals to DEFAULT_ENTITY_NAME
        defaultNotificationShouldNotBeFound("entityName.notEquals=" + DEFAULT_ENTITY_NAME);

        // Get all the notificationList where entityName not equals to UPDATED_ENTITY_NAME
        defaultNotificationShouldBeFound("entityName.notEquals=" + UPDATED_ENTITY_NAME);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityNameIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityName in DEFAULT_ENTITY_NAME or UPDATED_ENTITY_NAME
        defaultNotificationShouldBeFound("entityName.in=" + DEFAULT_ENTITY_NAME + "," + UPDATED_ENTITY_NAME);

        // Get all the notificationList where entityName equals to UPDATED_ENTITY_NAME
        defaultNotificationShouldNotBeFound("entityName.in=" + UPDATED_ENTITY_NAME);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityName is not null
        defaultNotificationShouldBeFound("entityName.specified=true");

        // Get all the notificationList where entityName is null
        defaultNotificationShouldNotBeFound("entityName.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityNameContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityName contains DEFAULT_ENTITY_NAME
        defaultNotificationShouldBeFound("entityName.contains=" + DEFAULT_ENTITY_NAME);

        // Get all the notificationList where entityName contains UPDATED_ENTITY_NAME
        defaultNotificationShouldNotBeFound("entityName.contains=" + UPDATED_ENTITY_NAME);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityNameNotContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityName does not contain DEFAULT_ENTITY_NAME
        defaultNotificationShouldNotBeFound("entityName.doesNotContain=" + DEFAULT_ENTITY_NAME);

        // Get all the notificationList where entityName does not contain UPDATED_ENTITY_NAME
        defaultNotificationShouldBeFound("entityName.doesNotContain=" + UPDATED_ENTITY_NAME);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityIdIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityId equals to DEFAULT_ENTITY_ID
        defaultNotificationShouldBeFound("entityId.equals=" + DEFAULT_ENTITY_ID);

        // Get all the notificationList where entityId equals to UPDATED_ENTITY_ID
        defaultNotificationShouldNotBeFound("entityId.equals=" + UPDATED_ENTITY_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityId not equals to DEFAULT_ENTITY_ID
        defaultNotificationShouldNotBeFound("entityId.notEquals=" + DEFAULT_ENTITY_ID);

        // Get all the notificationList where entityId not equals to UPDATED_ENTITY_ID
        defaultNotificationShouldBeFound("entityId.notEquals=" + UPDATED_ENTITY_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityIdIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityId in DEFAULT_ENTITY_ID or UPDATED_ENTITY_ID
        defaultNotificationShouldBeFound("entityId.in=" + DEFAULT_ENTITY_ID + "," + UPDATED_ENTITY_ID);

        // Get all the notificationList where entityId equals to UPDATED_ENTITY_ID
        defaultNotificationShouldNotBeFound("entityId.in=" + UPDATED_ENTITY_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityId is not null
        defaultNotificationShouldBeFound("entityId.specified=true");

        // Get all the notificationList where entityId is null
        defaultNotificationShouldNotBeFound("entityId.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityId is greater than or equal to DEFAULT_ENTITY_ID
        defaultNotificationShouldBeFound("entityId.greaterThanOrEqual=" + DEFAULT_ENTITY_ID);

        // Get all the notificationList where entityId is greater than or equal to UPDATED_ENTITY_ID
        defaultNotificationShouldNotBeFound("entityId.greaterThanOrEqual=" + UPDATED_ENTITY_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityId is less than or equal to DEFAULT_ENTITY_ID
        defaultNotificationShouldBeFound("entityId.lessThanOrEqual=" + DEFAULT_ENTITY_ID);

        // Get all the notificationList where entityId is less than or equal to SMALLER_ENTITY_ID
        defaultNotificationShouldNotBeFound("entityId.lessThanOrEqual=" + SMALLER_ENTITY_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityIdIsLessThanSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityId is less than DEFAULT_ENTITY_ID
        defaultNotificationShouldNotBeFound("entityId.lessThan=" + DEFAULT_ENTITY_ID);

        // Get all the notificationList where entityId is less than UPDATED_ENTITY_ID
        defaultNotificationShouldBeFound("entityId.lessThan=" + UPDATED_ENTITY_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationsByEntityIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where entityId is greater than DEFAULT_ENTITY_ID
        defaultNotificationShouldNotBeFound("entityId.greaterThan=" + DEFAULT_ENTITY_ID);

        // Get all the notificationList where entityId is greater than SMALLER_ENTITY_ID
        defaultNotificationShouldBeFound("entityId.greaterThan=" + SMALLER_ENTITY_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationsByViewedIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where viewed equals to DEFAULT_VIEWED
        defaultNotificationShouldBeFound("viewed.equals=" + DEFAULT_VIEWED);

        // Get all the notificationList where viewed equals to UPDATED_VIEWED
        defaultNotificationShouldNotBeFound("viewed.equals=" + UPDATED_VIEWED);
    }

    @Test
    @Transactional
    public void getAllNotificationsByViewedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where viewed not equals to DEFAULT_VIEWED
        defaultNotificationShouldNotBeFound("viewed.notEquals=" + DEFAULT_VIEWED);

        // Get all the notificationList where viewed not equals to UPDATED_VIEWED
        defaultNotificationShouldBeFound("viewed.notEquals=" + UPDATED_VIEWED);
    }

    @Test
    @Transactional
    public void getAllNotificationsByViewedIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where viewed in DEFAULT_VIEWED or UPDATED_VIEWED
        defaultNotificationShouldBeFound("viewed.in=" + DEFAULT_VIEWED + "," + UPDATED_VIEWED);

        // Get all the notificationList where viewed equals to UPDATED_VIEWED
        defaultNotificationShouldNotBeFound("viewed.in=" + UPDATED_VIEWED);
    }

    @Test
    @Transactional
    public void getAllNotificationsByViewedIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where viewed is not null
        defaultNotificationShouldBeFound("viewed.specified=true");

        // Get all the notificationList where viewed is null
        defaultNotificationShouldNotBeFound("viewed.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationsByNotificationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where notificationDate equals to DEFAULT_NOTIFICATION_DATE
        defaultNotificationShouldBeFound("notificationDate.equals=" + DEFAULT_NOTIFICATION_DATE);

        // Get all the notificationList where notificationDate equals to UPDATED_NOTIFICATION_DATE
        defaultNotificationShouldNotBeFound("notificationDate.equals=" + UPDATED_NOTIFICATION_DATE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByNotificationDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where notificationDate not equals to DEFAULT_NOTIFICATION_DATE
        defaultNotificationShouldNotBeFound("notificationDate.notEquals=" + DEFAULT_NOTIFICATION_DATE);

        // Get all the notificationList where notificationDate not equals to UPDATED_NOTIFICATION_DATE
        defaultNotificationShouldBeFound("notificationDate.notEquals=" + UPDATED_NOTIFICATION_DATE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByNotificationDateIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where notificationDate in DEFAULT_NOTIFICATION_DATE or UPDATED_NOTIFICATION_DATE
        defaultNotificationShouldBeFound("notificationDate.in=" + DEFAULT_NOTIFICATION_DATE + "," + UPDATED_NOTIFICATION_DATE);

        // Get all the notificationList where notificationDate equals to UPDATED_NOTIFICATION_DATE
        defaultNotificationShouldNotBeFound("notificationDate.in=" + UPDATED_NOTIFICATION_DATE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByNotificationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where notificationDate is not null
        defaultNotificationShouldBeFound("notificationDate.specified=true");

        // Get all the notificationList where notificationDate is null
        defaultNotificationShouldNotBeFound("notificationDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationsByNotificationDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where notificationDate is greater than or equal to DEFAULT_NOTIFICATION_DATE
        defaultNotificationShouldBeFound("notificationDate.greaterThanOrEqual=" + DEFAULT_NOTIFICATION_DATE);

        // Get all the notificationList where notificationDate is greater than or equal to UPDATED_NOTIFICATION_DATE
        defaultNotificationShouldNotBeFound("notificationDate.greaterThanOrEqual=" + UPDATED_NOTIFICATION_DATE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByNotificationDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where notificationDate is less than or equal to DEFAULT_NOTIFICATION_DATE
        defaultNotificationShouldBeFound("notificationDate.lessThanOrEqual=" + DEFAULT_NOTIFICATION_DATE);

        // Get all the notificationList where notificationDate is less than or equal to SMALLER_NOTIFICATION_DATE
        defaultNotificationShouldNotBeFound("notificationDate.lessThanOrEqual=" + SMALLER_NOTIFICATION_DATE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByNotificationDateIsLessThanSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where notificationDate is less than DEFAULT_NOTIFICATION_DATE
        defaultNotificationShouldNotBeFound("notificationDate.lessThan=" + DEFAULT_NOTIFICATION_DATE);

        // Get all the notificationList where notificationDate is less than UPDATED_NOTIFICATION_DATE
        defaultNotificationShouldBeFound("notificationDate.lessThan=" + UPDATED_NOTIFICATION_DATE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByNotificationDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where notificationDate is greater than DEFAULT_NOTIFICATION_DATE
        defaultNotificationShouldNotBeFound("notificationDate.greaterThan=" + DEFAULT_NOTIFICATION_DATE);

        // Get all the notificationList where notificationDate is greater than SMALLER_NOTIFICATION_DATE
        defaultNotificationShouldBeFound("notificationDate.greaterThan=" + SMALLER_NOTIFICATION_DATE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message equals to DEFAULT_MESSAGE
        defaultNotificationShouldBeFound("message.equals=" + DEFAULT_MESSAGE);

        // Get all the notificationList where message equals to UPDATED_MESSAGE
        defaultNotificationShouldNotBeFound("message.equals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message not equals to DEFAULT_MESSAGE
        defaultNotificationShouldNotBeFound("message.notEquals=" + DEFAULT_MESSAGE);

        // Get all the notificationList where message not equals to UPDATED_MESSAGE
        defaultNotificationShouldBeFound("message.notEquals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message in DEFAULT_MESSAGE or UPDATED_MESSAGE
        defaultNotificationShouldBeFound("message.in=" + DEFAULT_MESSAGE + "," + UPDATED_MESSAGE);

        // Get all the notificationList where message equals to UPDATED_MESSAGE
        defaultNotificationShouldNotBeFound("message.in=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message is not null
        defaultNotificationShouldBeFound("message.specified=true");

        // Get all the notificationList where message is null
        defaultNotificationShouldNotBeFound("message.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message contains DEFAULT_MESSAGE
        defaultNotificationShouldBeFound("message.contains=" + DEFAULT_MESSAGE);

        // Get all the notificationList where message contains UPDATED_MESSAGE
        defaultNotificationShouldNotBeFound("message.contains=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageNotContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message does not contain DEFAULT_MESSAGE
        defaultNotificationShouldNotBeFound("message.doesNotContain=" + DEFAULT_MESSAGE);

        // Get all the notificationList where message does not contain UPDATED_MESSAGE
        defaultNotificationShouldBeFound("message.doesNotContain=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByUserAccountIsEqualToSomething() throws Exception {
        // Get already existing entity
        UserAccount userAccount = notification.getUserAccount();
        notificationRepository.saveAndFlush(notification);
        Long userAccountId = userAccount.getId();

        // Get all the notificationList where userAccount equals to userAccountId
        defaultNotificationShouldBeFound("userAccountId.equals=" + userAccountId);

        // Get all the notificationList where userAccount equals to userAccountId + 1
        defaultNotificationShouldNotBeFound("userAccountId.equals=" + (userAccountId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNotificationShouldBeFound(String filter) throws Exception {
        restNotificationMockMvc
            .perform(get("/api/notifications?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notification.getId().intValue())))
            .andExpect(jsonPath("$.[*].entityName").value(hasItem(DEFAULT_ENTITY_NAME)))
            .andExpect(jsonPath("$.[*].entityId").value(hasItem(DEFAULT_ENTITY_ID.intValue())))
            .andExpect(jsonPath("$.[*].viewed").value(hasItem(DEFAULT_VIEWED.booleanValue())))
            .andExpect(jsonPath("$.[*].notificationDate").value(hasItem(DEFAULT_NOTIFICATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)));

        // Check, that the count call also returns 1
        restNotificationMockMvc
            .perform(get("/api/notifications/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNotificationShouldNotBeFound(String filter) throws Exception {
        restNotificationMockMvc
            .perform(get("/api/notifications?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNotificationMockMvc
            .perform(get("/api/notifications/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingNotification() throws Exception {
        // Get the notification
        restNotificationMockMvc.perform(get("/api/notifications/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNotification() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

        // Update the notification
        Notification updatedNotification = notificationRepository.findById(notification.getId()).get();
        // Disconnect from session so that the updates on updatedNotification are not directly saved in db
        em.detach(updatedNotification);
        updatedNotification
            .entityName(UPDATED_ENTITY_NAME)
            .entityId(UPDATED_ENTITY_ID)
            .viewed(UPDATED_VIEWED)
            .notificationDate(UPDATED_NOTIFICATION_DATE)
            .message(UPDATED_MESSAGE);
        NotificationDTO notificationDTO = notificationMapper.toDto(updatedNotification);

        restNotificationMockMvc
            .perform(
                put("/api/notifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isOk());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
        Notification testNotification = notificationList.get(notificationList.size() - 1);
        assertThat(testNotification.getEntityName()).isEqualTo(UPDATED_ENTITY_NAME);
        assertThat(testNotification.getEntityId()).isEqualTo(UPDATED_ENTITY_ID);
        assertThat(testNotification.isViewed()).isEqualTo(UPDATED_VIEWED);
        assertThat(testNotification.getNotificationDate()).isEqualTo(UPDATED_NOTIFICATION_DATE);
        assertThat(testNotification.getMessage()).isEqualTo(UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingNotification() throws Exception {
        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotificationMockMvc
            .perform(
                put("/api/notifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNotification() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        int databaseSizeBeforeDelete = notificationRepository.findAll().size();

        // Delete the notification
        restNotificationMockMvc
            .perform(delete("/api/notifications/{id}", notification.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
