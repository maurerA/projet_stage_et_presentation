package fr.insy2s.domain;

import static org.assertj.core.api.Assertions.assertThat;

import fr.insy2s.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

public class AdvertisementStatusTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdvertisementStatus.class);
        AdvertisementStatus advertisementStatus1 = new AdvertisementStatus();
        advertisementStatus1.setId(1L);
        AdvertisementStatus advertisementStatus2 = new AdvertisementStatus();
        advertisementStatus2.setId(advertisementStatus1.getId());
        assertThat(advertisementStatus1).isEqualTo(advertisementStatus2);
        advertisementStatus2.setId(2L);
        assertThat(advertisementStatus1).isNotEqualTo(advertisementStatus2);
        advertisementStatus1.setId(null);
        assertThat(advertisementStatus1).isNotEqualTo(advertisementStatus2);
    }
}
