package fr.insy2s.domain;

import static org.assertj.core.api.Assertions.assertThat;

import fr.insy2s.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

public class QuotationStatusTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(QuotationStatus.class);
        QuotationStatus quotationStatus1 = new QuotationStatus();
        quotationStatus1.setId(1L);
        QuotationStatus quotationStatus2 = new QuotationStatus();
        quotationStatus2.setId(quotationStatus1.getId());
        assertThat(quotationStatus1).isEqualTo(quotationStatus2);
        quotationStatus2.setId(2L);
        assertThat(quotationStatus1).isNotEqualTo(quotationStatus2);
        quotationStatus1.setId(null);
        assertThat(quotationStatus1).isNotEqualTo(quotationStatus2);
    }
}
