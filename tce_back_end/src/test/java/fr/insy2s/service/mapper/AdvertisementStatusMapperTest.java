package fr.insy2s.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AdvertisementStatusMapperTest {
    private AdvertisementStatusMapper advertisementStatusMapper;

    @BeforeEach
    public void setUp() {
        advertisementStatusMapper = new AdvertisementStatusMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(advertisementStatusMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(advertisementStatusMapper.fromId(null)).isNull();
    }
}
