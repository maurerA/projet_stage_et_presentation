package fr.insy2s.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class VehicleTypeMapperTest {
    private VehicleTypeMapper vehicleTypeMapper;

    @BeforeEach
    public void setUp() {
        vehicleTypeMapper = new VehicleTypeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(vehicleTypeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(vehicleTypeMapper.fromId(null)).isNull();
    }
}
