package fr.insy2s.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class QuotationMapperTest {
    private QuotationMapper quotationMapper;

    @BeforeEach
    public void setUp() {
        quotationMapper = new QuotationMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(quotationMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(quotationMapper.fromId(null)).isNull();
    }
}
