package fr.insy2s.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class NotationMapperTest {
    private NotationMapper notationMapper;

    @BeforeEach
    public void setUp() {
        notationMapper = new NotationMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(notationMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(notationMapper.fromId(null)).isNull();
    }
}
