package fr.insy2s.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AdvertisementMapperTest {
    private AdvertisementMapper advertisementMapper;

    @BeforeEach
    public void setUp() {
        advertisementMapper = new AdvertisementMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(advertisementMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(advertisementMapper.fromId(null)).isNull();
    }
}
