package fr.insy2s.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PhotoMapperTest {
    private PhotoMapper photoMapper;

    @BeforeEach
    public void setUp() {
        photoMapper = new PhotoMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(photoMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(photoMapper.fromId(null)).isNull();
    }
}
