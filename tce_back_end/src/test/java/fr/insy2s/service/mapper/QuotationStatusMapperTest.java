package fr.insy2s.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class QuotationStatusMapperTest {
    private QuotationStatusMapper quotationStatusMapper;

    @BeforeEach
    public void setUp() {
        quotationStatusMapper = new QuotationStatusMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(quotationStatusMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(quotationStatusMapper.fromId(null)).isNull();
    }
}
