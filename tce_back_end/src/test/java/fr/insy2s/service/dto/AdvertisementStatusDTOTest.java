package fr.insy2s.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import fr.insy2s.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

public class AdvertisementStatusDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdvertisementStatusDTO.class);
        AdvertisementStatusDTO advertisementStatusDTO1 = new AdvertisementStatusDTO();
        advertisementStatusDTO1.setId(1L);
        AdvertisementStatusDTO advertisementStatusDTO2 = new AdvertisementStatusDTO();
        assertThat(advertisementStatusDTO1).isNotEqualTo(advertisementStatusDTO2);
        advertisementStatusDTO2.setId(advertisementStatusDTO1.getId());
        assertThat(advertisementStatusDTO1).isEqualTo(advertisementStatusDTO2);
        advertisementStatusDTO2.setId(2L);
        assertThat(advertisementStatusDTO1).isNotEqualTo(advertisementStatusDTO2);
        advertisementStatusDTO1.setId(null);
        assertThat(advertisementStatusDTO1).isNotEqualTo(advertisementStatusDTO2);
    }
}
