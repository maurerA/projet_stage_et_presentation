package fr.insy2s.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import fr.insy2s.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

public class NotationDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NotationDTO.class);
        NotationDTO notationDTO1 = new NotationDTO();
        notationDTO1.setId(1L);
        NotationDTO notationDTO2 = new NotationDTO();
        assertThat(notationDTO1).isNotEqualTo(notationDTO2);
        notationDTO2.setId(notationDTO1.getId());
        assertThat(notationDTO1).isEqualTo(notationDTO2);
        notationDTO2.setId(2L);
        assertThat(notationDTO1).isNotEqualTo(notationDTO2);
        notationDTO1.setId(null);
        assertThat(notationDTO1).isNotEqualTo(notationDTO2);
    }
}
