import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import userAccount, {
  UserAccountState
} from 'app/entities/user-account/user-account.reducer';
// prettier-ignore
import company, {
  CompanyState
} from 'app/entities/company/company.reducer';
// prettier-ignore
import address, {
  AddressState
} from 'app/entities/address/address.reducer';
// prettier-ignore
import country, {
  CountryState
} from 'app/entities/country/country.reducer';
// prettier-ignore
import advertisement, {
  AdvertisementState
} from 'app/entities/advertisement/advertisement.reducer';
// prettier-ignore
import advertisementStatus, {
  AdvertisementStatusState
} from 'app/entities/advertisement-status/advertisement-status.reducer';
// prettier-ignore
import quotation, {
  QuotationState
} from 'app/entities/quotation/quotation.reducer';
// prettier-ignore
import quotationStatus, {
  QuotationStatusState
} from 'app/entities/quotation-status/quotation-status.reducer';
// prettier-ignore
import notation, {
  NotationState
} from 'app/entities/notation/notation.reducer';
// prettier-ignore
import vehicle, {
  VehicleState
} from 'app/entities/vehicle/vehicle.reducer';
// prettier-ignore
import vehicleType, {
  VehicleTypeState
} from 'app/entities/vehicle-type/vehicle-type.reducer';
// prettier-ignore
import message, {
  MessageState
} from 'app/entities/message/message.reducer';
// prettier-ignore
import photo, {
  PhotoState
} from 'app/entities/photo/photo.reducer';
// prettier-ignore
import notification, {
  NotificationState
} from 'app/entities/notification/notification.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly userAccount: UserAccountState;
  readonly company: CompanyState;
  readonly address: AddressState;
  readonly country: CountryState;
  readonly advertisement: AdvertisementState;
  readonly advertisementStatus: AdvertisementStatusState;
  readonly quotation: QuotationState;
  readonly quotationStatus: QuotationStatusState;
  readonly notation: NotationState;
  readonly vehicle: VehicleState;
  readonly vehicleType: VehicleTypeState;
  readonly message: MessageState;
  readonly photo: PhotoState;
  readonly notification: NotificationState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  userAccount,
  company,
  address,
  country,
  advertisement,
  advertisementStatus,
  quotation,
  quotationStatus,
  notation,
  vehicle,
  vehicleType,
  message,
  photo,
  notification,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
});

export default rootReducer;
