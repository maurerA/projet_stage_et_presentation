import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Translate, translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.entities.main')}
    id="entity-menu"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <MenuItem icon="asterisk" to="/user-account">
      <Translate contentKey="global.menu.entities.userAccount" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/company">
      <Translate contentKey="global.menu.entities.company" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/address">
      <Translate contentKey="global.menu.entities.address" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/country">
      <Translate contentKey="global.menu.entities.country" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/advertisement">
      <Translate contentKey="global.menu.entities.advertisement" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/advertisement-status">
      <Translate contentKey="global.menu.entities.advertisementStatus" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/quotation">
      <Translate contentKey="global.menu.entities.quotation" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/quotation-status">
      <Translate contentKey="global.menu.entities.quotationStatus" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/notation">
      <Translate contentKey="global.menu.entities.notation" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/vehicle">
      <Translate contentKey="global.menu.entities.vehicle" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/vehicle-type">
      <Translate contentKey="global.menu.entities.vehicleType" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/message">
      <Translate contentKey="global.menu.entities.message" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/photo">
      <Translate contentKey="global.menu.entities.photo" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/notification">
      <Translate contentKey="global.menu.entities.notification" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
