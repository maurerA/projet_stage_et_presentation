import { Moment } from 'moment';

export interface IMessage {
  id?: number;
  message?: string;
  dateMessage?: string;
  photoId?: number;
  receiverId?: number;
  senderId?: number;
  advertisementId?: number;
}

export const defaultValue: Readonly<IMessage> = {};
