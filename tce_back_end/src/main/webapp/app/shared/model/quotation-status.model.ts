export interface IQuotationStatus {
  id?: number;
  label?: string;
  codeRef?: string;
}

export const defaultValue: Readonly<IQuotationStatus> = {};
