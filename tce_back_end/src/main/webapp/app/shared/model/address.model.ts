export interface IAddress {
  id?: number;
  streetNumber?: string;
  complementAddress?: string;
  streetName?: string;
  zipCode?: string;
  town?: string;
  countryId?: number;
}

export const defaultValue: Readonly<IAddress> = {};
