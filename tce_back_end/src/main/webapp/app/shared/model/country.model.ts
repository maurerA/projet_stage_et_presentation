export interface ICountry {
  id?: number;
  codeRef?: string;
  label?: string;
}

export const defaultValue: Readonly<ICountry> = {};
