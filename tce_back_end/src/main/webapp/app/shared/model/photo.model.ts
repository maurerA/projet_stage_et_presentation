export interface IPhoto {
  id?: number;
  photoContentType?: string;
  photo?: any;
  photoPath?: string;
  deliveryManId?: number;
  advertisementId?: number;
}

export const defaultValue: Readonly<IPhoto> = {};
