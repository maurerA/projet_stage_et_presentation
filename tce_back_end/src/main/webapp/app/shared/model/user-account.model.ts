export interface IUserAccount {
  id?: number;
  phoneNumber?: string;
  available?: boolean;
  userId?: number;
  adresseId?: number;
  listBlockeds?: IUserAccount[];
  companyId?: number;
  listBlockedBies?: IUserAccount[];
}

export const defaultValue: Readonly<IUserAccount> = {
  available: false,
};
