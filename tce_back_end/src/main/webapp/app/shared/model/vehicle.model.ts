export interface IVehicle {
  id?: number;
  maxCapacityLength?: number;
  maxCapacityWidth?: number;
  maxCapacityHeight?: number;
  maxCapacityWeight?: number;
  active?: boolean;
  typeId?: number;
  deliveryManId?: number;
}

export const defaultValue: Readonly<IVehicle> = {
  active: false,
};
