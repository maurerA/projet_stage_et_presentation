import { Moment } from 'moment';

export interface INotification {
  id?: number;
  entityName?: string;
  entityId?: number;
  viewed?: boolean;
  notificationDate?: string;
  message?: string;
  userAccountId?: number;
}

export const defaultValue: Readonly<INotification> = {
  viewed: false,
};
