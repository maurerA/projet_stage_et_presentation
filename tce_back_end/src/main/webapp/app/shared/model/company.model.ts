import { IUserAccount } from 'app/shared/model/user-account.model';

export interface ICompany {
  id?: number;
  nameCompany?: string;
  numberSiretCompany?: string;
  numberSiren?: string;
  listDeliveryMen?: IUserAccount[];
}

export const defaultValue: Readonly<ICompany> = {};
