import { Moment } from 'moment';

export interface INotation {
  id?: number;
  numberStar?: number;
  message?: string;
  dateNote?: string;
  receiverId?: number;
  senderId?: number;
}

export const defaultValue: Readonly<INotation> = {};
