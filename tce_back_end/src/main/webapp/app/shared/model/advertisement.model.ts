import { Moment } from 'moment';

export interface IAdvertisement {
  id?: number;
  title?: string;
  weight?: number;
  length?: number;
  width?: number;
  height?: number;
  distanceKm?: number;
  delivered?: boolean;
  creationDate?: string;
  wishDate?: string;
  effectiveDate?: string;
  createdById?: number;
  statusId?: number;
  startAddressId?: number;
  arrivalAddressId?: number;
}

export const defaultValue: Readonly<IAdvertisement> = {
  delivered: false,
};
