export interface IAdvertisementStatus {
  id?: number;
  codeRef?: string;
  label?: string;
}

export const defaultValue: Readonly<IAdvertisementStatus> = {};
