import { Moment } from 'moment';

export interface IQuotation {
  id?: number;
  deliveryManPrice?: number;
  creationDateQuotation?: string;
  statusId?: number;
  deliveryManId?: number;
  advertisementId?: number;
}

export const defaultValue: Readonly<IQuotation> = {};
