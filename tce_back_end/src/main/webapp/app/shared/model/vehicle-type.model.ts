export interface IVehicleType {
  id?: number;
  codeRef?: string;
  label?: string;
}

export const defaultValue: Readonly<IVehicleType> = {};
