import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './notification.reducer';
import { INotification } from 'app/shared/model/notification.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface INotificationDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const NotificationDetail = (props: INotificationDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { notificationEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="tcebackendApp.notification.detail.title">Notification</Translate> [<b>{notificationEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="entityName">
              <Translate contentKey="tcebackendApp.notification.entityName">Entity Name</Translate>
            </span>
          </dt>
          <dd>{notificationEntity.entityName}</dd>
          <dt>
            <span id="entityId">
              <Translate contentKey="tcebackendApp.notification.entityId">Entity Id</Translate>
            </span>
          </dt>
          <dd>{notificationEntity.entityId}</dd>
          <dt>
            <span id="viewed">
              <Translate contentKey="tcebackendApp.notification.viewed">Viewed</Translate>
            </span>
          </dt>
          <dd>{notificationEntity.viewed ? 'true' : 'false'}</dd>
          <dt>
            <span id="notificationDate">
              <Translate contentKey="tcebackendApp.notification.notificationDate">Notification Date</Translate>
            </span>
          </dt>
          <dd>
            {notificationEntity.notificationDate ? (
              <TextFormat value={notificationEntity.notificationDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="message">
              <Translate contentKey="tcebackendApp.notification.message">Message</Translate>
            </span>
          </dt>
          <dd>{notificationEntity.message}</dd>
          <dt>
            <Translate contentKey="tcebackendApp.notification.userAccount">User Account</Translate>
          </dt>
          <dd>{notificationEntity.userAccountId ? notificationEntity.userAccountId : ''}</dd>
        </dl>
        <Button tag={Link} to="/notification" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/notification/${notificationEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ notification }: IRootState) => ({
  notificationEntity: notification.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(NotificationDetail);
