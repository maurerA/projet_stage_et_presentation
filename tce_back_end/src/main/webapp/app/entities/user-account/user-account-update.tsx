import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { IAddress } from 'app/shared/model/address.model';
import { getEntities as getAddresses } from 'app/entities/address/address.reducer';
import { getEntities as getUserAccounts } from 'app/entities/user-account/user-account.reducer';
import { ICompany } from 'app/shared/model/company.model';
import { getEntities as getCompanies } from 'app/entities/company/company.reducer';
import { getEntity, updateEntity, createEntity, reset } from './user-account.reducer';
import { IUserAccount } from 'app/shared/model/user-account.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IUserAccountUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const UserAccountUpdate = (props: IUserAccountUpdateProps) => {
  const [idslistBlocked, setIdslistBlocked] = useState([]);
  const [userId, setUserId] = useState('0');
  const [adresseId, setAdresseId] = useState('0');
  const [listBlockedById, setListBlockedById] = useState('0');
  const [companyId, setCompanyId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { userAccountEntity, users, addresses, userAccounts, companies, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/user-account' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getUsers();
    props.getAddresses();
    props.getUserAccounts();
    props.getCompanies();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...userAccountEntity,
        ...values,
        listBlockeds: mapIdList(values.listBlockeds),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="tcebackendApp.userAccount.home.createOrEditLabel">
            <Translate contentKey="tcebackendApp.userAccount.home.createOrEditLabel">Create or edit a UserAccount</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : userAccountEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="user-account-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="user-account-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="phoneNumberLabel" for="user-account-phoneNumber">
                  <Translate contentKey="tcebackendApp.userAccount.phoneNumber">Phone Number</Translate>
                </Label>
                <AvField
                  id="user-account-phoneNumber"
                  type="text"
                  name="phoneNumber"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup check>
                <Label id="availableLabel">
                  <AvInput id="user-account-available" type="checkbox" className="form-check-input" name="available" />
                  <Translate contentKey="tcebackendApp.userAccount.available">Available</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label for="user-account-user">
                  <Translate contentKey="tcebackendApp.userAccount.user">User</Translate>
                </Label>
                <AvInput id="user-account-user" type="select" className="form-control" name="userId" required>
                  {users
                    ? users.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="user-account-adresse">
                  <Translate contentKey="tcebackendApp.userAccount.adresse">Adresse</Translate>
                </Label>
                <AvInput id="user-account-adresse" type="select" className="form-control" name="adresseId" required>
                  {addresses
                    ? addresses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="user-account-listBlocked">
                  <Translate contentKey="tcebackendApp.userAccount.listBlocked">List Blocked</Translate>
                </Label>
                <AvInput
                  id="user-account-listBlocked"
                  type="select"
                  multiple
                  className="form-control"
                  name="listBlockeds"
                  value={userAccountEntity.listBlockeds && userAccountEntity.listBlockeds.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {userAccounts
                    ? userAccounts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="user-account-company">
                  <Translate contentKey="tcebackendApp.userAccount.company">Company</Translate>
                </Label>
                <AvInput id="user-account-company" type="select" className="form-control" name="companyId">
                  <option value="" key="0" />
                  {companies
                    ? companies.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/user-account" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  users: storeState.userManagement.users,
  addresses: storeState.address.entities,
  userAccounts: storeState.userAccount.entities,
  companies: storeState.company.entities,
  userAccountEntity: storeState.userAccount.entity,
  loading: storeState.userAccount.loading,
  updating: storeState.userAccount.updating,
  updateSuccess: storeState.userAccount.updateSuccess,
});

const mapDispatchToProps = {
  getUsers,
  getAddresses,
  getUserAccounts,
  getCompanies,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserAccountUpdate);
