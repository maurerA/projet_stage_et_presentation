import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import UserAccount from './user-account';
import Company from './company';
import Address from './address';
import Country from './country';
import Advertisement from './advertisement';
import AdvertisementStatus from './advertisement-status';
import Quotation from './quotation';
import QuotationStatus from './quotation-status';
import Notation from './notation';
import Vehicle from './vehicle';
import VehicleType from './vehicle-type';
import Message from './message';
import Photo from './photo';
import Notification from './notification';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}user-account`} component={UserAccount} />
      <ErrorBoundaryRoute path={`${match.url}company`} component={Company} />
      <ErrorBoundaryRoute path={`${match.url}address`} component={Address} />
      <ErrorBoundaryRoute path={`${match.url}country`} component={Country} />
      <ErrorBoundaryRoute path={`${match.url}advertisement`} component={Advertisement} />
      <ErrorBoundaryRoute path={`${match.url}advertisement-status`} component={AdvertisementStatus} />
      <ErrorBoundaryRoute path={`${match.url}quotation`} component={Quotation} />
      <ErrorBoundaryRoute path={`${match.url}quotation-status`} component={QuotationStatus} />
      <ErrorBoundaryRoute path={`${match.url}notation`} component={Notation} />
      <ErrorBoundaryRoute path={`${match.url}vehicle`} component={Vehicle} />
      <ErrorBoundaryRoute path={`${match.url}vehicle-type`} component={VehicleType} />
      <ErrorBoundaryRoute path={`${match.url}message`} component={Message} />
      <ErrorBoundaryRoute path={`${match.url}photo`} component={Photo} />
      <ErrorBoundaryRoute path={`${match.url}notification`} component={Notification} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
