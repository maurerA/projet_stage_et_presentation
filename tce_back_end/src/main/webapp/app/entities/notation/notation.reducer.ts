import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { INotation, defaultValue } from 'app/shared/model/notation.model';

export const ACTION_TYPES = {
  FETCH_NOTATION_LIST: 'notation/FETCH_NOTATION_LIST',
  FETCH_NOTATION: 'notation/FETCH_NOTATION',
  CREATE_NOTATION: 'notation/CREATE_NOTATION',
  UPDATE_NOTATION: 'notation/UPDATE_NOTATION',
  DELETE_NOTATION: 'notation/DELETE_NOTATION',
  RESET: 'notation/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INotation>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type NotationState = Readonly<typeof initialState>;

// Reducer

export default (state: NotationState = initialState, action): NotationState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NOTATION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NOTATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_NOTATION):
    case REQUEST(ACTION_TYPES.UPDATE_NOTATION):
    case REQUEST(ACTION_TYPES.DELETE_NOTATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_NOTATION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NOTATION):
    case FAILURE(ACTION_TYPES.CREATE_NOTATION):
    case FAILURE(ACTION_TYPES.UPDATE_NOTATION):
    case FAILURE(ACTION_TYPES.DELETE_NOTATION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_NOTATION_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_NOTATION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_NOTATION):
    case SUCCESS(ACTION_TYPES.UPDATE_NOTATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_NOTATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/notations';

// Actions

export const getEntities: ICrudGetAllAction<INotation> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_NOTATION_LIST,
    payload: axios.get<INotation>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<INotation> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NOTATION,
    payload: axios.get<INotation>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<INotation> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NOTATION,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INotation> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NOTATION,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<INotation> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NOTATION,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
