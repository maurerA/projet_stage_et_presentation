import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './notation.reducer';
import { INotation } from 'app/shared/model/notation.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface INotationDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const NotationDetail = (props: INotationDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { notationEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="tcebackendApp.notation.detail.title">Notation</Translate> [<b>{notationEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="numberStar">
              <Translate contentKey="tcebackendApp.notation.numberStar">Number Star</Translate>
            </span>
          </dt>
          <dd>{notationEntity.numberStar}</dd>
          <dt>
            <span id="message">
              <Translate contentKey="tcebackendApp.notation.message">Message</Translate>
            </span>
          </dt>
          <dd>{notationEntity.message}</dd>
          <dt>
            <span id="dateNote">
              <Translate contentKey="tcebackendApp.notation.dateNote">Date Note</Translate>
            </span>
          </dt>
          <dd>{notationEntity.dateNote ? <TextFormat value={notationEntity.dateNote} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <Translate contentKey="tcebackendApp.notation.receiver">Receiver</Translate>
          </dt>
          <dd>{notationEntity.receiverId ? notationEntity.receiverId : ''}</dd>
          <dt>
            <Translate contentKey="tcebackendApp.notation.sender">Sender</Translate>
          </dt>
          <dd>{notationEntity.senderId ? notationEntity.senderId : ''}</dd>
        </dl>
        <Button tag={Link} to="/notation" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/notation/${notationEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ notation }: IRootState) => ({
  notationEntity: notation.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(NotationDetail);
