import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUserAccount } from 'app/shared/model/user-account.model';
import { getEntities as getUserAccounts } from 'app/entities/user-account/user-account.reducer';
import { getEntity, updateEntity, createEntity, reset } from './notation.reducer';
import { INotation } from 'app/shared/model/notation.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface INotationUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const NotationUpdate = (props: INotationUpdateProps) => {
  const [receiverId, setReceiverId] = useState('0');
  const [senderId, setSenderId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { notationEntity, userAccounts, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/notation' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getUserAccounts();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.dateNote = convertDateTimeToServer(values.dateNote);

    if (errors.length === 0) {
      const entity = {
        ...notationEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="tcebackendApp.notation.home.createOrEditLabel">
            <Translate contentKey="tcebackendApp.notation.home.createOrEditLabel">Create or edit a Notation</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : notationEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="notation-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="notation-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="numberStarLabel" for="notation-numberStar">
                  <Translate contentKey="tcebackendApp.notation.numberStar">Number Star</Translate>
                </Label>
                <AvField
                  id="notation-numberStar"
                  type="string"
                  className="form-control"
                  name="numberStar"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="messageLabel" for="notation-message">
                  <Translate contentKey="tcebackendApp.notation.message">Message</Translate>
                </Label>
                <AvField id="notation-message" type="text" name="message" />
              </AvGroup>
              <AvGroup>
                <Label id="dateNoteLabel" for="notation-dateNote">
                  <Translate contentKey="tcebackendApp.notation.dateNote">Date Note</Translate>
                </Label>
                <AvInput
                  id="notation-dateNote"
                  type="datetime-local"
                  className="form-control"
                  name="dateNote"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.notationEntity.dateNote)}
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="notation-receiver">
                  <Translate contentKey="tcebackendApp.notation.receiver">Receiver</Translate>
                </Label>
                <AvInput id="notation-receiver" type="select" className="form-control" name="receiverId" required>
                  {userAccounts
                    ? userAccounts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="notation-sender">
                  <Translate contentKey="tcebackendApp.notation.sender">Sender</Translate>
                </Label>
                <AvInput id="notation-sender" type="select" className="form-control" name="senderId" required>
                  {userAccounts
                    ? userAccounts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/notation" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  userAccounts: storeState.userAccount.entities,
  notationEntity: storeState.notation.entity,
  loading: storeState.notation.loading,
  updating: storeState.notation.updating,
  updateSuccess: storeState.notation.updateSuccess,
});

const mapDispatchToProps = {
  getUserAccounts,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(NotationUpdate);
