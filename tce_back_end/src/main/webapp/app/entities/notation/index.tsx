import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Notation from './notation';
import NotationDetail from './notation-detail';
import NotationUpdate from './notation-update';
import NotationDeleteDialog from './notation-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NotationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NotationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NotationDetail} />
      <ErrorBoundaryRoute path={match.url} component={Notation} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={NotationDeleteDialog} />
  </>
);

export default Routes;
