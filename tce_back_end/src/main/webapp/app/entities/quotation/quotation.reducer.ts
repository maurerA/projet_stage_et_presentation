import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IQuotation, defaultValue } from 'app/shared/model/quotation.model';

export const ACTION_TYPES = {
  FETCH_QUOTATION_LIST: 'quotation/FETCH_QUOTATION_LIST',
  FETCH_QUOTATION: 'quotation/FETCH_QUOTATION',
  CREATE_QUOTATION: 'quotation/CREATE_QUOTATION',
  UPDATE_QUOTATION: 'quotation/UPDATE_QUOTATION',
  DELETE_QUOTATION: 'quotation/DELETE_QUOTATION',
  RESET: 'quotation/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IQuotation>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type QuotationState = Readonly<typeof initialState>;

// Reducer

export default (state: QuotationState = initialState, action): QuotationState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_QUOTATION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_QUOTATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_QUOTATION):
    case REQUEST(ACTION_TYPES.UPDATE_QUOTATION):
    case REQUEST(ACTION_TYPES.DELETE_QUOTATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_QUOTATION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_QUOTATION):
    case FAILURE(ACTION_TYPES.CREATE_QUOTATION):
    case FAILURE(ACTION_TYPES.UPDATE_QUOTATION):
    case FAILURE(ACTION_TYPES.DELETE_QUOTATION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_QUOTATION_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_QUOTATION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_QUOTATION):
    case SUCCESS(ACTION_TYPES.UPDATE_QUOTATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_QUOTATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/quotations';

// Actions

export const getEntities: ICrudGetAllAction<IQuotation> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_QUOTATION_LIST,
    payload: axios.get<IQuotation>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IQuotation> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_QUOTATION,
    payload: axios.get<IQuotation>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IQuotation> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_QUOTATION,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IQuotation> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_QUOTATION,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IQuotation> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_QUOTATION,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
