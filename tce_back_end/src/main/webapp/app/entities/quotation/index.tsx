import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Quotation from './quotation';
import QuotationDetail from './quotation-detail';
import QuotationUpdate from './quotation-update';
import QuotationDeleteDialog from './quotation-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={QuotationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={QuotationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={QuotationDetail} />
      <ErrorBoundaryRoute path={match.url} component={Quotation} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={QuotationDeleteDialog} />
  </>
);

export default Routes;
