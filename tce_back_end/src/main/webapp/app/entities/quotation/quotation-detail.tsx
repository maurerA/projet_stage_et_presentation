import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './quotation.reducer';
import { IQuotation } from 'app/shared/model/quotation.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IQuotationDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const QuotationDetail = (props: IQuotationDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { quotationEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="tcebackendApp.quotation.detail.title">Quotation</Translate> [<b>{quotationEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="deliveryManPrice">
              <Translate contentKey="tcebackendApp.quotation.deliveryManPrice">Delivery Man Price</Translate>
            </span>
          </dt>
          <dd>{quotationEntity.deliveryManPrice}</dd>
          <dt>
            <span id="creationDateQuotation">
              <Translate contentKey="tcebackendApp.quotation.creationDateQuotation">Creation Date Quotation</Translate>
            </span>
          </dt>
          <dd>
            {quotationEntity.creationDateQuotation ? (
              <TextFormat value={quotationEntity.creationDateQuotation} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="tcebackendApp.quotation.status">Status</Translate>
          </dt>
          <dd>{quotationEntity.statusId ? quotationEntity.statusId : ''}</dd>
          <dt>
            <Translate contentKey="tcebackendApp.quotation.deliveryMan">Delivery Man</Translate>
          </dt>
          <dd>{quotationEntity.deliveryManId ? quotationEntity.deliveryManId : ''}</dd>
          <dt>
            <Translate contentKey="tcebackendApp.quotation.advertisement">Advertisement</Translate>
          </dt>
          <dd>{quotationEntity.advertisementId ? quotationEntity.advertisementId : ''}</dd>
        </dl>
        <Button tag={Link} to="/quotation" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/quotation/${quotationEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ quotation }: IRootState) => ({
  quotationEntity: quotation.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(QuotationDetail);
