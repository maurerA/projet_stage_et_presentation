import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IQuotationStatus } from 'app/shared/model/quotation-status.model';
import { getEntities as getQuotationStatuses } from 'app/entities/quotation-status/quotation-status.reducer';
import { IUserAccount } from 'app/shared/model/user-account.model';
import { getEntities as getUserAccounts } from 'app/entities/user-account/user-account.reducer';
import { IAdvertisement } from 'app/shared/model/advertisement.model';
import { getEntities as getAdvertisements } from 'app/entities/advertisement/advertisement.reducer';
import { getEntity, updateEntity, createEntity, reset } from './quotation.reducer';
import { IQuotation } from 'app/shared/model/quotation.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IQuotationUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const QuotationUpdate = (props: IQuotationUpdateProps) => {
  const [statusId, setStatusId] = useState('0');
  const [deliveryManId, setDeliveryManId] = useState('0');
  const [advertisementId, setAdvertisementId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { quotationEntity, quotationStatuses, userAccounts, advertisements, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/quotation' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getQuotationStatuses();
    props.getUserAccounts();
    props.getAdvertisements();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.creationDateQuotation = convertDateTimeToServer(values.creationDateQuotation);

    if (errors.length === 0) {
      const entity = {
        ...quotationEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="tcebackendApp.quotation.home.createOrEditLabel">
            <Translate contentKey="tcebackendApp.quotation.home.createOrEditLabel">Create or edit a Quotation</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : quotationEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="quotation-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="quotation-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="deliveryManPriceLabel" for="quotation-deliveryManPrice">
                  <Translate contentKey="tcebackendApp.quotation.deliveryManPrice">Delivery Man Price</Translate>
                </Label>
                <AvField
                  id="quotation-deliveryManPrice"
                  type="string"
                  className="form-control"
                  name="deliveryManPrice"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="creationDateQuotationLabel" for="quotation-creationDateQuotation">
                  <Translate contentKey="tcebackendApp.quotation.creationDateQuotation">Creation Date Quotation</Translate>
                </Label>
                <AvInput
                  id="quotation-creationDateQuotation"
                  type="datetime-local"
                  className="form-control"
                  name="creationDateQuotation"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.quotationEntity.creationDateQuotation)}
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="quotation-status">
                  <Translate contentKey="tcebackendApp.quotation.status">Status</Translate>
                </Label>
                <AvInput id="quotation-status" type="select" className="form-control" name="statusId" required>
                  {quotationStatuses
                    ? quotationStatuses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="quotation-deliveryMan">
                  <Translate contentKey="tcebackendApp.quotation.deliveryMan">Delivery Man</Translate>
                </Label>
                <AvInput id="quotation-deliveryMan" type="select" className="form-control" name="deliveryManId" required>
                  {userAccounts
                    ? userAccounts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="quotation-advertisement">
                  <Translate contentKey="tcebackendApp.quotation.advertisement">Advertisement</Translate>
                </Label>
                <AvInput id="quotation-advertisement" type="select" className="form-control" name="advertisementId" required>
                  {advertisements
                    ? advertisements.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/quotation" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  quotationStatuses: storeState.quotationStatus.entities,
  userAccounts: storeState.userAccount.entities,
  advertisements: storeState.advertisement.entities,
  quotationEntity: storeState.quotation.entity,
  loading: storeState.quotation.loading,
  updating: storeState.quotation.updating,
  updateSuccess: storeState.quotation.updateSuccess,
});

const mapDispatchToProps = {
  getQuotationStatuses,
  getUserAccounts,
  getAdvertisements,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(QuotationUpdate);
