import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './quotation-status.reducer';
import { IQuotationStatus } from 'app/shared/model/quotation-status.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IQuotationStatusDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const QuotationStatusDetail = (props: IQuotationStatusDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { quotationStatusEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="tcebackendApp.quotationStatus.detail.title">QuotationStatus</Translate> [<b>{quotationStatusEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="label">
              <Translate contentKey="tcebackendApp.quotationStatus.label">Label</Translate>
            </span>
          </dt>
          <dd>{quotationStatusEntity.label}</dd>
          <dt>
            <span id="codeRef">
              <Translate contentKey="tcebackendApp.quotationStatus.codeRef">Code Ref</Translate>
            </span>
          </dt>
          <dd>{quotationStatusEntity.codeRef}</dd>
        </dl>
        <Button tag={Link} to="/quotation-status" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/quotation-status/${quotationStatusEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ quotationStatus }: IRootState) => ({
  quotationStatusEntity: quotationStatus.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(QuotationStatusDetail);
