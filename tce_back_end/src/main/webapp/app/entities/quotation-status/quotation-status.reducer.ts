import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IQuotationStatus, defaultValue } from 'app/shared/model/quotation-status.model';

export const ACTION_TYPES = {
  FETCH_QUOTATIONSTATUS_LIST: 'quotationStatus/FETCH_QUOTATIONSTATUS_LIST',
  FETCH_QUOTATIONSTATUS: 'quotationStatus/FETCH_QUOTATIONSTATUS',
  CREATE_QUOTATIONSTATUS: 'quotationStatus/CREATE_QUOTATIONSTATUS',
  UPDATE_QUOTATIONSTATUS: 'quotationStatus/UPDATE_QUOTATIONSTATUS',
  DELETE_QUOTATIONSTATUS: 'quotationStatus/DELETE_QUOTATIONSTATUS',
  RESET: 'quotationStatus/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IQuotationStatus>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type QuotationStatusState = Readonly<typeof initialState>;

// Reducer

export default (state: QuotationStatusState = initialState, action): QuotationStatusState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_QUOTATIONSTATUS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_QUOTATIONSTATUS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_QUOTATIONSTATUS):
    case REQUEST(ACTION_TYPES.UPDATE_QUOTATIONSTATUS):
    case REQUEST(ACTION_TYPES.DELETE_QUOTATIONSTATUS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_QUOTATIONSTATUS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_QUOTATIONSTATUS):
    case FAILURE(ACTION_TYPES.CREATE_QUOTATIONSTATUS):
    case FAILURE(ACTION_TYPES.UPDATE_QUOTATIONSTATUS):
    case FAILURE(ACTION_TYPES.DELETE_QUOTATIONSTATUS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_QUOTATIONSTATUS_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_QUOTATIONSTATUS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_QUOTATIONSTATUS):
    case SUCCESS(ACTION_TYPES.UPDATE_QUOTATIONSTATUS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_QUOTATIONSTATUS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/quotation-statuses';

// Actions

export const getEntities: ICrudGetAllAction<IQuotationStatus> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_QUOTATIONSTATUS_LIST,
  payload: axios.get<IQuotationStatus>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IQuotationStatus> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_QUOTATIONSTATUS,
    payload: axios.get<IQuotationStatus>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IQuotationStatus> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_QUOTATIONSTATUS,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IQuotationStatus> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_QUOTATIONSTATUS,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IQuotationStatus> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_QUOTATIONSTATUS,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
