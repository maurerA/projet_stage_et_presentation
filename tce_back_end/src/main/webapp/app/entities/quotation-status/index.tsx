import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import QuotationStatus from './quotation-status';
import QuotationStatusDetail from './quotation-status-detail';
import QuotationStatusUpdate from './quotation-status-update';
import QuotationStatusDeleteDialog from './quotation-status-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={QuotationStatusUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={QuotationStatusUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={QuotationStatusDetail} />
      <ErrorBoundaryRoute path={match.url} component={QuotationStatus} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={QuotationStatusDeleteDialog} />
  </>
);

export default Routes;
