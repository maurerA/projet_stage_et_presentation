import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUserAccount } from 'app/shared/model/user-account.model';
import { getEntities as getUserAccounts } from 'app/entities/user-account/user-account.reducer';
import { IAdvertisementStatus } from 'app/shared/model/advertisement-status.model';
import { getEntities as getAdvertisementStatuses } from 'app/entities/advertisement-status/advertisement-status.reducer';
import { IAddress } from 'app/shared/model/address.model';
import { getEntities as getAddresses } from 'app/entities/address/address.reducer';
import { getEntity, updateEntity, createEntity, reset } from './advertisement.reducer';
import { IAdvertisement } from 'app/shared/model/advertisement.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IAdvertisementUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AdvertisementUpdate = (props: IAdvertisementUpdateProps) => {
  const [createdById, setCreatedById] = useState('0');
  const [statusId, setStatusId] = useState('0');
  const [startAddressId, setStartAddressId] = useState('0');
  const [arrivalAddressId, setArrivalAddressId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { advertisementEntity, userAccounts, advertisementStatuses, addresses, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/advertisement' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getUserAccounts();
    props.getAdvertisementStatuses();
    props.getAddresses();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.creationDate = convertDateTimeToServer(values.creationDate);
    values.wishDate = convertDateTimeToServer(values.wishDate);
    values.effectiveDate = convertDateTimeToServer(values.effectiveDate);

    if (errors.length === 0) {
      const entity = {
        ...advertisementEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="tcebackendApp.advertisement.home.createOrEditLabel">
            <Translate contentKey="tcebackendApp.advertisement.home.createOrEditLabel">Create or edit a Advertisement</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : advertisementEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="advertisement-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="advertisement-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="titleLabel" for="advertisement-title">
                  <Translate contentKey="tcebackendApp.advertisement.title">Title</Translate>
                </Label>
                <AvField
                  id="advertisement-title"
                  type="text"
                  name="title"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="weightLabel" for="advertisement-weight">
                  <Translate contentKey="tcebackendApp.advertisement.weight">Weight</Translate>
                </Label>
                <AvField
                  id="advertisement-weight"
                  type="string"
                  className="form-control"
                  name="weight"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="lengthLabel" for="advertisement-length">
                  <Translate contentKey="tcebackendApp.advertisement.length">Length</Translate>
                </Label>
                <AvField
                  id="advertisement-length"
                  type="string"
                  className="form-control"
                  name="length"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="widthLabel" for="advertisement-width">
                  <Translate contentKey="tcebackendApp.advertisement.width">Width</Translate>
                </Label>
                <AvField
                  id="advertisement-width"
                  type="string"
                  className="form-control"
                  name="width"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="heightLabel" for="advertisement-height">
                  <Translate contentKey="tcebackendApp.advertisement.height">Height</Translate>
                </Label>
                <AvField
                  id="advertisement-height"
                  type="string"
                  className="form-control"
                  name="height"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="distanceKmLabel" for="advertisement-distanceKm">
                  <Translate contentKey="tcebackendApp.advertisement.distanceKm">Distance Km</Translate>
                </Label>
                <AvField
                  id="advertisement-distanceKm"
                  type="string"
                  className="form-control"
                  name="distanceKm"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup check>
                <Label id="deliveredLabel">
                  <AvInput id="advertisement-delivered" type="checkbox" className="form-check-input" name="delivered" />
                  <Translate contentKey="tcebackendApp.advertisement.delivered">Delivered</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label id="creationDateLabel" for="advertisement-creationDate">
                  <Translate contentKey="tcebackendApp.advertisement.creationDate">Creation Date</Translate>
                </Label>
                <AvInput
                  id="advertisement-creationDate"
                  type="datetime-local"
                  className="form-control"
                  name="creationDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.advertisementEntity.creationDate)}
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="wishDateLabel" for="advertisement-wishDate">
                  <Translate contentKey="tcebackendApp.advertisement.wishDate">Wish Date</Translate>
                </Label>
                <AvInput
                  id="advertisement-wishDate"
                  type="datetime-local"
                  className="form-control"
                  name="wishDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.advertisementEntity.wishDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="effectiveDateLabel" for="advertisement-effectiveDate">
                  <Translate contentKey="tcebackendApp.advertisement.effectiveDate">Effective Date</Translate>
                </Label>
                <AvInput
                  id="advertisement-effectiveDate"
                  type="datetime-local"
                  className="form-control"
                  name="effectiveDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.advertisementEntity.effectiveDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label for="advertisement-createdBy">
                  <Translate contentKey="tcebackendApp.advertisement.createdBy">Created By</Translate>
                </Label>
                <AvInput id="advertisement-createdBy" type="select" className="form-control" name="createdById" required>
                  {userAccounts
                    ? userAccounts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="advertisement-status">
                  <Translate contentKey="tcebackendApp.advertisement.status">Status</Translate>
                </Label>
                <AvInput id="advertisement-status" type="select" className="form-control" name="statusId">
                  <option value="" key="0" />
                  {advertisementStatuses
                    ? advertisementStatuses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="advertisement-startAddress">
                  <Translate contentKey="tcebackendApp.advertisement.startAddress">Start Address</Translate>
                </Label>
                <AvInput id="advertisement-startAddress" type="select" className="form-control" name="startAddressId" required>
                  {addresses
                    ? addresses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="advertisement-arrivalAddress">
                  <Translate contentKey="tcebackendApp.advertisement.arrivalAddress">Arrival Address</Translate>
                </Label>
                <AvInput id="advertisement-arrivalAddress" type="select" className="form-control" name="arrivalAddressId" required>
                  {addresses
                    ? addresses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/advertisement" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  userAccounts: storeState.userAccount.entities,
  advertisementStatuses: storeState.advertisementStatus.entities,
  addresses: storeState.address.entities,
  advertisementEntity: storeState.advertisement.entity,
  loading: storeState.advertisement.loading,
  updating: storeState.advertisement.updating,
  updateSuccess: storeState.advertisement.updateSuccess,
});

const mapDispatchToProps = {
  getUserAccounts,
  getAdvertisementStatuses,
  getAddresses,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AdvertisementUpdate);
