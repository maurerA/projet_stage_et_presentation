import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './advertisement.reducer';
import { IAdvertisement } from 'app/shared/model/advertisement.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';

export interface IAdvertisementProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Advertisement = (props: IAdvertisementProps) => {
  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE), props.location.search)
  );

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const sortEntities = () => {
    getAllEntities();
    const endURL = `?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`;
    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  useEffect(() => {
    const params = new URLSearchParams(props.location.search);
    const page = params.get('page');
    const sort = params.get('sort');
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPaginationState({
        ...paginationState,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [props.location.search]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage,
    });

  const { advertisementList, match, loading, totalItems } = props;
  return (
    <div>
      <h2 id="advertisement-heading">
        <Translate contentKey="tcebackendApp.advertisement.home.title">Advertisements</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="tcebackendApp.advertisement.home.createLabel">Create new Advertisement</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {advertisementList && advertisementList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('title')}>
                  <Translate contentKey="tcebackendApp.advertisement.title">Title</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('weight')}>
                  <Translate contentKey="tcebackendApp.advertisement.weight">Weight</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('length')}>
                  <Translate contentKey="tcebackendApp.advertisement.length">Length</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('width')}>
                  <Translate contentKey="tcebackendApp.advertisement.width">Width</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('height')}>
                  <Translate contentKey="tcebackendApp.advertisement.height">Height</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('distanceKm')}>
                  <Translate contentKey="tcebackendApp.advertisement.distanceKm">Distance Km</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('delivered')}>
                  <Translate contentKey="tcebackendApp.advertisement.delivered">Delivered</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('creationDate')}>
                  <Translate contentKey="tcebackendApp.advertisement.creationDate">Creation Date</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('wishDate')}>
                  <Translate contentKey="tcebackendApp.advertisement.wishDate">Wish Date</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('effectiveDate')}>
                  <Translate contentKey="tcebackendApp.advertisement.effectiveDate">Effective Date</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="tcebackendApp.advertisement.createdBy">Created By</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="tcebackendApp.advertisement.status">Status</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="tcebackendApp.advertisement.startAddress">Start Address</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="tcebackendApp.advertisement.arrivalAddress">Arrival Address</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {advertisementList.map((advertisement, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${advertisement.id}`} color="link" size="sm">
                      {advertisement.id}
                    </Button>
                  </td>
                  <td>{advertisement.title}</td>
                  <td>{advertisement.weight}</td>
                  <td>{advertisement.length}</td>
                  <td>{advertisement.width}</td>
                  <td>{advertisement.height}</td>
                  <td>{advertisement.distanceKm}</td>
                  <td>{advertisement.delivered ? 'true' : 'false'}</td>
                  <td>
                    {advertisement.creationDate ? (
                      <TextFormat type="date" value={advertisement.creationDate} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {advertisement.wishDate ? <TextFormat type="date" value={advertisement.wishDate} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {advertisement.effectiveDate ? (
                      <TextFormat type="date" value={advertisement.effectiveDate} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {advertisement.createdById ? (
                      <Link to={`user-account/${advertisement.createdById}`}>{advertisement.createdById}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {advertisement.statusId ? (
                      <Link to={`advertisement-status/${advertisement.statusId}`}>{advertisement.statusId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {advertisement.startAddressId ? (
                      <Link to={`address/${advertisement.startAddressId}`}>{advertisement.startAddressId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {advertisement.arrivalAddressId ? (
                      <Link to={`address/${advertisement.arrivalAddressId}`}>{advertisement.arrivalAddressId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${advertisement.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${advertisement.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${advertisement.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="tcebackendApp.advertisement.home.notFound">No Advertisements found</Translate>
            </div>
          )
        )}
      </div>
      {props.totalItems ? (
        <div className={advertisementList && advertisementList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={paginationState.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={paginationState.itemsPerPage}
              totalItems={props.totalItems}
            />
          </Row>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

const mapStateToProps = ({ advertisement }: IRootState) => ({
  advertisementList: advertisement.entities,
  loading: advertisement.loading,
  totalItems: advertisement.totalItems,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Advertisement);
