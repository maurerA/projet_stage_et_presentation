import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './advertisement.reducer';
import { IAdvertisement } from 'app/shared/model/advertisement.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAdvertisementDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AdvertisementDetail = (props: IAdvertisementDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { advertisementEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="tcebackendApp.advertisement.detail.title">Advertisement</Translate> [<b>{advertisementEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="title">
              <Translate contentKey="tcebackendApp.advertisement.title">Title</Translate>
            </span>
          </dt>
          <dd>{advertisementEntity.title}</dd>
          <dt>
            <span id="weight">
              <Translate contentKey="tcebackendApp.advertisement.weight">Weight</Translate>
            </span>
          </dt>
          <dd>{advertisementEntity.weight}</dd>
          <dt>
            <span id="length">
              <Translate contentKey="tcebackendApp.advertisement.length">Length</Translate>
            </span>
          </dt>
          <dd>{advertisementEntity.length}</dd>
          <dt>
            <span id="width">
              <Translate contentKey="tcebackendApp.advertisement.width">Width</Translate>
            </span>
          </dt>
          <dd>{advertisementEntity.width}</dd>
          <dt>
            <span id="height">
              <Translate contentKey="tcebackendApp.advertisement.height">Height</Translate>
            </span>
          </dt>
          <dd>{advertisementEntity.height}</dd>
          <dt>
            <span id="distanceKm">
              <Translate contentKey="tcebackendApp.advertisement.distanceKm">Distance Km</Translate>
            </span>
          </dt>
          <dd>{advertisementEntity.distanceKm}</dd>
          <dt>
            <span id="delivered">
              <Translate contentKey="tcebackendApp.advertisement.delivered">Delivered</Translate>
            </span>
          </dt>
          <dd>{advertisementEntity.delivered ? 'true' : 'false'}</dd>
          <dt>
            <span id="creationDate">
              <Translate contentKey="tcebackendApp.advertisement.creationDate">Creation Date</Translate>
            </span>
          </dt>
          <dd>
            {advertisementEntity.creationDate ? (
              <TextFormat value={advertisementEntity.creationDate} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="wishDate">
              <Translate contentKey="tcebackendApp.advertisement.wishDate">Wish Date</Translate>
            </span>
          </dt>
          <dd>
            {advertisementEntity.wishDate ? <TextFormat value={advertisementEntity.wishDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="effectiveDate">
              <Translate contentKey="tcebackendApp.advertisement.effectiveDate">Effective Date</Translate>
            </span>
          </dt>
          <dd>
            {advertisementEntity.effectiveDate ? (
              <TextFormat value={advertisementEntity.effectiveDate} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="tcebackendApp.advertisement.createdBy">Created By</Translate>
          </dt>
          <dd>{advertisementEntity.createdById ? advertisementEntity.createdById : ''}</dd>
          <dt>
            <Translate contentKey="tcebackendApp.advertisement.status">Status</Translate>
          </dt>
          <dd>{advertisementEntity.statusId ? advertisementEntity.statusId : ''}</dd>
          <dt>
            <Translate contentKey="tcebackendApp.advertisement.startAddress">Start Address</Translate>
          </dt>
          <dd>{advertisementEntity.startAddressId ? advertisementEntity.startAddressId : ''}</dd>
          <dt>
            <Translate contentKey="tcebackendApp.advertisement.arrivalAddress">Arrival Address</Translate>
          </dt>
          <dd>{advertisementEntity.arrivalAddressId ? advertisementEntity.arrivalAddressId : ''}</dd>
        </dl>
        <Button tag={Link} to="/advertisement" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/advertisement/${advertisementEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ advertisement }: IRootState) => ({
  advertisementEntity: advertisement.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AdvertisementDetail);
