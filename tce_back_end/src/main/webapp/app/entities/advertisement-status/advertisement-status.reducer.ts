import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IAdvertisementStatus, defaultValue } from 'app/shared/model/advertisement-status.model';

export const ACTION_TYPES = {
  FETCH_ADVERTISEMENTSTATUS_LIST: 'advertisementStatus/FETCH_ADVERTISEMENTSTATUS_LIST',
  FETCH_ADVERTISEMENTSTATUS: 'advertisementStatus/FETCH_ADVERTISEMENTSTATUS',
  CREATE_ADVERTISEMENTSTATUS: 'advertisementStatus/CREATE_ADVERTISEMENTSTATUS',
  UPDATE_ADVERTISEMENTSTATUS: 'advertisementStatus/UPDATE_ADVERTISEMENTSTATUS',
  DELETE_ADVERTISEMENTSTATUS: 'advertisementStatus/DELETE_ADVERTISEMENTSTATUS',
  RESET: 'advertisementStatus/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IAdvertisementStatus>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type AdvertisementStatusState = Readonly<typeof initialState>;

// Reducer

export default (state: AdvertisementStatusState = initialState, action): AdvertisementStatusState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ADVERTISEMENTSTATUS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ADVERTISEMENTSTATUS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_ADVERTISEMENTSTATUS):
    case REQUEST(ACTION_TYPES.UPDATE_ADVERTISEMENTSTATUS):
    case REQUEST(ACTION_TYPES.DELETE_ADVERTISEMENTSTATUS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_ADVERTISEMENTSTATUS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ADVERTISEMENTSTATUS):
    case FAILURE(ACTION_TYPES.CREATE_ADVERTISEMENTSTATUS):
    case FAILURE(ACTION_TYPES.UPDATE_ADVERTISEMENTSTATUS):
    case FAILURE(ACTION_TYPES.DELETE_ADVERTISEMENTSTATUS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_ADVERTISEMENTSTATUS_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_ADVERTISEMENTSTATUS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_ADVERTISEMENTSTATUS):
    case SUCCESS(ACTION_TYPES.UPDATE_ADVERTISEMENTSTATUS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_ADVERTISEMENTSTATUS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/advertisement-statuses';

// Actions

export const getEntities: ICrudGetAllAction<IAdvertisementStatus> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_ADVERTISEMENTSTATUS_LIST,
  payload: axios.get<IAdvertisementStatus>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IAdvertisementStatus> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ADVERTISEMENTSTATUS,
    payload: axios.get<IAdvertisementStatus>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IAdvertisementStatus> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ADVERTISEMENTSTATUS,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IAdvertisementStatus> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ADVERTISEMENTSTATUS,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IAdvertisementStatus> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ADVERTISEMENTSTATUS,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
