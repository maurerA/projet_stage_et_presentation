import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './advertisement-status.reducer';
import { IAdvertisementStatus } from 'app/shared/model/advertisement-status.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAdvertisementStatusDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AdvertisementStatusDetail = (props: IAdvertisementStatusDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { advertisementStatusEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="tcebackendApp.advertisementStatus.detail.title">AdvertisementStatus</Translate> [
          <b>{advertisementStatusEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="codeRef">
              <Translate contentKey="tcebackendApp.advertisementStatus.codeRef">Code Ref</Translate>
            </span>
          </dt>
          <dd>{advertisementStatusEntity.codeRef}</dd>
          <dt>
            <span id="label">
              <Translate contentKey="tcebackendApp.advertisementStatus.label">Label</Translate>
            </span>
          </dt>
          <dd>{advertisementStatusEntity.label}</dd>
        </dl>
        <Button tag={Link} to="/advertisement-status" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/advertisement-status/${advertisementStatusEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ advertisementStatus }: IRootState) => ({
  advertisementStatusEntity: advertisementStatus.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AdvertisementStatusDetail);
