import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import AdvertisementStatus from './advertisement-status';
import AdvertisementStatusDetail from './advertisement-status-detail';
import AdvertisementStatusUpdate from './advertisement-status-update';
import AdvertisementStatusDeleteDialog from './advertisement-status-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={AdvertisementStatusUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={AdvertisementStatusUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={AdvertisementStatusDetail} />
      <ErrorBoundaryRoute path={match.url} component={AdvertisementStatus} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={AdvertisementStatusDeleteDialog} />
  </>
);

export default Routes;
