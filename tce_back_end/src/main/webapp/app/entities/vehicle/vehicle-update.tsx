import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IVehicleType } from 'app/shared/model/vehicle-type.model';
import { getEntities as getVehicleTypes } from 'app/entities/vehicle-type/vehicle-type.reducer';
import { IUserAccount } from 'app/shared/model/user-account.model';
import { getEntities as getUserAccounts } from 'app/entities/user-account/user-account.reducer';
import { getEntity, updateEntity, createEntity, reset } from './vehicle.reducer';
import { IVehicle } from 'app/shared/model/vehicle.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IVehicleUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const VehicleUpdate = (props: IVehicleUpdateProps) => {
  const [typeId, setTypeId] = useState('0');
  const [deliveryManId, setDeliveryManId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { vehicleEntity, vehicleTypes, userAccounts, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/vehicle');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getVehicleTypes();
    props.getUserAccounts();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...vehicleEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="tcebackendApp.vehicle.home.createOrEditLabel">
            <Translate contentKey="tcebackendApp.vehicle.home.createOrEditLabel">Create or edit a Vehicle</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : vehicleEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="vehicle-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="vehicle-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="maxCapacityLengthLabel" for="vehicle-maxCapacityLength">
                  <Translate contentKey="tcebackendApp.vehicle.maxCapacityLength">Max Capacity Length</Translate>
                </Label>
                <AvField
                  id="vehicle-maxCapacityLength"
                  type="string"
                  className="form-control"
                  name="maxCapacityLength"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    min: { value: 0, errorMessage: translate('entity.validation.min', { min: 0 }) },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="maxCapacityWidthLabel" for="vehicle-maxCapacityWidth">
                  <Translate contentKey="tcebackendApp.vehicle.maxCapacityWidth">Max Capacity Width</Translate>
                </Label>
                <AvField
                  id="vehicle-maxCapacityWidth"
                  type="string"
                  className="form-control"
                  name="maxCapacityWidth"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    min: { value: 0, errorMessage: translate('entity.validation.min', { min: 0 }) },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="maxCapacityHeightLabel" for="vehicle-maxCapacityHeight">
                  <Translate contentKey="tcebackendApp.vehicle.maxCapacityHeight">Max Capacity Height</Translate>
                </Label>
                <AvField
                  id="vehicle-maxCapacityHeight"
                  type="string"
                  className="form-control"
                  name="maxCapacityHeight"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    min: { value: 0, errorMessage: translate('entity.validation.min', { min: 0 }) },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="maxCapacityWeightLabel" for="vehicle-maxCapacityWeight">
                  <Translate contentKey="tcebackendApp.vehicle.maxCapacityWeight">Max Capacity Weight</Translate>
                </Label>
                <AvField
                  id="vehicle-maxCapacityWeight"
                  type="string"
                  className="form-control"
                  name="maxCapacityWeight"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    min: { value: 0, errorMessage: translate('entity.validation.min', { min: 0 }) },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup check>
                <Label id="activeLabel">
                  <AvInput id="vehicle-active" type="checkbox" className="form-check-input" name="active" />
                  <Translate contentKey="tcebackendApp.vehicle.active">Active</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label for="vehicle-type">
                  <Translate contentKey="tcebackendApp.vehicle.type">Type</Translate>
                </Label>
                <AvInput id="vehicle-type" type="select" className="form-control" name="typeId" required>
                  {vehicleTypes
                    ? vehicleTypes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="vehicle-deliveryMan">
                  <Translate contentKey="tcebackendApp.vehicle.deliveryMan">Delivery Man</Translate>
                </Label>
                <AvInput id="vehicle-deliveryMan" type="select" className="form-control" name="deliveryManId" required>
                  {userAccounts
                    ? userAccounts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/vehicle" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  vehicleTypes: storeState.vehicleType.entities,
  userAccounts: storeState.userAccount.entities,
  vehicleEntity: storeState.vehicle.entity,
  loading: storeState.vehicle.loading,
  updating: storeState.vehicle.updating,
  updateSuccess: storeState.vehicle.updateSuccess,
});

const mapDispatchToProps = {
  getVehicleTypes,
  getUserAccounts,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VehicleUpdate);
