import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './vehicle.reducer';
import { IVehicle } from 'app/shared/model/vehicle.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IVehicleDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const VehicleDetail = (props: IVehicleDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { vehicleEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="tcebackendApp.vehicle.detail.title">Vehicle</Translate> [<b>{vehicleEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="maxCapacityLength">
              <Translate contentKey="tcebackendApp.vehicle.maxCapacityLength">Max Capacity Length</Translate>
            </span>
          </dt>
          <dd>{vehicleEntity.maxCapacityLength}</dd>
          <dt>
            <span id="maxCapacityWidth">
              <Translate contentKey="tcebackendApp.vehicle.maxCapacityWidth">Max Capacity Width</Translate>
            </span>
          </dt>
          <dd>{vehicleEntity.maxCapacityWidth}</dd>
          <dt>
            <span id="maxCapacityHeight">
              <Translate contentKey="tcebackendApp.vehicle.maxCapacityHeight">Max Capacity Height</Translate>
            </span>
          </dt>
          <dd>{vehicleEntity.maxCapacityHeight}</dd>
          <dt>
            <span id="maxCapacityWeight">
              <Translate contentKey="tcebackendApp.vehicle.maxCapacityWeight">Max Capacity Weight</Translate>
            </span>
          </dt>
          <dd>{vehicleEntity.maxCapacityWeight}</dd>
          <dt>
            <span id="active">
              <Translate contentKey="tcebackendApp.vehicle.active">Active</Translate>
            </span>
          </dt>
          <dd>{vehicleEntity.active ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="tcebackendApp.vehicle.type">Type</Translate>
          </dt>
          <dd>{vehicleEntity.typeId ? vehicleEntity.typeId : ''}</dd>
          <dt>
            <Translate contentKey="tcebackendApp.vehicle.deliveryMan">Delivery Man</Translate>
          </dt>
          <dd>{vehicleEntity.deliveryManId ? vehicleEntity.deliveryManId : ''}</dd>
        </dl>
        <Button tag={Link} to="/vehicle" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/vehicle/${vehicleEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ vehicle }: IRootState) => ({
  vehicleEntity: vehicle.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VehicleDetail);
