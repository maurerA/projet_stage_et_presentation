package fr.insy2s.service;

import fr.insy2s.config.Constants;
import fr.insy2s.domain.*;
import fr.insy2s.repository.*;
import fr.insy2s.security.AuthoritiesConstants;
import fr.insy2s.security.SecurityUtils;
import fr.insy2s.service.dto.UserDTO;
import fr.insy2s.web.rest.vm.ManagedUserVM;
import io.github.jhipster.security.RandomUtil;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private static class UserAccountResourceException extends RuntimeException {

        private UserAccountResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    private final CacheManager cacheManager;

    private final UserAccountRepository userAccountRepository;

    private final AddressRepository addressRepository;

    private final MailService mailService;

    private final CompanyRepository companyRepository;

    private final CountryRepository countryRepository;

    public UserService(
        AddressRepository addressRepository,
        UserRepository userRepository,
        PasswordEncoder passwordEncoder,
        AuthorityRepository authorityRepository,
        CacheManager cacheManager,
        UserAccountRepository userAccountRepository,
        MailService mailService,
        CompanyRepository companyRepository,
        CountryRepository countryRepository
    ) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.cacheManager = cacheManager;
        this.userAccountRepository = userAccountRepository;
        this.mailService = mailService;
        this.addressRepository = addressRepository;
        this.companyRepository = companyRepository;
        this.countryRepository = countryRepository;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository
            .findOneByActivationKey(key)
            .map(
                user -> {
                    // activate given user for the registration key.
                    user.setActivated(true);
                    user.setActivationKey(null);
                    this.clearUserCaches(user);
                    log.debug("Activated user: {}", user);
                    return user;
                }
            );
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);
        return userRepository
            .findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(
                user -> {
                    user.setPassword(passwordEncoder.encode(newPassword));
                    user.setResetKey(null);
                    user.setResetDate(null);
                    this.clearUserCaches(user);
                    return user;
                }
            );
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository
            .findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(
                user -> {
                    user.setResetKey(RandomUtil.generateResetKey());
                    user.setResetDate(Instant.now());
                    this.clearUserCaches(user);
                    return user;
                }
            );
    }

    public User registerUser(ManagedUserVM managedUser, String password) {
        userRepository
            .findOneByLogin(managedUser.getLogin().toLowerCase())
            .ifPresent(
                existingUser -> {
                    boolean removed = removeNonActivatedUser(existingUser);
                    if (!removed) {
                        throw new UsernameAlreadyUsedException();
                    }
                }
            );
        userRepository
            .findOneByEmailIgnoreCase(managedUser.getEmail())
            .ifPresent(
                existingUser -> {
                    boolean removed = removeNonActivatedUser(existingUser);
                    if (!removed) {
                        throw new EmailAlreadyUsedException();
                    }
                }
            );
        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(managedUser.getLogin().toLowerCase());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(managedUser.getFirstName());
        newUser.setLastName(managedUser.getLastName());
        if (managedUser.getEmail() != null) {
            newUser.setEmail(managedUser.getEmail().toLowerCase());
        }
        newUser.setImageUrl(managedUser.getImageUrl());
        newUser.setLangKey(managedUser.getLangKey());
        // new user is not active
        newUser.setActivated(true);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        Set<Authority> authorities = new HashSet<>();
        for (String auth : managedUser.getAuthorities()) {
            authorityRepository.findById(auth).ifPresent(authorities::add);
        }

        newUser.setAuthorities(authorities);

        userRepository.save(newUser);
        this.clearUserCaches(newUser);
        log.debug("Created Information for User: {}", newUser);

        Country country = new Country();
        country.setId(managedUser.getIdCountry());

        Address address = new Address();
        address.setStreetNumber(managedUser.getStreetNumber());
        address.setStreetName(managedUser.getStreetName());
        address.setComplementAddress(managedUser.getComplementAddress());
        address.setZipCode(managedUser.getZipCode());
        address.setTown(managedUser.getTown());
        address.setCountry(country);
        addressRepository.save(address);

        UserAccount userAccount = new UserAccount();
        userAccount.setUser(newUser);
        userAccount.setPhoneNumber(managedUser.getPhoneNumber());
        userAccount.setAdresse(address);
        userAccount.setAvailable(true);

        if (managedUser.getAuthorities().contains(AuthoritiesConstants.DELIVERYMAN)) {
            Company company = new Company();
            company.setNameCompany(managedUser.getNameCompany());
            company.setNumberSiren(managedUser.getNumberSiren());
            company.setNumberSiretCompany(managedUser.getNumberSiretCompany());
            companyRepository.save(company);

            userAccount.setCompany(company);
        }

        userAccountRepository.save(userAccount);

        return newUser;
    }

    private boolean removeNonActivatedUser(User existingUser) {
        if (existingUser.getActivated()) {
            return false;
        }
        userRepository.delete(existingUser);
        userRepository.flush();
        this.clearUserCaches(existingUser);
        return true;
    }

    public User createUser(UserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin().toLowerCase());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        if (userDTO.getEmail() != null) {
            user.setEmail(userDTO.getEmail().toLowerCase());
        }
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        if (userDTO.getAuthorities() != null) {
            Set<Authority> authorities = userDTO
                .getAuthorities()
                .stream()
                .map(authorityRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
            user.setAuthorities(authorities);
        }
        userRepository.save(user);
        this.clearUserCaches(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update.
     * @return updated user.
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional
            .of(userRepository.findById(userDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(
                user -> {
                    this.clearUserCaches(user);
                    user.setLogin(userDTO.getLogin().toLowerCase());
                    user.setFirstName(userDTO.getFirstName());
                    user.setLastName(userDTO.getLastName());
                    if (userDTO.getEmail() != null) {
                        user.setEmail(userDTO.getEmail().toLowerCase());
                    }
                    user.setImageUrl(userDTO.getImageUrl());
                    user.setActivated(userDTO.isActivated());
                    user.setLangKey(userDTO.getLangKey());
                    Set<Authority> managedAuthorities = user.getAuthorities();
                    managedAuthorities.clear();
                    userDTO
                        .getAuthorities()
                        .stream()
                        .map(authorityRepository::findById)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .forEach(managedAuthorities::add);
                    this.clearUserCaches(user);
                    log.debug("Changed Information for User: {}", user);
                    return user;
                }
            )
            .map(UserDTO::new);
    }

    public void deleteUser(String login) {
        userRepository
            .findOneByLogin(login)
            .ifPresent(
                user -> {
                    userRepository.delete(user);
                    this.clearUserCaches(user);
                    log.debug("Deleted User: {}", user);
                }
            );
    }

    public void disableAccount(String login) {
        Optional<User> user = userRepository.findOneByLogin(login);

        if (user.isEmpty()) {
            throw new UserAccountResourceException("The user cannot be found !");
        }
        user.get().setActivated(false);

        userRepository.saveAndFlush(user.get());
        this.clearUserCaches(user.get());
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user.
     * @param lastName  last name of user.
     * @param email     email id of user.
     * @param langKey   language key.
     * @param imageUrl  image URL of user.
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils
            .getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(
                user -> {
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    if (email != null) {
                        user.setEmail(email.toLowerCase());
                    }
                    user.setLangKey(langKey);
                    user.setImageUrl(imageUrl);
                    this.clearUserCaches(user);
                    log.debug("Changed Information for User: {}", user);
                }
            );
    }

    @Transactional
    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils
            .getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(
                user -> {
                    String currentEncryptedPassword = user.getPassword();
                    if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                        throw new InvalidPasswordException();
                    }
                    String encryptedPassword = passwordEncoder.encode(newPassword);
                    user.setPassword(encryptedPassword);
                    this.clearUserCaches(user);
                    log.debug("Changed password for User: {}", user);
                }
            );
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        userRepository
            .findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS))
            .forEach(
                user -> {
                    log.debug("Deleting not activated user {}", user.getLogin());
                    userRepository.delete(user);
                    this.clearUserCaches(user);
                }
            );
    }

    /**
     * Gets a list of all the authorities.
     * @return a list of all the authorities.
     */
    @Transactional(readOnly = true)
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        if (user.getEmail() != null) {
            Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
        }
    }

    /**
     * get an userAccount with user; adress, compagny, country
     *
     * @param login
     * @return ManagedUserVm
     *
     * @author MAURER Adrien TC-22
     */
    @Transactional(readOnly = true)
    public ManagedUserVM getUserAccountWithAuthorities(String login) {
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(login);

        if (!userAccount.isPresent()) {
            return null;
        }

        ManagedUserVM managedUserVM = new ManagedUserVM(userAccount.get());

        return managedUserVM;
    }

    /**
     * update profil user and deliveryMan
     * @param login
     * @param managedUserVM
     *
     * @author MAURER Adrien TC-22
     */
    public void updateUserAccount(String login, ManagedUserVM managedUserVM) {
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(login);
        if (userAccount.isEmpty()) {
            throw new UserAccountResourceException("User and userAccount could not be found");
        }

        //get user
        User user = userAccount.get().getUser();
        //get adress
        Address address = userAccount.get().getAdresse();
        //instanciation country
        Country country = new Country();
        //update User
        user.setFirstName(managedUserVM.getFirstName());
        user.setLastName(managedUserVM.getLastName());
        log.debug("Changed Information for UserAccount: {user}", userAccount.get().getUser());

        Set<String> authoritiesCurrentUser = SecurityUtils.getCurrentUserAuthorities();

        if (authoritiesCurrentUser.contains(AuthoritiesConstants.DELIVERYMAN)) {
            //update Compagny
            Company company = userAccount.get().getCompany();
            company.setNameCompany(managedUserVM.getNameCompany());
            company.setNumberSiren(managedUserVM.getNumberSiren());
            company.setNumberSiretCompany(managedUserVM.getNumberSiretCompany());
            // companyRepository.saveAndFlush(company);
            userAccount.get().setCompany(company);
            log.debug("Changed Information for UserAccount: {compagny}", userAccount.get().getCompany());
        }

        address.setComplementAddress(managedUserVM.getComplementAddress());
        address.setStreetName(managedUserVM.getStreetName());
        address.setStreetNumber(managedUserVM.getStreetNumber());
        address.setTown(managedUserVM.getTown());
        address.setZipCode(managedUserVM.getZipCode());

        log.debug("Changed Information for UserAccount: {adress}", userAccount.get().getAdresse());
        //update country
        country.setId(managedUserVM.getIdCountry());

        log.debug("Changed Information for UserAccount: {country}", userAccount.get().getAdresse().getCountry());

        //update country, address and user
        address.setCountry(country);
        userAccount.get().setUser(user);
        userAccount.get().setAdresse(address);

        //save and flush userAccount
        userAccountRepository.saveAndFlush(userAccount.get());
        this.clearUserCaches(userAccount.get().getUser());

        /**
         *  mail service with template
         *
         * @Author MAURER Adrien TC-54
         */

        String titleKey = "email.update.title";
        String templateName = "mail/updateProfilEmail";
        try {
            mailService.sendUpdateUserFromTemplate(userAccount.get().getUser(), templateName, titleKey);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }
}
