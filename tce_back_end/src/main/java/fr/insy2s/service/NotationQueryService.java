package fr.insy2s.service;

import fr.insy2s.domain.*; // for static metamodels
import fr.insy2s.domain.Notation;
import fr.insy2s.repository.NotationRepository;
import fr.insy2s.service.dto.NotationCriteria;
import fr.insy2s.service.dto.NotationDTO;
import fr.insy2s.service.mapper.NotationMapper;
import io.github.jhipster.service.QueryService;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for executing complex queries for {@link Notation} entities in the database.
 * The main input is a {@link NotationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NotationDTO} or a {@link Page} of {@link NotationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NotationQueryService extends QueryService<Notation> {
    private final Logger log = LoggerFactory.getLogger(NotationQueryService.class);

    private final NotationRepository notationRepository;

    private final NotationMapper notationMapper;

    public NotationQueryService(NotationRepository notationRepository, NotationMapper notationMapper) {
        this.notationRepository = notationRepository;
        this.notationMapper = notationMapper;
    }

    /**
     * Return a {@link List} of {@link NotationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NotationDTO> findByCriteria(NotationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Notation> specification = createSpecification(criteria);
        return notationMapper.toDto(notationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NotationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NotationDTO> findByCriteria(NotationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Notation> specification = createSpecification(criteria);
        return notationRepository.findAll(specification, page).map(notationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NotationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Notation> specification = createSpecification(criteria);
        return notationRepository.count(specification);
    }

    /**
     * Function to convert {@link NotationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Notation> createSpecification(NotationCriteria criteria) {
        Specification<Notation> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Notation_.id));
            }
            if (criteria.getNumberStar() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberStar(), Notation_.numberStar));
            }
            if (criteria.getMessage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMessage(), Notation_.message));
            }
            if (criteria.getDateNote() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateNote(), Notation_.dateNote));
            }
            if (criteria.getReceiverId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getReceiverId(),
                            root -> root.join(Notation_.receiver, JoinType.LEFT).get(UserAccount_.id)
                        )
                    );
            }
            if (criteria.getSenderId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getSenderId(), root -> root.join(Notation_.sender, JoinType.LEFT).get(UserAccount_.id))
                    );
            }
        }
        return specification;
    }
}
