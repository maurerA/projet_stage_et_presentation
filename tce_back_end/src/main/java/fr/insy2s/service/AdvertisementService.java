package fr.insy2s.service;

import fr.insy2s.domain.Advertisement;
import fr.insy2s.service.dto.AdvertisementDTO;
import fr.insy2s.web.rest.vm.ManagedAdvertisementCompactVM;
import fr.insy2s.web.rest.vm.ManagedAdvertisementVM;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link fr.insy2s.domain.Advertisement}.
 */
public interface AdvertisementService {
    /**
     * Save a advertisement.
     *
     * @param advertisementDTO the entity to save.
     * @return the persisted entity.
     */
    AdvertisementDTO save(AdvertisementDTO advertisementDTO);

    /**
     * Get all the advertisements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AdvertisementDTO> findAll(Pageable pageable);

    /**
     * Get the "id" advertisement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AdvertisementDTO> findOne(Long id);

    /**
     * Delete the "id" advertisement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * allows to catch advertissement and deliveryMan
     * @param id
     * @return details advertissement with Advertissement and deliveryman
     *
     * @Author MAURER Adrien TC-10
     */
    ManagedAdvertisementVM findDetailsAdvertissement(Long id);

    /**
     * @author Mohamed
     * Get available advertisements by pages.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AdvertisementDTO> findAvailableAdvertisements(Pageable pageable);

    /**
     * @author Mohamed
     * Get available advertisements by list.
     * @return the list of entities.
     */
    List<AdvertisementDTO> findAvailableAdvertisements();

    /**
     * allows to catch advertissement and deliveryMan
     * @param id
     * @return details advertissement with Advertissement and deliveryman
     *
     * @Author Mohamed
     */
    ManagedAdvertisementVM findDetailsAdvertissementForDelivery(Long id);

    void disableUserAdvertisements();

    void disableDeliverymanQuotations();

    /**@author Ebrahim
     * This method allows the user to create and save an advertisement with all his attributes and also save the others attributes objects
     * @param managedAdvertisementVM
     * @return
     */
    Advertisement createNewAdvertisement(@Valid ManagedAdvertisementVM managedAdvertisementVM);

    /**
     * @author Ebrahim
     * this methods allows the user to get a page of his own advertisements who are not archived
     * @param pageable
     * @return page of Advertisements of the user
     */
    Page<ManagedAdvertisementCompactVM> userAdvertisements(Pageable pageable);

    /**
     * @author Ebrahim
     * this methods allows the user to get a filtered page of his own advertisements who are not archived
     * @param pageable
     * @return a filtered page of Advertisements of the user
     */
    Page<ManagedAdvertisementCompactVM> userAdvertisementsFiltered(String title, Pageable pageable, String all);
}
