package fr.insy2s.service;

import fr.insy2s.domain.*; // for static metamodels
import fr.insy2s.domain.AdvertisementStatus;
import fr.insy2s.repository.AdvertisementStatusRepository;
import fr.insy2s.service.dto.AdvertisementStatusCriteria;
import fr.insy2s.service.dto.AdvertisementStatusDTO;
import fr.insy2s.service.mapper.AdvertisementStatusMapper;
import io.github.jhipster.service.QueryService;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for executing complex queries for {@link AdvertisementStatus} entities in the database.
 * The main input is a {@link AdvertisementStatusCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AdvertisementStatusDTO} or a {@link Page} of {@link AdvertisementStatusDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AdvertisementStatusQueryService extends QueryService<AdvertisementStatus> {
    private final Logger log = LoggerFactory.getLogger(AdvertisementStatusQueryService.class);

    private final AdvertisementStatusRepository advertisementStatusRepository;

    private final AdvertisementStatusMapper advertisementStatusMapper;

    public AdvertisementStatusQueryService(
        AdvertisementStatusRepository advertisementStatusRepository,
        AdvertisementStatusMapper advertisementStatusMapper
    ) {
        this.advertisementStatusRepository = advertisementStatusRepository;
        this.advertisementStatusMapper = advertisementStatusMapper;
    }

    /**
     * Return a {@link List} of {@link AdvertisementStatusDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AdvertisementStatusDTO> findByCriteria(AdvertisementStatusCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AdvertisementStatus> specification = createSpecification(criteria);
        return advertisementStatusMapper.toDto(advertisementStatusRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AdvertisementStatusDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AdvertisementStatusDTO> findByCriteria(AdvertisementStatusCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AdvertisementStatus> specification = createSpecification(criteria);
        return advertisementStatusRepository.findAll(specification, page).map(advertisementStatusMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AdvertisementStatusCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AdvertisementStatus> specification = createSpecification(criteria);
        return advertisementStatusRepository.count(specification);
    }

    /**
     * Function to convert {@link AdvertisementStatusCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AdvertisementStatus> createSpecification(AdvertisementStatusCriteria criteria) {
        Specification<AdvertisementStatus> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), AdvertisementStatus_.id));
            }
            if (criteria.getCodeRef() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodeRef(), AdvertisementStatus_.codeRef));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), AdvertisementStatus_.label));
            }
        }
        return specification;
    }
}
