package fr.insy2s.service;

import fr.insy2s.domain.*; // for static metamodels
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.UserAccountRepository;
import fr.insy2s.service.dto.UserAccountCriteria;
import fr.insy2s.service.dto.UserAccountDTO;
import fr.insy2s.service.mapper.UserAccountMapper;
import io.github.jhipster.service.QueryService;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for executing complex queries for {@link UserAccount} entities in the database.
 * The main input is a {@link UserAccountCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserAccountDTO} or a {@link Page} of {@link UserAccountDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserAccountQueryService extends QueryService<UserAccount> {
    private final Logger log = LoggerFactory.getLogger(UserAccountQueryService.class);

    private final UserAccountRepository userAccountRepository;

    private final UserAccountMapper userAccountMapper;

    public UserAccountQueryService(UserAccountRepository userAccountRepository, UserAccountMapper userAccountMapper) {
        this.userAccountRepository = userAccountRepository;
        this.userAccountMapper = userAccountMapper;
    }

    /**
     * Return a {@link List} of {@link UserAccountDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserAccountDTO> findByCriteria(UserAccountCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserAccount> specification = createSpecification(criteria);
        return userAccountMapper.toDto(userAccountRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserAccountDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserAccountDTO> findByCriteria(UserAccountCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserAccount> specification = createSpecification(criteria);
        return userAccountRepository.findAll(specification, page).map(userAccountMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserAccountCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserAccount> specification = createSpecification(criteria);
        return userAccountRepository.count(specification);
    }

    /**
     * Function to convert {@link UserAccountCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UserAccount> createSpecification(UserAccountCriteria criteria) {
        Specification<UserAccount> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), UserAccount_.id));
            }
            if (criteria.getPhoneNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhoneNumber(), UserAccount_.phoneNumber));
            }
            if (criteria.getAvailable() != null) {
                specification = specification.and(buildSpecification(criteria.getAvailable(), UserAccount_.available));
            }
            if (criteria.getUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getUserId(), root -> root.join(UserAccount_.user, JoinType.LEFT).get(User_.id))
                    );
            }
            if (criteria.getAdresseId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getAdresseId(), root -> root.join(UserAccount_.adresse, JoinType.LEFT).get(Address_.id))
                    );
            }
            if (criteria.getListBlockedId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getListBlockedId(),
                            root -> root.join(UserAccount_.listBlockeds, JoinType.LEFT).get(UserAccount_.id)
                        )
                    );
            }
            if (criteria.getCompanyId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getCompanyId(), root -> root.join(UserAccount_.company, JoinType.LEFT).get(Company_.id))
                    );
            }
            if (criteria.getListBlockedById() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getListBlockedById(),
                            root -> root.join(UserAccount_.listBlockedBies, JoinType.LEFT).get(UserAccount_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
