package fr.insy2s.service;

import fr.insy2s.domain.Quotation;
import fr.insy2s.service.dto.QuotationDTO;
import fr.insy2s.web.rest.vm.ManagedQuotationCompactVM;
import fr.insy2s.web.rest.vm.ManagedQuotationVM;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.parameters.P;

/**
 * Service Interface for managing {@link fr.insy2s.domain.Quotation}.
 */
public interface QuotationService {
    /**
     * Save a quotation.
     *
     * @param quotationDTO the entity to save.
     * @return the persisted entity.
     */
    QuotationDTO save(QuotationDTO quotationDTO);

    /**
     * Get all the quotations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<QuotationDTO> findAll(Pageable pageable);

    /**
     * Get the "id" quotation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<QuotationDTO> findOne(Long id);

    /**
     * Delete the "id" quotation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * @author Ebrahim and MAURER Adrien
     * @param id
     * @return the list of quotations of an ad
     */
    Page<ManagedQuotationCompactVM> getQuotationsFromAd(Long id, Pageable pageable);

    /**
     * @author Ebrahim
     * @param id
     * @return the quotation accepted
     */
    Quotation acceptQuotationsFromAd(Long id);

    /**
     * @author Ebrahim
     * @param id
     * @return the quotation refused
     */
    Quotation refuseQuotationsFromAd(Long id);

    /**
     * @author Amal Fares
     * @param Quotation DTO.
     * @return Quotation
     */
    Quotation createQuotation(QuotationDTO quotationDTO);

    /**
     * @author Amal Fares
     * @param Quotation DTO.
     * @return Quotation
     */
    Quotation updateQuotation(QuotationDTO quotationDTO);

    /** @author Ebrahim
     * @param id
     * @param pageable
     * @return a quotation
     */
    Optional<ManagedQuotationVM> getQuotationVM(Long id);

    /**
     * @author Ebrahim
     * @param id
     * @return the list of quotations of the deliveryman connected
     */
    List<ManagedQuotationCompactVM> getMyQuotations(Pageable pageable);

    /**
     * @author Ebrahim
     * @param id
     * @return the list of quotations filtered of the deliveryman connected
     */
    List<ManagedQuotationCompactVM> getMyQuotationsFilteredByState(String contentFilter, Pageable pageable);

    /**
     * @author Ebrahim
     * @param id
     * @return the list of quotations filtered of the deliveryman connected
     */
    List<ManagedQuotationCompactVM> getMyQuotationsFilteredByTitle(String contentFilter, Pageable pageable);

    /**
     * @author Ebrahim
     * @param id
     * @return the list of quotations filtered of the deliveryman connected
     */
    List<ManagedQuotationCompactVM> getMyQuotationsFilteredByTitleAndState(String contentFilter, Pageable pageable);
}
