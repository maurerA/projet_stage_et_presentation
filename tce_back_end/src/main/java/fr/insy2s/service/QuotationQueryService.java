package fr.insy2s.service;

import fr.insy2s.domain.*; // for static metamodels
import fr.insy2s.domain.Quotation;
import fr.insy2s.repository.QuotationRepository;
import fr.insy2s.service.dto.QuotationCriteria;
import fr.insy2s.service.dto.QuotationDTO;
import fr.insy2s.service.mapper.QuotationMapper;
import io.github.jhipster.service.QueryService;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for executing complex queries for {@link Quotation} entities in the database.
 * The main input is a {@link QuotationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link QuotationDTO} or a {@link Page} of {@link QuotationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class QuotationQueryService extends QueryService<Quotation> {
    private final Logger log = LoggerFactory.getLogger(QuotationQueryService.class);

    private final QuotationRepository quotationRepository;

    private final QuotationMapper quotationMapper;

    public QuotationQueryService(QuotationRepository quotationRepository, QuotationMapper quotationMapper) {
        this.quotationRepository = quotationRepository;
        this.quotationMapper = quotationMapper;
    }

    /**
     * Return a {@link List} of {@link QuotationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<QuotationDTO> findByCriteria(QuotationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Quotation> specification = createSpecification(criteria);
        return quotationMapper.toDto(quotationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link QuotationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<QuotationDTO> findByCriteria(QuotationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Quotation> specification = createSpecification(criteria);
        return quotationRepository.findAll(specification, page).map(quotationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(QuotationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Quotation> specification = createSpecification(criteria);
        return quotationRepository.count(specification);
    }

    /**
     * Function to convert {@link QuotationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Quotation> createSpecification(QuotationCriteria criteria) {
        Specification<Quotation> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Quotation_.id));
            }
            if (criteria.getDeliveryManPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeliveryManPrice(), Quotation_.deliveryManPrice));
            }
            if (criteria.getCreationDateQuotation() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getCreationDateQuotation(), Quotation_.creationDateQuotation));
            }
            if (criteria.getStatusId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getStatusId(),
                            root -> root.join(Quotation_.status, JoinType.LEFT).get(QuotationStatus_.id)
                        )
                    );
            }
            if (criteria.getDeliveryManId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getDeliveryManId(),
                            root -> root.join(Quotation_.deliveryMan, JoinType.LEFT).get(UserAccount_.id)
                        )
                    );
            }
            if (criteria.getAdvertisementId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAdvertisementId(),
                            root -> root.join(Quotation_.advertisement, JoinType.LEFT).get(Advertisement_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
