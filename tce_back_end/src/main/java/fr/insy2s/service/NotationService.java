package fr.insy2s.service;

import fr.insy2s.service.dto.NotationDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link fr.insy2s.domain.Notation}.
 */
public interface NotationService {
    /**
     * Save a notation.
     *
     * @param notationDTO the entity to save.
     * @return the persisted entity.
     */
    NotationDTO save(NotationDTO notationDTO);

    /**
     * Get all the notations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<NotationDTO> findAll(Pageable pageable);

    /**
     * Get the "id" notation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NotationDTO> findOne(Long id);

    /**
     * Delete the "id" notation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
