package fr.insy2s.service;

import fr.insy2s.service.dto.AdvertisementStatusDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.insy2s.domain.AdvertisementStatus}.
 */
public interface AdvertisementStatusService {
    /**
     * Save a advertisementStatus.
     *
     * @param advertisementStatusDTO the entity to save.
     * @return the persisted entity.
     */
    AdvertisementStatusDTO save(AdvertisementStatusDTO advertisementStatusDTO);

    /**
     * Get all the advertisementStatuses.
     *
     * @return the list of entities.
     */
    List<AdvertisementStatusDTO> findAll();

    /**
     * Get the "id" advertisementStatus.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AdvertisementStatusDTO> findOne(Long id);

    /**
     * Delete the "id" advertisementStatus.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
