package fr.insy2s.service;

import fr.insy2s.domain.*; // for static metamodels
import fr.insy2s.domain.VehicleType;
import fr.insy2s.repository.VehicleTypeRepository;
import fr.insy2s.service.dto.VehicleTypeCriteria;
import fr.insy2s.service.dto.VehicleTypeDTO;
import fr.insy2s.service.mapper.VehicleTypeMapper;
import io.github.jhipster.service.QueryService;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for executing complex queries for {@link VehicleType} entities in the database.
 * The main input is a {@link VehicleTypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link VehicleTypeDTO} or a {@link Page} of {@link VehicleTypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class VehicleTypeQueryService extends QueryService<VehicleType> {
    private final Logger log = LoggerFactory.getLogger(VehicleTypeQueryService.class);

    private final VehicleTypeRepository vehicleTypeRepository;

    private final VehicleTypeMapper vehicleTypeMapper;

    public VehicleTypeQueryService(VehicleTypeRepository vehicleTypeRepository, VehicleTypeMapper vehicleTypeMapper) {
        this.vehicleTypeRepository = vehicleTypeRepository;
        this.vehicleTypeMapper = vehicleTypeMapper;
    }

    /**
     * Return a {@link List} of {@link VehicleTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<VehicleTypeDTO> findByCriteria(VehicleTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<VehicleType> specification = createSpecification(criteria);
        return vehicleTypeMapper.toDto(vehicleTypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link VehicleTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<VehicleTypeDTO> findByCriteria(VehicleTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<VehicleType> specification = createSpecification(criteria);
        return vehicleTypeRepository.findAll(specification, page).map(vehicleTypeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(VehicleTypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<VehicleType> specification = createSpecification(criteria);
        return vehicleTypeRepository.count(specification);
    }

    /**
     * Function to convert {@link VehicleTypeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<VehicleType> createSpecification(VehicleTypeCriteria criteria) {
        Specification<VehicleType> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), VehicleType_.id));
            }
            if (criteria.getCodeRef() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodeRef(), VehicleType_.codeRef));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), VehicleType_.label));
            }
        }
        return specification;
    }
}
