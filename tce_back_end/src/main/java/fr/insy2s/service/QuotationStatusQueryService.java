package fr.insy2s.service;

import fr.insy2s.domain.*; // for static metamodels
import fr.insy2s.domain.QuotationStatus;
import fr.insy2s.repository.QuotationStatusRepository;
import fr.insy2s.service.dto.QuotationStatusCriteria;
import fr.insy2s.service.dto.QuotationStatusDTO;
import fr.insy2s.service.mapper.QuotationStatusMapper;
import io.github.jhipster.service.QueryService;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for executing complex queries for {@link QuotationStatus} entities in the database.
 * The main input is a {@link QuotationStatusCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link QuotationStatusDTO} or a {@link Page} of {@link QuotationStatusDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class QuotationStatusQueryService extends QueryService<QuotationStatus> {
    private final Logger log = LoggerFactory.getLogger(QuotationStatusQueryService.class);

    private final QuotationStatusRepository quotationStatusRepository;

    private final QuotationStatusMapper quotationStatusMapper;

    public QuotationStatusQueryService(QuotationStatusRepository quotationStatusRepository, QuotationStatusMapper quotationStatusMapper) {
        this.quotationStatusRepository = quotationStatusRepository;
        this.quotationStatusMapper = quotationStatusMapper;
    }

    /**
     * Return a {@link List} of {@link QuotationStatusDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<QuotationStatusDTO> findByCriteria(QuotationStatusCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<QuotationStatus> specification = createSpecification(criteria);
        return quotationStatusMapper.toDto(quotationStatusRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link QuotationStatusDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<QuotationStatusDTO> findByCriteria(QuotationStatusCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<QuotationStatus> specification = createSpecification(criteria);
        return quotationStatusRepository.findAll(specification, page).map(quotationStatusMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(QuotationStatusCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<QuotationStatus> specification = createSpecification(criteria);
        return quotationStatusRepository.count(specification);
    }

    /**
     * Function to convert {@link QuotationStatusCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<QuotationStatus> createSpecification(QuotationStatusCriteria criteria) {
        Specification<QuotationStatus> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), QuotationStatus_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), QuotationStatus_.label));
            }
            if (criteria.getCodeRef() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodeRef(), QuotationStatus_.codeRef));
            }
        }
        return specification;
    }
}
