package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.insy2s.domain.Vehicle} entity.
 */
@ApiModel(
    description = "The vehicle entity allows every\ndeliveryman to register the vehicle\nhe'll use during deliveries\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class VehicleDTO implements Serializable {
    private Long id;

    @NotNull
    @DecimalMin(value = "0")
    private Float maxCapacityLength;

    @NotNull
    @DecimalMin(value = "0")
    private Float maxCapacityWidth;

    @NotNull
    @DecimalMin(value = "0")
    private Float maxCapacityHeight;

    @NotNull
    @DecimalMin(value = "0")
    private Float maxCapacityWeight;

    @NotNull
    private Boolean active;

    private Long typeId;

    private Long deliveryManId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getMaxCapacityLength() {
        return maxCapacityLength;
    }

    public void setMaxCapacityLength(Float maxCapacityLength) {
        this.maxCapacityLength = maxCapacityLength;
    }

    public Float getMaxCapacityWidth() {
        return maxCapacityWidth;
    }

    public void setMaxCapacityWidth(Float maxCapacityWidth) {
        this.maxCapacityWidth = maxCapacityWidth;
    }

    public Float getMaxCapacityHeight() {
        return maxCapacityHeight;
    }

    public void setMaxCapacityHeight(Float maxCapacityHeight) {
        this.maxCapacityHeight = maxCapacityHeight;
    }

    public Float getMaxCapacityWeight() {
        return maxCapacityWeight;
    }

    public void setMaxCapacityWeight(Float maxCapacityWeight) {
        this.maxCapacityWeight = maxCapacityWeight;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long vehicleTypeId) {
        this.typeId = vehicleTypeId;
    }

    public Long getDeliveryManId() {
        return deliveryManId;
    }

    public void setDeliveryManId(Long userAccountId) {
        this.deliveryManId = userAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VehicleDTO)) {
            return false;
        }

        return id != null && id.equals(((VehicleDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VehicleDTO{" +
            "id=" + getId() +
            ", maxCapacityLength=" + getMaxCapacityLength() +
            ", maxCapacityWidth=" + getMaxCapacityWidth() +
            ", maxCapacityHeight=" + getMaxCapacityHeight() +
            ", maxCapacityWeight=" + getMaxCapacityWeight() +
            ", active='" + isActive() + "'" +
            ", typeId=" + getTypeId() +
            ", deliveryManId=" + getDeliveryManId() +
            "}";
    }
}
