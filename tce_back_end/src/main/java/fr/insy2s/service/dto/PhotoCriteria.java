package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.Photo} entity. This class is used
 * in {@link fr.insy2s.web.rest.PhotoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /photos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhotoCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter photoPath;

    private LongFilter deliveryManId;

    private LongFilter advertisementId;

    public PhotoCriteria() {}

    public PhotoCriteria(PhotoCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.photoPath = other.photoPath == null ? null : other.photoPath.copy();
        this.deliveryManId = other.deliveryManId == null ? null : other.deliveryManId.copy();
        this.advertisementId = other.advertisementId == null ? null : other.advertisementId.copy();
    }

    @Override
    public PhotoCriteria copy() {
        return new PhotoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(StringFilter photoPath) {
        this.photoPath = photoPath;
    }

    public LongFilter getDeliveryManId() {
        return deliveryManId;
    }

    public void setDeliveryManId(LongFilter deliveryManId) {
        this.deliveryManId = deliveryManId;
    }

    public LongFilter getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(LongFilter advertisementId) {
        this.advertisementId = advertisementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PhotoCriteria that = (PhotoCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(photoPath, that.photoPath) &&
            Objects.equals(deliveryManId, that.deliveryManId) &&
            Objects.equals(advertisementId, that.advertisementId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, photoPath, deliveryManId, advertisementId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhotoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (photoPath != null ? "photoPath=" + photoPath + ", " : "") +
                (deliveryManId != null ? "deliveryManId=" + deliveryManId + ", " : "") +
                (advertisementId != null ? "advertisementId=" + advertisementId + ", " : "") +
            "}";
    }
}
