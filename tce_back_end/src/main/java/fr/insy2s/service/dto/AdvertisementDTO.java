package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.insy2s.domain.Advertisement} entity.
 */
@ApiModel(
    description = "The advertisement entity allows thanks to its fields\nto provide a maximum of indication to the deliverers to estimate\nthe estimate of the race\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class AdvertisementDTO implements Serializable {
    private Long id;

    @NotNull
    private String title;

    @NotNull
    private Float weight;

    @NotNull
    private Float length;

    @NotNull
    private Float width;

    @NotNull
    private Float height;

    @NotNull
    private Float distanceKm;

    @NotNull
    private Boolean delivered;

    @NotNull
    private ZonedDateTime creationDate;

    private ZonedDateTime wishDate;

    private ZonedDateTime effectiveDate;

    private Long createdById;

    private Long statusId;

    private Long startAddressId;

    private Long arrivalAddressId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getDistanceKm() {
        return distanceKm;
    }

    public void setDistanceKm(Float distanceKm) {
        this.distanceKm = distanceKm;
    }

    public Boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(Boolean delivered) {
        this.delivered = delivered;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public ZonedDateTime getWishDate() {
        return wishDate;
    }

    public void setWishDate(ZonedDateTime wishDate) {
        this.wishDate = wishDate;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long userAccountId) {
        this.createdById = userAccountId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long advertisementStatusId) {
        this.statusId = advertisementStatusId;
    }

    public Long getStartAddressId() {
        return startAddressId;
    }

    public void setStartAddressId(Long addressId) {
        this.startAddressId = addressId;
    }

    public Long getArrivalAddressId() {
        return arrivalAddressId;
    }

    public void setArrivalAddressId(Long addressId) {
        this.arrivalAddressId = addressId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdvertisementDTO)) {
            return false;
        }

        return id != null && id.equals(((AdvertisementDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AdvertisementDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", weight=" + getWeight() +
            ", length=" + getLength() +
            ", width=" + getWidth() +
            ", height=" + getHeight() +
            ", distanceKm=" + getDistanceKm() +
            ", delivered='" + isDelivered() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", wishDate='" + getWishDate() + "'" +
            ", effectiveDate='" + getEffectiveDate() + "'" +
            ", createdById=" + getCreatedById() +
            ", statusId=" + getStatusId() +
            ", startAddressId=" + getStartAddressId() +
            ", arrivalAddressId=" + getArrivalAddressId() +
            "}";
    }
}
