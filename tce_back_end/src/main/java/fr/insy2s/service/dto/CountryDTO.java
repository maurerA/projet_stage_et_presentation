package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.insy2s.domain.Country} entity.
 */
@ApiModel(
    description = "The country entity is linked to the Address entity.\nWe don't use an enum because we prefer to display\nthe label in a drop-down list.\nIn this way, the administrator will be able\nto manage the countries.\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class CountryDTO implements Serializable {
    private Long id;

    @NotNull
    private String codeRef;

    @NotNull
    private String label;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeRef() {
        return codeRef;
    }

    public void setCodeRef(String codeRef) {
        this.codeRef = codeRef;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CountryDTO)) {
            return false;
        }

        return id != null && id.equals(((CountryDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CountryDTO{" +
            "id=" + getId() +
            ", codeRef='" + getCodeRef() + "'" +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
