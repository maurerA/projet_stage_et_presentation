package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import javax.persistence.Lob;

/**
 * A DTO for the {@link fr.insy2s.domain.Photo} entity.
 */
@ApiModel(
    description = "The photo entity allows you to add a visual\nof the package but also allows you to exchange visual\ninformation such as the condition of the package\nby the carrier or the customer.\nit also makes it possible to store\nthe administrative documents of the delivery person.\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class PhotoDTO implements Serializable {
    private Long id;

    @Lob
    private byte[] photo;

    private String photoContentType;
    private String photoPath;

    private Long deliveryManId;

    private Long advertisementId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public Long getDeliveryManId() {
        return deliveryManId;
    }

    public void setDeliveryManId(Long userAccountId) {
        this.deliveryManId = userAccountId;
    }

    public Long getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(Long advertisementId) {
        this.advertisementId = advertisementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhotoDTO)) {
            return false;
        }

        return id != null && id.equals(((PhotoDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhotoDTO{" +
            "id=" + getId() +
            ", photo='" + getPhoto() + "'" +
            ", photoPath='" + getPhotoPath() + "'" +
            ", deliveryManId=" + getDeliveryManId() +
            ", advertisementId=" + getAdvertisementId() +
            "}";
    }
}
