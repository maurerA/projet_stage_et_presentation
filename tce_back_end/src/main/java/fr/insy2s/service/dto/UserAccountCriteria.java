package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.UserAccount} entity. This class is used
 * in {@link fr.insy2s.web.rest.UserAccountResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-accounts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserAccountCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter phoneNumber;

    private BooleanFilter available;

    private LongFilter userId;

    private LongFilter adresseId;

    private LongFilter listBlockedId;

    private LongFilter companyId;

    private LongFilter listBlockedById;

    public UserAccountCriteria() {}

    public UserAccountCriteria(UserAccountCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.phoneNumber = other.phoneNumber == null ? null : other.phoneNumber.copy();
        this.available = other.available == null ? null : other.available.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.adresseId = other.adresseId == null ? null : other.adresseId.copy();
        this.listBlockedId = other.listBlockedId == null ? null : other.listBlockedId.copy();
        this.companyId = other.companyId == null ? null : other.companyId.copy();
        this.listBlockedById = other.listBlockedById == null ? null : other.listBlockedById.copy();
    }

    @Override
    public UserAccountCriteria copy() {
        return new UserAccountCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(StringFilter phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public BooleanFilter getAvailable() {
        return available;
    }

    public void setAvailable(BooleanFilter available) {
        this.available = available;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getAdresseId() {
        return adresseId;
    }

    public void setAdresseId(LongFilter adresseId) {
        this.adresseId = adresseId;
    }

    public LongFilter getListBlockedId() {
        return listBlockedId;
    }

    public void setListBlockedId(LongFilter listBlockedId) {
        this.listBlockedId = listBlockedId;
    }

    public LongFilter getCompanyId() {
        return companyId;
    }

    public void setCompanyId(LongFilter companyId) {
        this.companyId = companyId;
    }

    public LongFilter getListBlockedById() {
        return listBlockedById;
    }

    public void setListBlockedById(LongFilter listBlockedById) {
        this.listBlockedById = listBlockedById;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserAccountCriteria that = (UserAccountCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(phoneNumber, that.phoneNumber) &&
            Objects.equals(available, that.available) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(adresseId, that.adresseId) &&
            Objects.equals(listBlockedId, that.listBlockedId) &&
            Objects.equals(companyId, that.companyId) &&
            Objects.equals(listBlockedById, that.listBlockedById)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phoneNumber, available, userId, adresseId, listBlockedId, companyId, listBlockedById);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserAccountCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (phoneNumber != null ? "phoneNumber=" + phoneNumber + ", " : "") +
                (available != null ? "available=" + available + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (adresseId != null ? "adresseId=" + adresseId + ", " : "") +
                (listBlockedId != null ? "listBlockedId=" + listBlockedId + ", " : "") +
                (companyId != null ? "companyId=" + companyId + ", " : "") +
                (listBlockedById != null ? "listBlockedById=" + listBlockedById + ", " : "") +
            "}";
    }
}
