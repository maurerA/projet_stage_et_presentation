package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.Address} entity. This class is used
 * in {@link fr.insy2s.web.rest.AddressResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /addresses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AddressCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter streetNumber;

    private StringFilter complementAddress;

    private StringFilter streetName;

    private StringFilter zipCode;

    private StringFilter town;

    private LongFilter countryId;

    public AddressCriteria() {}

    public AddressCriteria(AddressCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.streetNumber = other.streetNumber == null ? null : other.streetNumber.copy();
        this.complementAddress = other.complementAddress == null ? null : other.complementAddress.copy();
        this.streetName = other.streetName == null ? null : other.streetName.copy();
        this.zipCode = other.zipCode == null ? null : other.zipCode.copy();
        this.town = other.town == null ? null : other.town.copy();
        this.countryId = other.countryId == null ? null : other.countryId.copy();
    }

    @Override
    public AddressCriteria copy() {
        return new AddressCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(StringFilter streetNumber) {
        this.streetNumber = streetNumber;
    }

    public StringFilter getComplementAddress() {
        return complementAddress;
    }

    public void setComplementAddress(StringFilter complementAddress) {
        this.complementAddress = complementAddress;
    }

    public StringFilter getStreetName() {
        return streetName;
    }

    public void setStreetName(StringFilter streetName) {
        this.streetName = streetName;
    }

    public StringFilter getZipCode() {
        return zipCode;
    }

    public void setZipCode(StringFilter zipCode) {
        this.zipCode = zipCode;
    }

    public StringFilter getTown() {
        return town;
    }

    public void setTown(StringFilter town) {
        this.town = town;
    }

    public LongFilter getCountryId() {
        return countryId;
    }

    public void setCountryId(LongFilter countryId) {
        this.countryId = countryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AddressCriteria that = (AddressCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(streetNumber, that.streetNumber) &&
            Objects.equals(complementAddress, that.complementAddress) &&
            Objects.equals(streetName, that.streetName) &&
            Objects.equals(zipCode, that.zipCode) &&
            Objects.equals(town, that.town) &&
            Objects.equals(countryId, that.countryId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, streetNumber, complementAddress, streetName, zipCode, town, countryId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AddressCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (streetNumber != null ? "streetNumber=" + streetNumber + ", " : "") +
                (complementAddress != null ? "complementAddress=" + complementAddress + ", " : "") +
                (streetName != null ? "streetName=" + streetName + ", " : "") +
                (zipCode != null ? "zipCode=" + zipCode + ", " : "") +
                (town != null ? "town=" + town + ", " : "") +
                (countryId != null ? "countryId=" + countryId + ", " : "") +
            "}";
    }
}
