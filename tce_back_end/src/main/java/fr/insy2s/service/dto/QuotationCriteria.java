package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.Quotation} entity. This class is used
 * in {@link fr.insy2s.web.rest.QuotationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /quotations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class QuotationCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private FloatFilter deliveryManPrice;

    private ZonedDateTimeFilter creationDateQuotation;

    private LongFilter statusId;

    private LongFilter deliveryManId;

    private LongFilter advertisementId;

    public QuotationCriteria() {}

    public QuotationCriteria(QuotationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.deliveryManPrice = other.deliveryManPrice == null ? null : other.deliveryManPrice.copy();
        this.creationDateQuotation = other.creationDateQuotation == null ? null : other.creationDateQuotation.copy();
        this.statusId = other.statusId == null ? null : other.statusId.copy();
        this.deliveryManId = other.deliveryManId == null ? null : other.deliveryManId.copy();
        this.advertisementId = other.advertisementId == null ? null : other.advertisementId.copy();
    }

    @Override
    public QuotationCriteria copy() {
        return new QuotationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public FloatFilter getDeliveryManPrice() {
        return deliveryManPrice;
    }

    public void setDeliveryManPrice(FloatFilter deliveryManPrice) {
        this.deliveryManPrice = deliveryManPrice;
    }

    public ZonedDateTimeFilter getCreationDateQuotation() {
        return creationDateQuotation;
    }

    public void setCreationDateQuotation(ZonedDateTimeFilter creationDateQuotation) {
        this.creationDateQuotation = creationDateQuotation;
    }

    public LongFilter getStatusId() {
        return statusId;
    }

    public void setStatusId(LongFilter statusId) {
        this.statusId = statusId;
    }

    public LongFilter getDeliveryManId() {
        return deliveryManId;
    }

    public void setDeliveryManId(LongFilter deliveryManId) {
        this.deliveryManId = deliveryManId;
    }

    public LongFilter getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(LongFilter advertisementId) {
        this.advertisementId = advertisementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final QuotationCriteria that = (QuotationCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(deliveryManPrice, that.deliveryManPrice) &&
            Objects.equals(creationDateQuotation, that.creationDateQuotation) &&
            Objects.equals(statusId, that.statusId) &&
            Objects.equals(deliveryManId, that.deliveryManId) &&
            Objects.equals(advertisementId, that.advertisementId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, deliveryManPrice, creationDateQuotation, statusId, deliveryManId, advertisementId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuotationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (deliveryManPrice != null ? "deliveryManPrice=" + deliveryManPrice + ", " : "") +
                (creationDateQuotation != null ? "creationDateQuotation=" + creationDateQuotation + ", " : "") +
                (statusId != null ? "statusId=" + statusId + ", " : "") +
                (deliveryManId != null ? "deliveryManId=" + deliveryManId + ", " : "") +
                (advertisementId != null ? "advertisementId=" + advertisementId + ", " : "") +
            "}";
    }
}
