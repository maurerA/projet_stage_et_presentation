package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.insy2s.domain.QuotationStatus} entity.
 */
@ApiModel(
    description = "As for the country entity,\nthe QuotationState entity makes\nthe code more flexible.\nHowever, several origin states are possible\nincluding archiving, accepted, refused...\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class QuotationStatusDTO implements Serializable {
    private Long id;

    @NotNull
    private String label;

    @NotNull
    private String codeRef;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCodeRef() {
        return codeRef;
    }

    public void setCodeRef(String codeRef) {
        this.codeRef = codeRef;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuotationStatusDTO)) {
            return false;
        }

        return id != null && id.equals(((QuotationStatusDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuotationStatusDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", codeRef='" + getCodeRef() + "'" +
            "}";
    }
}
