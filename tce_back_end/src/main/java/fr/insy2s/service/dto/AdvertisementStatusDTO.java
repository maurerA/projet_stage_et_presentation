package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.insy2s.domain.AdvertisementStatus} entity.
 */
@ApiModel(
    description = "As for the country entity, the QuotationState entity makes the code more flexible\nDifferents states are : active, delivered, archived, saved, expired\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class AdvertisementStatusDTO implements Serializable {
    private Long id;

    @NotNull
    private String codeRef;

    @NotNull
    private String label;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeRef() {
        return codeRef;
    }

    public void setCodeRef(String codeRef) {
        this.codeRef = codeRef;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdvertisementStatusDTO)) {
            return false;
        }

        return id != null && id.equals(((AdvertisementStatusDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AdvertisementStatusDTO{" +
            "id=" + getId() +
            ", codeRef='" + getCodeRef() + "'" +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
