package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.insy2s.domain.UserAccount} entity.
 */
@ApiModel(
    description = "UserAccount is the User's extension\nUser's is an entity created by jhipster, to handle login/password\nUserAccount is an entity to centralize all the data linked with the user\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class UserAccountDTO implements Serializable {
    private Long id;

    @NotNull
    private String phoneNumber;

    @NotNull
    private Boolean available;

    private Long userId;

    private Long adresseId;
    private Set<UserAccountDTO> listBlockeds = new HashSet<>();

    private Long companyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAdresseId() {
        return adresseId;
    }

    public void setAdresseId(Long addressId) {
        this.adresseId = addressId;
    }

    public Set<UserAccountDTO> getListBlockeds() {
        return listBlockeds;
    }

    public void setListBlockeds(Set<UserAccountDTO> userAccounts) {
        this.listBlockeds = userAccounts;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserAccountDTO)) {
            return false;
        }

        return id != null && id.equals(((UserAccountDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserAccountDTO{" +
            "id=" + getId() +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", available='" + isAvailable() + "'" +
            ", userId=" + getUserId() +
            ", adresseId=" + getAdresseId() +
            ", listBlockeds='" + getListBlockeds() + "'" +
            ", companyId=" + getCompanyId() +
            "}";
    }
}
