package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.insy2s.domain.Notation} entity.
 */
@ApiModel(
    description = "The rating entity allows each customer\nto assess the service provided by the delivery person.\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class NotationDTO implements Serializable {
    private Long id;

    @NotNull
    private Integer numberStar;

    private String message;

    @NotNull
    private ZonedDateTime dateNote;

    private Long receiverId;

    private Long senderId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumberStar() {
        return numberStar;
    }

    public void setNumberStar(Integer numberStar) {
        this.numberStar = numberStar;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getDateNote() {
        return dateNote;
    }

    public void setDateNote(ZonedDateTime dateNote) {
        this.dateNote = dateNote;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long userAccountId) {
        this.receiverId = userAccountId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long userAccountId) {
        this.senderId = userAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NotationDTO)) {
            return false;
        }

        return id != null && id.equals(((NotationDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NotationDTO{" +
            "id=" + getId() +
            ", numberStar=" + getNumberStar() +
            ", message='" + getMessage() + "'" +
            ", dateNote='" + getDateNote() + "'" +
            ", receiverId=" + getReceiverId() +
            ", senderId=" + getSenderId() +
            "}";
    }
}
