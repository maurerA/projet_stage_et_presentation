package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.Notation} entity. This class is used
 * in {@link fr.insy2s.web.rest.NotationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /notations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NotationCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter numberStar;

    private StringFilter message;

    private ZonedDateTimeFilter dateNote;

    private LongFilter receiverId;

    private LongFilter senderId;

    public NotationCriteria() {}

    public NotationCriteria(NotationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.numberStar = other.numberStar == null ? null : other.numberStar.copy();
        this.message = other.message == null ? null : other.message.copy();
        this.dateNote = other.dateNote == null ? null : other.dateNote.copy();
        this.receiverId = other.receiverId == null ? null : other.receiverId.copy();
        this.senderId = other.senderId == null ? null : other.senderId.copy();
    }

    @Override
    public NotationCriteria copy() {
        return new NotationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getNumberStar() {
        return numberStar;
    }

    public void setNumberStar(IntegerFilter numberStar) {
        this.numberStar = numberStar;
    }

    public StringFilter getMessage() {
        return message;
    }

    public void setMessage(StringFilter message) {
        this.message = message;
    }

    public ZonedDateTimeFilter getDateNote() {
        return dateNote;
    }

    public void setDateNote(ZonedDateTimeFilter dateNote) {
        this.dateNote = dateNote;
    }

    public LongFilter getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(LongFilter receiverId) {
        this.receiverId = receiverId;
    }

    public LongFilter getSenderId() {
        return senderId;
    }

    public void setSenderId(LongFilter senderId) {
        this.senderId = senderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NotationCriteria that = (NotationCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(numberStar, that.numberStar) &&
            Objects.equals(message, that.message) &&
            Objects.equals(dateNote, that.dateNote) &&
            Objects.equals(receiverId, that.receiverId) &&
            Objects.equals(senderId, that.senderId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numberStar, message, dateNote, receiverId, senderId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NotationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (numberStar != null ? "numberStar=" + numberStar + ", " : "") +
                (message != null ? "message=" + message + ", " : "") +
                (dateNote != null ? "dateNote=" + dateNote + ", " : "") +
                (receiverId != null ? "receiverId=" + receiverId + ", " : "") +
                (senderId != null ? "senderId=" + senderId + ", " : "") +
            "}";
    }
}
