package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.insy2s.domain.Quotation} entity.
 */
@ApiModel(
    description = "The quotation entity allows deliverers\nto estimate the cost of delivery to the customer\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class QuotationDTO implements Serializable {
    private Long id;

    @NotNull
    private Float deliveryManPrice;

    @NotNull
    private ZonedDateTime creationDateQuotation;

    private Long statusId;

    private Long deliveryManId;

    private Long advertisementId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getDeliveryManPrice() {
        return deliveryManPrice;
    }

    public void setDeliveryManPrice(Float deliveryManPrice) {
        this.deliveryManPrice = deliveryManPrice;
    }

    public ZonedDateTime getCreationDateQuotation() {
        return creationDateQuotation;
    }

    public void setCreationDateQuotation(ZonedDateTime creationDateQuotation) {
        this.creationDateQuotation = creationDateQuotation;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long quotationStatusId) {
        this.statusId = quotationStatusId;
    }

    public Long getDeliveryManId() {
        return deliveryManId;
    }

    public void setDeliveryManId(Long userAccountId) {
        this.deliveryManId = userAccountId;
    }

    public Long getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(Long advertisementId) {
        this.advertisementId = advertisementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuotationDTO)) {
            return false;
        }

        return id != null && id.equals(((QuotationDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuotationDTO{" +
            "id=" + getId() +
            ", deliveryManPrice=" + getDeliveryManPrice() +
            ", creationDateQuotation='" + getCreationDateQuotation() + "'" +
            ", statusId=" + getStatusId() +
            ", deliveryManId=" + getDeliveryManId() +
            ", advertisementId=" + getAdvertisementId() +
            "}";
    }
}
