package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.insy2s.domain.Company} entity.
 */
@ApiModel(
    description = "entity Company is an extension for the possible futur\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class CompanyDTO implements Serializable {
    private Long id;

    @NotNull
    private String nameCompany;

    @NotNull
    private String numberSiretCompany;

    @NotNull
    private String numberSiren;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getNumberSiretCompany() {
        return numberSiretCompany;
    }

    public void setNumberSiretCompany(String numberSiretCompany) {
        this.numberSiretCompany = numberSiretCompany;
    }

    public String getNumberSiren() {
        return numberSiren;
    }

    public void setNumberSiren(String numberSiren) {
        this.numberSiren = numberSiren;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyDTO)) {
            return false;
        }

        return id != null && id.equals(((CompanyDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyDTO{" +
            "id=" + getId() +
            ", nameCompany='" + getNameCompany() + "'" +
            ", numberSiretCompany='" + getNumberSiretCompany() + "'" +
            ", numberSiren='" + getNumberSiren() + "'" +
            "}";
    }
}
