package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.Advertisement} entity. This class is used
 * in {@link fr.insy2s.web.rest.AdvertisementResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /advertisements?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AdvertisementCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter title;

    private FloatFilter weight;

    private FloatFilter length;

    private FloatFilter width;

    private FloatFilter height;

    private FloatFilter distanceKm;

    private BooleanFilter delivered;

    private ZonedDateTimeFilter creationDate;

    private ZonedDateTimeFilter wishDate;

    private ZonedDateTimeFilter effectiveDate;

    private LongFilter createdById;

    private LongFilter statusId;

    private LongFilter startAddressId;

    private LongFilter arrivalAddressId;

    public AdvertisementCriteria() {}

    public AdvertisementCriteria(AdvertisementCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.title = other.title == null ? null : other.title.copy();
        this.weight = other.weight == null ? null : other.weight.copy();
        this.length = other.length == null ? null : other.length.copy();
        this.width = other.width == null ? null : other.width.copy();
        this.height = other.height == null ? null : other.height.copy();
        this.distanceKm = other.distanceKm == null ? null : other.distanceKm.copy();
        this.delivered = other.delivered == null ? null : other.delivered.copy();
        this.creationDate = other.creationDate == null ? null : other.creationDate.copy();
        this.wishDate = other.wishDate == null ? null : other.wishDate.copy();
        this.effectiveDate = other.effectiveDate == null ? null : other.effectiveDate.copy();
        this.createdById = other.createdById == null ? null : other.createdById.copy();
        this.statusId = other.statusId == null ? null : other.statusId.copy();
        this.startAddressId = other.startAddressId == null ? null : other.startAddressId.copy();
        this.arrivalAddressId = other.arrivalAddressId == null ? null : other.arrivalAddressId.copy();
    }

    @Override
    public AdvertisementCriteria copy() {
        return new AdvertisementCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public FloatFilter getWeight() {
        return weight;
    }

    public void setWeight(FloatFilter weight) {
        this.weight = weight;
    }

    public FloatFilter getLength() {
        return length;
    }

    public void setLength(FloatFilter length) {
        this.length = length;
    }

    public FloatFilter getWidth() {
        return width;
    }

    public void setWidth(FloatFilter width) {
        this.width = width;
    }

    public FloatFilter getHeight() {
        return height;
    }

    public void setHeight(FloatFilter height) {
        this.height = height;
    }

    public FloatFilter getDistanceKm() {
        return distanceKm;
    }

    public void setDistanceKm(FloatFilter distanceKm) {
        this.distanceKm = distanceKm;
    }

    public BooleanFilter getDelivered() {
        return delivered;
    }

    public void setDelivered(BooleanFilter delivered) {
        this.delivered = delivered;
    }

    public ZonedDateTimeFilter getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(ZonedDateTimeFilter creationDate) {
        this.creationDate = creationDate;
    }

    public ZonedDateTimeFilter getWishDate() {
        return wishDate;
    }

    public void setWishDate(ZonedDateTimeFilter wishDate) {
        this.wishDate = wishDate;
    }

    public ZonedDateTimeFilter getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTimeFilter effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public LongFilter getCreatedById() {
        return createdById;
    }

    public void setCreatedById(LongFilter createdById) {
        this.createdById = createdById;
    }

    public LongFilter getStatusId() {
        return statusId;
    }

    public void setStatusId(LongFilter statusId) {
        this.statusId = statusId;
    }

    public LongFilter getStartAddressId() {
        return startAddressId;
    }

    public void setStartAddressId(LongFilter startAddressId) {
        this.startAddressId = startAddressId;
    }

    public LongFilter getArrivalAddressId() {
        return arrivalAddressId;
    }

    public void setArrivalAddressId(LongFilter arrivalAddressId) {
        this.arrivalAddressId = arrivalAddressId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AdvertisementCriteria that = (AdvertisementCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(title, that.title) &&
            Objects.equals(weight, that.weight) &&
            Objects.equals(length, that.length) &&
            Objects.equals(width, that.width) &&
            Objects.equals(height, that.height) &&
            Objects.equals(distanceKm, that.distanceKm) &&
            Objects.equals(delivered, that.delivered) &&
            Objects.equals(creationDate, that.creationDate) &&
            Objects.equals(wishDate, that.wishDate) &&
            Objects.equals(effectiveDate, that.effectiveDate) &&
            Objects.equals(createdById, that.createdById) &&
            Objects.equals(statusId, that.statusId) &&
            Objects.equals(startAddressId, that.startAddressId) &&
            Objects.equals(arrivalAddressId, that.arrivalAddressId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            title,
            weight,
            length,
            width,
            height,
            distanceKm,
            delivered,
            creationDate,
            wishDate,
            effectiveDate,
            createdById,
            statusId,
            startAddressId,
            arrivalAddressId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AdvertisementCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (weight != null ? "weight=" + weight + ", " : "") +
                (length != null ? "length=" + length + ", " : "") +
                (width != null ? "width=" + width + ", " : "") +
                (height != null ? "height=" + height + ", " : "") +
                (distanceKm != null ? "distanceKm=" + distanceKm + ", " : "") +
                (delivered != null ? "delivered=" + delivered + ", " : "") +
                (creationDate != null ? "creationDate=" + creationDate + ", " : "") +
                (wishDate != null ? "wishDate=" + wishDate + ", " : "") +
                (effectiveDate != null ? "effectiveDate=" + effectiveDate + ", " : "") +
                (createdById != null ? "createdById=" + createdById + ", " : "") +
                (statusId != null ? "statusId=" + statusId + ", " : "") +
                (startAddressId != null ? "startAddressId=" + startAddressId + ", " : "") +
                (arrivalAddressId != null ? "arrivalAddressId=" + arrivalAddressId + ", " : "") +
            "}";
    }
}
