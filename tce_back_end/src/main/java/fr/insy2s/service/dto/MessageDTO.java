package fr.insy2s.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.insy2s.domain.Message} entity.
 */
@ApiModel(
    description = "The message entity allows every\ncustomer and deliverymen to\ntalk with each others for deliveries\nwhen a customer accepts a deliveryman's\nquotation.\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed"
)
public class MessageDTO implements Serializable {
    private Long id;

    @NotNull
    private String message;

    @NotNull
    private ZonedDateTime dateMessage;

    private Long photoId;

    private Long receiverId;

    private Long senderId;

    private Long advertisementId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getDateMessage() {
        return dateMessage;
    }

    public void setDateMessage(ZonedDateTime dateMessage) {
        this.dateMessage = dateMessage;
    }

    public Long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Long photoId) {
        this.photoId = photoId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long userAccountId) {
        this.receiverId = userAccountId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long userAccountId) {
        this.senderId = userAccountId;
    }

    public Long getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(Long advertisementId) {
        this.advertisementId = advertisementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MessageDTO)) {
            return false;
        }

        return id != null && id.equals(((MessageDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MessageDTO{" +
            "id=" + getId() +
            ", message='" + getMessage() + "'" +
            ", dateMessage='" + getDateMessage() + "'" +
            ", photoId=" + getPhotoId() +
            ", receiverId=" + getReceiverId() +
            ", senderId=" + getSenderId() +
            ", advertisementId=" + getAdvertisementId() +
            "}";
    }
}
