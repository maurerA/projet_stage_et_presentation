package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.QuotationStatus} entity. This class is used
 * in {@link fr.insy2s.web.rest.QuotationStatusResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /quotation-statuses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class QuotationStatusCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private StringFilter codeRef;

    public QuotationStatusCriteria() {}

    public QuotationStatusCriteria(QuotationStatusCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.codeRef = other.codeRef == null ? null : other.codeRef.copy();
    }

    @Override
    public QuotationStatusCriteria copy() {
        return new QuotationStatusCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public StringFilter getCodeRef() {
        return codeRef;
    }

    public void setCodeRef(StringFilter codeRef) {
        this.codeRef = codeRef;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final QuotationStatusCriteria that = (QuotationStatusCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(label, that.label) && Objects.equals(codeRef, that.codeRef);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, label, codeRef);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuotationStatusCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (codeRef != null ? "codeRef=" + codeRef + ", " : "") +
            "}";
    }
}
