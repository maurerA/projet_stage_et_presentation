package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.Company} entity. This class is used
 * in {@link fr.insy2s.web.rest.CompanyResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /companies?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanyCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nameCompany;

    private StringFilter numberSiretCompany;

    private StringFilter numberSiren;

    private LongFilter listDeliveryManId;

    public CompanyCriteria() {}

    public CompanyCriteria(CompanyCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nameCompany = other.nameCompany == null ? null : other.nameCompany.copy();
        this.numberSiretCompany = other.numberSiretCompany == null ? null : other.numberSiretCompany.copy();
        this.numberSiren = other.numberSiren == null ? null : other.numberSiren.copy();
        this.listDeliveryManId = other.listDeliveryManId == null ? null : other.listDeliveryManId.copy();
    }

    @Override
    public CompanyCriteria copy() {
        return new CompanyCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(StringFilter nameCompany) {
        this.nameCompany = nameCompany;
    }

    public StringFilter getNumberSiretCompany() {
        return numberSiretCompany;
    }

    public void setNumberSiretCompany(StringFilter numberSiretCompany) {
        this.numberSiretCompany = numberSiretCompany;
    }

    public StringFilter getNumberSiren() {
        return numberSiren;
    }

    public void setNumberSiren(StringFilter numberSiren) {
        this.numberSiren = numberSiren;
    }

    public LongFilter getListDeliveryManId() {
        return listDeliveryManId;
    }

    public void setListDeliveryManId(LongFilter listDeliveryManId) {
        this.listDeliveryManId = listDeliveryManId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanyCriteria that = (CompanyCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(nameCompany, that.nameCompany) &&
            Objects.equals(numberSiretCompany, that.numberSiretCompany) &&
            Objects.equals(numberSiren, that.numberSiren) &&
            Objects.equals(listDeliveryManId, that.listDeliveryManId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameCompany, numberSiretCompany, numberSiren, listDeliveryManId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nameCompany != null ? "nameCompany=" + nameCompany + ", " : "") +
                (numberSiretCompany != null ? "numberSiretCompany=" + numberSiretCompany + ", " : "") +
                (numberSiren != null ? "numberSiren=" + numberSiren + ", " : "") +
                (listDeliveryManId != null ? "listDeliveryManId=" + listDeliveryManId + ", " : "") +
            "}";
    }
}
