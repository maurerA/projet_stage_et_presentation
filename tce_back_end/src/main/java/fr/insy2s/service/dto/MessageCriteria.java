package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.Message} entity. This class is used
 * in {@link fr.insy2s.web.rest.MessageResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /messages?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MessageCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter message;

    private ZonedDateTimeFilter dateMessage;

    private LongFilter photoId;

    private LongFilter receiverId;

    private LongFilter senderId;

    private LongFilter advertisementId;

    public MessageCriteria() {}

    public MessageCriteria(MessageCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.message = other.message == null ? null : other.message.copy();
        this.dateMessage = other.dateMessage == null ? null : other.dateMessage.copy();
        this.photoId = other.photoId == null ? null : other.photoId.copy();
        this.receiverId = other.receiverId == null ? null : other.receiverId.copy();
        this.senderId = other.senderId == null ? null : other.senderId.copy();
        this.advertisementId = other.advertisementId == null ? null : other.advertisementId.copy();
    }

    @Override
    public MessageCriteria copy() {
        return new MessageCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMessage() {
        return message;
    }

    public void setMessage(StringFilter message) {
        this.message = message;
    }

    public ZonedDateTimeFilter getDateMessage() {
        return dateMessage;
    }

    public void setDateMessage(ZonedDateTimeFilter dateMessage) {
        this.dateMessage = dateMessage;
    }

    public LongFilter getPhotoId() {
        return photoId;
    }

    public void setPhotoId(LongFilter photoId) {
        this.photoId = photoId;
    }

    public LongFilter getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(LongFilter receiverId) {
        this.receiverId = receiverId;
    }

    public LongFilter getSenderId() {
        return senderId;
    }

    public void setSenderId(LongFilter senderId) {
        this.senderId = senderId;
    }

    public LongFilter getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(LongFilter advertisementId) {
        this.advertisementId = advertisementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MessageCriteria that = (MessageCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(message, that.message) &&
            Objects.equals(dateMessage, that.dateMessage) &&
            Objects.equals(photoId, that.photoId) &&
            Objects.equals(receiverId, that.receiverId) &&
            Objects.equals(senderId, that.senderId) &&
            Objects.equals(advertisementId, that.advertisementId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, message, dateMessage, photoId, receiverId, senderId, advertisementId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MessageCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (message != null ? "message=" + message + ", " : "") +
                (dateMessage != null ? "dateMessage=" + dateMessage + ", " : "") +
                (photoId != null ? "photoId=" + photoId + ", " : "") +
                (receiverId != null ? "receiverId=" + receiverId + ", " : "") +
                (senderId != null ? "senderId=" + senderId + ", " : "") +
                (advertisementId != null ? "advertisementId=" + advertisementId + ", " : "") +
            "}";
    }
}
