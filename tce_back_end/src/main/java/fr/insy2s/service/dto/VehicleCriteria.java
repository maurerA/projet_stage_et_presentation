package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.Vehicle} entity. This class is used
 * in {@link fr.insy2s.web.rest.VehicleResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /vehicles?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class VehicleCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private FloatFilter maxCapacityLength;

    private FloatFilter maxCapacityWidth;

    private FloatFilter maxCapacityHeight;

    private FloatFilter maxCapacityWeight;

    private BooleanFilter active;

    private LongFilter typeId;

    private LongFilter deliveryManId;

    public VehicleCriteria() {}

    public VehicleCriteria(VehicleCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.maxCapacityLength = other.maxCapacityLength == null ? null : other.maxCapacityLength.copy();
        this.maxCapacityWidth = other.maxCapacityWidth == null ? null : other.maxCapacityWidth.copy();
        this.maxCapacityHeight = other.maxCapacityHeight == null ? null : other.maxCapacityHeight.copy();
        this.maxCapacityWeight = other.maxCapacityWeight == null ? null : other.maxCapacityWeight.copy();
        this.active = other.active == null ? null : other.active.copy();
        this.typeId = other.typeId == null ? null : other.typeId.copy();
        this.deliveryManId = other.deliveryManId == null ? null : other.deliveryManId.copy();
    }

    @Override
    public VehicleCriteria copy() {
        return new VehicleCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public FloatFilter getMaxCapacityLength() {
        return maxCapacityLength;
    }

    public void setMaxCapacityLength(FloatFilter maxCapacityLength) {
        this.maxCapacityLength = maxCapacityLength;
    }

    public FloatFilter getMaxCapacityWidth() {
        return maxCapacityWidth;
    }

    public void setMaxCapacityWidth(FloatFilter maxCapacityWidth) {
        this.maxCapacityWidth = maxCapacityWidth;
    }

    public FloatFilter getMaxCapacityHeight() {
        return maxCapacityHeight;
    }

    public void setMaxCapacityHeight(FloatFilter maxCapacityHeight) {
        this.maxCapacityHeight = maxCapacityHeight;
    }

    public FloatFilter getMaxCapacityWeight() {
        return maxCapacityWeight;
    }

    public void setMaxCapacityWeight(FloatFilter maxCapacityWeight) {
        this.maxCapacityWeight = maxCapacityWeight;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }

    public LongFilter getTypeId() {
        return typeId;
    }

    public void setTypeId(LongFilter typeId) {
        this.typeId = typeId;
    }

    public LongFilter getDeliveryManId() {
        return deliveryManId;
    }

    public void setDeliveryManId(LongFilter deliveryManId) {
        this.deliveryManId = deliveryManId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final VehicleCriteria that = (VehicleCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(maxCapacityLength, that.maxCapacityLength) &&
            Objects.equals(maxCapacityWidth, that.maxCapacityWidth) &&
            Objects.equals(maxCapacityHeight, that.maxCapacityHeight) &&
            Objects.equals(maxCapacityWeight, that.maxCapacityWeight) &&
            Objects.equals(active, that.active) &&
            Objects.equals(typeId, that.typeId) &&
            Objects.equals(deliveryManId, that.deliveryManId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, maxCapacityLength, maxCapacityWidth, maxCapacityHeight, maxCapacityWeight, active, typeId, deliveryManId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VehicleCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (maxCapacityLength != null ? "maxCapacityLength=" + maxCapacityLength + ", " : "") +
                (maxCapacityWidth != null ? "maxCapacityWidth=" + maxCapacityWidth + ", " : "") +
                (maxCapacityHeight != null ? "maxCapacityHeight=" + maxCapacityHeight + ", " : "") +
                (maxCapacityWeight != null ? "maxCapacityWeight=" + maxCapacityWeight + ", " : "") +
                (active != null ? "active=" + active + ", " : "") +
                (typeId != null ? "typeId=" + typeId + ", " : "") +
                (deliveryManId != null ? "deliveryManId=" + deliveryManId + ", " : "") +
            "}";
    }
}
