package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.Notification} entity. This class is used
 * in {@link fr.insy2s.web.rest.NotificationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /notifications?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NotificationCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter entityName;

    private LongFilter entityId;

    private BooleanFilter viewed;

    private LocalDateFilter notificationDate;

    private StringFilter message;

    private LongFilter userAccountId;

    public NotificationCriteria() {}

    public NotificationCriteria(NotificationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.entityName = other.entityName == null ? null : other.entityName.copy();
        this.entityId = other.entityId == null ? null : other.entityId.copy();
        this.viewed = other.viewed == null ? null : other.viewed.copy();
        this.notificationDate = other.notificationDate == null ? null : other.notificationDate.copy();
        this.message = other.message == null ? null : other.message.copy();
        this.userAccountId = other.userAccountId == null ? null : other.userAccountId.copy();
    }

    @Override
    public NotificationCriteria copy() {
        return new NotificationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEntityName() {
        return entityName;
    }

    public void setEntityName(StringFilter entityName) {
        this.entityName = entityName;
    }

    public LongFilter getEntityId() {
        return entityId;
    }

    public void setEntityId(LongFilter entityId) {
        this.entityId = entityId;
    }

    public BooleanFilter getViewed() {
        return viewed;
    }

    public void setViewed(BooleanFilter viewed) {
        this.viewed = viewed;
    }

    public LocalDateFilter getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(LocalDateFilter notificationDate) {
        this.notificationDate = notificationDate;
    }

    public StringFilter getMessage() {
        return message;
    }

    public void setMessage(StringFilter message) {
        this.message = message;
    }

    public LongFilter getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(LongFilter userAccountId) {
        this.userAccountId = userAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NotificationCriteria that = (NotificationCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(entityName, that.entityName) &&
            Objects.equals(entityId, that.entityId) &&
            Objects.equals(viewed, that.viewed) &&
            Objects.equals(notificationDate, that.notificationDate) &&
            Objects.equals(message, that.message) &&
            Objects.equals(userAccountId, that.userAccountId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, entityName, entityId, viewed, notificationDate, message, userAccountId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NotificationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (entityName != null ? "entityName=" + entityName + ", " : "") +
                (entityId != null ? "entityId=" + entityId + ", " : "") +
                (viewed != null ? "viewed=" + viewed + ", " : "") +
                (notificationDate != null ? "notificationDate=" + notificationDate + ", " : "") +
                (message != null ? "message=" + message + ", " : "") +
                (userAccountId != null ? "userAccountId=" + userAccountId + ", " : "") +
            "}";
    }
}
