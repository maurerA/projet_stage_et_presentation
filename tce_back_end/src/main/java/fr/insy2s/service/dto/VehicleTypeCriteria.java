package fr.insy2s.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link fr.insy2s.domain.VehicleType} entity. This class is used
 * in {@link fr.insy2s.web.rest.VehicleTypeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /vehicle-types?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class VehicleTypeCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter codeRef;

    private StringFilter label;

    public VehicleTypeCriteria() {}

    public VehicleTypeCriteria(VehicleTypeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.codeRef = other.codeRef == null ? null : other.codeRef.copy();
        this.label = other.label == null ? null : other.label.copy();
    }

    @Override
    public VehicleTypeCriteria copy() {
        return new VehicleTypeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCodeRef() {
        return codeRef;
    }

    public void setCodeRef(StringFilter codeRef) {
        this.codeRef = codeRef;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final VehicleTypeCriteria that = (VehicleTypeCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(codeRef, that.codeRef) && Objects.equals(label, that.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codeRef, label);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VehicleTypeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (codeRef != null ? "codeRef=" + codeRef + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
            "}";
    }
}
