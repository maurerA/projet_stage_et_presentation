package fr.insy2s.service.impl;

import fr.insy2s.domain.QuotationStatus;
import fr.insy2s.repository.QuotationStatusRepository;
import fr.insy2s.service.QuotationStatusService;
import fr.insy2s.service.dto.QuotationStatusDTO;
import fr.insy2s.service.mapper.QuotationStatusMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link QuotationStatus}.
 */
@Service
@Transactional
public class QuotationStatusServiceImpl implements QuotationStatusService {
    private final Logger log = LoggerFactory.getLogger(QuotationStatusServiceImpl.class);

    private final QuotationStatusRepository quotationStatusRepository;

    private final QuotationStatusMapper quotationStatusMapper;

    public QuotationStatusServiceImpl(QuotationStatusRepository quotationStatusRepository, QuotationStatusMapper quotationStatusMapper) {
        this.quotationStatusRepository = quotationStatusRepository;
        this.quotationStatusMapper = quotationStatusMapper;
    }

    @Override
    public QuotationStatusDTO save(QuotationStatusDTO quotationStatusDTO) {
        log.debug("Request to save QuotationStatus : {}", quotationStatusDTO);
        QuotationStatus quotationStatus = quotationStatusMapper.toEntity(quotationStatusDTO);
        quotationStatus = quotationStatusRepository.save(quotationStatus);
        return quotationStatusMapper.toDto(quotationStatus);
    }

    @Override
    @Transactional(readOnly = true)
    public List<QuotationStatusDTO> findAll() {
        log.debug("Request to get all QuotationStatuses");
        return quotationStatusRepository
            .findAll()
            .stream()
            .map(quotationStatusMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<QuotationStatusDTO> findOne(Long id) {
        log.debug("Request to get QuotationStatus : {}", id);
        return quotationStatusRepository.findById(id).map(quotationStatusMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete QuotationStatus : {}", id);
        quotationStatusRepository.deleteById(id);
    }
}
