package fr.insy2s.service.impl;

import fr.insy2s.domain.Notation;
import fr.insy2s.repository.NotationRepository;
import fr.insy2s.service.NotationService;
import fr.insy2s.service.dto.NotationDTO;
import fr.insy2s.service.mapper.NotationMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Notation}.
 */
@Service
@Transactional
public class NotationServiceImpl implements NotationService {
    private final Logger log = LoggerFactory.getLogger(NotationServiceImpl.class);

    private final NotationRepository notationRepository;

    private final NotationMapper notationMapper;

    public NotationServiceImpl(NotationRepository notationRepository, NotationMapper notationMapper) {
        this.notationRepository = notationRepository;
        this.notationMapper = notationMapper;
    }

    @Override
    public NotationDTO save(NotationDTO notationDTO) {
        log.debug("Request to save Notation : {}", notationDTO);
        Notation notation = notationMapper.toEntity(notationDTO);
        notation = notationRepository.save(notation);
        return notationMapper.toDto(notation);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<NotationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Notations");
        return notationRepository.findAll(pageable).map(notationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<NotationDTO> findOne(Long id) {
        log.debug("Request to get Notation : {}", id);
        return notationRepository.findById(id).map(notationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Notation : {}", id);
        notationRepository.deleteById(id);
    }
}
