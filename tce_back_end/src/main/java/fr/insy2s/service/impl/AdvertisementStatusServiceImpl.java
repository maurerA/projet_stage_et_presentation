package fr.insy2s.service.impl;

import fr.insy2s.domain.AdvertisementStatus;
import fr.insy2s.repository.AdvertisementStatusRepository;
import fr.insy2s.service.AdvertisementStatusService;
import fr.insy2s.service.dto.AdvertisementStatusDTO;
import fr.insy2s.service.mapper.AdvertisementStatusMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link AdvertisementStatus}.
 */
@Service
@Transactional
public class AdvertisementStatusServiceImpl implements AdvertisementStatusService {
    private final Logger log = LoggerFactory.getLogger(AdvertisementStatusServiceImpl.class);

    private final AdvertisementStatusRepository advertisementStatusRepository;

    private final AdvertisementStatusMapper advertisementStatusMapper;

    public AdvertisementStatusServiceImpl(
        AdvertisementStatusRepository advertisementStatusRepository,
        AdvertisementStatusMapper advertisementStatusMapper
    ) {
        this.advertisementStatusRepository = advertisementStatusRepository;
        this.advertisementStatusMapper = advertisementStatusMapper;
    }

    @Override
    public AdvertisementStatusDTO save(AdvertisementStatusDTO advertisementStatusDTO) {
        log.debug("Request to save AdvertisementStatus : {}", advertisementStatusDTO);
        AdvertisementStatus advertisementStatus = advertisementStatusMapper.toEntity(advertisementStatusDTO);
        advertisementStatus = advertisementStatusRepository.save(advertisementStatus);
        return advertisementStatusMapper.toDto(advertisementStatus);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdvertisementStatusDTO> findAll() {
        log.debug("Request to get all AdvertisementStatuses");
        return advertisementStatusRepository
            .findAll()
            .stream()
            .map(advertisementStatusMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AdvertisementStatusDTO> findOne(Long id) {
        log.debug("Request to get AdvertisementStatus : {}", id);
        return advertisementStatusRepository.findById(id).map(advertisementStatusMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete AdvertisementStatus : {}", id);
        advertisementStatusRepository.deleteById(id);
    }
}
