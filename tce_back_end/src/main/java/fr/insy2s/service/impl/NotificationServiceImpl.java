package fr.insy2s.service.impl;

import fr.insy2s.domain.Notification;
import fr.insy2s.domain.User;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.NotificationRepository;
import fr.insy2s.service.NotificationService;
import fr.insy2s.service.dto.NotificationDTO;
import fr.insy2s.service.mapper.NotificationMapper;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Notification}.
 */
@Service
@Transactional
public class NotificationServiceImpl implements NotificationService {
    private final Logger log = LoggerFactory.getLogger(NotificationServiceImpl.class);

    private final NotificationRepository notificationRepository;

    private final NotificationMapper notificationMapper;

    public NotificationServiceImpl(NotificationRepository notificationRepository, NotificationMapper notificationMapper) {
        this.notificationRepository = notificationRepository;
        this.notificationMapper = notificationMapper;
    }

    @Override
    public NotificationDTO save(NotificationDTO notificationDTO) {
        log.debug("Request to save Notification : {}", notificationDTO);
        Notification notification = notificationMapper.toEntity(notificationDTO);
        notification = notificationRepository.save(notification);
        return notificationMapper.toDto(notification);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<NotificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Notifications");
        return notificationRepository.findAll(pageable).map(notificationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<NotificationDTO> findOne(Long id) {
        log.debug("Request to get Notification : {}", id);
        return notificationRepository.findById(id).map(notificationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Notification : {}", id);
        notificationRepository.deleteById(id);
    }

    /**
     *  allows to get all advertisement one user
     * @param User user
     * @return liste notification
     * @Author MAURER Adrien TC-46
     */
    @Override
    @Transactional(readOnly = true)
    public List<NotificationDTO> findNotificationofOneUser(User user) {
        log.debug("Request to get all Notifications for one User");

        return notificationRepository
            .findAllByUserAccountUser(user)
            .stream()
            .filter(notificationDTO -> notificationDTO.isViewed() != true)
            .map(notificationMapper::toDto)
            .sorted(Comparator.comparing(NotificationDTO::getNotificationDate))
            .collect(Collectors.toCollection(LinkedList::new));
    }
}
