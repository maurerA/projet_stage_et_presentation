package fr.insy2s.service.impl;

import fr.insy2s.domain.*;
import fr.insy2s.repository.*;
import fr.insy2s.security.SecurityUtils;
import fr.insy2s.service.MailService;
import fr.insy2s.service.QuotationService;
import fr.insy2s.service.dto.QuotationDTO;
import fr.insy2s.service.mapper.QuotationMapper;
import fr.insy2s.web.rest.vm.ManagedQuotationCompactVM;
import fr.insy2s.web.rest.vm.ManagedQuotationVM;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Quotation}.
 */
@Service
@Transactional
public class QuotationServiceImpl implements QuotationService {
    private final Logger log = LoggerFactory.getLogger(QuotationServiceImpl.class);

    private final QuotationRepository quotationRepository;

    private final AdvertisementRepository advertisementRepository;

    private final UserAccountRepository userAccountRepository;

    private final QuotationStatusRepository quotationStatusRepository;

    private final NotificationRepository notificationRepository;

    private final QuotationMapper quotationMapper;

    private final UserRepository userRepository;

    private final MailService mailService;

    public static final String MESSAGE = "vous avez reçu un devis de ";

    public static final String SUJET = "reçu : notification client-exp : transporteur ";

    public final String REFUS_TRANSPORTEUR = "Refus Transporteur";

    public QuotationServiceImpl(
        QuotationRepository quotationRepository,
        QuotationMapper quotationMapper,
        AdvertisementRepository advertisementRepository,
        UserAccountRepository userAccountRepository,
        QuotationStatusRepository quotationStatusRepository,
        NotificationRepository notificationRepository,
        UserRepository userRepository,
        MailService mailService
    ) {
        this.quotationRepository = quotationRepository;
        this.advertisementRepository = advertisementRepository;
        this.quotationMapper = quotationMapper;
        this.userAccountRepository = userAccountRepository;
        this.quotationStatusRepository = quotationStatusRepository;
        this.notificationRepository = notificationRepository;
        this.userRepository = userRepository;
        this.mailService = mailService;
    }

    @Override
    public QuotationDTO save(QuotationDTO quotationDTO) {
        log.debug("Request to save Quotation : {}", quotationDTO);
        Quotation quotation = quotationMapper.toEntity(quotationDTO);
        quotation = quotationRepository.save(quotation);
        return quotationMapper.toDto(quotation);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<QuotationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Quotations");
        return quotationRepository.findAll(pageable).map(quotationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<QuotationDTO> findOne(Long id) {
        log.debug("Request to get Quotation : {}", id);
        return quotationRepository.findById(id).map(quotationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Quotation : {}", id);
        quotationRepository.deleteById(id);
    }

    /**
     * @param id
     *
     * @return page (quotation)
     * @Author MAURER Adrien and Ebrahim TC-26
     */
    @Override
    public Page<ManagedQuotationCompactVM> getQuotationsFromAd(Long id, Pageable pageable) {
        Advertisement advertisement = advertisementRepository.findById(id).get();

        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();

        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());
        if (userAccount.get().getId() != advertisement.getCreatedBy().getId()) {
            return null;
        }

        Page<Quotation> page = quotationRepository.findByAdvertisement(advertisement, pageable);

        List<ManagedQuotationCompactVM> result = page
            .stream()
            .filter(quotation -> !(REFUS_TRANSPORTEUR.equalsIgnoreCase(quotation.getStatus().getLabel())))
            .map(quotation -> new ManagedQuotationCompactVM(quotation))
            .collect(Collectors.toList());

        Page<ManagedQuotationCompactVM> quotationCompactVMPage = new PageImpl<ManagedQuotationCompactVM>(result);

        return quotationCompactVMPage;
    }

    @Override
    public Quotation acceptQuotationsFromAd(Long id) {
        Quotation quotation = quotationRepository.findById(id).get();
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());
        if (userAccount.get().getId() != quotation.getAdvertisement().getCreatedBy().getId()) {
            return null;
        }
        QuotationStatus quotationStatusAccepted = quotationStatusRepository.findById(3L).get();
        QuotationStatus quotationStatusRefused = quotationStatusRepository.findById(4L).get();
        List<Quotation> quotationList = quotationRepository.findByAdvertisementId(quotation.getAdvertisement().getId());
        for (Quotation quotation2 : quotationList) {
            if (quotation2.getStatus().getId() != 2L) {
                quotation2.setStatus(quotationStatusRefused);
                quotationRepository.save(quotation2);
            }
        }
        createNotificationForDeliveryman(quotation);
        quotation.setStatus(quotationStatusAccepted);
        quotationRepository.save(quotation);
        return quotation;
    }

    public Quotation refuseQuotationsFromAd(Long id) {
        Quotation quotation = quotationRepository.findById(id).get();
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());
        if (userAccount.get().getId() != quotation.getAdvertisement().getCreatedBy().getId()) {
            return null;
        }
        QuotationStatus quotationStatusRefused = quotationStatusRepository.findById(4L).get();

        quotation.setStatus(quotationStatusRefused);
        return quotationRepository.save(quotation);
    }

    /**
     * @param quotationDTO
     * @return quotation
     * @Author Fares Amal TC-17
     */

    @Override
    public Quotation createQuotation(QuotationDTO quotationDTO) {
        String login = SecurityUtils.getCurrentUserLogin().get();
        UserAccount userAccount = userAccountRepository.findByUserLogin(login).get();
        Advertisement advertisement = advertisementRepository.findById(quotationDTO.getAdvertisementId()).get();
        ZonedDateTime creationDateQuotation = ZonedDateTime.now();
        QuotationStatus quotationStatus = quotationStatusRepository.findById(1L).get();
        Quotation quotation = new Quotation();

        quotation.setDeliveryMan(userAccount);
        quotation.setAdvertisement(advertisement);
        quotation.setCreationDateQuotation(creationDateQuotation);
        quotation.setStatus(quotationStatus);
        quotation.setDeliveryManPrice(quotationDTO.getDeliveryManPrice());
        //add service email and notification TC-46 MAURER Adrien
        creatNotificationAndSendEmail(creationDateQuotation, advertisement, userAccount, quotationDTO);

        // TODO Auto-generated method stub
        return quotationRepository.save(quotation);
    }

    /**
     * @param quotationDTO
     * @return quotation
     * @Author Fares Amal TC-20
     */

    @Override
    public Quotation updateQuotation(QuotationDTO quotationDTO) {
        String login = SecurityUtils.getCurrentUserLogin().get();
        UserAccount userAccount = userAccountRepository.findByUserLogin(login).get();
        Advertisement advertisement = advertisementRepository.findById(quotationDTO.getAdvertisementId()).get();
        ZonedDateTime creationDateQuotation = ZonedDateTime.now();
        QuotationStatus quotationStatus = quotationStatusRepository.findById(1L).get();
        Quotation quotation = new Quotation();

        quotation.setDeliveryMan(userAccount);
        quotation.setId(quotationDTO.getId());
        quotation.setAdvertisement(advertisement);
        quotation.setCreationDateQuotation(creationDateQuotation);
        quotation.setStatus(quotationStatus);
        quotation.setDeliveryManPrice(quotationDTO.getDeliveryManPrice());

        return quotationRepository.saveAndFlush(quotation);
    }

    public Optional<ManagedQuotationVM> getQuotationVM(Long id) {
        Optional<Quotation> quotationDTO = quotationRepository.findById(id);
        Quotation quotation = quotationRepository.findById(id).get();
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());
        if (userAccount.get().getId() != quotation.getAdvertisement().getCreatedBy().getId()) {
            return null;
        }
        ManagedQuotationVM managedQuotationVM = new ManagedQuotationVM(quotationDTO.get());
        return Optional.ofNullable(managedQuotationVM);
    }

    @Override
    public List<ManagedQuotationCompactVM> getMyQuotations(Pageable pageable) {
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());

        Page<Quotation> page = quotationRepository.findByDeliveryManId(userAccount.get().getId(), pageable);
        List<ManagedQuotationCompactVM> result = page
            .stream()
            .map(quotation -> new ManagedQuotationCompactVM(quotation))
            .collect(Collectors.toList());

        return result;
    }

    private void createNotificationForDeliveryman(Quotation quotation) {
        Notification notification = new Notification();
        ZonedDateTime timeNow = ZonedDateTime.now();

        notification.setUserAccount(quotation.getDeliveryMan());
        notification.setEntityName(SUJET);
        notification.setEntityId(quotation.getId());
        notification.setViewed(false);
        notification.setNotificationDate(timeNow.toLocalDate());
        notification.setMessage(
            "Le " +
            timeNow.toLocalDate() +
            " votre devis de " +
            quotation.getDeliveryManPrice() +
            " € au sujet de l'annonce " +
            quotation.getAdvertisement().getTitle() +
            " a été accepté."
        );
        notificationRepository.save(notification);

        //part of email
        User user = quotation.getDeliveryMan().getUser();
        String titleKey = "notification.email.deliveryman.title";
        String templateName = "mail/notificationQuotationAcceptedDeliveryman";

        try {
            mailService.sendMailQuotationAcceptedDeliveryman(user, templateName, titleKey);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    /**
     * allow to create Notification and send Email to quotation for user consummer
     * @param creationDateQuotation
     * @param advertisement
     * @param userAccount
     * @Author MAURER adrien TC-46 Notification
     */
    public void creatNotificationAndSendEmail(
        ZonedDateTime creationDateQuotation,
        Advertisement advertisement,
        UserAccount userAccount,
        QuotationDTO quotationDTO
    ) {
        //part of notification
        Notification notification = new Notification();
        notification.setUserAccount(advertisement.getCreatedBy());
        notification.setEntityName(SUJET);
        notification.setEntityId(advertisement.getId());
        notification.setViewed(false);
        notification.setNotificationDate(creationDateQuotation.toLocalDate());
        notification.setMessage(
            "Le " +
            creationDateQuotation.toLocalDate() +
            " " +
            MESSAGE +
            quotationDTO.getDeliveryManPrice() +
            " € au sujet de votre annonce " +
            advertisement.getTitle()
        );
        notificationRepository.save(notification);
        //part of email
        User user = userRepository.getOne(advertisement.getCreatedBy().getUser().getId());
        String titleKey = "email.notification.title";
        String templateName = "mail/notificationsUserEmail";

        try {
            mailService.sendUpdateUserFromTemplate(user, templateName, titleKey);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    @Override
    public List<ManagedQuotationCompactVM> getMyQuotationsFilteredByState(String contentFilter, Pageable pageable) {
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());

        Page<Quotation> page = quotationRepository.findByDeliveryManId(userAccount.get().getId(), pageable);
        List<ManagedQuotationCompactVM> result = page
            .stream()
            .filter(quotation -> StringUtils.containsIgnoreCase(quotation.getStatus().getLabel(), contentFilter))
            .map(quotation -> new ManagedQuotationCompactVM(quotation))
            .collect(Collectors.toList());

        return result;
    }

    @Override
    public List<ManagedQuotationCompactVM> getMyQuotationsFilteredByTitle(String contentFilter, Pageable pageable) {
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());

        Page<Quotation> page = quotationRepository.findByDeliveryManId(userAccount.get().getId(), pageable);
        List<ManagedQuotationCompactVM> result = page
            .stream()
            .filter(quotation -> StringUtils.containsIgnoreCase(quotation.getAdvertisement().getTitle(), contentFilter))
            .map(quotation -> new ManagedQuotationCompactVM(quotation))
            .collect(Collectors.toList());

        return result;
    }

    @Override
    public List<ManagedQuotationCompactVM> getMyQuotationsFilteredByTitleAndState(String contentFilter, Pageable pageable) {
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());

        Page<Quotation> page = quotationRepository.findByDeliveryManIdAndAdvertisementTitleContainingIgnoreCaseOrStatusLabelContainingIgnoreCase(
            userAccount.get().getId(),
            contentFilter,
            contentFilter,
            pageable
        );
        List<ManagedQuotationCompactVM> result = page
            .stream()
            .map(quotation -> new ManagedQuotationCompactVM(quotation))
            .collect(Collectors.toList());

        return result;
    }
}
