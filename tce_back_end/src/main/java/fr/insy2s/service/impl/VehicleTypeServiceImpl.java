package fr.insy2s.service.impl;

import fr.insy2s.domain.VehicleType;
import fr.insy2s.repository.VehicleTypeRepository;
import fr.insy2s.service.VehicleTypeService;
import fr.insy2s.service.dto.VehicleTypeDTO;
import fr.insy2s.service.mapper.VehicleTypeMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link VehicleType}.
 */
@Service
@Transactional
public class VehicleTypeServiceImpl implements VehicleTypeService {
    private final Logger log = LoggerFactory.getLogger(VehicleTypeServiceImpl.class);

    private final VehicleTypeRepository vehicleTypeRepository;

    private final VehicleTypeMapper vehicleTypeMapper;

    public VehicleTypeServiceImpl(VehicleTypeRepository vehicleTypeRepository, VehicleTypeMapper vehicleTypeMapper) {
        this.vehicleTypeRepository = vehicleTypeRepository;
        this.vehicleTypeMapper = vehicleTypeMapper;
    }

    @Override
    public VehicleTypeDTO save(VehicleTypeDTO vehicleTypeDTO) {
        log.debug("Request to save VehicleType : {}", vehicleTypeDTO);
        VehicleType vehicleType = vehicleTypeMapper.toEntity(vehicleTypeDTO);
        vehicleType = vehicleTypeRepository.save(vehicleType);
        return vehicleTypeMapper.toDto(vehicleType);
    }

    @Override
    @Transactional(readOnly = true)
    public List<VehicleTypeDTO> findAll() {
        log.debug("Request to get all VehicleTypes");
        return vehicleTypeRepository.findAll().stream().map(vehicleTypeMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<VehicleTypeDTO> findOne(Long id) {
        log.debug("Request to get VehicleType : {}", id);
        return vehicleTypeRepository.findById(id).map(vehicleTypeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete VehicleType : {}", id);
        vehicleTypeRepository.deleteById(id);
    }
}
