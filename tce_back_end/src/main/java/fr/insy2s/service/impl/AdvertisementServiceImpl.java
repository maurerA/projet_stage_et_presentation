package fr.insy2s.service.impl;

import fr.insy2s.domain.Address;
import fr.insy2s.domain.Advertisement;
import fr.insy2s.domain.AdvertisementStatus;
import fr.insy2s.domain.Authority;
import fr.insy2s.domain.Country;
import fr.insy2s.domain.Quotation;
import fr.insy2s.domain.QuotationStatus;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.repository.AddressRepository;
import fr.insy2s.repository.AdvertisementRepository;
import fr.insy2s.repository.AdvertisementStatusRepository;
import fr.insy2s.repository.CountryRepository;
import fr.insy2s.repository.QuotationRepository;
import fr.insy2s.repository.QuotationStatusRepository;
import fr.insy2s.repository.UserAccountRepository;
import fr.insy2s.security.AuthoritiesConstants;
import fr.insy2s.security.SecurityUtils;
import fr.insy2s.service.AdvertisementService;
import fr.insy2s.service.dto.AdvertisementDTO;
import fr.insy2s.service.mapper.AdvertisementMapper;
import fr.insy2s.web.rest.vm.ManagedAdvertisementCompactVM;
import fr.insy2s.web.rest.vm.ManagedAdvertisementVM;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Advertisement}.
 */
@Service
@Transactional
public class AdvertisementServiceImpl implements AdvertisementService {

    private static class UserAccountResourceException extends RuntimeException {

        private UserAccountResourceException(String message) {
            super(message);
        }
    }

    private static class AccountResourceException extends RuntimeException {

        private AccountResourceException(String message) {
            super(message);
        }
    }

    private static class AdvertisementException extends RuntimeException {

        private AdvertisementException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(AdvertisementServiceImpl.class);

    private final AdvertisementRepository advertisementRepository;

    private final AdvertisementStatusRepository advertisementStatusRepository;

    private final CountryRepository countryRepository;

    private final AdvertisementMapper advertisementMapper;

    private final AddressRepository addressRepository;

    private final UserAccountRepository userAccountRepository;

    private final QuotationRepository quotationRepository;

    private final QuotationStatusRepository quotationStatusRepository;

    public static final String ALL = "all";

    public static final String TITLE = "Titre";

    public static final String STATE = "Etat";

    public static final String ARCHIVEE = "Archivee";

    public static final Long ID_REFUS_CLIENT = 4L;

    public static final Long ID_REFUS_TRANSPORTEUR = 2L;

    public AdvertisementServiceImpl(
        AdvertisementRepository advertisementRepository,
        AdvertisementMapper advertisementMapper,
        AddressRepository addressRepository,
        UserAccountRepository userAccountRepository,
        AdvertisementStatusRepository advertisementStatusRepository,
        CountryRepository countryRepository,
        QuotationRepository quotationRepository,
        QuotationStatusRepository quotationStatusRepository
    ) {
        this.advertisementRepository = advertisementRepository;
        this.advertisementStatusRepository = advertisementStatusRepository;
        this.advertisementMapper = advertisementMapper;
        this.addressRepository = addressRepository;
        this.userAccountRepository = userAccountRepository;
        this.countryRepository = countryRepository;
        this.quotationRepository = quotationRepository;
        this.quotationStatusRepository = quotationStatusRepository;
    }

    @Override
    public AdvertisementDTO save(AdvertisementDTO advertisementDTO) {
        log.debug("Request to save Advertisement : {}", advertisementDTO);
        Advertisement advertisement = advertisementMapper.toEntity(advertisementDTO);
        advertisement = advertisementRepository.save(advertisement);
        return advertisementMapper.toDto(advertisement);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AdvertisementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Advertisements");
        return advertisementRepository.findAll(pageable).map(advertisementMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AdvertisementDTO> findOne(Long id) {
        log.debug("Request to get Advertisement : {}", id);
        return advertisementRepository.findById(id).map(advertisementMapper::toDto);
    }

    /**
     * @author Ebrahim
     * this method archives the advertisement and set all the quotation to denied by User
     * there is a control to see if you're either an admin or the creator of the advertisement
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Advertisement : {}", id);
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());
        Advertisement advertisement = advertisementRepository.findById(id).get();
        // get Advertisement Status with label "Archivee"
        AdvertisementStatus advertisementStatus = advertisementStatusRepository.findById(2L).get();
        boolean verificationAuthorities = false;
        for (Authority authority : userAccount.get().getUser().getAuthorities()) {
            if (authority.getName().equals(AuthoritiesConstants.ADMIN)) {
                verificationAuthorities = true;
            }
        }

        if (advertisement.getCreatedBy().getId() == userAccount.get().getId() || verificationAuthorities) {
            List<Quotation> quotationList = quotationRepository.findByAdvertisementId(id);
            //get quotationStatus where the offer is denied by the client
            QuotationStatus quotationStatus = quotationStatusRepository.findById(ID_REFUS_CLIENT).get();

            for (Quotation quotation : quotationList) {
                quotation.setStatus(quotationStatus);
                quotationRepository.save(quotation);
            }
            advertisement.setStatus(advertisementStatus);
            advertisementRepository.save(advertisement);
        }
    }

    /**
     * @author Mohamed
     * this method archives  set all the quotation to denied by Deliveryman
     */
    @Override
    public void disableDeliverymanQuotations() {
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());

        List<Quotation> DeliverymanQuotations = quotationRepository.findByDeliveryManId(userAccount.get().getId());

        QuotationStatus quotationStatus = quotationStatusRepository.findById(ID_REFUS_TRANSPORTEUR).get();
        for (Quotation quotation : DeliverymanQuotations) {
            quotation.setStatus(quotationStatus);
            quotationRepository.save(quotation);
        }
    }

    /**
     * @author Mohamed
     * this method archives the advertisements User and  set all the quotation to denied by User
     */
    public void disableUserAdvertisements() {
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());
        List<Advertisement> UserAdvertisements = advertisementRepository.findByCreatedById(userAccount.get().getId());

        for (Advertisement advertisement : UserAdvertisements) {
            delete(advertisement.getId());
        }
    }

    /**@author Ebrahim
     * This method allows the user to create and save an advertisement with all his attributes and also save the others attributes objects
     * @param managedAdvertisementVM
     * @return
     */
    @Override
    public Advertisement createNewAdvertisement(ManagedAdvertisementVM managedAdvertisementVM) {
        Advertisement advertisement = new Advertisement(managedAdvertisementVM);
        Country countryDepartAddress = countryRepository.findById(managedAdvertisementVM.getCountryId()).get();
        Country countryDestinationAddress = countryRepository.findById(managedAdvertisementVM.getCountryId2()).get();
        Address departAddress = new Address(managedAdvertisementVM, countryDepartAddress, Address.CONSTRUCTOR_TYPE_DEPART);
        Address destinationAddress = new Address(managedAdvertisementVM, countryDestinationAddress, Address.CONSTRUCTOR_TYPE_ARRIVEE);

        advertisement.setStartAddress(departAddress);
        advertisement.setArrivalAddress(destinationAddress);
        advertisement.setDelivered(false);
        advertisement.setDistanceKm(0F);

        advertisement.setStartAddress(adressAlreadyExist(advertisement.getStartAddress(), advertisement));
        addressRepository.save(advertisement.getStartAddress());

        advertisement.setArrivalAddress(adressAlreadyExist(advertisement.getArrivalAddress(), advertisement));
        addressRepository.save(advertisement.getArrivalAddress());

        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());
        advertisement.setCreatedBy(userAccount.get());

        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        advertisement.setCreationDate(zonedDateTime);

        Optional<AdvertisementStatus> advertisementStatus = advertisementStatusRepository.findById(1L);
        advertisement.setStatus(advertisementStatus.get());

        return advertisementRepository.save(advertisement);
    }

    /** @author Ebrahim
     * This method checks if the addressAlready exists
     * @param addressAdvertisement
     * @param advertisement
     * @return the address if it already exists or the one given in parameters
     */
    private Address adressAlreadyExist(Address addressAdvertisement, Advertisement advertisement) {
        if (
            addressRepository.existsAddressByStreetNumberAndStreetNameAndComplementAddressAndTownAndZipCodeAndCountryId(
                addressAdvertisement.getStreetNumber(),
                addressAdvertisement.getStreetName(),
                addressAdvertisement.getComplementAddress(),
                addressAdvertisement.getTown(),
                addressAdvertisement.getZipCode(),
                addressAdvertisement.getCountry().getId()
            )
        ) {
            return addressRepository
                .findByStreetNumberAndStreetNameAndComplementAddressAndTownAndZipCodeAndCountryId(
                    addressAdvertisement.getStreetNumber(),
                    addressAdvertisement.getStreetName(),
                    addressAdvertisement.getComplementAddress(),
                    addressAdvertisement.getTown(),
                    addressAdvertisement.getZipCode(),
                    addressAdvertisement.getCountry().getId()
                )
                .get();
        }

        return addressAdvertisement;
    }

    /**
     * @author Ebrahim
     * this methods allows the user to get a page of his own advertisements who are not archived
     * @param pageable
     * @return page of Advertisements of the user
     */
    @Override
    public Page<ManagedAdvertisementCompactVM> userAdvertisements(Pageable pageable) {
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());
        Page<ManagedAdvertisementCompactVM> page = advertisementRepository.findByCreatedById(userAccount.get().getId(), pageable);
        List<ManagedAdvertisementCompactVM> result = page
            .stream()
            .filter(advertisement -> !(ARCHIVEE.equalsIgnoreCase(advertisement.getStatusLabel())))
            .collect(Collectors.toList());

        page = new PageImpl<ManagedAdvertisementCompactVM>(result);

        return page;
    }

    /**
     * @author Ebrahim
     * this methods allows the user to get a filtered page of his own advertisements who are not archived
     * @param pageable
     * @return a filtered page of Advertisements of the user
     */
    @Override
    public Page<ManagedAdvertisementCompactVM> userAdvertisementsFiltered(String filter, Pageable pageable, String request) {
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(userLogin.get());
        Page<ManagedAdvertisementCompactVM> page = null;
        if (ALL.equals(request)) {
            page =
                advertisementRepository.findByCreatedByIdAndTitleContainingIgnoreCaseOrStatusLabelContainingIgnoreCase(
                    userAccount.get().getId(),
                    filter,
                    filter,
                    pageable
                );
        } else if (TITLE.equals(request)) {
            page = advertisementRepository.findByCreatedByIdAndTitleContainingIgnoreCase(userAccount.get().getId(), filter, pageable);
        } else if (STATE.equals(request)) {
            page = advertisementRepository.findByCreatedByIdAndStatusLabelContainingIgnoreCase(userAccount.get().getId(), filter, pageable);
        }

        List<ManagedAdvertisementCompactVM> result = page
            .stream()
            .filter(advertisement -> !(ARCHIVEE.equalsIgnoreCase(advertisement.getStatusLabel())))
            .collect(Collectors.toList());
        page = new PageImpl<ManagedAdvertisementCompactVM>(result);
        return page;
    }

    /**
     * manage service Advertisement details with name company and name deliveryMAn
     * @param id
     * @return ManagedAdvertisementVM
     *
     * @Author MAURER Adrien TC-10
     */
    @Override
    public ManagedAdvertisementVM findDetailsAdvertissement(Long id) {
        //recup du login
        String login = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new AccountResourceException("Current user login not found"));
        //recuperation de l userAccount connecte
        Optional<UserAccount> userAccount = userAccountRepository.findByUserLogin(login);
        log.debug("Request to findByUserLogin of userAccount : {}", login);
        if (userAccount.isEmpty()) {
            throw new UserAccountResourceException("userAccount could not be found");
        }
        //recuperation de l annonce avec l id donné en parametre
        Optional<Advertisement> advertisement = advertisementRepository.findById(id);
        log.debug("Request to findById of advertisement : {}", id);
        if (advertisement.isEmpty()) {
            throw new AdvertisementException("Advertisement could not be found");
        }
        //verif si l utilisateur connecte correspond a l utilisateur qui a cree l annonce
        Long idUserAccount = userAccount.get().getId();
        UserAccount userAccountCreatedBy = advertisement.get().getCreatedBy();

        if (idUserAccount != userAccountCreatedBy.getId()) {
            throw new UserAccountResourceException("warning userAccount connected has not this Advertisement");
        }

        // verif recuperation du devis si un transporteur a pris le devis
        List<Quotation> quotationList = quotationRepository.findByAdvertisementId(id);

        for (Quotation quotation : quotationList) {
            if (quotation.getStatus().getLabel().equalsIgnoreCase(Quotation.QUOTATION_ACCEPTED_BY_CLIENT)) {
                return new ManagedAdvertisementVM(advertisement.get(), quotation);
            }
        }
        return new ManagedAdvertisementVM(advertisement.get());
    }

    /**
     * manage service Advertisement details with name company and name deliveryMAn
     * @param id
     * @return ManagedAdvertisementVM
     *
     * @Author Mohamed
     */
    @Override
    public ManagedAdvertisementVM findDetailsAdvertissementForDelivery(Long id) {
        //recuperation de l annonce avec l id donné en parametre
        Optional<Advertisement> advertisement = advertisementRepository.findById(id);
        log.debug("Request to findById of advertisement : {}", id);
        if (advertisement.isEmpty()) {
            throw new AdvertisementException("Advertisement could not be found");
        }

        // verif recuperation du devis si un transporteur a pris le devis
        List<Quotation> quotationList = quotationRepository.findByAdvertisementId(id);

        for (Quotation quotation : quotationList) {
            if (quotation.getStatus().getLabel().equalsIgnoreCase(Quotation.QUOTATION_ACCEPTED_BY_CLIENT)) {
                return new ManagedAdvertisementVM(advertisement.get(), quotation);
            }
        }

        return new ManagedAdvertisementVM(advertisement.get());
    }

    /**
     * @author Mohamed
     * return pages of available advertisements DTO
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AdvertisementDTO> findAvailableAdvertisements(Pageable pageable) {
        log.debug("Request to get Advertisements by status");
        return advertisementRepository.findAllAvailableAdvertissements(pageable).map(advertisementMapper::toDto);
    }

    /**
     * @author Mohamed
     * return list of available advertisements DTO
     */
    @Override
    @Transactional(readOnly = true)
    public List<AdvertisementDTO> findAvailableAdvertisements() {
        log.debug("Request to get Advertisements by status");
        List<Advertisement> advertisements = advertisementRepository.findAllAvailableAdvertissements();
        return advertisementMapper.toDto(advertisements);
    }
}
