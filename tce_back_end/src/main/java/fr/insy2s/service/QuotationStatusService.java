package fr.insy2s.service;

import fr.insy2s.service.dto.QuotationStatusDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.insy2s.domain.QuotationStatus}.
 */
public interface QuotationStatusService {
    /**
     * Save a quotationStatus.
     *
     * @param quotationStatusDTO the entity to save.
     * @return the persisted entity.
     */
    QuotationStatusDTO save(QuotationStatusDTO quotationStatusDTO);

    /**
     * Get all the quotationStatuses.
     *
     * @return the list of entities.
     */
    List<QuotationStatusDTO> findAll();

    /**
     * Get the "id" quotationStatus.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<QuotationStatusDTO> findOne(Long id);

    /**
     * Delete the "id" quotationStatus.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
