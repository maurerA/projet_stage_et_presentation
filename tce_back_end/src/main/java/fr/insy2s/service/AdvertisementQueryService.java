package fr.insy2s.service;

// for static metamodels
import fr.insy2s.domain.Address_;
import fr.insy2s.domain.Advertisement;
import fr.insy2s.domain.AdvertisementStatus_;
import fr.insy2s.domain.Advertisement_;
import fr.insy2s.domain.UserAccount_;
import fr.insy2s.repository.AdvertisementRepository;
import fr.insy2s.service.dto.AdvertisementCriteria;
import fr.insy2s.service.dto.AdvertisementDTO;
import fr.insy2s.service.mapper.AdvertisementMapper;
import io.github.jhipster.service.QueryService;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for executing complex queries for {@link Advertisement} entities in the database.
 * The main input is a {@link AdvertisementCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AdvertisementDTO} or a {@link Page} of {@link AdvertisementDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AdvertisementQueryService extends QueryService<Advertisement> {
    private final Logger log = LoggerFactory.getLogger(AdvertisementQueryService.class);

    private final AdvertisementRepository advertisementRepository;

    private final AdvertisementMapper advertisementMapper;

    public AdvertisementQueryService(AdvertisementRepository advertisementRepository, AdvertisementMapper advertisementMapper) {
        this.advertisementRepository = advertisementRepository;
        this.advertisementMapper = advertisementMapper;
    }

    /**
     * Return a {@link List} of {@link AdvertisementDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AdvertisementDTO> findByCriteria(AdvertisementCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Advertisement> specification = createSpecification(criteria);
        return advertisementMapper.toDto(advertisementRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AdvertisementDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AdvertisementDTO> findByCriteria(AdvertisementCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Advertisement> specification = createSpecification(criteria);
        return advertisementRepository.findAll(specification, page).map(advertisementMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AdvertisementCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Advertisement> specification = createSpecification(criteria);
        return advertisementRepository.count(specification);
    }

    /**
     * @author Mohamed
     * Return a {@link List} of {@link AdvertisementDTO} which matches the id status from the database.
     * @param status The status of the advertisement
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AdvertisementDTO> findByStatusAvailable() {
        log.debug("find by statusID : 1");
        return advertisementMapper.toDto(advertisementRepository.findAllAvailableAdvertissements());
    }

    /**
     * @author Mohamed
     * Return a {@link Page} of {@link AdvertisementDTO} which matches the id status from the database.
     * @param status The status of the advertisement
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AdvertisementDTO> findByStatusAvailable(Pageable page) {
        log.debug("find by statusId : 1, page: {}", page);
        return advertisementRepository.findAllAvailableAdvertissements(page).map(advertisementMapper::toDto);
    }

    /**
     * Function to convert {@link AdvertisementCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Advertisement> createSpecification(AdvertisementCriteria criteria) {
        Specification<Advertisement> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Advertisement_.id));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), Advertisement_.title));
            }
            if (criteria.getWeight() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getWeight(), Advertisement_.weight));
            }
            if (criteria.getLength() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLength(), Advertisement_.length));
            }
            if (criteria.getWidth() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getWidth(), Advertisement_.width));
            }
            if (criteria.getHeight() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getHeight(), Advertisement_.height));
            }
            if (criteria.getDistanceKm() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDistanceKm(), Advertisement_.distanceKm));
            }
            if (criteria.getDelivered() != null) {
                specification = specification.and(buildSpecification(criteria.getDelivered(), Advertisement_.delivered));
            }
            if (criteria.getCreationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationDate(), Advertisement_.creationDate));
            }
            if (criteria.getWishDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getWishDate(), Advertisement_.wishDate));
            }
            if (criteria.getEffectiveDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEffectiveDate(), Advertisement_.effectiveDate));
            }
            if (criteria.getCreatedById() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCreatedById(),
                            root -> root.join(Advertisement_.createdBy, JoinType.LEFT).get(UserAccount_.id)
                        )
                    );
            }
            if (criteria.getStatusId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getStatusId(),
                            root -> root.join(Advertisement_.status, JoinType.LEFT).get(AdvertisementStatus_.id)
                        )
                    );
            }
            if (criteria.getStartAddressId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getStartAddressId(),
                            root -> root.join(Advertisement_.startAddress, JoinType.LEFT).get(Address_.id)
                        )
                    );
            }
            if (criteria.getArrivalAddressId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getArrivalAddressId(),
                            root -> root.join(Advertisement_.arrivalAddress, JoinType.LEFT).get(Address_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
