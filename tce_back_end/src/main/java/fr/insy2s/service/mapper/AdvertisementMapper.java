package fr.insy2s.service.mapper;

import fr.insy2s.domain.*;
import fr.insy2s.service.dto.AdvertisementDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Advertisement} and its DTO {@link AdvertisementDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserAccountMapper.class, AdvertisementStatusMapper.class, AddressMapper.class })
public interface AdvertisementMapper extends EntityMapper<AdvertisementDTO, Advertisement> {
    @Mapping(source = "createdBy.id", target = "createdById")
    @Mapping(source = "status.id", target = "statusId")
    @Mapping(source = "startAddress.id", target = "startAddressId")
    @Mapping(source = "arrivalAddress.id", target = "arrivalAddressId")
    AdvertisementDTO toDto(Advertisement advertisement);

    @Mapping(source = "createdById", target = "createdBy")
    @Mapping(source = "statusId", target = "status")
    @Mapping(source = "startAddressId", target = "startAddress")
    @Mapping(source = "arrivalAddressId", target = "arrivalAddress")
    Advertisement toEntity(AdvertisementDTO advertisementDTO);

    default Advertisement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Advertisement advertisement = new Advertisement();
        advertisement.setId(id);
        return advertisement;
    }
}
