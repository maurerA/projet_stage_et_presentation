package fr.insy2s.service.mapper;

import fr.insy2s.domain.*;
import fr.insy2s.service.dto.VehicleTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link VehicleType} and its DTO {@link VehicleTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VehicleTypeMapper extends EntityMapper<VehicleTypeDTO, VehicleType> {
    default VehicleType fromId(Long id) {
        if (id == null) {
            return null;
        }
        VehicleType vehicleType = new VehicleType();
        vehicleType.setId(id);
        return vehicleType;
    }
}
