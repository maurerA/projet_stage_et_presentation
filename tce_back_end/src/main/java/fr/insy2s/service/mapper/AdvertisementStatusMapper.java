package fr.insy2s.service.mapper;

import fr.insy2s.domain.*;
import fr.insy2s.service.dto.AdvertisementStatusDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AdvertisementStatus} and its DTO {@link AdvertisementStatusDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdvertisementStatusMapper extends EntityMapper<AdvertisementStatusDTO, AdvertisementStatus> {
    default AdvertisementStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdvertisementStatus advertisementStatus = new AdvertisementStatus();
        advertisementStatus.setId(id);
        return advertisementStatus;
    }
}
