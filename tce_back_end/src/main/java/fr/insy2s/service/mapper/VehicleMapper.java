package fr.insy2s.service.mapper;

import fr.insy2s.domain.*;
import fr.insy2s.service.dto.VehicleDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Vehicle} and its DTO {@link VehicleDTO}.
 */
@Mapper(componentModel = "spring", uses = { VehicleTypeMapper.class, UserAccountMapper.class })
public interface VehicleMapper extends EntityMapper<VehicleDTO, Vehicle> {
    @Mapping(source = "type.id", target = "typeId")
    @Mapping(source = "deliveryMan.id", target = "deliveryManId")
    VehicleDTO toDto(Vehicle vehicle);

    @Mapping(source = "typeId", target = "type")
    @Mapping(source = "deliveryManId", target = "deliveryMan")
    Vehicle toEntity(VehicleDTO vehicleDTO);

    default Vehicle fromId(Long id) {
        if (id == null) {
            return null;
        }
        Vehicle vehicle = new Vehicle();
        vehicle.setId(id);
        return vehicle;
    }
}
