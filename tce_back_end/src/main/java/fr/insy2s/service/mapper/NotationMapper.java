package fr.insy2s.service.mapper;

import fr.insy2s.domain.*;
import fr.insy2s.service.dto.NotationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Notation} and its DTO {@link NotationDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserAccountMapper.class })
public interface NotationMapper extends EntityMapper<NotationDTO, Notation> {
    @Mapping(source = "receiver.id", target = "receiverId")
    @Mapping(source = "sender.id", target = "senderId")
    NotationDTO toDto(Notation notation);

    @Mapping(source = "receiverId", target = "receiver")
    @Mapping(source = "senderId", target = "sender")
    Notation toEntity(NotationDTO notationDTO);

    default Notation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Notation notation = new Notation();
        notation.setId(id);
        return notation;
    }
}
