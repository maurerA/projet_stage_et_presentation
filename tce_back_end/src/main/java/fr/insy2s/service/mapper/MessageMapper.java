package fr.insy2s.service.mapper;

import fr.insy2s.domain.*;
import fr.insy2s.service.dto.MessageDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Message} and its DTO {@link MessageDTO}.
 */
@Mapper(componentModel = "spring", uses = { PhotoMapper.class, UserAccountMapper.class, AdvertisementMapper.class })
public interface MessageMapper extends EntityMapper<MessageDTO, Message> {
    @Mapping(source = "photo.id", target = "photoId")
    @Mapping(source = "receiver.id", target = "receiverId")
    @Mapping(source = "sender.id", target = "senderId")
    @Mapping(source = "advertisement.id", target = "advertisementId")
    MessageDTO toDto(Message message);

    @Mapping(source = "photoId", target = "photo")
    @Mapping(source = "receiverId", target = "receiver")
    @Mapping(source = "senderId", target = "sender")
    @Mapping(source = "advertisementId", target = "advertisement")
    Message toEntity(MessageDTO messageDTO);

    default Message fromId(Long id) {
        if (id == null) {
            return null;
        }
        Message message = new Message();
        message.setId(id);
        return message;
    }
}
