package fr.insy2s.service.mapper;

import fr.insy2s.domain.*;
import fr.insy2s.service.dto.UserAccountDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserAccount} and its DTO {@link UserAccountDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class, AddressMapper.class, CompanyMapper.class })
public interface UserAccountMapper extends EntityMapper<UserAccountDTO, UserAccount> {
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "adresse.id", target = "adresseId")
    @Mapping(source = "company.id", target = "companyId")
    UserAccountDTO toDto(UserAccount userAccount);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "adresseId", target = "adresse")
    @Mapping(target = "removeListBlocked", ignore = true)
    @Mapping(source = "companyId", target = "company")
    @Mapping(target = "listBlockedBies", ignore = true)
    @Mapping(target = "removeListBlockedBy", ignore = true)
    UserAccount toEntity(UserAccountDTO userAccountDTO);

    default UserAccount fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserAccount userAccount = new UserAccount();
        userAccount.setId(id);
        return userAccount;
    }
}
