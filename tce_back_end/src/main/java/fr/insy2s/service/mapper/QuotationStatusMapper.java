package fr.insy2s.service.mapper;

import fr.insy2s.domain.*;
import fr.insy2s.service.dto.QuotationStatusDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link QuotationStatus} and its DTO {@link QuotationStatusDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface QuotationStatusMapper extends EntityMapper<QuotationStatusDTO, QuotationStatus> {
    default QuotationStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        QuotationStatus quotationStatus = new QuotationStatus();
        quotationStatus.setId(id);
        return quotationStatus;
    }
}
