package fr.insy2s.service.mapper;

import fr.insy2s.domain.*;
import fr.insy2s.service.dto.QuotationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Quotation} and its DTO {@link QuotationDTO}.
 */
@Mapper(componentModel = "spring", uses = { QuotationStatusMapper.class, UserAccountMapper.class, AdvertisementMapper.class })
public interface QuotationMapper extends EntityMapper<QuotationDTO, Quotation> {
    @Mapping(source = "status.id", target = "statusId")
    @Mapping(source = "deliveryMan.id", target = "deliveryManId")
    @Mapping(source = "advertisement.id", target = "advertisementId")
    QuotationDTO toDto(Quotation quotation);

    @Mapping(source = "statusId", target = "status")
    @Mapping(source = "deliveryManId", target = "deliveryMan")
    @Mapping(source = "advertisementId", target = "advertisement")
    Quotation toEntity(QuotationDTO quotationDTO);

    default Quotation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Quotation quotation = new Quotation();
        quotation.setId(id);
        return quotation;
    }
}
