package fr.insy2s.service.mapper;

import fr.insy2s.domain.*;
import fr.insy2s.service.dto.PhotoDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Photo} and its DTO {@link PhotoDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserAccountMapper.class, AdvertisementMapper.class })
public interface PhotoMapper extends EntityMapper<PhotoDTO, Photo> {
    @Mapping(source = "deliveryMan.id", target = "deliveryManId")
    @Mapping(source = "advertisement.id", target = "advertisementId")
    PhotoDTO toDto(Photo photo);

    @Mapping(source = "deliveryManId", target = "deliveryMan")
    @Mapping(source = "advertisementId", target = "advertisement")
    Photo toEntity(PhotoDTO photoDTO);

    default Photo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Photo photo = new Photo();
        photo.setId(id);
        return photo;
    }
}
