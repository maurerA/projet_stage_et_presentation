package fr.insy2s.repository;

import fr.insy2s.domain.UserAccount;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the UserAccount entity.
 */
@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long>, JpaSpecificationExecutor<UserAccount> {
    @Query(
        value = "select distinct userAccount from UserAccount userAccount left join fetch userAccount.listBlockeds",
        countQuery = "select count(distinct userAccount) from UserAccount userAccount"
    )
    Page<UserAccount> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct userAccount from UserAccount userAccount left join fetch userAccount.listBlockeds")
    List<UserAccount> findAllWithEagerRelationships();

    @Query("select userAccount from UserAccount userAccount left join fetch userAccount.listBlockeds where userAccount.id =:id")
    Optional<UserAccount> findOneWithEagerRelationships(@Param("id") Long id);

    /**
     * get an useraccount with user, adress, compagny and country
     * @param login
     * @return useraccount
     * @author MAURER adrien-TC22, ibrahim El Kadiri
     */
    Optional<UserAccount> findByUserLogin(String login);
}
