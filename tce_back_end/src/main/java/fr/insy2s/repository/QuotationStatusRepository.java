package fr.insy2s.repository;

import fr.insy2s.domain.QuotationStatus;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the QuotationStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuotationStatusRepository extends JpaRepository<QuotationStatus, Long>, JpaSpecificationExecutor<QuotationStatus> {}
