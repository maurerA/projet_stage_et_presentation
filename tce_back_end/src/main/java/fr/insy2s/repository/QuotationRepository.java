package fr.insy2s.repository;

import fr.insy2s.domain.Advertisement;
import fr.insy2s.domain.Quotation;
import fr.insy2s.web.rest.vm.ManagedAdvertisementCompactVM;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Quotation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuotationRepository extends JpaRepository<Quotation, Long>, JpaSpecificationExecutor<Quotation> {
    List<Quotation> findByAdvertisementId(Long id);

    Page<Quotation> findByAdvertisement(Advertisement advertisement, Pageable pageable);

    Page<Quotation> findByDeliveryManId(Long id, Pageable pageable);

    List<Quotation> findByDeliveryManId(Long id);

    Page<Quotation> findByDeliveryManIdAndAdvertisementTitleContainingIgnoreCaseOrStatusLabelContainingIgnoreCase(
        Long id,
        String title,
        String Status,
        Pageable pageable
    );
}
