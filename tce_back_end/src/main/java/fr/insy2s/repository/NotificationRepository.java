package fr.insy2s.repository;

import fr.insy2s.domain.Notification;
import fr.insy2s.domain.User;
import fr.insy2s.domain.UserAccount;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Notification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long>, JpaSpecificationExecutor<Notification> {
    List<Notification> findAllByUserAccountUser(User user);
}
