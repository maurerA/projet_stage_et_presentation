package fr.insy2s.repository;

import fr.insy2s.domain.AdvertisementStatus;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AdvertisementStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdvertisementStatusRepository
    extends JpaRepository<AdvertisementStatus, Long>, JpaSpecificationExecutor<AdvertisementStatus> {}
