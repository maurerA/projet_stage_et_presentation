package fr.insy2s.repository;

import fr.insy2s.domain.Advertisement;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.web.rest.vm.ManagedAdvertisementCompactVM;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Advertisement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Long>, JpaSpecificationExecutor<Advertisement> {
    Page<ManagedAdvertisementCompactVM> findByCreatedById(Long id, Pageable pageable);

    Page<ManagedAdvertisementCompactVM> findByCreatedByIdAndTitleContainingIgnoreCaseOrStatusLabelContainingIgnoreCase(
        Long id,
        String title,
        String Status,
        Pageable pageable
    );

    Page<ManagedAdvertisementCompactVM> findByCreatedByIdAndTitleContainingIgnoreCase(Long id, String title, Pageable pageable);

    Page<ManagedAdvertisementCompactVM> findByCreatedByIdAndStatusLabelContainingIgnoreCase(Long id, String Status, Pageable pageable);

    List<Advertisement> findByCreatedById(Long id);

    /**
     * @author Mohamed
     * @param pageable
     * @return the available advertisements for deliverymens (with the status)
     */
    @Query("from Advertisement a where a.status.id = 1")
    Page<Advertisement> findAllAvailableAdvertissements(Pageable pageable);

    @Query("from Advertisement a where a.status.id = 1")
    List<Advertisement> findAllAvailableAdvertissements();
}
