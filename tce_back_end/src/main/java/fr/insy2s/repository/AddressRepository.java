package fr.insy2s.repository;

import fr.insy2s.domain.Address;
import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Address entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AddressRepository extends JpaRepository<Address, Long>, JpaSpecificationExecutor<Address> {
    /**
     * @author Ebrahim
     * @param streetNumber
     * @param streetName
     * @param complementAddress
     * @param town
     * @param zipCode
     * @param idCountry
     * @return
     */
    boolean existsAddressByStreetNumberAndStreetNameAndComplementAddressAndTownAndZipCodeAndCountryId(
        String streetNumber,
        String streetName,
        String complementAddress,
        String town,
        String zipCode,
        Long idCountry
    );

    Optional<Address> findByStreetNumberAndStreetNameAndComplementAddressAndTownAndZipCodeAndCountryId(
        String streetNumber,
        String streetName,
        String complementAddress,
        String town,
        String zipCode,
        Long idCountry
    );
}
