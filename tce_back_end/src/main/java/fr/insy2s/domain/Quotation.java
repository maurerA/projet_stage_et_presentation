package fr.insy2s.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The quotation entity allows deliverers\nto estimate the cost of delivery to the customer\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed
 */
@Entity
@Table(name = "quotation")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Quotation implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String QUOTATION_ACCEPTED_BY_CLIENT = "devis accepte client";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "delivery_man_price", nullable = false)
    private Float deliveryManPrice;

    @NotNull
    @Column(name = "creation_date_quotation", nullable = false)
    private ZonedDateTime creationDateQuotation;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "quotations", allowSetters = true)
    private QuotationStatus status;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "quotations", allowSetters = true)
    private UserAccount deliveryMan;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "quotations", allowSetters = true)
    private Advertisement advertisement;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getDeliveryManPrice() {
        return deliveryManPrice;
    }

    public Quotation deliveryManPrice(Float deliveryManPrice) {
        this.deliveryManPrice = deliveryManPrice;
        return this;
    }

    public void setDeliveryManPrice(Float deliveryManPrice) {
        this.deliveryManPrice = deliveryManPrice;
    }

    public ZonedDateTime getCreationDateQuotation() {
        return creationDateQuotation;
    }

    public Quotation creationDateQuotation(ZonedDateTime creationDateQuotation) {
        this.creationDateQuotation = creationDateQuotation;
        return this;
    }

    public void setCreationDateQuotation(ZonedDateTime creationDateQuotation) {
        this.creationDateQuotation = creationDateQuotation;
    }

    public QuotationStatus getStatus() {
        return status;
    }

    public Quotation status(QuotationStatus quotationStatus) {
        this.status = quotationStatus;
        return this;
    }

    public void setStatus(QuotationStatus quotationStatus) {
        this.status = quotationStatus;
    }

    public UserAccount getDeliveryMan() {
        return deliveryMan;
    }

    public Quotation deliveryMan(UserAccount userAccount) {
        this.deliveryMan = userAccount;
        return this;
    }

    public void setDeliveryMan(UserAccount userAccount) {
        this.deliveryMan = userAccount;
    }

    public Advertisement getAdvertisement() {
        return advertisement;
    }

    public Quotation advertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
        return this;
    }

    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Quotation)) {
            return false;
        }
        return id != null && id.equals(((Quotation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Quotation{" +
            "id=" + getId() +
            ", deliveryManPrice=" + getDeliveryManPrice() +
            ", creationDateQuotation='" + getCreationDateQuotation() + "'" +
            "}";
    }
}
