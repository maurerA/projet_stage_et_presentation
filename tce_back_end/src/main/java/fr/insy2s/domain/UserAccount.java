package fr.insy2s.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * UserAccount is the User's extension\nUser's is an entity created by jhipster, to handle login/password\nUserAccount is an entity to centralize all the data linked with the user\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed
 */
@Entity
@Table(name = "user_account")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserAccount implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @NotNull
    @Column(name = "available", nullable = false)
    private Boolean available;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private User user;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "userAccounts", allowSetters = true)
    private Address adresse;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(
        name = "user_account_list_blocked",
        joinColumns = @JoinColumn(name = "user_account_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "list_blocked_id", referencedColumnName = "id")
    )
    private Set<UserAccount> listBlockeds = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "listDeliveryMen", allowSetters = true)
    private Company company;

    @ManyToMany(mappedBy = "listBlockeds")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<UserAccount> listBlockedBies = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public UserAccount phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean isAvailable() {
        return available;
    }

    public UserAccount available(Boolean available) {
        this.available = available;
        return this;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public User getUser() {
        return user;
    }

    public UserAccount user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Address getAdresse() {
        return adresse;
    }

    public UserAccount adresse(Address address) {
        this.adresse = address;
        return this;
    }

    public void setAdresse(Address address) {
        this.adresse = address;
    }

    public Set<UserAccount> getListBlockeds() {
        return listBlockeds;
    }

    public UserAccount listBlockeds(Set<UserAccount> userAccounts) {
        this.listBlockeds = userAccounts;
        return this;
    }

    public UserAccount addListBlocked(UserAccount userAccount) {
        this.listBlockeds.add(userAccount);
        userAccount.getListBlockedBies().add(this);
        return this;
    }

    public UserAccount removeListBlocked(UserAccount userAccount) {
        this.listBlockeds.remove(userAccount);
        userAccount.getListBlockedBies().remove(this);
        return this;
    }

    public void setListBlockeds(Set<UserAccount> userAccounts) {
        this.listBlockeds = userAccounts;
    }

    public Company getCompany() {
        return company;
    }

    public UserAccount company(Company company) {
        this.company = company;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Set<UserAccount> getListBlockedBies() {
        return listBlockedBies;
    }

    public UserAccount listBlockedBies(Set<UserAccount> userAccounts) {
        this.listBlockedBies = userAccounts;
        return this;
    }

    public UserAccount addListBlockedBy(UserAccount userAccount) {
        this.listBlockedBies.add(userAccount);
        userAccount.getListBlockeds().add(this);
        return this;
    }

    public UserAccount removeListBlockedBy(UserAccount userAccount) {
        this.listBlockedBies.remove(userAccount);
        userAccount.getListBlockeds().remove(this);
        return this;
    }

    public void setListBlockedBies(Set<UserAccount> userAccounts) {
        this.listBlockedBies = userAccounts;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserAccount)) {
            return false;
        }
        return id != null && id.equals(((UserAccount) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserAccount{" +
            "id=" + getId() +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", available='" + isAvailable() + "'" +
            "}";
    }
}
