package fr.insy2s.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.insy2s.web.rest.vm.ManagedAdvertisementVM;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * the Address entity is used to enter\nthe address of the customer and the delivery person.\nThe entity also provides the departure address
 * \nof the Entity Advertisement and its arrival address.\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed
 */

@Entity
@Table(name = "address")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Address implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String CONSTRUCTOR_TYPE_DEPART = "Depart";
    public static final String CONSTRUCTOR_TYPE_ARRIVEE = "Arrivee";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "street_number", nullable = false)
    private String streetNumber;

    @Column(name = "complement_address")
    private String complementAddress;

    @NotNull
    @Column(name = "street_name", nullable = false)
    private String streetName;

    @NotNull
    @Column(name = "zip_code", nullable = false)
    private String zipCode;

    @NotNull
    @Column(name = "town", nullable = false)
    private String town;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "addresses", allowSetters = true)
    private Country country;

    public Address() {}

    public Address(ManagedAdvertisementVM managedAdvertisementVM, Country countryAddress, String constructorType) {
        if (CONSTRUCTOR_TYPE_DEPART.equals(constructorType)) {
            this.streetNumber(managedAdvertisementVM.getStreetNumber());
            this.complementAddress(managedAdvertisementVM.getComplementAddress());
            this.streetName(managedAdvertisementVM.getStreetName());
            this.town(managedAdvertisementVM.getTown());
            this.zipCode(managedAdvertisementVM.getZipCode());
            this.country(countryAddress);
        } else if (CONSTRUCTOR_TYPE_ARRIVEE.equals(constructorType)) {
            this.streetNumber(managedAdvertisementVM.getStreetNumber2());
            this.complementAddress(managedAdvertisementVM.getComplementAddress2());
            this.streetName(managedAdvertisementVM.getStreetName2());
            this.town(managedAdvertisementVM.getTown2());
            this.zipCode(managedAdvertisementVM.getZipCode2());
            this.country(countryAddress);
        }
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public Address streetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
        return this;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getComplementAddress() {
        return complementAddress;
    }

    public Address complementAddress(String complementAddress) {
        this.complementAddress = complementAddress;
        return this;
    }

    public void setComplementAddress(String complementAddress) {
        this.complementAddress = complementAddress;
    }

    public String getStreetName() {
        return streetName;
    }

    public Address streetName(String streetName) {
        this.streetName = streetName;
        return this;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public Address zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getTown() {
        return town;
    }

    public Address town(String town) {
        this.town = town;
        return this;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public Country getCountry() {
        return country;
    }

    public Address country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Address)) {
            return false;
        }
        return id != null && id.equals(((Address) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Address{" +
            "id=" + getId() +
            ", streetNumber='" + getStreetNumber() + "'" +
            ", complementAddress='" + getComplementAddress() + "'" +
            ", streetName='" + getStreetName() + "'" +
            ", zipCode='" + getZipCode() + "'" +
            ", town='" + getTown() + "'" +
            "}";
    }
}
