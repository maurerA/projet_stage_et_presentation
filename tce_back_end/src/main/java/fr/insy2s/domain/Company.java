package fr.insy2s.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * entity Company is an extension for the possible futur\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed
 */
@Entity
@Table(name = "company")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Company implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name_company", nullable = false, unique = true)
    private String nameCompany;

    @NotNull
    @Column(name = "number_siret_company", nullable = false, unique = true)
    private String numberSiretCompany;

    @NotNull
    @Column(name = "number_siren", nullable = false, unique = true)
    private String numberSiren;

    @OneToMany(mappedBy = "company")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<UserAccount> listDeliveryMen = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public Company nameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
        return this;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getNumberSiretCompany() {
        return numberSiretCompany;
    }

    public Company numberSiretCompany(String numberSiretCompany) {
        this.numberSiretCompany = numberSiretCompany;
        return this;
    }

    public void setNumberSiretCompany(String numberSiretCompany) {
        this.numberSiretCompany = numberSiretCompany;
    }

    public String getNumberSiren() {
        return numberSiren;
    }

    public Company numberSiren(String numberSiren) {
        this.numberSiren = numberSiren;
        return this;
    }

    public void setNumberSiren(String numberSiren) {
        this.numberSiren = numberSiren;
    }

    public Set<UserAccount> getListDeliveryMen() {
        return listDeliveryMen;
    }

    public Company listDeliveryMen(Set<UserAccount> userAccounts) {
        this.listDeliveryMen = userAccounts;
        return this;
    }

    public Company addListDeliveryMan(UserAccount userAccount) {
        this.listDeliveryMen.add(userAccount);
        userAccount.setCompany(this);
        return this;
    }

    public Company removeListDeliveryMan(UserAccount userAccount) {
        this.listDeliveryMen.remove(userAccount);
        userAccount.setCompany(null);
        return this;
    }

    public void setListDeliveryMen(Set<UserAccount> userAccounts) {
        this.listDeliveryMen = userAccounts;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Company)) {
            return false;
        }
        return id != null && id.equals(((Company) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Company{" +
            "id=" + getId() +
            ", nameCompany='" + getNameCompany() + "'" +
            ", numberSiretCompany='" + getNumberSiretCompany() + "'" +
            ", numberSiren='" + getNumberSiren() + "'" +
            "}";
    }
}
