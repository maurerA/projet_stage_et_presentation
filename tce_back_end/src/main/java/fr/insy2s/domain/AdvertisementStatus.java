package fr.insy2s.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * As for the country entity, the QuotationState entity makes the code more flexible\nDifferents states are : active, delivered, archived, saved, expired\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed
 */
@Entity
@Table(name = "advertisement_status")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AdvertisementStatus implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "code_ref", nullable = false, unique = true)
    private String codeRef;

    @NotNull
    @Column(name = "label", nullable = false, unique = true)
    private String label;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeRef() {
        return codeRef;
    }

    public AdvertisementStatus codeRef(String codeRef) {
        this.codeRef = codeRef;
        return this;
    }

    public void setCodeRef(String codeRef) {
        this.codeRef = codeRef;
    }

    public String getLabel() {
        return label;
    }

    public AdvertisementStatus label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdvertisementStatus)) {
            return false;
        }
        return id != null && id.equals(((AdvertisementStatus) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AdvertisementStatus{" +
            "id=" + getId() +
            ", codeRef='" + getCodeRef() + "'" +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
