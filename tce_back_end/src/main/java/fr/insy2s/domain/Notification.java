package fr.insy2s.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * the notification entity allows\nthe user, whether a customer or a deliver,\nto receive as information all changes to his account\nNB : the entityName and entityId fields are created\nto generalize notifications to the whole application\nfor possible extension.\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed
 */
@Entity
@Table(name = "notification")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Notification implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "entity_name")
    private String entityName;

    @Column(name = "entity_id")
    private Long entityId;

    @NotNull
    @Column(name = "viewed", nullable = false)
    private Boolean viewed;

    @NotNull
    @Column(name = "notification_date", nullable = false)
    private LocalDate notificationDate;

    @Column(name = "message")
    private String message;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "notifications", allowSetters = true)
    private UserAccount userAccount;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntityName() {
        return entityName;
    }

    public Notification entityName(String entityName) {
        this.entityName = entityName;
        return this;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Long getEntityId() {
        return entityId;
    }

    public Notification entityId(Long entityId) {
        this.entityId = entityId;
        return this;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Boolean isViewed() {
        return viewed;
    }

    public Notification viewed(Boolean viewed) {
        this.viewed = viewed;
        return this;
    }

    public void setViewed(Boolean viewed) {
        this.viewed = viewed;
    }

    public LocalDate getNotificationDate() {
        return notificationDate;
    }

    public Notification notificationDate(LocalDate notificationDate) {
        this.notificationDate = notificationDate;
        return this;
    }

    public void setNotificationDate(LocalDate notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getMessage() {
        return message;
    }

    public Notification message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public Notification userAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
        return this;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notification)) {
            return false;
        }
        return id != null && id.equals(((Notification) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Notification{" +
            "id=" + getId() +
            ", entityName='" + getEntityName() + "'" +
            ", entityId=" + getEntityId() +
            ", viewed='" + isViewed() + "'" +
            ", notificationDate='" + getNotificationDate() + "'" +
            ", message='" + getMessage() + "'" +
            "}";
    }
}
