package fr.insy2s.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The message entity allows every\ncustomer and deliverymen to\ntalk with each others for deliveries\nwhen a customer accepts a deliveryman's\nquotation.\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed
 */
@Entity
@Table(name = "message")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "message", nullable = false)
    private String message;

    @NotNull
    @Column(name = "date_message", nullable = false)
    private ZonedDateTime dateMessage;

    @OneToOne
    @JoinColumn(unique = true)
    private Photo photo;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "messages", allowSetters = true)
    private UserAccount receiver;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "messages", allowSetters = true)
    private UserAccount sender;

    @ManyToOne
    @JsonIgnoreProperties(value = "messages", allowSetters = true)
    private Advertisement advertisement;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public Message message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getDateMessage() {
        return dateMessage;
    }

    public Message dateMessage(ZonedDateTime dateMessage) {
        this.dateMessage = dateMessage;
        return this;
    }

    public void setDateMessage(ZonedDateTime dateMessage) {
        this.dateMessage = dateMessage;
    }

    public Photo getPhoto() {
        return photo;
    }

    public Message photo(Photo photo) {
        this.photo = photo;
        return this;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public Message receiver(UserAccount userAccount) {
        this.receiver = userAccount;
        return this;
    }

    public void setReceiver(UserAccount userAccount) {
        this.receiver = userAccount;
    }

    public UserAccount getSender() {
        return sender;
    }

    public Message sender(UserAccount userAccount) {
        this.sender = userAccount;
        return this;
    }

    public void setSender(UserAccount userAccount) {
        this.sender = userAccount;
    }

    public Advertisement getAdvertisement() {
        return advertisement;
    }

    public Message advertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
        return this;
    }

    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Message)) {
            return false;
        }
        return id != null && id.equals(((Message) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Message{" +
            "id=" + getId() +
            ", message='" + getMessage() + "'" +
            ", dateMessage='" + getDateMessage() + "'" +
            "}";
    }
}
