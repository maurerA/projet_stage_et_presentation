package fr.insy2s.domain;

public class Filter {
    private String contentFilter;

    private String filterType;

    public Filter() {}

    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public String getContentFilter() {
        return contentFilter;
    }

    public void setContentFilter(String contentFilter) {
        this.contentFilter = contentFilter;
    }
}
