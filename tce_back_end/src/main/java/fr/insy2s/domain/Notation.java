package fr.insy2s.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The rating entity allows each customer\nto assess the service provided by the delivery person.\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed
 */
@Entity
@Table(name = "notation")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Notation implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "number_star", nullable = false)
    private Integer numberStar;

    @Column(name = "message")
    private String message;

    @NotNull
    @Column(name = "date_note", nullable = false)
    private ZonedDateTime dateNote;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "notations", allowSetters = true)
    private UserAccount receiver;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "notations", allowSetters = true)
    private UserAccount sender;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumberStar() {
        return numberStar;
    }

    public Notation numberStar(Integer numberStar) {
        this.numberStar = numberStar;
        return this;
    }

    public void setNumberStar(Integer numberStar) {
        this.numberStar = numberStar;
    }

    public String getMessage() {
        return message;
    }

    public Notation message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getDateNote() {
        return dateNote;
    }

    public Notation dateNote(ZonedDateTime dateNote) {
        this.dateNote = dateNote;
        return this;
    }

    public void setDateNote(ZonedDateTime dateNote) {
        this.dateNote = dateNote;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public Notation receiver(UserAccount userAccount) {
        this.receiver = userAccount;
        return this;
    }

    public void setReceiver(UserAccount userAccount) {
        this.receiver = userAccount;
    }

    public UserAccount getSender() {
        return sender;
    }

    public Notation sender(UserAccount userAccount) {
        this.sender = userAccount;
        return this;
    }

    public void setSender(UserAccount userAccount) {
        this.sender = userAccount;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notation)) {
            return false;
        }
        return id != null && id.equals(((Notation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Notation{" +
            "id=" + getId() +
            ", numberStar=" + getNumberStar() +
            ", message='" + getMessage() + "'" +
            ", dateNote='" + getDateNote() + "'" +
            "}";
    }
}
