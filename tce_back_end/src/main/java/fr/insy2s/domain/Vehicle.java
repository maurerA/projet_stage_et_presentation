package fr.insy2s.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The vehicle entity allows every\ndeliveryman to register the vehicle\nhe'll use during deliveries\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed
 */
@Entity
@Table(name = "vehicle")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Vehicle implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "max_capacity_length", nullable = false)
    private Float maxCapacityLength;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "max_capacity_width", nullable = false)
    private Float maxCapacityWidth;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "max_capacity_height", nullable = false)
    private Float maxCapacityHeight;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "max_capacity_weight", nullable = false)
    private Float maxCapacityWeight;

    @NotNull
    @Column(name = "active", nullable = false)
    private Boolean active;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "vehicles", allowSetters = true)
    private VehicleType type;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "vehicles", allowSetters = true)
    private UserAccount deliveryMan;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getMaxCapacityLength() {
        return maxCapacityLength;
    }

    public Vehicle maxCapacityLength(Float maxCapacityLength) {
        this.maxCapacityLength = maxCapacityLength;
        return this;
    }

    public void setMaxCapacityLength(Float maxCapacityLength) {
        this.maxCapacityLength = maxCapacityLength;
    }

    public Float getMaxCapacityWidth() {
        return maxCapacityWidth;
    }

    public Vehicle maxCapacityWidth(Float maxCapacityWidth) {
        this.maxCapacityWidth = maxCapacityWidth;
        return this;
    }

    public void setMaxCapacityWidth(Float maxCapacityWidth) {
        this.maxCapacityWidth = maxCapacityWidth;
    }

    public Float getMaxCapacityHeight() {
        return maxCapacityHeight;
    }

    public Vehicle maxCapacityHeight(Float maxCapacityHeight) {
        this.maxCapacityHeight = maxCapacityHeight;
        return this;
    }

    public void setMaxCapacityHeight(Float maxCapacityHeight) {
        this.maxCapacityHeight = maxCapacityHeight;
    }

    public Float getMaxCapacityWeight() {
        return maxCapacityWeight;
    }

    public Vehicle maxCapacityWeight(Float maxCapacityWeight) {
        this.maxCapacityWeight = maxCapacityWeight;
        return this;
    }

    public void setMaxCapacityWeight(Float maxCapacityWeight) {
        this.maxCapacityWeight = maxCapacityWeight;
    }

    public Boolean isActive() {
        return active;
    }

    public Vehicle active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public VehicleType getType() {
        return type;
    }

    public Vehicle type(VehicleType vehicleType) {
        this.type = vehicleType;
        return this;
    }

    public void setType(VehicleType vehicleType) {
        this.type = vehicleType;
    }

    public UserAccount getDeliveryMan() {
        return deliveryMan;
    }

    public Vehicle deliveryMan(UserAccount userAccount) {
        this.deliveryMan = userAccount;
        return this;
    }

    public void setDeliveryMan(UserAccount userAccount) {
        this.deliveryMan = userAccount;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vehicle)) {
            return false;
        }
        return id != null && id.equals(((Vehicle) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Vehicle{" +
            "id=" + getId() +
            ", maxCapacityLength=" + getMaxCapacityLength() +
            ", maxCapacityWidth=" + getMaxCapacityWidth() +
            ", maxCapacityHeight=" + getMaxCapacityHeight() +
            ", maxCapacityWeight=" + getMaxCapacityWeight() +
            ", active='" + isActive() + "'" +
            "}";
    }
}
