package fr.insy2s.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.insy2s.web.rest.vm.ManagedAdvertisementVM;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The advertisement entity allows thanks to its fields\nto provide a maximum of indication to the deliverers to estimate\nthe estimate of the race\n\n@Authors : El kadiri Ibrahim, Fares Amal, Maurer Adrien, Meddah Mohamed
 */
@Entity
@Table(name = "advertisement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Advertisement implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Column(name = "weight", nullable = false)
    private Float weight;

    @NotNull
    @Column(name = "length", nullable = false)
    private Float length;

    @NotNull
    @Column(name = "width", nullable = false)
    private Float width;

    @NotNull
    @Column(name = "height", nullable = false)
    private Float height;

    @NotNull
    @Column(name = "distance_km", nullable = false)
    private Float distanceKm;

    @NotNull
    @Column(name = "delivered", nullable = false)
    private Boolean delivered;

    @NotNull
    @Column(name = "creation_date", nullable = false)
    private ZonedDateTime creationDate;

    @Column(name = "wish_date")
    private ZonedDateTime wishDate;

    @Column(name = "effective_date")
    private ZonedDateTime effectiveDate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "advertisements", allowSetters = true)
    private UserAccount createdBy;

    @ManyToOne
    @JsonIgnoreProperties(value = "advertisements", allowSetters = true)
    private AdvertisementStatus status;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "advertisements", allowSetters = true)
    private Address startAddress;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "advertisements", allowSetters = true)
    private Address arrivalAddress;

    public Advertisement(ManagedAdvertisementVM managedAdvertisementVM) {
        this.title(managedAdvertisementVM.getTitle());
        this.height(managedAdvertisementVM.getHeight());
        this.weight(managedAdvertisementVM.getWeight());
        this.length(managedAdvertisementVM.getLength());
        this.width(managedAdvertisementVM.getWidth());
    }

    public Advertisement() {}

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Advertisement title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getWeight() {
        return weight;
    }

    public Advertisement weight(Float weight) {
        this.weight = weight;
        return this;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getLength() {
        return length;
    }

    public Advertisement length(Float length) {
        this.length = length;
        return this;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public Advertisement width(Float width) {
        this.width = width;
        return this;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getHeight() {
        return height;
    }

    public Advertisement height(Float height) {
        this.height = height;
        return this;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getDistanceKm() {
        return distanceKm;
    }

    public Advertisement distanceKm(Float distanceKm) {
        this.distanceKm = distanceKm;
        return this;
    }

    public void setDistanceKm(Float distanceKm) {
        this.distanceKm = distanceKm;
    }

    public Boolean isDelivered() {
        return delivered;
    }

    public Advertisement delivered(Boolean delivered) {
        this.delivered = delivered;
        return this;
    }

    public void setDelivered(Boolean delivered) {
        this.delivered = delivered;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public Advertisement creationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public ZonedDateTime getWishDate() {
        return wishDate;
    }

    public Advertisement wishDate(ZonedDateTime wishDate) {
        this.wishDate = wishDate;
        return this;
    }

    public void setWishDate(ZonedDateTime wishDate) {
        this.wishDate = wishDate;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public Advertisement effectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
        return this;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public UserAccount getCreatedBy() {
        return createdBy;
    }

    public Advertisement createdBy(UserAccount userAccount) {
        this.createdBy = userAccount;
        return this;
    }

    public void setCreatedBy(UserAccount userAccount) {
        this.createdBy = userAccount;
    }

    public AdvertisementStatus getStatus() {
        return status;
    }

    public Advertisement status(AdvertisementStatus advertisementStatus) {
        this.status = advertisementStatus;
        return this;
    }

    public void setStatus(AdvertisementStatus advertisementStatus) {
        this.status = advertisementStatus;
    }

    public Address getStartAddress() {
        return startAddress;
    }

    public Advertisement startAddress(Address address) {
        this.startAddress = address;
        return this;
    }

    public void setStartAddress(Address address) {
        this.startAddress = address;
    }

    public Address getArrivalAddress() {
        return arrivalAddress;
    }

    public Advertisement arrivalAddress(Address address) {
        this.arrivalAddress = address;
        return this;
    }

    public void setArrivalAddress(Address address) {
        this.arrivalAddress = address;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Advertisement)) {
            return false;
        }
        return id != null && id.equals(((Advertisement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Advertisement{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", weight=" + getWeight() +
            ", length=" + getLength() +
            ", width=" + getWidth() +
            ", height=" + getHeight() +
            ", distanceKm=" + getDistanceKm() +
            ", delivered='" + isDelivered() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", wishDate='" + getWishDate() + "'" +
            ", effectiveDate='" + getEffectiveDate() + "'" +
            "}";
    }
}
