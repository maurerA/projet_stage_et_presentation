package fr.insy2s.web.rest.vm;

import fr.insy2s.domain.Quotation;
import java.time.ZonedDateTime;

public class ManagedQuotationVM {
    private Long id;

    private Float deliveryManPrice;

    private ZonedDateTime creationDateQuotation;

    private String statusLabel;

    private Long statusId;

    private String deliveryManName;

    private Long idCompany;

    private String nameCompany;

    private String numberSiretCompany;

    private String numberSiren;

    private Long advertisementId;

    public ManagedQuotationVM() {}

    public ManagedQuotationVM(Quotation quotation) {
        this.id = quotation.getId();
        this.deliveryManPrice = quotation.getDeliveryManPrice();
        this.creationDateQuotation = ZonedDateTime.now();
        this.statusLabel = quotation.getStatus().getLabel();
        this.statusId = quotation.getStatus().getId();
        this.deliveryManName = quotation.getDeliveryMan().getUser().getLastName();
        this.idCompany = quotation.getDeliveryMan().getCompany().getId();
        this.nameCompany = quotation.getDeliveryMan().getCompany().getNameCompany();
        this.numberSiretCompany = quotation.getDeliveryMan().getCompany().getNumberSiretCompany();
        this.numberSiren = quotation.getDeliveryMan().getCompany().getNumberSiren();
        this.advertisementId = quotation.getAdvertisement().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getDeliveryManPrice() {
        return deliveryManPrice;
    }

    public void setDeliveryManPrice(Float deliveryManPrice) {
        this.deliveryManPrice = deliveryManPrice;
    }

    public ZonedDateTime getCreationDateQuotation() {
        return creationDateQuotation;
    }

    public void setCreationDateQuotation(ZonedDateTime creationDateQuotation) {
        this.creationDateQuotation = creationDateQuotation;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getDeliveryManName() {
        return deliveryManName;
    }

    public void setDeliveryManName(String deliveryManName) {
        this.deliveryManName = deliveryManName;
    }

    public Long getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Long idCompany) {
        this.idCompany = idCompany;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getNumberSiretCompany() {
        return numberSiretCompany;
    }

    public void setNumberSiretCompany(String numberSiretCompany) {
        this.numberSiretCompany = numberSiretCompany;
    }

    public String getNumberSiren() {
        return numberSiren;
    }

    public void setNumberSiren(String numberSiren) {
        this.numberSiren = numberSiren;
    }

    public Long getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(Long advertisementId) {
        this.advertisementId = advertisementId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}
