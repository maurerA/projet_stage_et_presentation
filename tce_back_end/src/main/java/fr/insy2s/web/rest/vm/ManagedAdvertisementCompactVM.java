package fr.insy2s.web.rest.vm;

import fr.insy2s.domain.Advertisement;
import java.time.ZonedDateTime;

public class ManagedAdvertisementCompactVM {
    private Long id;

    private String title;

    private ZonedDateTime creationDate;

    private String statusLabel;

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public ManagedAdvertisementCompactVM() {}

    public ManagedAdvertisementCompactVM(Advertisement advertisement) {
        this.id = advertisement.getId();
        this.title = advertisement.getTitle();
        this.creationDate = advertisement.getCreationDate();
        this.statusLabel = advertisement.getStatus().getLabel();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
