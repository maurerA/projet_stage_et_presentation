package fr.insy2s.web.rest;

import fr.insy2s.domain.Advertisement;
import fr.insy2s.service.AdvertisementQueryService;
import fr.insy2s.service.AdvertisementService;
import fr.insy2s.service.dto.AdvertisementCriteria;
import fr.insy2s.service.dto.AdvertisementDTO;
import fr.insy2s.service.impl.AdvertisementServiceImpl;
import fr.insy2s.web.rest.errors.BadRequestAlertException;
import fr.insy2s.web.rest.vm.ManagedAdvertisementCompactVM;
import fr.insy2s.web.rest.vm.ManagedAdvertisementVM;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * REST controller for managing {@link fr.insy2s.domain.Advertisement}.
 */
@RestController
@RequestMapping("/api")
public class AdvertisementResource {
    private final Logger log = LoggerFactory.getLogger(AdvertisementResource.class);

    private static final String ENTITY_NAME = "advertisement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AdvertisementService advertisementService;

    private final AdvertisementQueryService advertisementQueryService;

    public AdvertisementResource(AdvertisementService advertisementService, AdvertisementQueryService advertisementQueryService) {
        this.advertisementService = advertisementService;
        this.advertisementQueryService = advertisementQueryService;
    }

    /**
     * {@code POST  /advertisements} : Create a new advertisement.
     *
     * @param advertisementDTO the advertisementDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new advertisementDTO, or with status {@code 400 (Bad Request)} if the advertisement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/advertisements")
    public ResponseEntity<AdvertisementDTO> createAdvertisement(@Valid @RequestBody AdvertisementDTO advertisementDTO)
        throws URISyntaxException {
        if (advertisementDTO.getId() != null) {
            throw new BadRequestAlertException("A new advertisement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdvertisementDTO result = advertisementService.save(advertisementDTO);
        return ResponseEntity
            .created(new URI("/api/advertisements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /advertisements} : Updates an existing advertisement.
     *
     * @param advertisementDTO the advertisementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated advertisementDTO,
     * or with status {@code 400 (Bad Request)} if the advertisementDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the advertisementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/advertisements")
    public ResponseEntity<AdvertisementDTO> updateAdvertisement(@Valid @RequestBody AdvertisementDTO advertisementDTO)
        throws URISyntaxException {
        log.debug("REST request to update Advertisement : {}", advertisementDTO);
        if (advertisementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AdvertisementDTO result = advertisementService.save(advertisementDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, advertisementDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /advertisements} : get all the advertisements.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of advertisements in body.
     */
    @GetMapping("/advertisements")
    public ResponseEntity<List<AdvertisementDTO>> getAllAdvertisements(AdvertisementCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Advertisements by criteria: {}", criteria);
        Page<AdvertisementDTO> page = advertisementQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /advertisements/count} : count all the advertisements.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/advertisements/count")
    public ResponseEntity<Long> countAdvertisements(AdvertisementCriteria criteria) {
        log.debug("REST request to count Advertisements by criteria: {}", criteria);
        return ResponseEntity.ok().body(advertisementQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /advertisements/:id} : get the "id" advertisement.
     *
     * @param id the id of the advertisementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the advertisementDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/advertisements/{id}")
    public ResponseEntity<AdvertisementDTO> getAdvertisement(@PathVariable Long id) {
        log.debug("REST request to get Advertisement : {}", id);
        Optional<AdvertisementDTO> advertisementDTO = advertisementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(advertisementDTO);
    }

    /**
     * {@code DELETE  /advertisements/:id} : delete the "id" advertisement.
     *
     * @param id the id of the advertisementDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/advertisements/{id}")
    public ResponseEntity<Void> deleteAdvertisement(@PathVariable Long id) {
        log.debug("REST request to delete Advertisement : {}", id);
        advertisementService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**@author Ebrahim
     * This request allows the user to create and save an advertisement with all his attributes and also save the others attributes objects
     * @param advertisementDTO
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/advertisements/create")
    public ResponseEntity<Long> createNewAdvertisement(@Valid @RequestBody ManagedAdvertisementVM managedAdvertisementVM)
        throws URISyntaxException {
        Advertisement result = advertisementService.createNewAdvertisement(managedAdvertisementVM);
        return ResponseEntity
            .created(new URI("/api/advertisements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result.getId());
    }

    /**
     * @author Ebrahim
     * @param id
     * @return this method returns the list of advertisements created by the current logged user
     */
    @GetMapping("/advertisements/myAdvertisements")
    public ResponseEntity<List<ManagedAdvertisementCompactVM>> getUserAdvertisements(Pageable pageable) {
        Page<ManagedAdvertisementCompactVM> page = advertisementService.userAdvertisements(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * @author Ebrahim
     * @param id
     * @return this method returns the list of advertisements created by the current logged user which is filtered
     */
    @PostMapping("/advertisements/myAdvertisementsfiltered")
    public ResponseEntity<List<ManagedAdvertisementCompactVM>> filterUserAdvertisements(
        Pageable pageable,
        @RequestBody ManagedAdvertisementVM filter
    ) {
        Page<ManagedAdvertisementCompactVM> page = advertisementService.userAdvertisementsFiltered(
            filter.getTitle(),
            pageable,
            AdvertisementServiceImpl.ALL
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * @author Ebrahim
     * @param id
     * @return this method returns the list of advertisements created by the current logged user which is filtered
     */
    @PostMapping("/advertisements/myAdvertisements/filtered/byTitle")
    public ResponseEntity<List<ManagedAdvertisementCompactVM>> filterUserAdvertisementsByTitle(
        Pageable pageable,
        @RequestBody ManagedAdvertisementVM filter
    ) {
        Page<ManagedAdvertisementCompactVM> page = advertisementService.userAdvertisementsFiltered(
            filter.getTitle(),
            pageable,
            AdvertisementServiceImpl.TITLE
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * @author Ebrahim
     * @param id
     * @return this method returns the list of advertisements created by the current logged user which is filtered
     */
    @PostMapping("/advertisements/myAdvertisements/filtered/byState")
    public ResponseEntity<List<ManagedAdvertisementCompactVM>> filterUserAdvertisementsByState(
        Pageable pageable,
        @RequestBody ManagedAdvertisementVM filter
    ) {
        Page<ManagedAdvertisementCompactVM> page = advertisementService.userAdvertisementsFiltered(
            filter.getTitle(),
            pageable,
            AdvertisementServiceImpl.STATE
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     *
     * {@code GET  /details-advertisements/:id} : get the "id" advertisement.
     *
     * @param id the id of the advertisementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the advertisementDTO, or with status {@code 404 (Not Found)}.
     *
     * @Author MAURER Adrien TC-10
     */
    @GetMapping("/details-advertisements/{id}")
    public ResponseEntity<ManagedAdvertisementVM> getDetailsOfAdvertisement(@PathVariable Long id) {
        log.debug("REST request to get details-Advertisement : {}", id);
        ManagedAdvertisementVM managedAdvertisementVM = advertisementService.findDetailsAdvertissement(id);

        return ResponseEntity.ok(managedAdvertisementVM);
    }

    /**
     *
     * {@code GET  /details-advertisements/:id} : get the "id" advertisement.
     *
     * @param id the id of the advertisementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the advertisementDTO, or with status {@code 404 (Not Found)}.
     *
     * @Author Mohamed
     */
    @GetMapping("/details-advertisements-for-deliverymen/{id}")
    public ResponseEntity<ManagedAdvertisementVM> getAdvertisementsDetails(@PathVariable Long id) {
        log.debug("REST request to get details-Advertisement for Deliverymen : {}", id);
        ManagedAdvertisementVM managedAdvertisementVM = advertisementService.findDetailsAdvertissementForDelivery(id);

        return ResponseEntity.ok(managedAdvertisementVM);
    }

    /**
     * @author Mohamed
     * {@code GET  /advertisementsByStatus} : get the advertisements which match with the status.
     * @param pageable the pagination information.
     * @param status the status of the advertisement.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of advertisements in body.
     */
    @GetMapping("/advertisements/display-availables")
    public ResponseEntity<List<AdvertisementDTO>> getAdvertisementsByStatus(Pageable pageable) {
        log.debug("REST request to get Advertisements with the status available");
        Page<AdvertisementDTO> page = advertisementQueryService.findByStatusAvailable(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
