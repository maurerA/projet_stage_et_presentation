package fr.insy2s.web.rest;

import fr.insy2s.service.VehicleTypeQueryService;
import fr.insy2s.service.VehicleTypeService;
import fr.insy2s.service.dto.VehicleTypeCriteria;
import fr.insy2s.service.dto.VehicleTypeDTO;
import fr.insy2s.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for managing {@link fr.insy2s.domain.VehicleType}.
 */
@RestController
@RequestMapping("/api")
public class VehicleTypeResource {
    private final Logger log = LoggerFactory.getLogger(VehicleTypeResource.class);

    private static final String ENTITY_NAME = "vehicleType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VehicleTypeService vehicleTypeService;

    private final VehicleTypeQueryService vehicleTypeQueryService;

    public VehicleTypeResource(VehicleTypeService vehicleTypeService, VehicleTypeQueryService vehicleTypeQueryService) {
        this.vehicleTypeService = vehicleTypeService;
        this.vehicleTypeQueryService = vehicleTypeQueryService;
    }

    /**
     * {@code POST  /vehicle-types} : Create a new vehicleType.
     *
     * @param vehicleTypeDTO the vehicleTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vehicleTypeDTO, or with status {@code 400 (Bad Request)} if the vehicleType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vehicle-types")
    public ResponseEntity<VehicleTypeDTO> createVehicleType(@Valid @RequestBody VehicleTypeDTO vehicleTypeDTO) throws URISyntaxException {
        log.debug("REST request to save VehicleType : {}", vehicleTypeDTO);
        if (vehicleTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new vehicleType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VehicleTypeDTO result = vehicleTypeService.save(vehicleTypeDTO);
        return ResponseEntity
            .created(new URI("/api/vehicle-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /vehicle-types} : Updates an existing vehicleType.
     *
     * @param vehicleTypeDTO the vehicleTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vehicleTypeDTO,
     * or with status {@code 400 (Bad Request)} if the vehicleTypeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vehicleTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vehicle-types")
    public ResponseEntity<VehicleTypeDTO> updateVehicleType(@Valid @RequestBody VehicleTypeDTO vehicleTypeDTO) throws URISyntaxException {
        log.debug("REST request to update VehicleType : {}", vehicleTypeDTO);
        if (vehicleTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VehicleTypeDTO result = vehicleTypeService.save(vehicleTypeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, vehicleTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /vehicle-types} : get all the vehicleTypes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vehicleTypes in body.
     */
    @GetMapping("/vehicle-types")
    public ResponseEntity<List<VehicleTypeDTO>> getAllVehicleTypes(VehicleTypeCriteria criteria) {
        log.debug("REST request to get VehicleTypes by criteria: {}", criteria);
        List<VehicleTypeDTO> entityList = vehicleTypeQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /vehicle-types/count} : count all the vehicleTypes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/vehicle-types/count")
    public ResponseEntity<Long> countVehicleTypes(VehicleTypeCriteria criteria) {
        log.debug("REST request to count VehicleTypes by criteria: {}", criteria);
        return ResponseEntity.ok().body(vehicleTypeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /vehicle-types/:id} : get the "id" vehicleType.
     *
     * @param id the id of the vehicleTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vehicleTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vehicle-types/{id}")
    public ResponseEntity<VehicleTypeDTO> getVehicleType(@PathVariable Long id) {
        log.debug("REST request to get VehicleType : {}", id);
        Optional<VehicleTypeDTO> vehicleTypeDTO = vehicleTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(vehicleTypeDTO);
    }

    /**
     * {@code DELETE  /vehicle-types/:id} : delete the "id" vehicleType.
     *
     * @param id the id of the vehicleTypeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vehicle-types/{id}")
    public ResponseEntity<Void> deleteVehicleType(@PathVariable Long id) {
        log.debug("REST request to delete VehicleType : {}", id);
        vehicleTypeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
