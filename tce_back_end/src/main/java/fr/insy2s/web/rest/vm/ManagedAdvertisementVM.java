package fr.insy2s.web.rest.vm;

import fr.insy2s.domain.Advertisement;
import fr.insy2s.domain.Quotation;
import java.io.Serializable;

/**
 * add attribut and add constructoe for get Advertisemen, status and userAccount with lastName,NameCompagny,
 * @Author MAURER Adrien TC-10
 */
public class ManagedAdvertisementVM implements Serializable {
    private String title;

    private Float weight;

    private Float length;

    private Float width;

    private Float height;

    private String streetNumber;

    private String complementAddress;

    private String streetName;

    private String zipCode;

    private String town;

    private String countryLabel;

    private String streetNumber2;

    private String complementAddress2;

    private String streetName2;

    private String zipCode2;

    private String town2;

    private String countryLabel2;

    private Long idStatus;

    private String codeRefStatus;

    private String labelStatus;

    private Long companyId;

    private String nameCompany;

    private Long userId;

    private String lastName;

    private Long countryId;

    private Long countryId2;

    public ManagedAdvertisementVM() {}

    public ManagedAdvertisementVM(Advertisement advertisement, Quotation quotation) {
        //information colis
        this.title = advertisement.getTitle();
        this.weight = advertisement.getWeight();
        this.length = advertisement.getLength();
        this.width = advertisement.getWidth();
        this.height = advertisement.getHeight();
        //Starting Address
        this.streetNumber = advertisement.getStartAddress().getStreetNumber();
        this.complementAddress = advertisement.getStartAddress().getComplementAddress();
        this.streetName = advertisement.getStartAddress().getStreetName();
        this.zipCode = advertisement.getStartAddress().getZipCode();
        this.town = advertisement.getStartAddress().getTown();
        this.countryLabel = advertisement.getStartAddress().getCountry().getLabel();
        this.countryId = advertisement.getStartAddress().getCountry().getId();
        //Destination Address
        this.streetNumber2 = advertisement.getArrivalAddress().getStreetNumber();
        this.complementAddress2 = advertisement.getArrivalAddress().getComplementAddress();
        this.streetName2 = advertisement.getArrivalAddress().getStreetName();
        this.zipCode2 = advertisement.getArrivalAddress().getZipCode();
        this.town2 = advertisement.getArrivalAddress().getTown();
        this.countryLabel2 = advertisement.getArrivalAddress().getCountry().getLabel();
        this.countryId2 = advertisement.getArrivalAddress().getCountry().getId();
        //advertissement status
        this.idStatus = advertisement.getStatus().getId();
        this.codeRefStatus = advertisement.getStatus().getCodeRef();
        this.labelStatus = advertisement.getStatus().getLabel();
        this.companyId = quotation.getDeliveryMan().getCompany().getId();
        //name company and deliveryMan
        this.nameCompany = quotation.getDeliveryMan().getCompany().getNameCompany();
        this.userId = quotation.getDeliveryMan().getUser().getId();
        this.lastName = quotation.getDeliveryMan().getUser().getLastName();
    }

    public ManagedAdvertisementVM(Advertisement advertisement) {
        this.title = advertisement.getTitle();
        this.weight = advertisement.getWeight();
        this.length = advertisement.getLength();
        this.width = advertisement.getWidth();
        this.height = advertisement.getHeight();
        this.streetNumber = advertisement.getStartAddress().getStreetNumber();
        this.complementAddress = advertisement.getStartAddress().getComplementAddress();
        this.streetName = advertisement.getStartAddress().getStreetName();
        this.zipCode = advertisement.getStartAddress().getZipCode();
        this.town = advertisement.getStartAddress().getTown();
        this.countryLabel = advertisement.getStartAddress().getCountry().getLabel();
        this.streetNumber2 = advertisement.getArrivalAddress().getStreetNumber();
        this.complementAddress2 = advertisement.getArrivalAddress().getComplementAddress();
        this.streetName2 = advertisement.getArrivalAddress().getStreetName();
        this.zipCode2 = advertisement.getArrivalAddress().getZipCode();
        this.town2 = advertisement.getArrivalAddress().getTown();
        this.countryLabel2 = advertisement.getArrivalAddress().getCountry().getLabel();
        this.idStatus = advertisement.getStatus().getId();
        this.codeRefStatus = advertisement.getStatus().getCodeRef();
        this.labelStatus = advertisement.getStatus().getLabel();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getComplementAddress() {
        return complementAddress;
    }

    public void setComplementAddress(String complementAddress) {
        this.complementAddress = complementAddress;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getStreetNumber2() {
        return streetNumber2;
    }

    public void setStreetNumber2(String streetNumber2) {
        this.streetNumber2 = streetNumber2;
    }

    public String getComplementAddress2() {
        return complementAddress2;
    }

    public void setComplementAddress2(String complementAddress2) {
        this.complementAddress2 = complementAddress2;
    }

    public String getStreetName2() {
        return streetName2;
    }

    public void setStreetName2(String streetName2) {
        this.streetName2 = streetName2;
    }

    public String getZipCode2() {
        return zipCode2;
    }

    public void setZipCode2(String zipCode2) {
        this.zipCode2 = zipCode2;
    }

    public String getTown2() {
        return town2;
    }

    public void setTown2(String town2) {
        this.town2 = town2;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Long idStatus) {
        this.idStatus = idStatus;
    }

    public String getCodeRefStatus() {
        return codeRefStatus;
    }

    public void setCodeRefStatus(String codeRefStatus) {
        this.codeRefStatus = codeRefStatus;
    }

    public String getLabel() {
        return labelStatus;
    }

    public void setLabel(String label) {
        this.labelStatus = label;
    }

    public String getCountryLabel() {
        return countryLabel;
    }

    public void setCountryLabel(String countryLabel) {
        this.countryLabel = countryLabel;
    }

    public String getCountryLabel2() {
        return countryLabel2;
    }

    public void setCountryLabel2(String countryLabel2) {
        this.countryLabel2 = countryLabel2;
    }

    public String getLabelStatus() {
        return labelStatus;
    }

    public void setLabelStatus(String labelStatus) {
        this.labelStatus = labelStatus;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getCountryId2() {
        return countryId2;
    }

    public void setCountryId2(Long countryId2) {
        this.countryId2 = countryId2;
    }

    @Override
    public String toString() {
        return (
            "ManagedAdvertisementVM{" +
            "title='" +
            title +
            '\'' +
            ", weight=" +
            weight +
            ", length=" +
            length +
            ", width=" +
            width +
            ", height=" +
            height +
            ", streetNumber='" +
            streetNumber +
            '\'' +
            ", complementAddress='" +
            complementAddress +
            '\'' +
            ", streetName='" +
            streetName +
            '\'' +
            ", zipCode='" +
            zipCode +
            '\'' +
            ", town='" +
            town +
            '\'' +
            ", countryLabel='" +
            countryLabel +
            '\'' +
            ", streetNumber2='" +
            streetNumber2 +
            '\'' +
            ", complementAddress2='" +
            complementAddress2 +
            '\'' +
            ", streetName2='" +
            streetName2 +
            '\'' +
            ", zipCode2='" +
            zipCode2 +
            '\'' +
            ", town2='" +
            town2 +
            '\'' +
            ", countryLabel2='" +
            countryLabel2 +
            '\'' +
            ", idStatus=" +
            idStatus +
            ", codeRefStatus='" +
            codeRefStatus +
            '\'' +
            ", labelStatus='" +
            labelStatus +
            '\'' +
            ", companyId=" +
            companyId +
            ", nameCompany='" +
            nameCompany +
            '\'' +
            ", userId=" +
            userId +
            ", lastName='" +
            lastName +
            '\'' +
            ", countryId=" +
            countryId +
            ", countryId2=" +
            countryId2 +
            '}'
        );
    }
}
