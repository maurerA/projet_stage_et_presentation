package fr.insy2s.web.rest;

import fr.insy2s.service.QuotationStatusQueryService;
import fr.insy2s.service.QuotationStatusService;
import fr.insy2s.service.dto.QuotationStatusCriteria;
import fr.insy2s.service.dto.QuotationStatusDTO;
import fr.insy2s.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for managing {@link fr.insy2s.domain.QuotationStatus}.
 */
@RestController
@RequestMapping("/api")
public class QuotationStatusResource {
    private final Logger log = LoggerFactory.getLogger(QuotationStatusResource.class);

    private static final String ENTITY_NAME = "quotationStatus";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final QuotationStatusService quotationStatusService;

    private final QuotationStatusQueryService quotationStatusQueryService;

    public QuotationStatusResource(QuotationStatusService quotationStatusService, QuotationStatusQueryService quotationStatusQueryService) {
        this.quotationStatusService = quotationStatusService;
        this.quotationStatusQueryService = quotationStatusQueryService;
    }

    /**
     * {@code POST  /quotation-statuses} : Create a new quotationStatus.
     *
     * @param quotationStatusDTO the quotationStatusDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new quotationStatusDTO, or with status {@code 400 (Bad Request)} if the quotationStatus has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/quotation-statuses")
    public ResponseEntity<QuotationStatusDTO> createQuotationStatus(@Valid @RequestBody QuotationStatusDTO quotationStatusDTO)
        throws URISyntaxException {
        log.debug("REST request to save QuotationStatus : {}", quotationStatusDTO);
        if (quotationStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new quotationStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        QuotationStatusDTO result = quotationStatusService.save(quotationStatusDTO);
        return ResponseEntity
            .created(new URI("/api/quotation-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /quotation-statuses} : Updates an existing quotationStatus.
     *
     * @param quotationStatusDTO the quotationStatusDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quotationStatusDTO,
     * or with status {@code 400 (Bad Request)} if the quotationStatusDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the quotationStatusDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/quotation-statuses")
    public ResponseEntity<QuotationStatusDTO> updateQuotationStatus(@Valid @RequestBody QuotationStatusDTO quotationStatusDTO)
        throws URISyntaxException {
        log.debug("REST request to update QuotationStatus : {}", quotationStatusDTO);
        if (quotationStatusDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        QuotationStatusDTO result = quotationStatusService.save(quotationStatusDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, quotationStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /quotation-statuses} : get all the quotationStatuses.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of quotationStatuses in body.
     */
    @GetMapping("/quotation-statuses")
    public ResponseEntity<List<QuotationStatusDTO>> getAllQuotationStatuses(QuotationStatusCriteria criteria) {
        log.debug("REST request to get QuotationStatuses by criteria: {}", criteria);
        List<QuotationStatusDTO> entityList = quotationStatusQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /quotation-statuses/count} : count all the quotationStatuses.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/quotation-statuses/count")
    public ResponseEntity<Long> countQuotationStatuses(QuotationStatusCriteria criteria) {
        log.debug("REST request to count QuotationStatuses by criteria: {}", criteria);
        return ResponseEntity.ok().body(quotationStatusQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /quotation-statuses/:id} : get the "id" quotationStatus.
     *
     * @param id the id of the quotationStatusDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the quotationStatusDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/quotation-statuses/{id}")
    public ResponseEntity<QuotationStatusDTO> getQuotationStatus(@PathVariable Long id) {
        log.debug("REST request to get QuotationStatus : {}", id);
        Optional<QuotationStatusDTO> quotationStatusDTO = quotationStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(quotationStatusDTO);
    }

    /**
     * {@code DELETE  /quotation-statuses/:id} : delete the "id" quotationStatus.
     *
     * @param id the id of the quotationStatusDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/quotation-statuses/{id}")
    public ResponseEntity<Void> deleteQuotationStatus(@PathVariable Long id) {
        log.debug("REST request to delete QuotationStatus : {}", id);
        quotationStatusService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
