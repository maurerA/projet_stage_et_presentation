package fr.insy2s.web.rest;

import fr.insy2s.service.NotationQueryService;
import fr.insy2s.service.NotationService;
import fr.insy2s.service.dto.NotationCriteria;
import fr.insy2s.service.dto.NotationDTO;
import fr.insy2s.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * REST controller for managing {@link fr.insy2s.domain.Notation}.
 */
@RestController
@RequestMapping("/api")
public class NotationResource {
    private final Logger log = LoggerFactory.getLogger(NotationResource.class);

    private static final String ENTITY_NAME = "notation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NotationService notationService;

    private final NotationQueryService notationQueryService;

    public NotationResource(NotationService notationService, NotationQueryService notationQueryService) {
        this.notationService = notationService;
        this.notationQueryService = notationQueryService;
    }

    /**
     * {@code POST  /notations} : Create a new notation.
     *
     * @param notationDTO the notationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new notationDTO, or with status {@code 400 (Bad Request)} if the notation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/notations")
    public ResponseEntity<NotationDTO> createNotation(@Valid @RequestBody NotationDTO notationDTO) throws URISyntaxException {
        log.debug("REST request to save Notation : {}", notationDTO);
        if (notationDTO.getId() != null) {
            throw new BadRequestAlertException("A new notation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NotationDTO result = notationService.save(notationDTO);
        return ResponseEntity
            .created(new URI("/api/notations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /notations} : Updates an existing notation.
     *
     * @param notationDTO the notationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated notationDTO,
     * or with status {@code 400 (Bad Request)} if the notationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the notationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/notations")
    public ResponseEntity<NotationDTO> updateNotation(@Valid @RequestBody NotationDTO notationDTO) throws URISyntaxException {
        log.debug("REST request to update Notation : {}", notationDTO);
        if (notationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NotationDTO result = notationService.save(notationDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, notationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /notations} : get all the notations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of notations in body.
     */
    @GetMapping("/notations")
    public ResponseEntity<List<NotationDTO>> getAllNotations(NotationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Notations by criteria: {}", criteria);
        Page<NotationDTO> page = notationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /notations/count} : count all the notations.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/notations/count")
    public ResponseEntity<Long> countNotations(NotationCriteria criteria) {
        log.debug("REST request to count Notations by criteria: {}", criteria);
        return ResponseEntity.ok().body(notationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /notations/:id} : get the "id" notation.
     *
     * @param id the id of the notationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the notationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/notations/{id}")
    public ResponseEntity<NotationDTO> getNotation(@PathVariable Long id) {
        log.debug("REST request to get Notation : {}", id);
        Optional<NotationDTO> notationDTO = notationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(notationDTO);
    }

    /**
     * {@code DELETE  /notations/:id} : delete the "id" notation.
     *
     * @param id the id of the notationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/notations/{id}")
    public ResponseEntity<Void> deleteNotation(@PathVariable Long id) {
        log.debug("REST request to delete Notation : {}", id);
        notationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
