package fr.insy2s.web.rest.vm;

import fr.insy2s.domain.Authority;
import fr.insy2s.domain.UserAccount;
import fr.insy2s.security.AuthoritiesConstants;
import fr.insy2s.security.SecurityUtils;
import fr.insy2s.service.dto.UserDTO;
import java.util.Set;
import javax.validation.constraints.Size;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */

/**
 * add constructor for update et get account user, user, adress, country and compagny
 *
 * @author MAURER Adrien TC-22
 *
 */
public class ManagedUserVM extends UserDTO {
    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    //Attributes class Address
    private Long AddressId;
    private String streetNumber;
    private String complementAddress;
    private String streetName;
    private String zipCode;
    private String town;
    //Attributes class Country
    private Long idCountry;
    private String codeRef;
    private String label;
    //Attributes class UserAccount
    private Long userAccountId;
    private String phoneNumber;
    //Attributes class Company
    private Long companyId;
    private String nameCompany;
    private String numberSiretCompany;
    private String numberSiren;

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }

    public ManagedUserVM(
        String streetNumber,
        String complementAddress,
        String streetName,
        String zipCode,
        String town,
        String codeRef,
        String label,
        String nameCompany,
        String numberSiretCompany,
        String numberSiren,
        String phoneNumber
    ) {
        super();
        this.streetNumber = streetNumber;
        this.complementAddress = complementAddress;
        this.streetName = streetName;
        this.zipCode = zipCode;
        this.town = town;
        this.codeRef = codeRef;
        this.label = label;
        this.nameCompany = nameCompany;
        this.numberSiretCompany = numberSiretCompany;
        this.numberSiren = numberSiren;
        this.phoneNumber = phoneNumber;
    }

    /**
     * constructor allows change userAccount to ManagedUserVm
     * use for get and update user in front end
     * @param userAccount
     *
     * @author MAURER adrien TC-22
     */
    public ManagedUserVM(UserAccount userAccount) {
        super(userAccount.getUser());
        //binding Address
        this.AddressId = userAccount.getAdresse().getId();
        this.streetNumber = userAccount.getAdresse().getStreetNumber();
        this.complementAddress = userAccount.getAdresse().getComplementAddress();
        this.streetName = userAccount.getAdresse().getStreetName();
        this.zipCode = userAccount.getAdresse().getZipCode();
        this.town = userAccount.getAdresse().getTown();
        //binding country
        this.idCountry = userAccount.getAdresse().getCountry().getId();
        this.codeRef = userAccount.getAdresse().getCountry().getCodeRef();
        this.label = userAccount.getAdresse().getCountry().getLabel();
        //binding company
        Set<String> authoritiesCurrentUser = SecurityUtils.getCurrentUserAuthorities();

        if (authoritiesCurrentUser.contains(AuthoritiesConstants.DELIVERYMAN)) {
            this.companyId = userAccount.getCompany().getId();
            this.nameCompany = userAccount.getCompany().getNameCompany();
            this.numberSiretCompany = userAccount.getCompany().getNumberSiretCompany();
            this.numberSiren = userAccount.getCompany().getNumberSiren();
        }
        //binding phoneNumber
        this.userAccountId = userAccount.getId();
        this.phoneNumber = userAccount.getPhoneNumber();
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getComplementAddress() {
        return complementAddress;
    }

    public void setComplementAddress(String complementAddress) {
        this.complementAddress = complementAddress;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCodeRef() {
        return codeRef;
    }

    public void setCodeRef(String codeRef) {
        this.codeRef = codeRef;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getNumberSiretCompany() {
        return numberSiretCompany;
    }

    public void setNumberSiretCompany(String numberSiretCompany) {
        this.numberSiretCompany = numberSiretCompany;
    }

    public String getNumberSiren() {
        return numberSiren;
    }

    public void setNumberSiren(String numberSiren) {
        this.numberSiren = numberSiren;
    }

    public Long getAddressId() {
        return AddressId;
    }

    public void setAddressId(Long addressId) {
        AddressId = addressId;
    }

    public Long getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(Long idCountry) {
        this.idCountry = idCountry;
    }

    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long userAccountId) {
        this.userAccountId = userAccountId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return (
            "ManagedUserVM{" +
            "password='" +
            password +
            '\'' +
            ", AddressId=" +
            AddressId +
            ", streetNumber='" +
            streetNumber +
            '\'' +
            ", complementAddress='" +
            complementAddress +
            '\'' +
            ", streetName='" +
            streetName +
            '\'' +
            ", zipCode='" +
            zipCode +
            '\'' +
            ", town='" +
            town +
            '\'' +
            ", idCountry=" +
            idCountry +
            ", codeRef='" +
            codeRef +
            '\'' +
            ", label='" +
            label +
            '\'' +
            ", userAccountId=" +
            userAccountId +
            ", phoneNumber='" +
            phoneNumber +
            '\'' +
            ", companyId=" +
            companyId +
            ", nameCompany='" +
            nameCompany +
            '\'' +
            ", numberSiretCompany='" +
            numberSiretCompany +
            '\'' +
            ", numberSiren='" +
            numberSiren +
            '\'' +
            '}'
        );
    }
}
