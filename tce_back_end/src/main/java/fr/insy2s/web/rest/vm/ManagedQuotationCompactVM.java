package fr.insy2s.web.rest.vm;

import fr.insy2s.domain.Quotation;
import java.io.Serializable;

public class ManagedQuotationCompactVM implements Serializable {
    private Long idQuotation;
    private Float deliveryManPrice;
    private String statusLabel;
    private String title;
    private Long idAnnonce;

    public ManagedQuotationCompactVM() {}

    public ManagedQuotationCompactVM(Quotation quotation) {
        this.idQuotation = quotation.getId();
        this.title = quotation.getAdvertisement().getTitle();
        this.idAnnonce = quotation.getAdvertisement().getId();
        this.deliveryManPrice = quotation.getDeliveryManPrice();
        this.statusLabel = quotation.getStatus().getLabel();
    }

    public Long getIdAnnonce() {
        return idAnnonce;
    }

    public void setIdAnnonce(Long idAnnonce) {
        this.idAnnonce = idAnnonce;
    }

    public Long getIdQuotation() {
        return idQuotation;
    }

    public void setIdQuotation(Long idQuotation) {
        this.idQuotation = idQuotation;
    }

    public Float getDeliveryManPrice() {
        return deliveryManPrice;
    }

    public void setDeliveryManPrice(Float deliveryManPrice) {
        this.deliveryManPrice = deliveryManPrice;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return (
            "ManagedQuotationCompactVM{" +
            "idQuotation=" +
            idQuotation +
            ", deliveryManPrice=" +
            deliveryManPrice +
            ", statusLabel='" +
            statusLabel +
            '\'' +
            ", title='" +
            title +
            '\'' +
            '}'
        );
    }
}
