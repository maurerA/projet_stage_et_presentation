package fr.insy2s.web.rest;

import fr.insy2s.service.AdvertisementStatusQueryService;
import fr.insy2s.service.AdvertisementStatusService;
import fr.insy2s.service.dto.AdvertisementStatusCriteria;
import fr.insy2s.service.dto.AdvertisementStatusDTO;
import fr.insy2s.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for managing {@link fr.insy2s.domain.AdvertisementStatus}.
 */
@RestController
@RequestMapping("/api")
public class AdvertisementStatusResource {
    private final Logger log = LoggerFactory.getLogger(AdvertisementStatusResource.class);

    private static final String ENTITY_NAME = "advertisementStatus";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AdvertisementStatusService advertisementStatusService;

    private final AdvertisementStatusQueryService advertisementStatusQueryService;

    public AdvertisementStatusResource(
        AdvertisementStatusService advertisementStatusService,
        AdvertisementStatusQueryService advertisementStatusQueryService
    ) {
        this.advertisementStatusService = advertisementStatusService;
        this.advertisementStatusQueryService = advertisementStatusQueryService;
    }

    /**
     * {@code POST  /advertisement-statuses} : Create a new advertisementStatus.
     *
     * @param advertisementStatusDTO the advertisementStatusDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new advertisementStatusDTO, or with status {@code 400 (Bad Request)} if the advertisementStatus has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/advertisement-statuses")
    public ResponseEntity<AdvertisementStatusDTO> createAdvertisementStatus(
        @Valid @RequestBody AdvertisementStatusDTO advertisementStatusDTO
    )
        throws URISyntaxException {
        log.debug("REST request to save AdvertisementStatus : {}", advertisementStatusDTO);
        if (advertisementStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new advertisementStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdvertisementStatusDTO result = advertisementStatusService.save(advertisementStatusDTO);
        return ResponseEntity
            .created(new URI("/api/advertisement-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /advertisement-statuses} : Updates an existing advertisementStatus.
     *
     * @param advertisementStatusDTO the advertisementStatusDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated advertisementStatusDTO,
     * or with status {@code 400 (Bad Request)} if the advertisementStatusDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the advertisementStatusDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/advertisement-statuses")
    public ResponseEntity<AdvertisementStatusDTO> updateAdvertisementStatus(
        @Valid @RequestBody AdvertisementStatusDTO advertisementStatusDTO
    )
        throws URISyntaxException {
        log.debug("REST request to update AdvertisementStatus : {}", advertisementStatusDTO);
        if (advertisementStatusDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AdvertisementStatusDTO result = advertisementStatusService.save(advertisementStatusDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, advertisementStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /advertisement-statuses} : get all the advertisementStatuses.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of advertisementStatuses in body.
     */
    @GetMapping("/advertisement-statuses")
    public ResponseEntity<List<AdvertisementStatusDTO>> getAllAdvertisementStatuses(AdvertisementStatusCriteria criteria) {
        log.debug("REST request to get AdvertisementStatuses by criteria: {}", criteria);
        List<AdvertisementStatusDTO> entityList = advertisementStatusQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /advertisement-statuses/count} : count all the advertisementStatuses.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/advertisement-statuses/count")
    public ResponseEntity<Long> countAdvertisementStatuses(AdvertisementStatusCriteria criteria) {
        log.debug("REST request to count AdvertisementStatuses by criteria: {}", criteria);
        return ResponseEntity.ok().body(advertisementStatusQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /advertisement-statuses/:id} : get the "id" advertisementStatus.
     *
     * @param id the id of the advertisementStatusDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the advertisementStatusDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/advertisement-statuses/{id}")
    public ResponseEntity<AdvertisementStatusDTO> getAdvertisementStatus(@PathVariable Long id) {
        log.debug("REST request to get AdvertisementStatus : {}", id);
        Optional<AdvertisementStatusDTO> advertisementStatusDTO = advertisementStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(advertisementStatusDTO);
    }

    /**
     * {@code DELETE  /advertisement-statuses/:id} : delete the "id" advertisementStatus.
     *
     * @param id the id of the advertisementStatusDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/advertisement-statuses/{id}")
    public ResponseEntity<Void> deleteAdvertisementStatus(@PathVariable Long id) {
        log.debug("REST request to delete AdvertisementStatus : {}", id);
        advertisementStatusService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
