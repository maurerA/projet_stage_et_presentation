package fr.insy2s.web.rest;

import fr.insy2s.domain.Filter;
import fr.insy2s.domain.Quotation;
import fr.insy2s.service.QuotationQueryService;
import fr.insy2s.service.QuotationService;
import fr.insy2s.service.dto.QuotationCriteria;
import fr.insy2s.service.dto.QuotationDTO;
import fr.insy2s.web.rest.errors.BadRequestAlertException;
import fr.insy2s.web.rest.vm.ManagedQuotationCompactVM;
import fr.insy2s.web.rest.vm.ManagedQuotationVM;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * REST controller for managing {@link fr.insy2s.domain.Quotation}.
 */
@RestController
@RequestMapping("/api")
public class QuotationResource {
    private final Logger log = LoggerFactory.getLogger(QuotationResource.class);

    private static final String ENTITY_NAME = "quotation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final QuotationService quotationService;

    private final QuotationQueryService quotationQueryService;

    private final String ALL = "ALL";

    private final String TITLE = "TITLE";

    private final String STATE = "STATE";

    public QuotationResource(QuotationService quotationService, QuotationQueryService quotationQueryService) {
        this.quotationService = quotationService;
        this.quotationQueryService = quotationQueryService;
    }

    /**
     * {@code POST  /quotations} : Create a new quotation.
     *
     * @param quotationDTO the quotationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new quotationDTO, or with status {@code 400 (Bad Request)} if the quotation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/quotations")
    public ResponseEntity<QuotationDTO> createQuotation(@Valid @RequestBody QuotationDTO quotationDTO) throws URISyntaxException {
        log.debug("REST request to save Quotation : {}", quotationDTO);
        if (quotationDTO.getId() != null) {
            throw new BadRequestAlertException("A new quotation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        QuotationDTO result = quotationService.save(quotationDTO);
        return ResponseEntity
            .created(new URI("/api/quotations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /quotations} : get all the quotations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of quotations in body.
     */
    @GetMapping("/quotations")
    public ResponseEntity<List<QuotationDTO>> getAllQuotations(QuotationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Quotations by criteria: {}", criteria);
        Page<QuotationDTO> page = quotationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /quotations/count} : count all the quotations.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/quotations/count")
    public ResponseEntity<Long> countQuotations(QuotationCriteria criteria) {
        log.debug("REST request to count Quotations by criteria: {}", criteria);
        return ResponseEntity.ok().body(quotationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /quotations/:id} : get the "id" quotation.
     *
     * @param id the id of the quotationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the quotationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/quotations/{id}")
    public ResponseEntity<QuotationDTO> getQuotation(@PathVariable Long id) {
        log.debug("REST request to get Quotation : {}", id);
        Optional<QuotationDTO> quotationDTO = quotationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(quotationDTO);
    }

    /**
     * {@code DELETE  /quotations/:id} : delete the "id" quotation.
     *
     * @param id the id of the quotationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/quotations/{id}")
    public ResponseEntity<Void> deleteQuotation(@PathVariable Long id) {
        log.debug("REST request to delete Quotation : {}", id);
        quotationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * @author Ebrahim and MAURER Adrien for TC-26
     *
     * @param id the id of the advertisement to retrieve to get all the quotations associate.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the Quotation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/advertisement/quotations/{id}")
    public ResponseEntity<List<ManagedQuotationCompactVM>> getQuotationFromAdvertisement(@PathVariable Long id, Pageable pageable) {
        Page<ManagedQuotationCompactVM> page = quotationService.getQuotationsFromAd(id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * @author Ebrahim
     *
     * @param id the id of the quotation accepted.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the Quotation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/advertisement/quotationAccepted/{id}")
    public ResponseEntity<Quotation> quotationAcceptedFromAdvertisement(@PathVariable Long id) {
        Quotation quotation = quotationService.acceptQuotationsFromAd(id);
        return ResponseEntity.ok().body(quotation);
    }

    /**
     * @author Ebrahim
     *
     * @param id the id of the quotation accepted.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the Quotation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/advertisement/quotationRefused/{id}")
    public ResponseEntity<Quotation> quotationRefusedFromAdvertisement(@PathVariable Long id) {
        Quotation quotation = quotationService.refuseQuotationsFromAd(id);
        return ResponseEntity.ok().body(quotation);
    }

    /**@author Amal Fares
     * This request allows the user to create and save an quotation with all his attributes and also save the others attributes objects
     * @param quotationDTO
     * @return
     * @throws URISyntaxException
     */

    @PostMapping("/quotation/create")
    public ResponseEntity<Long> createNewQuotation(@RequestBody QuotationDTO quotationDTO) throws URISyntaxException {
        Quotation quotation = quotationService.createQuotation(quotationDTO);

        return ResponseEntity
            .created(new URI("/api/quotation/create/" + quotation.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, quotation.getId().toString()))
            .body(quotation.getId());
    }

    /**
     * {@code PUT  /quotations} : Updates an existing quotation.
     *
     * @param quotationDTO the quotationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quotationDTO,
     * or with status {@code 400 (Bad Request)} if the quotationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the quotationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/quotation/update")
    public ResponseEntity<Quotation> updateQuotation(@RequestBody QuotationDTO quotationDTO) throws URISyntaxException {
        if (quotationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Quotation quotation = quotationService.updateQuotation(quotationDTO);

        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, quotation.getId().toString()))
            .body(quotation);
    }

    /**
     * @author Ebrahim
     *
     * @param id the id of the quotation.
     * @return the ResponseEntity with the ManagedQuotationVM associated
     */
    @GetMapping("/quotation/{id}")
    public ResponseEntity<ManagedQuotationVM> getQuotationVM(@PathVariable Long id) {
        log.debug("REST request to get Quotation : {}", id);
        Optional<ManagedQuotationVM> quotationDTO = quotationService.getQuotationVM(id);
        return ResponseUtil.wrapOrNotFound(quotationDTO);
    }

    /**
     * @author Ebrahim
     * @return the ResponseEntity with the list of quotations of the deliveryman connected
     */
    @GetMapping("/myquotations")
    public List<ManagedQuotationCompactVM> getQuotationVM(Pageable pageable) {
        List<ManagedQuotationCompactVM> page = quotationService.getMyQuotations(pageable);
        return page;
    }

    /**
     * @author Ebrahim
     * @return the ResponseEntity with the list of quotations of the deliveryman connected
     */
    @PostMapping("/myquotationsfiltered")
    public List<ManagedQuotationCompactVM> getQuotationVMFiltered(@RequestBody Filter filter, Pageable pageable) {
        List<ManagedQuotationCompactVM> page = null;
        if (ALL.equals(filter.getFilterType())) {
            page = quotationService.getMyQuotationsFilteredByTitleAndState(filter.getContentFilter(), pageable);
        } else if (TITLE.equals(filter.getFilterType())) {
            page = quotationService.getMyQuotationsFilteredByTitle(filter.getContentFilter(), pageable);
        } else if (STATE.equals(filter.getFilterType())) {
            page = quotationService.getMyQuotationsFilteredByState(filter.getContentFilter(), pageable);
        }

        return page;
    }
}
